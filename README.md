# Piromania

## Building

This is a Maven project, so to build simply enter the piro-app directory and run maven commands:

| Action  | Command  |
| ------- | -------- |
|  Run    | `mvn compile exec:java` |
| Javadoc | `mvn site`      |
| Compile | `mvn compile`   |
| Clean   | `mvn clean`     |
| Build Jar | `mvn clean compile assembly:single` |

The built jar can then be executed by running the following in the piro-app directory:
```bash
java -jar "target/piro-app-1.0-SNAPSHOT-jar-with-dependencies.jar"
```
Warning: This game was developed for java 14, so ensure that java 14 is used to run the jarfile.