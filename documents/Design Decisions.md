# Design Decisions

## Storing Money

We decided to represent monetary values in cents using integers to eliminate rounding errors. To ensure that this abstraction is not confusing, we created a Currency class which supports both Dollars and Cents.

## Measuring Ship's hold size

We decided to use integers to represent this as well, using an arbitrary 'volume' measurement.