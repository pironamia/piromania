package nz.piromania;

import java.awt.Dimension;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;
import javax.swing.SwingUtilities;
import nz.piromania.gui.GuiRenderer;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.Engine;
import nz.piromania.reactiveengine.component.ImageComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.ui.ApplicationComponent;

/**
 * The entrypoint to this application.
 */
public class App {

  private static final int screenWidth = 800;
  private static final int screenHeight = 600;

  private final Engine reactiveEngine;
  private final GuiRenderer renderer;
  private Boolean exiting = false;

  /**
   * Attempts to load the game fonts into the GuiRenderer. This is non-critical though, so errors
   * are ignored.
   */
  private void tryLoadFonts(GuiRenderer renderer) {
    try {
      final InputStream fontFile =
          App.class.getClassLoader().getResourceAsStream("font/PirataOne-Regular.ttf");
      renderer.registerFont(renderer.loadTrueTypeFont(fontFile));
      System.out.println("Registered font successfully.");
    } catch (NullPointerException e) {
      System.out.println("Failed to load font:");
      e.printStackTrace();
    } catch (IOException e) {
      System.out.println("Failed to load font:");
      e.printStackTrace();
    } catch (FontFormatException e) {
      System.out.println("Failed to load font:");
      e.printStackTrace();
    }
  }

  App() {
    reactiveEngine = new Engine();
    renderer = new GuiRenderer(
        "Piromania",
        ImageComponent.tryLoadImageFromResourcesFile("Logo.png", 50, 50),
        new Dimension(screenWidth, screenHeight),
        this::exit
    );
    tryLoadFonts(renderer);

    reactiveEngine.setRenderConsumer(renderer);

    System.out.println("Initialisation Complete.");
  }

  private void runEngineTick() {
    reactiveEngine.tick();

    if (!this.exiting) {
      SwingUtilities.invokeLater(this::runEngineTick);
    } else {
      System.out.println("Exiting... Goodbye!");
      renderer.exit();
    }
  }

  /**
   * Enter the game loop.
   */
  private void run() {
    System.out.println("Starting...");
    System.out.println("Injecting root component...");

    final Component rootComponent = new Provider(
        this,
        new ApplicationComponent()
    );

    reactiveEngine.initialise(rootComponent);
    SwingUtilities.invokeLater(this::runEngineTick);
  }

  /**
   * Entrypoint of this application. Bootstraps core classes and enters game loop.
   */
  public static void main(String[] args) {
    final App app = new App();
    app.run();
  }

  public void exit() {
    exiting = true;
  }

  public static int getScreenWidth() {
    return screenWidth;
  }

  public static int getScreenHeight() {
    return screenHeight;
  }
}
