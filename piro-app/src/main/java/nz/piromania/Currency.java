package nz.piromania;


/**
 * This class is to simplify the currency used by the game, whilst allowing for human readability.
 * Everything is stored in an amount of cents, however dollars can be parsed for simplicity.
 */

public final class Currency {

  private final int cents;

  /**
   * Initialize a new Currency, which stores all money representations as cents.
   *
   * @param cents the amount in cents that should be represented
   */
  public Currency(int cents) {
    this(cents, false);
  }

  /**
   * Initialize a new Currency, which stores all money representations as cents.
   *
   * @param cents         the amount in cents that should be represented
   * @param allowNegative whether to allow the currency to be negative. If false and the currency is
   *                      negative, will throw an IllegalArgumentException.
   */
  public Currency(int cents, boolean allowNegative) {
    if (cents < 0 && !allowNegative) {
      throw new IllegalArgumentException("Currency can not be negative");
    }
    this.cents = cents;
  }

  /**
   * Initialize a new Currency, which stores all money representations as cents.
   *
   * @param dollars the amount in dollars that should be represented, gets multiplied by 100 to
   *                convert to cents
   * @param cents   the amount in cents that should be represented
   */
  public Currency(int dollars, int cents) {
    if (dollars < 0 || cents < 0) {
      throw new IllegalArgumentException("Currency can not be negative");
    }
    this.cents = dollars * 100 + cents;
  }
  
  public int getCents() {
    return cents;
  }
  
  /**
   * Adds the value of another <code>Currency</code> to this <code>Currency</code>'s value.
   *
   * @param currencyToAdd       the other <code>Currency</code> value to add to this
   * @return                    a <code>Currency</code> object with the new value
   */
  public Currency add(Currency currencyToAdd) {
    return new Currency(this.cents + currencyToAdd.getCents());
  }

  /**
   * Subtracts the value of another <code>Currency</code> to this <code>Currency</code>'s value.
   *
   * @param currencyToSubtract the other <code>Currency</code> value to subtract to this
   * @return a <code>Currency</code> object with the new value
   */
  public Currency subtract(Currency currencyToSubtract) {
    return subtract(currencyToSubtract, false);
  }

  /**
   * Subtracts the value of another <code>Currency</code> to this <code>Currency</code>'s value.
   *
   * @param currencyToSubtract the other <code>Currency</code> value to subtract to this
   * @param allowNegative      whether to allow negative currency, if false negative result will
   *                           cause an IllegalArgumentException to be thrown.
   * @return a <code>Currency</code> object with the new value
   */
  public Currency subtract(Currency currencyToSubtract, boolean allowNegative) {
    if (this.cents - currencyToSubtract.getCents() < 0 && !allowNegative) {
      throw new IllegalArgumentException("Currency can not be negative");
    }
    return new Currency(this.cents - currencyToSubtract.getCents(), allowNegative);
  }

  /**
   * Multiplies the value of another <code>Currency</code> to this <code>Currency</code>'s value.
   *
   * @param currencyToMultiply  the other <code>Currency</code> value to multiply to this
   * @return a <code>Currency</code> object with the new value
   */
  public Currency multiply(Currency currencyToMultiply) {
    return new Currency(this.cents * currencyToMultiply.getCents());
  }
  
  /**
   * Multiplies the value of this <code>Currency</code> with a value. Rounds to nearest integer.
   *
   * @param valueToMultiply     the value to multiply to this
   * @return                    a <code>Currency</code> object with the new value
   */
  public Currency multiply(float valueToMultiply) {
    return new Currency(Math.round(this.cents * valueToMultiply));
  }
  
  /**
   * Divides the value of another <code>Currency</code> from this <code>Currency</code>'s value.
   *
   * @param currencyToDivide    the other <code>Currency</code> value to divide by
   * @return                    a <code>Currency</code> object with the new value
   */
  public Currency divide(Currency currencyToDivide) {
    if (currencyToDivide.getCents() == 0) { 
      throw new IllegalArgumentException("Divisor is 0");
    }
    return new Currency(this.cents / currencyToDivide.getCents());
  }
  
  /**
   * Divides the value of this <code>Currency</code> with a value. Rounds to nearest integer.
   *
   * @param valueToDivide       the value to divide by
   * @return                    a <code>Currency</code> object with the new value
   */
  public Currency divide(float valueToDivide) {
    if (valueToDivide == 0) {
      throw new IllegalArgumentException("Divisor is 0");
    }
    return new Currency(Math.round(this.cents / valueToDivide));
  }
  
  /**
   * Checks if two currencies are equal by comparing the money inside them.
   *
   * @param currencyToCheck the currency to check against
   * @return                boolean denoting if they're equal or not
   */
  public boolean equals(Currency currencyToCheck) {
    return this.cents == currencyToCheck.getCents();
  }
  
  @Override
  public String toString() {
    return String.format(
        "%s$%.2f",
        (cents < 0) ? "-" : "",
        Math.abs(cents) / 100.0
    );
  }

  /**
   * This comparator checks if a currency is less, equal, or more than another currency.
   * Uses standard comparator syntax of 
   *     -1 if this &lt; that,
   *     0 if this == that,
   *     1 if this &gt; that.
   *
   * @param currencyToCompare   the currency to compare with
   * @return                    -1 if this currency is less,
   *     0 if this currency is equal,
   *     1 if this currency is more
   */
  public int compareTo(Currency currencyToCompare) {
    return (cents < currencyToCompare.getCents()) ? -1
        : (cents > currencyToCompare.getCents()) ? 1
        : 0; 
  }
}

