package nz.piromania;

/**
 * Method used to ensure things are supplied as a float.
 * The method in this interface should be overwritten and cast to a float.
 * Example:
 * new FloatSupplier() {
 *   public float getValue() {
 *     return (float) Class.variable;
 *   }
 * };  
 */

public interface FloatSupplier {
  float getValue();
}
