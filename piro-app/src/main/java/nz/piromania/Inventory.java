package nz.piromania;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import nz.piromania.gameenvironment.items.Item;
import nz.piromania.reactiveengine.reactivity.Action;
import nz.piromania.reactiveengine.reactivity.Observable;
import nz.piromania.reactiveengine.reactivity.collections.ObservableArrayList;

/**
 * This class is used to keep track of an inventory.
 * It has a capacity, list of items, and a total worth amount.
 */
public class Inventory {
  private final Observable<Integer> capacity;
  private final ObservableArrayList<Item> items = 
      new ObservableArrayList<Item>();
  private final Observable<Currency> worth = 
      new Observable<Currency>(new Currency(0));
  
  /**
   * Used after JSON loads an inventory,
   * this will calculate the worth of the inventory after
   * all the variables have been confirmed.
   */
  public void initialize() {
    Action.run(() -> {
      for (Item item : getItems()) {
        worth.set(getWorth().add(item.getItemBaseValue()));
      }
    });
  }
  
  /**
   * Creates an inventory with infinite capacity, and no items.
   */
  public Inventory() {
    this.capacity = new Observable<Integer>(Integer.MAX_VALUE);
  }
  
  /**
   * Creates an inventory with limited capacity, and no items.
   *
   * @param capacity    the capacity the inventory has
   */
  public Inventory(int capacity) {
    this.capacity = new Observable<Integer>(capacity);
  }
  
  /**
   * Creates an inventory with infinite capacity, and items.
   * NOTE: The items added here will *NOT* affect the capacity.
   *
   * @param items       the items inside the inventory
   */
  public Inventory(ArrayList<Item> items) {
    this();
    Action.run(() -> {
      this.items.clear();
      this.items.addAll(items);
      for (Item item : getItems()) {
        worth.set(getWorth().add(item.getItemBaseValue()));
      }
    });
  }
  
  /**
   * Creates an inventory with limited capacity, and items.
   * NOTE: The items added here will *NOT* affect the capacity.
   *
   * @param capacity    the capacity the inventory has
   * @param items       the items inside the inventory
   */
  public Inventory(int capacity, ArrayList<Item> items) {
    this(capacity);
    Action.run(() -> {
      this.items.clear();
      this.items.addAll(items);
      for (Item item : getItems()) {
        worth.set(getWorth().add(item.getItemBaseValue()));
      }
    });
  }
  
  public int getCapacity() {
    return capacity.get();
  }
  
  public Currency getWorth() {
    return worth.get();
  }
  
  public ObservableArrayList<Item> getItems() {
    return items;
  }
  
  /**
   * Loops through all items in this inventory
   * to match a specified ID.
   *
   * @param itemId                      the ID to search for
   * @return                            an item if found
   * @throws IllegalArgumentException   if ID not found
   */
  public Item getItem(int itemId) {
    for (Item item : getItems()) {
      if (item.getId() == itemId) {
        return item;
      }
    }
    throw new IllegalArgumentException("Item ID not found.");
  }
  
  /**
   * Changes the capacity size by adding to it.
   * NOTE: If when added, capacity goes below zero, 
   * the capacity will NOT be changed, and false will be returned.
   *
   * @param capacitySizeToAdd   the capacity to add to the existing capacity
   * @return                    boolean for if the new capacity size is valid or not
   */
  public boolean changeCapacity(int capacitySizeToAdd) {
    if (getCapacity() + capacitySizeToAdd < 0) {
      return false;
    } else {
      Action.run(() -> {
        capacity.set(getCapacity() + capacitySizeToAdd);
      });
      return true;
    }
  }
  
  /**
   * Adds an <code>Item</code> to the inventory if there is capacity remaining.
   *
   * @param itemToAdd   the item to add to the inventory
   * @return            true if item is added, false otherwise
   */
  public boolean addItem(Item itemToAdd) {
    return Action.run(() -> {
      if (getCapacity() - itemToAdd.getItemSize() < 0) {
        return false;
      } else {
        capacity.set(getCapacity() - itemToAdd.getItemSize());
        worth.set(getWorth().add(itemToAdd.getItemBaseValue()));
        return getItems().add(itemToAdd);
      }
    });
  }
  
  /**
   * Removes an <code>Item</code> from the inventory.
   *
   * @param itemToRemove    the item to remove from the inventory
   * @return                true if removal was successful, false otherwise
   */
  public boolean removeItem(Item itemToRemove) {
    return Action.run(() -> {
      if (getItems().remove(itemToRemove)) {
        worth.set(getWorth().subtract(itemToRemove.getItemBaseValue()));
        capacity.set(getCapacity() + itemToRemove.getItemSize());
        return true;
      } else {
        return false;
      }
    });
  }
  
  /**
   * Used to sort all items in the inventory in ascending order.
   *
   * @param sortBy type of sorting to use on the item, default is ID
   *     otherwise, can be one of the following:
   *     base-value
   *     size
   *     name
   *     type
   */
  public void sortItems(String sortBy) {
    //Sort by ID if sortBy isn't another valid option.
    //This comparator checks if an item ID is less, equal, or more than another items ID.
    //Uses standard comparator syntax of -1 if this < that, 0 if this == that, 1 if this > that.
    Comparator<Item> comparator;
    switch (sortBy) {
      case "base-value":
        comparator = (item1, item2) -> item1.getItemBaseValue().compareTo(item2.getItemBaseValue());
        break;
      case "size": 
        comparator = (item1, item2) -> (item1.getItemSize() < item2.getItemSize()) ? -1
            : (item1.getItemSize() == item2.getItemSize()) ? 0
            : 1;
        break;
      case "name":
        comparator = (item1, item2) -> (item1.getItemName().compareTo(item2.getItemName()));
        break;
      case "type":
        comparator = (item1, item2) -> (item1.getItemType().compareTo(item2.getItemType()));
        break;
      default:
        comparator = (item1, item2) -> (item1.getId() < item2.getId()) ? -1
            : (item1.getId() == item2.getId()) ? 0 
            : 1;
        break;
    }
    Collections.sort(getItems(), comparator);
  }
  
  /**
   * Used to sort all items in the inventory in ascending order.
   *
   * @param sortBy type of sorting to use on the item, default ID
   *     otherwise, can be one of the following:
   *     base-value
   *     size
   *     name
   *     type
   * @param reverse if the output should be reversed or not
   */
  public void sortItems(String sortBy, boolean reverse) {
    //Sort by ID if sortBy isn't another valid option.
    //This comparator checks if an item ID is less, equal, or more than another items ID.
    //Uses standard comparator syntax of -1 if this < that, 0 if this == that, 1 if this > that.
    Comparator<Item> comparator;
    switch (sortBy) {
      case "base-value":
        comparator = (item1, item2) -> item1.getItemBaseValue().compareTo(item2.getItemBaseValue());
        break;
      case "size": 
        comparator = (item1, item2) -> (item1.getItemSize() < item2.getItemSize()) ? -1
            : (item1.getItemSize() == item2.getItemSize()) ? 0
            : 1;
        break;
      case "name":
        comparator = (item1, item2) -> (item1.getItemName().compareTo(item2.getItemName()));
        break;
      case "type":
        comparator = (item1, item2) -> (item1.getItemType().compareTo(item2.getItemType()));
        break;
      default:
        comparator = (item1, item2) -> (item1.getId() < item2.getId()) ? -1
            : (item1.getId() == item2.getId()) ? 0 
            : 1;
        break;
    }
    Collections.sort(getItems(), comparator);
    if (reverse) { 
      Collections.reverse(getItems());
    }
  }
  
  @Override
  public String toString() {
    String output = "(Remaining) Capacity: " + getCapacity()
        +  "\nWorth: " + getWorth();
    if (!getItems().isEmpty()) {
      output += "\nItems:";
      for (Item item : getItems()) {
        output += "\nItem:\n";
        output += item.toString();
      }
    }
    return output;
  }
}
