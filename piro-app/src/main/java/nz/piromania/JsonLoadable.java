package nz.piromania;

/**
 * If the class implements some sort of ID system
 * used for JSON, then it should implement this.
 * By extension, this interface also forces classes
 * to have a needsInitialization method to work out
 * if initialization should be done after the JSON
 * is loaded.
 */

public interface JsonLoadable {
  int getId();
  
  /**
   * Initializes this class after the JSON has been loaded.
   * Handy if it calculates some variables based on other variables.
   * In general however, this will be empty.
   */
  void initialize();
}
