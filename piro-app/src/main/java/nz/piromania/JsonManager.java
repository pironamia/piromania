package nz.piromania;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import nz.piromania.gameenvironment.eventsandchoices.choiceactions.ActionDeserializer;
import nz.piromania.gameenvironment.eventsandchoices.choiceactions.ChoiceAction;
import nz.piromania.reactiveengine.reactivity.Action;
import nz.piromania.reactiveengine.reactivity.Observable;
import nz.piromania.serialisation.ObservableInstanceCreator;

/**
 * A generic JSON Manager that can load from a specified format.
 * See the JsonREADME.md for the specific format. TODO
 * (Side note: this is a minor miracle that this works.)
 *
 * @author Andrew
 */
public final class JsonManager<T extends JsonLoadable> {
  private static final Random randomGenerator = new Random();
  private final Class<T> classType;
  private final Map<Integer, Object> allData = new HashMap<>();
  private final ArrayList<Integer> keys = new ArrayList<Integer>();
  private static final GsonBuilder builder = new GsonBuilder();
  
  static {
    builder.registerTypeAdapter(Observable.class, new ObservableInstanceCreator());
    builder.registerTypeAdapter(ChoiceAction.class, new ActionDeserializer());
  }
  
  private static final Gson gson = builder.create();
  
  /**
   * Used for establishing the type of data the JSON should be loading.
   * NOTE: The class MUST implement HasGetId.
   *
   * @param classType   the type of class that should be loaded,
   *     used like this: JsonManager.createGeneric(Item.class) for example
   * @return            a new JsonManager to work with
   */
  public static <U extends JsonLoadable> JsonManager<U> createGeneric(Class<U> classType) {
    return new JsonManager<U>(classType);
  }
  
  protected JsonManager(Class<T> classType) {
    this.classType = classType;
  }
  
  /**
   * Generates all the data in the specified JSON file. MUST be called before any
   * get functions. As a side note, it's a miracle this works.
   *
   * @param filePath                the path to the JSON file
   * @throws FileNotFoundException  if the JSON is malformed or missing
   */
  @SuppressWarnings({"unchecked", "hiding"})
  public <T extends JsonLoadable> void generate(String filePath) throws FileNotFoundException {
    //¯\_(ツ)_/¯
    Class<T> classType = (Class<T>) this.getClassType();
    final InputStream inputStream = getClass().getClassLoader().getResourceAsStream(filePath);
    if (inputStream == null) {
      throw new FileNotFoundException();
    }
    JsonReader reader = new JsonReader(new InputStreamReader(inputStream));
    //No, really, ¯\_(ツ)_/¯
    Action.run(() -> {
      ArrayList<T> allGsonData = gson.fromJson(reader,
          TypeToken.getParameterized(ArrayList.class, classType).getType());

      for (T data : allGsonData) {
        allData.put(data.getId(), data);
        keys.add(data.getId());
      }
    });
  }
  
  private Class<T> getClassType() {
    return this.classType;
  }
  
  /**
   * Gets the data tied to an ID.
   *
   * @param id  the specified id
   * @return    the data
   */
  @SuppressWarnings("unchecked")
  public T getData(int id) {
    return (T) allData.get(id);
  }
  
  /**
   * Gets the data tied to a random ID.
   * There MUST be at least one ID for this to work.
   *
   * @return    the random data
   */
  @SuppressWarnings("unchecked")
  public T getRandomData() {
    return (T) allData.get(keys.get(randomGenerator.nextInt(keys.size())));
  }
  
  /**
   * Used for getting how many items are in
   * a JsonManager's data. Handy for mass initialization.
   *
   * @return    the length of the data stored
   */
  public int getDataSize() {
    return keys.size();
  }
  
  /**
   * Used for getting all the data.
   * Handy for iteration through unknown keys.
   *
   * @return    a map of IDs tied to objects
   */
  public Map<Integer, Object> getAllData() {
    return allData;
  }
  
  /**
   * Used for getting all IDs on a data set.
   * Useful for iteration through unknown amount
   * of items.
   *
   * @return    an ArrayList of IDs
   */
  public ArrayList<Integer> getAllIds() {
    return keys;
  }
}
