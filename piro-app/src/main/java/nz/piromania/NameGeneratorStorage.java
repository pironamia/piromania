package nz.piromania;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.Random;

/**
 * Stores all the names from a name generator.
 * Stores a list of prefixes to put before, and also
 * many postfixes in case the postfix has to be specified.
 * This system is a bit crude, but that doesn't matter.
 */
public final class NameGeneratorStorage implements JsonLoadable {
  private static final transient Random randomGenerator = new Random();
  @SerializedName(value = "prefix")
  private final ArrayList<String> prefix;
  @SerializedName(value = "postfixes")
  private final ArrayList<ArrayList<String>> postfixes;
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void initialize() {}
  
  /**
   * Generates a NameGeneratorStorage. 
   * Note: This should exclusively be done by GSON.
   *
   * @param prefix      gets put before any randomly generated name
   * @param postfixes   gets put after the prefix to create a random name
   */
  public NameGeneratorStorage(ArrayList<String> prefix, ArrayList<ArrayList<String>> postfixes) {
    this.prefix = prefix;
    this.postfixes = postfixes;
  }
  
  /**
   * Generates a completely random name,
   * with a random Postfix from Postfixes.
   *
   * @return    a random name
   */
  public String generateRandomName() {
    ArrayList<String> randomPostfix = postfixes.get(randomGenerator.nextInt(postfixes.size()));
    return prefix.get(randomGenerator.nextInt(prefix.size())) 
        + " " + randomPostfix.get(randomGenerator.nextInt(randomPostfix.size()));
  }
  
  /**
   * Generates a random name, from a certain postfix.
   * Useful for giving certain types meaning, for example stores.
   *
   * @param index   the index of which the postfix list is stored
   * @return        a random name with a postfix matching the index
   */
  public String generateRandomName(int index) {
    ArrayList<String> postfix = postfixes.get(index);
    return prefix.get(randomGenerator.nextInt(prefix.size()))
        + " " + postfix.get(randomGenerator.nextInt(postfix.size()));
  }
  
  @Override
  public String toString() {
    String output = "Prefixes:";
    output += "\n" + prefix;
    output += "\nPostfixes:";
    for (ArrayList<String> postfix : postfixes) {
      output += "\n" + postfix.toString();
    }
    return output;
  }
  
  @Override
  public int getId() {
    return 0;
  }
}
