package nz.piromania;

import com.google.gson.annotations.SerializedName;
import java.util.Map;
import nz.piromania.reactiveengine.reactivity.Action;
import nz.piromania.reactiveengine.reactivity.Observable;

/**
 * This class is used to store stats of something.
 * Could be an <code>UpgradeItem</code>, a <code>Ship</code>, or similar.
 * <p>List of Stats:</p>
 * <ul>
 *      <li>Max Hitpoints</li>
 *      <li>Risk multiplier</li>
 *      <li>Damage potential</li>
 *      <li>Cargo capacity</li>
 *      <li>Travel speed</li>
 * </ul>
 */
public class Stats {
  @SerializedName(value = "hitpoint-max")
  private final Observable<Integer> maxHitpoints = new Observable<>(0);
  @SerializedName(value = "risk-multiplier")
  private final Observable<Float> riskMultiplier = new Observable<>(0f);
  @SerializedName(value = "damage-potential")
  private final Observable<Integer> damagePotential = new Observable<>(0);
  @SerializedName(value = "cargo-capacity")
  private final Observable<Integer> cargoCapacity = new Observable<>(0);
  @SerializedName(value = "travel-multiplier")
  private final Observable<Float> travelMultiplier = new Observable<>(0f);
   
  /**
   * Initializes stats for something.
   * Could be an <code>UpgradeItem</code>, a <code>Ship</code>, or similar.
   *
   * @param maxHitpoints        the maximum amount of hitpoints this has
   * @param riskMultiplier      the risk multiplier this has, on ship used to determine
   *     events
   * @param damagePotential     the amount of damage potential this has
   * @param cargoCapacity       the amount of cargo capacity this has
   * @param travelMultiplier    the travel multiplier this has, on ships used to determine
   *     travel speed
   */
  public Stats(int maxHitpoints, float riskMultiplier, 
      int damagePotential, int cargoCapacity, float travelMultiplier) {
    Action.run(() -> {
      this.maxHitpoints.set(maxHitpoints);
      this.riskMultiplier.set(riskMultiplier);
      this.damagePotential.set(damagePotential);
      this.cargoCapacity.set(cargoCapacity);
      this.travelMultiplier.set(travelMultiplier);
    });
  }
  
  public int getMaxHitpoints() {
    return maxHitpoints.get();
  }
  
  public float getRiskMultiplier() {
    return riskMultiplier.get();
  }
  
  public int getDamagePotential() {
    return damagePotential.get();
  }
  
  public int getCargoCapacity() {
    return cargoCapacity.get();
  }

  public float getTravelMultiplier() {
    return travelMultiplier.get();
  }
  
  /**
   * Modifies the stats of something. Could be an Item, could be a Ship, etc.
   *     by adding all values of another <code>Stats</code> type.
   *     For multipliers, additive is specified for if it should be additive
   *     or multiplicative.
   *     If it is additive, the following will happen:
   *     1
   *     + 1.2
   *     = 1.2
   *     + 1.2
   *     = 1.4
   *     NOTE: 1 gets subtracted from the number applied to keep consistency.
   *     For instance. 0.8 would REDUCE the BASE multiplier by 20%.
   *     If it is multiplicative (or not additive), the following will happen:
   *     1
   *     + 1.2
   *     = 1.2
   *     + 1.2
   *     = 1.44
   *     NOTE: 1 does NOT get subtracted from the number applied to keep consistency.
   *     For instance, 0.8 would REDUCE the CURRENT multiplier by 20%.
   *
   * @param statsToApply the <code>Stats</code> to add to this component
   * @param additive     whether or not multipliers should be additive or multiplicative
   *     true for additive, false for multiplicative
   */
  public void changeStats(Stats statsToApply, boolean additive) {
    //Add all non-multipliers.
    Action.run(() -> {
      maxHitpoints.set(getMaxHitpoints() + statsToApply.getMaxHitpoints());
      damagePotential.set(getDamagePotential() + statsToApply.getDamagePotential());
      cargoCapacity.set(getCargoCapacity() + statsToApply.getCargoCapacity());
      if (additive) {
        riskMultiplier.set(getRiskMultiplier() + (statsToApply.getRiskMultiplier() - 1));
        travelMultiplier.set(getTravelMultiplier() + (statsToApply.getTravelMultiplier() - 1));
      } else {
        riskMultiplier.set(getRiskMultiplier() * statsToApply.getRiskMultiplier());
        travelMultiplier.set(getTravelMultiplier() * statsToApply.getTravelMultiplier());
      }
    });
  }

  @Override
  public String toString() {
    String output = "";
    Map<String, FloatSupplier> variables = Map.of(
        "Max hitpoints: ", () -> (float) getMaxHitpoints(), 
        "Risk multiplier: ", () -> (float) getRiskMultiplier(),
        "Damage potential: ", () -> (float) getDamagePotential(),
        "Cargo capacity: ", () -> (float) getCargoCapacity(),
        "Travel multiplier: ", () -> (float) getTravelMultiplier()
    );
    for (Map.Entry<String, FloatSupplier> entry : variables.entrySet()) {
      output += "\n" + entry.getKey();
      float value = entry.getValue().getValue();
      int threshold = 0;
      String belowThresholdColour = "§R";
      String aboveThresholdColour = "§G";
      if (entry.getKey().contains("multiplier")) {
        //If it's a multiplier, set threshold to 1 to determine if it's good or bad.
        threshold = 1;
      }
      if (entry.getKey().contains("Risk")) {
        //Here the colours are inverted as -20 is good, but 40 is bad, for example.
        belowThresholdColour = "§G";
        aboveThresholdColour = "§R";
      }
      if (value < threshold) {
        output += belowThresholdColour;
      } else if (value > threshold) {
        output += aboveThresholdColour;
      }
      output += value + "§!";
    }
    return output;
  }
}
