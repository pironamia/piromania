package nz.piromania;

import java.util.ArrayList;
import java.util.Random;

/**
 * This is used by GSON for storing an array of
 * strings. Handy for randomly generated things
 * such as names, descriptions, etc.
 */
public class StringStorage implements JsonLoadable {
  private static final transient Random randomGenerator = new Random();
  private final ArrayList<String> strings;
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void initialize() {}
  
  /**
   * Create a StringStorage. 
   * Note: Only GSON should be working with this class explicitly.
   *
   * @param strings the strings to parse in
   */
  public StringStorage(ArrayList<String> strings) {
    this.strings = strings;
  }
  
  /**
   * Gets a random string.
   *
   * @return    a random string
   */
  public String generateRandomString() {
    return strings.get(randomGenerator.nextInt(strings.size()));
  }
  
  @Override
  public int getId() {
    return 0;
  }
  
  @Override
  public String toString() {
    return strings.toString();
  }
}
