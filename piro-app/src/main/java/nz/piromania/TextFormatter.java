package nz.piromania;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import nz.piromania.gameenvironment.ships.Ship;
import nz.piromania.gameenvironment.ships.ShipVariables;

/**
 * This class is used for all text formatting in the game.
 * If you parse an input text, it will go through and replace any special designated characters
 * with predefined replacements. This allows for ANSI in the console, variables to be put into text
 * in real time, and icons to be rendered if the GUI supports it.
 * TODO: Add icons in the future once GUI is ready for it. £icon£
 * 
 * <p>System heavily based on Stellaris' localisation modding.<a>https://stellaris.paradoxwikis.com/Localisation_modding#.24_Codes</a></p>
 */

public class TextFormatter {

  private static final Map<String, String> colors;
  private static final String ANSI_RESET = "\u001B[0m";
  private static final String ANSI_BLACK = "\u001B[30m";
  private static final String ANSI_RED = "\u001B[31m";
  private static final String ANSI_GREEN = "\u001B[32m";
  private static final String ANSI_YELLOW = "\u001B[33m";
  private static final String ANSI_BLUE = "\u001B[34m";
  private static final String ANSI_PURPLE = "\u001B[35m";
  private static final String ANSI_CYAN = "\u001B[36m";
  private static final String ANSI_WHITE = "\u001B[37m";
  private static final Map<String, FloatSupplier> variables;
  private static final Map<String, String> icons;
  
  static {
    colors = new HashMap<>();
    colors.put("§!", ANSI_RESET);
    colors.put("§_", ANSI_BLACK);
    colors.put("§R", ANSI_RED);
    colors.put("§G", ANSI_GREEN);
    colors.put("§Y", ANSI_YELLOW);
    colors.put("§B", ANSI_BLUE);
    colors.put("§P", ANSI_PURPLE);
    colors.put("§C", ANSI_CYAN);
    colors.put("§W", ANSI_WHITE);
    variables = new HashMap<>();


    //For now, hard coded values. Later, paths to image files?
    icons = new HashMap<>();
    icons.put("£damage-potential£", "damage potential");
    icons.put("£cargo-capacity£", "cargo capacity");
    icons.put("£max-hitpoints£", "max hitpoints");
    icons.put("£current-hitpoints£", "current hitpoints");
    icons.put("£travel-speed£", "travel speed");
  }
  
  private final boolean supportsAnsi;
  private final Ship ship;
  
  /**
   * Constructs a text formatter, used for parsing all predefined text into a better format.
   *
   * @param isAnsiSupported declares whether or not ANSI is supported, 
   *     to be determined at launch by the user
   * @param ship            the ship used for ShipVariables for variable
   *     matching
   */
  public TextFormatter(boolean isAnsiSupported, Ship ship) {
    supportsAnsi = isAnsiSupported;
    this.ship = ship;
    for (ShipVariables shipVariable : ShipVariables.values()) {
      //I.E. Matches $damage-potential$ or $damage-potential|.*$
      if (shipVariable.getShipVariable().getVariableFunction()
          .getValue(this.ship).getClass().getSimpleName().equals("Currency")) {
        Currency currency = 
            (Currency) shipVariable.getShipVariable().getVariableFunction().getValue(this.ship);
        variables.put(String.format("\\$%1$s\\$|\\$%1$s\\|\\d+\\$", 
            shipVariable.getShipVariable().getVariableAlias()),
            () -> currency.getCents() / 100);
      } else {
        variables.put(String.format("\\$%1$s\\$|\\$%1$s\\|\\d+\\$", 
            shipVariable.getShipVariable().getVariableAlias()),
            () -> Float.parseFloat(shipVariable.getShipVariable().getVariableFunction()
                .getValue(this.ship).toString()));
      }
    }
  }
  /**
   * The main function of the text formatter, this will parse any strings to remove special values
   * and replace it with the ones intended to be seen.
   *
   * @param textToFormat    the text supplied to the formatter to be formatted
   * @return                a String containing all the changes the formatter made
   */
  
  public String formatText(String textToFormat) {
    String output = textToFormat;
    if (supportsAnsi) {
      //For all supported ANSI colors
      for (Map.Entry<String, String> entry : colors.entrySet()) {
        //Replace them with their escape codes
        output = output.replace(entry.getKey(), entry.getValue());
      }
    } else {
      //Otherwise just put an empty string (removing all special color syntax)
      for (String key : colors.keySet()) {
        output = output.replace(key, "");
      }
    }   
    
    for (Map.Entry<String, FloatSupplier> entry : variables.entrySet()) {
      Pattern pattern = Pattern.compile(entry.getKey());
      Matcher matcher = pattern.matcher(output);
      while (matcher.find()) {
        //Found $variable-name|, next check the digits after |
        Pattern subPattern = Pattern.compile("\\d+");
        Matcher subMatcher = subPattern.matcher(matcher.group());
        //I really would prefer this to be dynamic. If I have time, there will be
        //a map of maps to add in the condition of if a variable is a currency or not.
        String tempReplacer = "";
        if (entry.getKey().contains("wages") || entry.getKey().contains("damage-cost")) {
          //Default rounding to no decimal places
          tempReplacer = "$%.0f";
        } else {
          //Default rounding to no decimal places
          tempReplacer = "%.0f";
        }
        //Should only ever be one instance of string of digits
        if (subMatcher.find()) {
          //I really would prefer this to be dynamic. If I have time, there will be
          //a map of maps to add in the condition of if a variable is a currency or not.
          if (entry.getKey().contains("wages") || entry.getKey().contains("damage-cost")) {
            //If there's a string of digits, format to the amount of digits supplied
            tempReplacer = String.format("$x.%sf", subMatcher.group()).replace("x", "%");
          } else {
            //If there's a string of digits, format to the amount of digits supplied
            tempReplacer = String.format("x.%sf", subMatcher.group()).replace("x", "%");
          }
        }
        //Replace found variables in text with the values from the variables
        //rounded to supplied digits (if supplied).   
        output = output.replace(
            matcher.group(),
            String.format(tempReplacer, entry.getValue().getValue())
        );
        
      }
    }
    
    //TODO: When icons are added
    for (Map.Entry<String, String> entry : icons.entrySet()) {
      output = output.replace(entry.getKey(), entry.getValue());
    }
    return output;
  }
}
