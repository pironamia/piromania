package nz.piromania;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import nz.piromania.gameenvironment.ships.Ship;
import nz.piromania.gameenvironment.ships.ShipVariables;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.component.text.style.TextStyle;

/**
 * This class is used for all text formatting in the game. If you parse an input text, it will go
 * through and replace any special designated characters with predefined replacements. This allows
 * for ANSI in the console, variables to be put into text in real time, and icons to be rendered if
 * the GUI supports it. TODO: Add icons in the future once GUI is ready for it. £icon£
 *
 * <p>System heavily based on Stellaris' localisation modding.<a>https://stellaris.paradoxwikis.com/Localisation_modding#.24_Codes</a></p>
 */
public class TextFormatterGui {

  private static final Map<String, Color> colors;
  private static final String defaultColorCommand = "§!";

  private static final Map<String, String> icons;

  private static final Pattern initialTextSpanPattern = Pattern.compile("[^§]+|§");
  private static final Pattern textSpanPattern = Pattern.compile("§([^§]+|§)");

  static {
    colors = new HashMap<>();
    colors.put("§_", Color.BLACK);
    //colors.put("§!", Color.BLACK);
    colors.put("§R", Color.RED);
    colors.put("§G", new Color(24, 140, 35));
    colors.put("§Y", Color.YELLOW);
    colors.put("§B", Color.BLUE);
    colors.put("§P", new Color(167, 44, 201));
    colors.put("§C", Color.CYAN);
    colors.put("§W", Color.WHITE);

    //For now, hard coded values. Later, paths to image files?
    icons = new HashMap<>();
    icons.put("£damage-potential£", "damage potential");
    icons.put("£cargo-capacity£", "cargo capacity");
    icons.put("£max-hitpoints£", "max hitpoints");
    icons.put("£current-hitpoints£", "current hitpoints");
    icons.put("£travel-speed£", "travel speed");
  }

  private final Map<String, FloatSupplier> variables = new HashMap<>();
  private final Ship ship;

  /**
   * Constructs a text formatter, used for parsing all predefined text into a better format.
   *
   * @param ship the ship used for ShipVariables for variable matching
   */
  public TextFormatterGui(Ship ship) {
    this.ship = ship;
    for (ShipVariables shipVariable : ShipVariables.values()) {
      //I.E. Matches $damage-potential$ or $damage-potential|.*$
      if (shipVariable.getShipVariable().getVariableFunction()
          .getValue(this.ship).getClass().getSimpleName().equals("Currency")) {
        Currency currency =
            (Currency) shipVariable.getShipVariable().getVariableFunction().getValue(this.ship);
        variables.put(String.format("\\$%1$s\\$|\\$%1$s\\|\\d+\\$",
            shipVariable.getShipVariable().getVariableAlias()),
            () -> currency.getCents() / 100);
      } else {
        variables.put(String.format("\\$%1$s\\$|\\$%1$s\\|\\d+\\$",
            shipVariable.getShipVariable().getVariableAlias()),
            () -> Float.parseFloat(shipVariable.getShipVariable().getVariableFunction()
                .getValue(this.ship).toString()));
      }
    }
  }

  /**
   * The main function of the text formatter, this will parse any strings to remove special values
   * and replace it with the ones intended to be seen.
   *
   * @param textToFormatUnprocessed the text supplied to the formatter to be formatted
   * @param defaultColour           the colour to apply to text that has no other colour specified
   * @return a String containing all the changes the formatter made
   */
  public ArrayList<TextSpan> formatText(String textToFormatUnprocessed, Color defaultColour) {
    String textToFormat = textToFormatUnprocessed;
    for (final String iconId : icons.keySet()) {
      textToFormat = textToFormat.replaceAll(iconId, icons.get(iconId));
    }

    final TextStyle defaultStyle = TextStyle.plain.copyWith(
        null,
        null,
        null,
        null,
        defaultColour
    );

    final ArrayList<TextSpan> output = new ArrayList<>();
    if (!textToFormat.startsWith("§")) {
      final Matcher initialColorMatcher = initialTextSpanPattern.matcher(textToFormat);
      if (initialColorMatcher.find()) {
        output.add(new TextSpan(defaultStyle, initialColorMatcher.group()));
      } else {
        output.add(new TextSpan(
            defaultStyle,
            textToFormat
        ));
      }
    }
    //Matches §[any string of any length]§
    //I.E.: §Red§ -> §Red
    final Matcher colorMatcher = textSpanPattern.matcher(textToFormat);
    while (colorMatcher.find()) {
      if (colorMatcher.group().length() > 2) {
        final String colorMatcherGroup = colorMatcher.group().substring(0, 2);

        output.add(new TextSpan(
            TextStyle.plain.copyWith(
                null,
                null,
                null,
                null,
                (colorMatcherGroup.equals(defaultColorCommand)) ? defaultColour
                    : colors.get(colorMatcherGroup)
            ),
            colorMatcher.group().substring(2)
        ));
      }
    }

    return output;
  }

  /**
   * The main function of the text formatter, this will parse any strings to remove special values
   * and replace it with the ones intended to be seen.
   *
   * @param textToFormatUnprocessed the text supplied to the formatter to be formatted
   * @return a String containing all the changes the formatter made
   */
  public ArrayList<TextSpan> formatText(String textToFormatUnprocessed) {
    return formatText(textToFormatUnprocessed, Color.BLACK);
  }
}
