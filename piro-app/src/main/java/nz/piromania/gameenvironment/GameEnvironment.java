package nz.piromania.gameenvironment;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import nz.piromania.App;
import nz.piromania.Currency;
import nz.piromania.JsonManager;
import nz.piromania.NameGeneratorStorage;
import nz.piromania.StringStorage;
import nz.piromania.TextFormatterGui;
import nz.piromania.gameenvironment.eventsandchoices.Choice;
import nz.piromania.gameenvironment.eventsandchoices.Event;
import nz.piromania.gameenvironment.eventsandchoices.choiceactions.ChoiceAction;
import nz.piromania.gameenvironment.eventsandchoices.choiceactions.EndGameAction;
import nz.piromania.gameenvironment.eventsandchoices.choiceactions.LoadEventAction;
import nz.piromania.gameenvironment.eventsandchoices.choiceactions.LoadIslandAction;
import nz.piromania.gameenvironment.islands.Island;
import nz.piromania.gameenvironment.items.Item;
import nz.piromania.gameenvironment.items.ItemData;
import nz.piromania.gameenvironment.items.ItemType;
import nz.piromania.gameenvironment.items.UpgradeItem;
import nz.piromania.gameenvironment.routes.Route;
import nz.piromania.gameenvironment.ships.Ship;
import nz.piromania.gameenvironment.stores.Personality;
import nz.piromania.gameenvironment.stores.Store;
import nz.piromania.reactiveengine.reactivity.Action;
import nz.piromania.reactiveengine.reactivity.Observable;
import nz.piromania.ui.AppTextStyles;
import nz.piromania.ui.Screen;
import nz.piromania.ui.ScreenManager;
import nz.piromania.ui.screen.EventScreenComponentArgs;

/**
 * The entire game wrapped up in a neat bow. This is where any UI should latch onto, and retrieve
 * all values from. This class should be used to store data. Anything temporary should be loaded in
 * the GUI.
 */
public final class GameEnvironment {

  private static final Random randomGenerator = new Random();
  private final JsonManager<ItemData> itemManager;
  private final JsonManager<Ship> shipManager;
  private final JsonManager<Event> goodEventManager;
  private final JsonManager<Event> neutralEventManager;
  private final JsonManager<Event> badEventManager;
  private final JsonManager<Personality> personalityManager;
  private final JsonManager<NameGeneratorStorage> storeNameManager;
  private final JsonManager<Store> storeManager;
  private final JsonManager<StringStorage> islandNameManager;
  private final JsonManager<StringStorage> islandDescriptionManager;
  private final JsonManager<StringStorage> islandImageManager;
  private final ScreenManager screenManager = new ScreenManager(Screen.WELCOME, null);
  private final String directory;
  private final Observable<Integer> currentDay = new Observable<Integer>(0);
  private final Observable<Island> currentIsland = new Observable<Island>(null);
  private String traderName;
  //private Inventory traderInventory;
  private Map<Store, Map<Item, Currency>> soldLedger;
  private Observable<Currency> traderMoney;
  private int gameDuration;
  private Ship ship;
  private TextFormatterGui textFormatterGui;
  private ArrayList<Island> islands = new ArrayList<Island>();
  private Island tradersHomeIsland;
  private ArrayList<ArrayList<Route>> currentRoutes = new ArrayList<ArrayList<Route>>();
  private transient BufferedImage islandMap;

  /**
   * Initializes a game environment. Also initializes all the managers that the game environment
   * uses.
   *
   * @param directory where the json files are stored example: "src/main/resources/"
   * @throws Exception if any error occurs during initialization
   */
  public GameEnvironment(String directory) throws Exception {
    this.directory = directory;
    //First, let's get all those JSON files initialized.
    //Loads all items
    this.itemManager = JsonManager.createGeneric(ItemData.class);
    itemManager.generate(directory + "Items/Items.json");
    //Loads all ships
    this.shipManager = JsonManager.createGeneric(Ship.class);
    shipManager.generate(directory + "Ships/Ships.json");
    //Splitting events into 3 different managers for simplicity
    //Loads all good events
    this.goodEventManager = JsonManager.createGeneric(Event.class);
    goodEventManager.generate(directory + "Events/GoodEvents.json");
    //Loads all neutral events
    this.neutralEventManager = JsonManager.createGeneric(Event.class);
    neutralEventManager.generate(directory + "Events/NeutralEvents.json");
    //Loads all bad events
    this.badEventManager = JsonManager.createGeneric(Event.class);
    badEventManager.generate(directory + "Events/BadEvents.json");
    //Loads all personalities
    this.personalityManager = JsonManager.createGeneric(Personality.class);
    personalityManager.generate(directory + "Stores/Personalities.json");
    //Loads all store names
    this.storeNameManager = JsonManager.createGeneric(NameGeneratorStorage.class);
    storeNameManager.generate(directory + "Stores/StoreNames.json");
    //Loads all stores
    this.storeManager = JsonManager.createGeneric(Store.class);
    storeManager.generate(directory + "Stores/Stores.json");
    //Stores have to be specially loaded
    for (int i : storeManager.getAllIds()) {
      storeManager.getData(i).initialize(personalityManager);
    }
    //Loads all island names
    this.islandNameManager = JsonManager.createGeneric(StringStorage.class);
    islandNameManager.generate(directory + "Islands/IslandNames.json");
    //Loads all island descriptions
    this.islandDescriptionManager = JsonManager.createGeneric(StringStorage.class);
    islandDescriptionManager.generate(directory + "Islands/IslandDescriptions.json");
    //Loads all island image paths
    this.islandImageManager = JsonManager.createGeneric(StringStorage.class);
    islandImageManager.generate(directory + "Islands/IslandImages.json");
    //Initializes every manager, and everything stored in every manager
    for (JsonManager<?> manager : Arrays.asList(
        itemManager,
        shipManager,
        goodEventManager,
        neutralEventManager,
        badEventManager,
        personalityManager,
        storeNameManager,
        storeManager,
        islandNameManager,
        islandDescriptionManager
    )) {
      for (int i : manager.getAllIds()) {
        manager.getData(i).initialize();
      }
    }
  }

  /**
   * Starts the game.
   *
   * @param traderName      the trader's name
   * @param gameDuration    the duration of the game
   * @param ship            the trader's ship
   */
  public void startGame(String traderName, int gameDuration, Ship ship) {
    this.traderName = traderName;
    this.gameDuration = gameDuration;
    this.ship = ship;
    this.textFormatterGui = new TextFormatterGui(ship);
    //If the player should start with anything, it should be decided here.
    //Perhaps default upgrades? Who knows.
    //ship.getInventory() = new Inventory?? Who knows.
    //this.traderInventory = new Inventory(ship.getShipStats().getCargoCapacity());
    //Currently, start them off with 500$.
    //TODO: Balance patch.
    traderMoney = new Observable<Currency>(new Currency(2000, 00));
    generateIslands();
    this.soldLedger = new HashMap<Store, Map<Item, Currency>>(Map.of(
        islands.get(0).getStore(), new HashMap<Item, Currency>(),
        islands.get(1).getStore(), new HashMap<Item, Currency>(),
        islands.get(2).getStore(), new HashMap<Item, Currency>(),
        islands.get(3).getStore(), new HashMap<Item, Currency>(),
        islands.get(4).getStore(), new HashMap<Item, Currency>()
    ));
    screenManager.show(Screen.ISLAND_OVERVIEW);
  }

  /**
   * Used in initialization to generate all the islands. Also calls generateStores() and
   * generateRoutes().
   */
  private void generateIslands() {
    ArrayList<Store> stores = generateStores();
    for (int i = 0; i < 5; i++) {
      final var containerObject = new Object() {
        String islandName;
        String islandImage;
      };

      do {
        containerObject.islandName = islandNameManager.getData(0).generateRandomString();
      } while (islands.stream().anyMatch(
          (island) -> island.getName().equals(containerObject.islandName)
      ));

      do {
        containerObject.islandImage = islandImageManager.getData(0).generateRandomString();
      } while (islands.stream().anyMatch(
          (island) -> island.getImagePath().equals(containerObject.islandImage)
      ));

      islands.add(new Island(
          containerObject.islandName,
          islandDescriptionManager.getData(0).generateRandomString(),
          directory + containerObject.islandImage,
          stores.get(i),
          islands
      ));
    }
    //Trader's home island should be one closest to the center.
    int minDistance = Integer.MAX_VALUE;
    for (Island island : islands) {
      double islandDistance = island
          .calculateDistance(App.getScreenWidth() / 2, App.getScreenHeight() / 2);
      if (islandDistance < minDistance) {
        tradersHomeIsland = island;
        minDistance = (int) islandDistance;
      }
    }
    setCurrentIsland(tradersHomeIsland);
    //Randomize the inventory of the store
    //This also sets the prices of buying/selling
    getCurrentIsland().getStore().randomizeInventory(itemManager);
    //Create some routes to all other islands.
    generateRoutes();
  }

  /**
   * Generates all the stores. Prioritises JSON.
   *
   * @return all the stores
   */
  private ArrayList<Store> generateStores() {
    ArrayList<Store> stores = new ArrayList<>();
    //Ensure those from JSON are ALWAYS loaded.
    for (int i : storeManager.getAllIds()) {
      stores.add(storeManager.getData(i));
      storeManager.getData(i).randomizeInventory(itemManager);
    }
    //Generates additional stores so that there's 5 stores.
    //Note: JSON generated stores ID's should only go up to the length
    //of the manager, or else there will be a duplicate ID here.
    for (int i = storeManager.getDataSize(); i < 5; i++) {
      //Randomly generate buying/selling type
      ItemType buyingType = ItemType.values()[randomGenerator.nextInt(ItemType.values().length)];
      int sellingIndex = randomGenerator.nextInt(ItemType.values().length);
      ItemType sellingType = ItemType.values()[sellingIndex];
      Store store = new Store(
          i,
          storeNameManager.getData(0).generateRandomName(sellingIndex),
          personalityManager,
          buyingType,
          sellingType
      );
      stores.add(store);
      store.randomizeInventory(itemManager);
    }
    return stores;
  }

  /**
   * Generates routes to every other island from this island.
   */
  private void generateRoutes() {
    for (Island island : islands) {
      if (getCurrentIsland() != island) {
        currentRoutes.add(getCurrentIsland().generateRoutes(island, ship, 3));
      }
    }
  }

  /**
   * Takes a route. Advances the day, sets a new island, generates new routes. May also return an
   * event for the GUI to handle.
   *
   * @param routeToTake the route to be taken
   * @return an event if one is encountered
   */
  public Event takeRoute(Route routeToTake) {
    setCurrentDay(getCurrentDay() + routeToTake.calculateTravelTime());
    setCurrentIsland(routeToTake.getDestination());
    setTraderMoney(getTraderMoney().subtract(calculateSailingCost(routeToTake)));
    ship.healShipToFull();
    //An island has been hopped, so set new store prices for every OTHER store
    //Also randomize their inventories a bit
    for (Island island : islands) {
      if (!island.equals(getCurrentIsland())) {
        island.getStore().randomizeInventory(itemManager);
      }
    }
    //Set routes to be empty
    currentRoutes = new ArrayList<ArrayList<Route>>();
    //Fill it up with new generated routes
    generateRoutes();
    float risk = routeToTake.calculateRisk();
    float badChance = risk;
    //Chance 0.65 to chance the % that goes to neutral/good
    //1 means always neutral
    //0.5 would mean equal chance of neutral or good
    //0 means always good
    float neutralChance = (1 - badChance) * 0.65f;
    float goodChance = 1 - badChance - neutralChance;
    float randomFloat = randomGenerator.nextFloat();
    Event event = null;
    if (randomFloat <= goodChance) {
      if (goodEventManager.getDataSize() != 0) {
        event = runEvent(goodEventManager.getRandomData(), "GOOD");
      }
    } else if (randomFloat <= goodChance + neutralChance) {
      if (neutralEventManager.getDataSize() != 0) {
        //For now, no neutral events.
        //event = runEvent(neutralEventManager.getRandomData(), "NEUTRAL"); 
      }
    } else {
      if (badEventManager.getDataSize() != 0) {
        Event randomBadEvent = badEventManager.getRandomData();
        //Note: A way around this that SHOULD be implemented in the
        //future, is by having a "consequential events" file instead.
        while (randomBadEvent.getId() == 2001
            || randomBadEvent.getId() == 2002
            || randomBadEvent.getId() == 2003) {
          randomBadEvent = badEventManager.getRandomData();
        }
        event = runEvent(randomBadEvent, "BAD");
      }
    }
    if (event == null) {
      screenManager.show(Screen.ISLAND_OVERVIEW);
    }
    return event;
  }

  /**
   * Runs an event. This is where data gets overwritten in the events, so any dice-rolls, or
   * explicit changes should be done here. Example: Unfortunate weather
   *
   * <pre>
   * <code>
   * //Unfortunate weather ID
   * case 2004:
   *   int damageToTake = randomGenerator.nextInt((int) (ship.getCurrentHitpoints() * 0.75f));
   *   //Only one choice, it's to brace and take damage.
   *   event.getChoices().get(0).setTooltip("Take " + damageToTake + " damage!");
   *   event.getChoices().get(0).getActions().get(0).setData(
   *       "hitpointsToChange", -damageToTake, ship
   *   );
   *   return event;
   * </code>
   * </pre>
   *
   * @param event     the event to write over
   * @param eventType the event type, decided in takeRoute()
   * @return the modified event
   */
  @SuppressWarnings({"unchecked", "rawtypes"})
  public Event runEvent(Event event, String eventType) {
    System.out.println(event.getTitle());
    screenManager.show(Screen.EVENT, new EventScreenComponentArgs(event));
    for (Choice choice : event.getChoices()) {
      boolean containsLoadEvent = false;
      if (choice.getActions() != null) {
        for (ChoiceAction action : choice.getActions()) {
          //Makes sure we don't load an event and then an island directly after
          if (action instanceof LoadEventAction) {
            containsLoadEvent = true;
            break;
          }
        }
      } else {
        //If it has no actions, better make sure it loads the next island
        choice.setActions(
            new ArrayList<ChoiceAction>(Arrays.asList(new LoadIslandAction<ChoiceAction>(this))));
      }
      if (!containsLoadEvent) {
        choice.getActions().add(new LoadIslandAction<ChoiceAction>(this));
      }
    }
    switch (eventType) {
      case "GOOD":
        switch (event.getId()) {
          //Rescued Sailors ID
          case 0:
            int centsToRecieve = randomGenerator.nextInt(200000) + 20000;
            event.getChoices().get(0).setTooltip(
                "You take the monetary reward for saving these souls.\nRecieve §G" + new Currency(
                    centsToRecieve) + "§!");
            setTraderMoney(getTraderMoney().add(new Currency(centsToRecieve)));
            return event;
          default:
            break;
        }
        break;
      case "NEUTRAL":
        switch (event.getId()) {
          //Currently, no neutral events exist. So, just do nothing.
          case 1000:
            break;
          default:
            break;
        }
        break;
      case "BAD":
        switch (event.getId()) {
          //Pirates ID
          case 2000:
            int diceRoll = randomGenerator.nextInt(6) + 1;
            //TODO: What seems fair for this?
            int pirateDamagePotential = randomGenerator.nextInt(1500) + 250;
            System.out.println("Rolled " + diceRoll);
            System.out.println("Pirate damage potential: " + pirateDamagePotential);
            System.out
                .println("Player damage potential: " + ship.getShipStats().getDamagePotential());
            float successChance = 1f;
            for (int i = 1; i < 7; i++) {
              if (ship.getShipStats().getDamagePotential() * i < pirateDamagePotential) {
                System.out.println("Rolling a " + i + " will fail.");
                System.out.println(
                    ship.getShipStats().getDamagePotential() * i + " < " + pirateDamagePotential);
                successChance -= (1f / 6f);
                System.out.println(successChance);
              } else {
                break;
              }
            }
            event.getChoices().get(0).setTooltip(String
                .format("Roll the dice, see what happens. %.2f%% chance of winning.",
                    Math.abs(successChance * 100)));
            //Queue a new event depending on the roll
            if (ship.getShipStats().getDamagePotential() * diceRoll >= pirateDamagePotential) {
              //Win!
              System.out.println("WIN!");
              event.getChoices().get(0).getActions().get(0)
                  .setData(null, badEventManager.getData(2001), this);
            } else {
              if (ship.getInventory().getWorth().getCents() >= new Currency(500, 00).getCents()) {
                //Robbed!
                System.out.println("ROBBED!");
                event.getChoices().get(0).getActions().get(0)
                    .setData(null, badEventManager.getData(2002), this);
              } else {
                //Death!
                System.out.println("DEATH!");
                event.getChoices().get(0).getActions().get(0)
                    .setData(null, badEventManager.getData(2003), this);
              }
            }
            return event;
          //Pirates Pt. 2: Win ID
          case 2001:

            return event;
          //Pirates Pt. 2: Robbed ID
          case 2002:
            for (Item item : ship.getInventory().getItems()) {
              if (item instanceof UpgradeItem) {
                //Makes sure not to remove applied upgrades
                if (((UpgradeItem) item).getAppliedLocation() == null) {
                  ship.getInventory().removeItem(item);
                }
              } else {
                ship.getInventory().removeItem(item);
              }
            }
            return event;
          //Pirates Pt. 2: Death ID
          case 2003:
            event.getChoices().get(0)
                .setActions(new ArrayList<ChoiceAction>(Arrays.asList(new EndGameAction(this))));
            return event;
          //Unfortunate weather ID
          case 2004:
            //Up to 75% damage of the current ship HP.
            int damageToTake = randomGenerator.nextInt(
                (int) (ship.getCurrentHitpoints() * 0.9f) + 1);
            //Only one choice, it's to brace and take damage.
            event.getChoices().get(0).setTooltip("Take " + damageToTake + " damage!");
            event.getChoices().get(0).getActions().get(0).setData(
                "hitpointsToChange", -damageToTake, ship
            );
            return event;
          default:
            break;
        }
        break;
      default:
        break;
    }
    return null;
  }

  //These functions are helper functions, for when values from the game
  //environment are parsed.

  /**
   * Helper function that sets the correct values on the way up to the GUI after the GUI calls it.
   * Buys an item from the current store.
   *
   * @param itemToBuy the item to buy
   * @return the item bought (can be ignored)
   */
  public Item buyItem(Item itemToBuy) {
    Item itemBought
        = getCurrentIsland().getStore().buyFromStore(itemToBuy, getTraderMoney(), soldLedger);
    if (itemBought != null) {
      setTraderMoney(getTraderMoney().subtract(itemToBuy.getPurchasedValue()));
      ship.getInventory().addItem(itemBought);
    }
    return itemBought;
  }

  /**
   * Helper function that sets the correct values on the way up to the GUI after the GUI calls it.
   * Sells an item to the current store.
   *
   * @param itemToSell the item to sell
   * @return the amount the item sold for (can be ignored)
   */
  public Currency sellItem(Item itemToSell) {
    Currency moneyMade
        = getCurrentIsland().getStore().sellToStore(itemToSell, ship.getInventory(), soldLedger);
    setTraderMoney(getTraderMoney().add(moneyMade));
    return moneyMade;
  }

  public int getCurrentDay() {
    return currentDay.get();
  }

  /**
   * Sets the current day of the adventure.
   *
   * @param day the new day
   */
  public void setCurrentDay(int day) {
    Action.run(() -> {
      currentDay.set(day);
    });
  }

  public String getTraderName() {
    return traderName;
  }

  public Map<Store, Map<Item, Currency>> getSoldLedger() {
    return soldLedger;
  }

  public Currency getTraderMoney() {
    return traderMoney.get();
  }

  /**
   * Sets the traders money to a new currency.
   *
   * @param currency the new amount of money
   */
  public void setTraderMoney(Currency currency) {
    Action.run(() -> {
      traderMoney.set(currency);
    });
  }

  public int getGameDuration() {
    return gameDuration;
  }

  public Ship getShip() {
    return ship;
  }

  public Island getTradersHomeIsland() {
    return tradersHomeIsland;
  }

  public Island getCurrentIsland() {
    return currentIsland.get();
  }

  /**
   * Sets the current island the trader's on.
   *
   * @param island the new island
   */
  public void setCurrentIsland(Island island) {
    Action.run(() -> {
      currentIsland.set(island);
    });
  }

  public ArrayList<ArrayList<Route>> getCurrentRoutes() {
    return currentRoutes;
  }

  public ArrayList<Island> getAllIslands() {
    return islands;
  }

  public JsonManager<ItemData> getItemManager() {
    return itemManager;
  }

  public JsonManager<Ship> getShipManager() {
    return shipManager;
  }

  public JsonManager<Event> getGoodEventManager() {
    return goodEventManager;
  }

  public JsonManager<Event> getNeutralEventManager() {
    return neutralEventManager;
  }

  public JsonManager<Event> getBadEventManager() {
    return badEventManager;
  }

  public JsonManager<Personality> getPersonalityManager() {
    return personalityManager;
  }

  public JsonManager<NameGeneratorStorage> getStoreNameManager() {
    return storeNameManager;
  }

  public JsonManager<Store> getStoreManager() {
    return storeManager;
  }

  public JsonManager<StringStorage> getIslandNameManager() {
    return islandNameManager;
  }

  public JsonManager<StringStorage> getIslandDescriptionManager() {
    return islandDescriptionManager;
  }

  public ScreenManager getScreenManager() {
    return screenManager;
  }

  public TextFormatterGui getTextFormatterGui() {
    return textFormatterGui;
  }

  public void setTextFormatterGui(TextFormatterGui textFormatterGui) {
    this.textFormatterGui = textFormatterGui;
  }

  /**
   * Calculates the cost to the player of sailing the given route. This includes the cost of fixing
   * any damage to the ship.
   *
   * @param route the route to sail
   * @return the amount it costs to sail this route
   */
  public Currency calculateSailingCost(Route route) {
    return ship.getWages()
        .multiply(route.calculateTravelTime())
        .add(ship.getDamageCost())
        ;
  }

  /**
   * Generates an image depicting the map of the islands to give a picture of their relative
   * locations.
   */
  private BufferedImage generateMap() {
    final int imageWidth = App.getScreenWidth();
    final int imageHeight = App.getScreenHeight() - 50;

    final BufferedImage bufferedImage = new BufferedImage(
        imageWidth,
        imageHeight,
        BufferedImage.TYPE_INT_ARGB
    );
    final Graphics2D g = bufferedImage.createGraphics();
    g.setColor(new Color(0, 197, 255));
    g.fillRect(0, 0, imageWidth, imageHeight);

    for (Island island : islands) {
      //Random green color
      g.setColor(new Color(18, randomGenerator.nextInt(122) + 100, 20));
      Polygon polygon;
      final int polygonPointCount = randomGenerator.nextInt(6) + 5;
      double angle = Math.PI * 2 / polygonPointCount;
      int radius = randomGenerator.nextInt(15) + 6;
      polygon = new Polygon();
      for (int i = 0; i < polygonPointCount; i++) {
        double target = i * angle;
        double newAngle = target + (randomGenerator.nextFloat() - 0.66f) * angle * 0.33;
        double x = Math.cos(newAngle) * radius;
        double y = Math.sin(newAngle) * radius;
        polygon.addPoint((int) x, (int) y);
      }
      polygon.translate(island.getXPosition(), island.getYPosition() - 30);
      g.fillPolygon(polygon);
    }
    g.dispose();
    return bufferedImage;
  }

  /**
   * Generates an image depicting the map of the islands (or uses cached one), and renders island
   * names on labels, highlighting the current island in red.
   *
   * @return BufferedImage of the map with added labels
   */
  public BufferedImage getMap() {
    if (islandMap == null) {
      islandMap = generateMap();
    }

    final Graphics2D g = islandMap.createGraphics();
    g.setFont(new Font(AppTextStyles.headingStyle.getFontFamily(), Font.PLAIN, 18));
    //Modifies the image to set the current island to be red text
    for (Island island : islands) {
      if (island == getCurrentIsland()) {
        g.setColor(Color.RED);
      } else {
        g.setColor(Color.BLACK);
      }
      g.drawString(island.getName(), island.getXPosition(), island.getYPosition());
    }
    g.dispose();
    return islandMap;
  }
}
