package nz.piromania.gameenvironment.eventsandchoices;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import nz.piromania.gameenvironment.eventsandchoices.choiceactions.ChoiceAction;
import nz.piromania.reactiveengine.reactivity.Action;
import nz.piromania.reactiveengine.reactivity.Observable;

/**
 * This class represents a <code>Choice</code> that will be shown to the user.
 * If the user picks this Choice, then all the <code>Action</code>s defined will be run.
 */
public class Choice {
  @SerializedName(value = "title")
  private final String choiceTitle;
  @SerializedName(value = "tooltip")
  private final Observable<String> choiceTooltip;
  @SerializedName(value = "actions")
  private ArrayList<ChoiceAction> choiceActions;
  
  /**
   * Creates a new choice with a title, tooltip, and a list of actions.
   *
   * @param title   brief description or summary of the choice, I.E. "Surrender" 
   * @param tooltip a more in depth detailed description of what will happen
   * @param actions a list of actions that will be run if this choice is picked
   */
  public Choice(String title, String tooltip, ArrayList<ChoiceAction> actions) {
    choiceTitle = title;
    choiceTooltip = new Observable<String>(tooltip);
    choiceActions = actions;
  }
  
  public String getTitle() {
    return choiceTitle;
  }
  
  public String getTooltip() {
    return choiceTooltip.get();
  }
  
  /**
   * Updates a choice with a new tooltip.
   *
   * @param newTooltip  the new tooltip to use
   */
  public void setTooltip(String newTooltip) {
    Action.run(() -> {
      choiceTooltip.set(newTooltip);
    });
  }
  
  public ArrayList<ChoiceAction> getActions() {
    return choiceActions;
  }
  
  public void setActions(ArrayList<ChoiceAction> choiceActions) {
    this.choiceActions = choiceActions;
  }
  
  /**
   * Should be run if the user picks this choice. Will run through all actions
   * executing them one by one.
   */
  public void pickChoice() {
    for (ChoiceAction action : choiceActions) {
      action.runAction();
    }
  }
  
  @Override
  public String toString() {
    String output = "Choice: \nTitle: " + choiceTitle
        + "\nTooltip: " + getTooltip();
    for (ChoiceAction action : choiceActions) {
      output += "\nAction:\n" + action.toString();
    }
    return output;
  }
}