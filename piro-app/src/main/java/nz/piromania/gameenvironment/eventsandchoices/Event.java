package nz.piromania.gameenvironment.eventsandchoices;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import nz.piromania.JsonLoadable;

/**
 * This class describes an <code>Event</code>. This will be shown to the user
 *     for them to make a decision on what to do next.
 * Note: conditional choices (I.E. if damage-potential &gt;= 80)
 *     should not be done here, and instead before when deciding
 *     the choices to include in the <code>Event</code>.
 *
 * @see Choice
 */
public class Event implements JsonLoadable {
  @SerializedName(value = "id")
  private final int eventId;
  @SerializedName(value = "title")
  private final String eventTitle;
  @SerializedName(value = "description")
  private final String eventDescription;
  @SerializedName(value = "image")
  private final String eventImagePath;
  @SerializedName(value = "choices")
  private final ArrayList<Choice> eventChoices;
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void initialize() {}
  
  /**
   * Defines a new Event without an image.
   *
   * @param id          the event's id
   * @param title       the event's name
   * @param description the event's description
   * @param choices     the choices the user can pick from
   */
  public Event(int id, String title, String description, ArrayList<Choice> choices) {
    eventId = id;
    eventTitle = title;
    eventDescription = description;
    eventImagePath = "";
    eventChoices = choices;
  }
  
  /**
   * Defines a new event with an image.
   *
   * @param id          the event's id
   * @param title       the event's name
   * @param description the event's description
   * @param imagePath   the path to the image displayed
   * @param choices     the choices the user can pick from
   */
  public Event(int id, String title, String description, 
      String imagePath, ArrayList<Choice> choices) {
    eventId = id;
    eventTitle = title;
    eventDescription = description;
    eventImagePath = imagePath;
    eventChoices = choices;
  }
  
  @Override
  public int getId() {
    return eventId;
  }
  
  public String getTitle() {
    return eventTitle;
  }
  
  public String getDescription() {
    return eventDescription;
  }
  
  public String getImagePath() {
    return eventImagePath;
  }
  
  public ArrayList<Choice> getChoices() {
    return eventChoices;
  }
  
  @Override
  public String toString() {
    String output = "ID: " + eventId
        + "\nTitle: " + eventTitle
        + "\nDescription: " + eventDescription
        + "\nImage Path: " + eventImagePath
        + "\nChoices:\n";
    for (Choice choice : eventChoices) {
      output += choice.toString();
    }
    return output;
  }
}
