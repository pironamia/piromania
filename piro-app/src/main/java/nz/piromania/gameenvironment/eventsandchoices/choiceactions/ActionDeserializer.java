package nz.piromania.gameenvironment.eventsandchoices.choiceactions;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import java.lang.reflect.Type;

/**
 * This class is used to deserialize interfaces, specifically ChoiceActions.
 * Code inspired by: https://technology.finra.org/code/serialize-deserialize-interfaces-in-java.html
 */
public class ActionDeserializer implements JsonDeserializer<ChoiceAction> {  
  
  @Override
  public ChoiceAction deserialize(JsonElement json, Type typeOfT, 
      JsonDeserializationContext context) throws JsonParseException {
    JsonObject actionObject = json.getAsJsonObject();
    JsonPrimitive actionPrimitive = (JsonPrimitive) actionObject.get("action-type");
    String className = actionPrimitive.getAsString();
    Class<?> actionClass;
    try {
      actionClass = Class.forName(className);
    } catch (ClassNotFoundException e) {
      throw new JsonParseException("Error parsing JSON: Class not found.\n" + e);
    }
    return context.deserialize(actionObject.get("DATA"), actionClass);
  }
}