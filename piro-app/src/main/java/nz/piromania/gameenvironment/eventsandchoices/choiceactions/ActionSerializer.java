package nz.piromania.gameenvironment.eventsandchoices.choiceactions;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;

/**
 * This class is used to serialize interfaces, specifically actions.
 * Code inspired by: https://technology.finra.org/code/serialize-deserialize-interfaces-in-java.html
 */
public class ActionSerializer implements JsonSerializer<ChoiceAction> {

  @Override
  public JsonElement serialize(ChoiceAction action, Type typeOfSrc, 
      JsonSerializationContext context) {
    JsonObject actionObject = new JsonObject();
    actionObject.addProperty("action-type", action.getClass().getName());
    actionObject.add("DATA", context.serialize(action));
    return actionObject;
  }
}