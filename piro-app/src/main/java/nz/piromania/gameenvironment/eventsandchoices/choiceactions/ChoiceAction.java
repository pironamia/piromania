package nz.piromania.gameenvironment.eventsandchoices.choiceactions;

/**
 * This ChoiceAction interface is used to define all ChoiceActions.
 * ChoiceActions may do whatever they'd like, 
 *     but must be able to run with .runAction().
 * Similarly, if the data of the action can be overwritten,
 *     then that should be done so in setData().
 * Also, if the action requires another object to perform its
 *     function, then that can be retrieved with dataTarget.
 */

public interface ChoiceAction<T> {
  
  /**
   * Runs the action.
   */
  T runAction();
  
  /**
   * Sets the data of an action.
   *
   * @param variableToChange    handy String implementation for setData to use
   * @param dataToSet           the new data
   * @param dataTarget          in case the data needs to be applied to a target
   */
  void setData(String variableToChange, T dataToSet, T dataTarget);
  
  String toString();
}
