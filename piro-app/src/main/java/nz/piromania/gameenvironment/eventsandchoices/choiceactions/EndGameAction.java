package nz.piromania.gameenvironment.eventsandchoices.choiceactions;

import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.ui.Screen;

/**
 * Action that triggers the end of the game.
 */
public class EndGameAction<T> implements ChoiceAction {

  private GameEnvironment gameEnvironment;

  /**
   * Passes a game environment to this class for the class to be
   * able to call .show() later.
   *
   * @param gameEnvironment the running game environment
   */
  public EndGameAction(GameEnvironment gameEnvironment) {
    this.gameEnvironment = gameEnvironment;
  }

  @Override
  public T runAction() {
    gameEnvironment.getScreenManager().show(Screen.GAME_OVER);
    return null;
  }

  @Override
  public void setData(String variableToChange, Object dataToSet, Object dataTarget) {
    gameEnvironment = (GameEnvironment) dataTarget;
  }
}
