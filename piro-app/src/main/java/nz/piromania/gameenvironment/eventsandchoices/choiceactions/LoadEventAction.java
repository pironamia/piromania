package nz.piromania.gameenvironment.eventsandchoices.choiceactions;

import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.gameenvironment.eventsandchoices.Event;

/**
 * This choice action represents loading another action.
 * This is used for sequential events. For now, hard coded to
 * return bad events. Can be changed to extend to any event type,
 * or rather 'SEQ' in future.
 */
public class LoadEventAction<T> implements ChoiceAction {
  private GameEnvironment gameEnvironment;
  private Event eventToRun;

  @Override
  public T runAction() {
    gameEnvironment.runEvent(eventToRun, "BAD");
    return (T) eventToRun;
  }

  @Override
  public void setData(String variableToChange, Object dataToSet, Object dataTarget) {
    gameEnvironment = (GameEnvironment) dataTarget;
    eventToRun = (Event) dataToSet;
  }

}
