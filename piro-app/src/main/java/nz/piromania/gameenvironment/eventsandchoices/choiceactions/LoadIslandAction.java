package nz.piromania.gameenvironment.eventsandchoices.choiceactions;

import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.ui.Screen;

/**
 * This choice action loads an island. This is handy for ending an
 * event chain, or just ending an event in general. All it does is
 * switch screens.
 */
public class LoadIslandAction<T> implements ChoiceAction {
  private GameEnvironment gameEnvironment;
  
  /**
   * Passes a game environment to this class for the class to be
   * able to call .show() later.
   *
   * @param gameEnvironment the running game environment
   */
  public LoadIslandAction(GameEnvironment gameEnvironment) {
    this.gameEnvironment = gameEnvironment;
  }
  
  @Override
  public T runAction() {
    gameEnvironment.getScreenManager().show(Screen.ISLAND_OVERVIEW);
    return null;
  }

  @Override
  public void setData(String variableToChange, Object dataToSet, Object dataTarget) {
    gameEnvironment = (GameEnvironment) dataTarget;
  }
}
