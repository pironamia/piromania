package nz.piromania.gameenvironment.eventsandchoices.choiceactions;

import nz.piromania.gameenvironment.ships.Ship;

/**
 * This action is used to modify the ship in any way.
 * Add any new ways to modify the ship here when necessary.
 */
public class ModifyShipAction<T> implements ChoiceAction {
  private Ship ship;
  private int hitpointsToChange;
  
  /**
   * Creates a new ModifyShipAction, which currently only
   * changes the hitpoints of a ship.
   *
   * @param hitpoints the hitpoints to change
   */
  public ModifyShipAction(int hitpoints) {
    hitpointsToChange = hitpoints;
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public T runAction() {
    if (ship.getCurrentHitpoints() == ship.getShipStats().getMaxHitpoints()) {
      ship.changeHp(hitpointsToChange);
    }
    return null;
  }
  
  @Override
  public String toString() {
    return "ModifyShipAction:"
        +  "\nModifies a ship"
        +  "\nTargetted ship:\n" + ship
        +  "\nHitpoints to change: " + hitpointsToChange;
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void setData(String variableToChange, Object dataToSet, Object dataTarget) {
    ship = (Ship) dataTarget;
    if (variableToChange == "hitpointsToChange") {
      hitpointsToChange = (int) dataToSet;
    }
  }
}
