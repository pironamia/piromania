package nz.piromania.gameenvironment.islands;

import java.util.ArrayList;
import java.util.Random;
import nz.piromania.App;
import nz.piromania.gameenvironment.routes.Route;
import nz.piromania.gameenvironment.ships.Ship;
import nz.piromania.gameenvironment.stores.Store;

/**
 * The islands class is an almost purely aesthetic one. It's used for storing an Island, which has a
 * name, description, image, store, coordinates, and list of routes to all other islands. Most of
 * this class should be randomly generated.
 */
@SuppressWarnings("checkstyle:AbbreviationAsWordInName")
public class Island {

  private static final Random randomGenerator = new Random();
  private final String name;
  private final String description;
  private final String imagePath;
  private final Store store;
  //int between -width and width
  //int between 100 and width - 100
  private int islandXPosition = randomGenerator.nextInt(
      App.getScreenWidth() - 300) + 100;
  //int between 100 and height - 100
  //int between -height and height
  private int islandYPosition = randomGenerator.nextInt(
      App.getScreenHeight() - 200) + 100;

  /**
   * Creates an island and randomly generates coordinates for it.
   *
   * @param name         the island's name
   * @param description  the island's description
   * @param imagePath    the image to represent the island
   * @param store        the island's attached store
   * @param otherIslands other islands (used for generating coordinates)
   */
  public Island(String name, String description, String imagePath,
      Store store, ArrayList<Island> otherIslands) {
    this.name = name;
    this.description = description;
    this.imagePath = imagePath;
    this.store = store;
    for (int i = 0; i < otherIslands.size(); i++) {
      double distance = Math.sqrt(
          (otherIslands.get(i).getXPosition() - islandXPosition)
              * (otherIslands.get(i).getXPosition() - islandXPosition)
              + (otherIslands.get(i).getYPosition() - islandYPosition)
              * (otherIslands.get(i).getYPosition() - islandYPosition)
      );
      //Arbitrary distance val.
      if (distance <= 200) {
        //Try a new position
        islandXPosition = randomGenerator.nextInt(
            App.getScreenWidth() - 200) + 100;
        islandYPosition = randomGenerator.nextInt(
            App.getScreenHeight() - 200) + 100;
        i = -1;
        continue;
      }
    }
  }

  /**
   * Generates an island with a specified (X, Y) coordinate. NOTE: This will NOT check against
   * distance of other islands.
   *
   * @param name        the island's name
   * @param description the island's description
   * @param imagePath   the image to represent the island
   * @param store       the island's attached store
   * @param coordX      the island's X coordinates
   * @param coordY      the island's Y coordinates
   */
  public Island(String name, String description, String imagePath,
      Store store, int coordX, int coordY) {
    this.name = name;
    this.description = description;
    this.imagePath = imagePath;
    this.store = store;
    islandXPosition = coordX;
    islandYPosition = coordY;
  }

  /**
   * Generates routes from this island to a specified island. Will ALWAYS guarantee at least one
   * route.
   *
   * @param otherIsland         the island to generate routes to
   * @param ship                the player's ship
   * @param maxRoutesToGenerate the amount of routes to generate (must be &gt; 0)
   * @return an arraylist of generated routes
   */
  public ArrayList<Route> generateRoutes(Island otherIsland, Ship ship, int maxRoutesToGenerate) {
    ArrayList<Route> output = new ArrayList<Route>();
    //Guarantees at least one route
    for (int i = -1; i < randomGenerator.nextInt(maxRoutesToGenerate); i++) {
      output.add(new Route(this, otherIsland, ship));
    }
    return output;
  }

  /**
   * Gets the distance from a point specified by (x, y).
   *
   * @param x the x coordinate of the point
   * @param y the y coordinate of the point
   * @return the distance from the island to the point
   */
  public double calculateDistance(int x, int y) {
    return Math.sqrt(
        (x - islandXPosition)
            * (x - islandXPosition)
            + (y - islandYPosition)
            * (y - islandYPosition)
    );
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public String getImagePath() {
    return imagePath;
  }

  public Store getStore() {
    return store;
  }

  public int getXPosition() {
    return islandXPosition;
  }

  public int getYPosition() {
    return islandYPosition;
  }

  @Override
  public String toString() {
    return "Island:"
        + "\nIsland name: " + name
        + "\nIsland description: " + description
        + "\nIsland image path: " + imagePath
        + "\nIsland coordinates: (" + islandXPosition + ", " + islandYPosition + ")";
  }
}
