package nz.piromania.gameenvironment.items;

import com.google.gson.annotations.SerializedName;
import nz.piromania.Currency;
import nz.piromania.reactiveengine.reactivity.Action;
import nz.piromania.reactiveengine.reactivity.Observable;

/**
 * This class inherits static data from <code>ItemData</code>, and adds on the values of how much
 * the <code>Item</code> was bought for, and how much it was sold for. (Defaults 0)
 */

public class Item extends ItemData {
  //Note: JSON for inventories can leak into this class.
  @SerializedName(value = "purchased-value")
  //Caution: not final due to JSON.
  private Observable<Currency> purchasedFor = new Observable<Currency>(new Currency(0));
  @SerializedName(value = "sold-value")
  private Observable<Currency> soldFor = new Observable<Currency>(new Currency(0));
  
  /**
   * {@inheritDoc}
   */
  public void initialize() {
    if (purchasedFor == null) {
      purchasedFor = new Observable<Currency>(new Currency(0));
    }
    if (soldFor == null) {
      soldFor = new Observable<Currency>(new Currency(0));
    }
  }
  
  /**
   * Calling this constructor sets all the data for the item,
   *     and the leaves the purchase amount as default.
   *
   * @param id          the unique id the item will have
   * @param name        the name of the item
   * @param description the description of the item
   * @param baseValue   the base value of the item,
   *     before any modifiers are added by shops
   * @param type        the type of item it is,
   *     used for determining what modifiers should be applied by shops
   */
  public Item(int id, String name, String description, String imagePath,
      Currency baseValue, ItemType type, int itemSize) {
    super(id, name, description, imagePath, baseValue, type, itemSize);
  }
  
  /**
   * Calling this constructor sets all the data for the item,
   *     including the amount it was purchased for.
   *
   * @param id              the unique id the item will have
   * @param name            the name of the item
   * @param description     the description of the item
   * @param baseValue       the base value of the item,
   *     before any modifiers are added by shops
   * @param type            the type of item it is,
   *     used for determining what modifiers should be applied by shops
   * @param purchasedValue  the amount the item was purchased for
   */
  public Item(int id, String name, String description, String imagePath,
      Currency baseValue, ItemType type, int itemSize, Currency purchasedValue) {
    super(id, name, description, imagePath, baseValue, type, itemSize);
    setPurchasedValue(purchasedValue);
  }
  
  public Currency getPurchasedValue() {
    return purchasedFor.get();
  }
  
  public Currency getSoldValue() {
    return soldFor.get();
  }

  /**
   * Sets the purchased value of the item.
   *
   * @param purchasedValue  the new purchased value
   */
  public void setPurchasedValue(Currency purchasedValue) {
    Action.run(() -> {
      purchasedFor.set(purchasedValue);
    });
  }
  
  /**
   * Sets the sold value of the item.
   *
   * @param soldValue   the new sold value
   */
  public void setSoldValue(Currency soldValue) {
    Action.run(() -> {
      soldFor.set(soldValue);
    });
  }

  /**
   * Returns a Gui-friendly summary text for this item.
   */
  public String getGuiSummary() {
    final StringBuilder builder = new StringBuilder();

    builder.append(this.getItemName());
    builder.append("\n");

    builder.append(this.getItemDescription());
    builder.append("\n\n");
    builder.append(String.format(
        "Size: %d",
        getItemSize()
    ));
    builder.append("\n");
    builder.append(String.format(
        "Base value: %s",
        getItemBaseValue()
    ));

    return builder.toString();
  }

  @Override
  public String toString() {
    if (getSoldValue().equals(new Currency(0))) {
      return super.toString() + "\nPurchased for: " + getPurchasedValue().toString();
    }
    return super.toString() + "\nPurchased for: " + getPurchasedValue().toString()
        + "\nSold for: " + getSoldValue().toString();
  }
}
