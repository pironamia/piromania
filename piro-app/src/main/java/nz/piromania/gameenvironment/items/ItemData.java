package nz.piromania.gameenvironment.items;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import nz.piromania.Currency;
import nz.piromania.JsonLoadable;
import nz.piromania.Stats;

/**
 * This class is used for storing immutable information about <code>Item</code>s.
 * Used to instantiate <code>Item</code>s and <code>UpgradeItem</code>s with predefined data.
 *
 * @see Item
 * @see UpgradeItem
 */

public class ItemData implements JsonLoadable {
  @SerializedName(value = "id")
  private final int itemId;
  @SerializedName(value = "name")
  private final String itemName;
  @SerializedName(value = "description")
  private final String itemDescription;
  @SerializedName(value = "image-path")
  private final String itemImagePath;
  @SerializedName(value = "base-value")
  private final Currency itemBaseValue;
  @SerializedName(value = "type")
  private final ItemType itemType;
  @SerializedName(value = "size")
  private final int itemSize;
  @SerializedName(value = "upgrade-types")
  private final ArrayList<UpgradeType> initialUpgradeTypes;
  @SerializedName(value = "upgrade-stats")
  private final Stats initialUpgradeStats;
  @SerializedName(value = "additive-multipliers")
  private final boolean additive;
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void initialize() {}
  
  /**
   * Initializes an <code>ItemData</code>.
   *     Calling this constructor is equivalent to constructing an <code>Item</code>. 
   *
   * @param id                  the unique id the item will have
   * @param name                the name of the item
   * @param description         the description of the item
   * @param imagePath           the image path of this item
   * @param baseValue           the base value of the item, before any modifiers are added by shops
   * @param type                the type of item it is,
   *     used for determining what modifiers should be applied by shops
   * @param size                the amount of cargo space this item takes up
   */
  public ItemData(int id, String name, String description, String imagePath,
      Currency baseValue, ItemType type, int size) {
    itemId = id;
    itemName = name;
    itemDescription = description;
    itemImagePath = imagePath;
    itemBaseValue = baseValue;
    itemType = type;
    itemSize = size;
    initialUpgradeTypes = new ArrayList<>();
    initialUpgradeStats = new Stats(0, 0, 0, 0, 0);
    additive = true;
  }
  
  /**
   * Initializes an <code>ItemData</code>.
   *     Calling this constructor is equivalent to constructing an <code>UpgradeItem</code>. 
   *
   * @param id                  the unique id the item will have
   * @param name                the name of the item
   * @param description         the description of the item
   * @param imagePath           the image path of this item
   * @param baseValue           the base value of the item, before any modifiers are added by shops
   * @param type                the type of item it is,
   *     used for determining what modifiers should be applied by shops
   * @param size                the amount of cargo space this item takes up
   * @param upgradeTypes        the initial upgrade types of the item defined in the Items.json file
   *     only used by upgrades
   * @param upgradeStats        the initial upgrade stats of the item defined in the Items.json file
   *     only used by upgrades
   * @param additive     whether or not multipliers should be additive or multiplicative
   *     true for additive, false for multiplicative
   */
  public ItemData(int id, String name, String description, String imagePath,
      Currency baseValue, ItemType type, int size, 
      ArrayList<UpgradeType> upgradeTypes, Stats upgradeStats, boolean additive) {
    itemId = id;
    itemName = name;
    itemDescription = description;
    itemImagePath = imagePath;
    itemBaseValue = baseValue;
    itemType = type;
    itemSize = size;
    initialUpgradeTypes = upgradeTypes;
    initialUpgradeStats = upgradeStats;
    this.additive = additive;
  }
  
  @Override
  public int getId() {
    return itemId;
  }
  
  public String getItemName() {
    return itemName;
  }
  
  public String getItemDescription() {
    return itemDescription;
  }
  
  public String getImagePath() {
    return itemImagePath;
  }
  
  public Currency getItemBaseValue() {
    return itemBaseValue; 
  }
  
  public ItemType getItemType() {
    return itemType;
  }  
  
  public int getItemSize() {
    return itemSize;
  }
  
  /**
   * Creates an instance of the <code>Item</code> to use, without modifying the base data.
   *
   * @return    new <code>Item</code> instance, ready for modifying or storing 
   */
  public Item createItemInstance() {
    return new Item(itemId, itemName, itemDescription, itemImagePath,
        itemBaseValue, itemType, itemSize);
  }
  
  /**
   * Creates an instance of the <code>Item</code> to use, without modifying the base data.
   * Also parses in a monetary value to instantly set how much the item was bought for.
   *
   * @return    new <code>Item</code> instance, ready for modifying or storing 
   */
  public Item createItemInstance(Currency purchasedValue) {
    return new Item(itemId, itemName, itemDescription, itemImagePath, 
        itemBaseValue, itemType, itemSize, purchasedValue);
  }
  
  /**
   * Creates an instance of the <code>UpgradeItem</code> to use, without modifying the base data.
   *
   * @return    new <code>UpgradeItem</code> instance, ready for modifying or storing 
   */
  public UpgradeItem createUpgradeInstance() {
    return new UpgradeItem(itemId, itemName, itemDescription, itemImagePath,
        itemBaseValue, itemType, itemSize, 
        initialUpgradeTypes, initialUpgradeStats, additive);
  }
  
  /**
   * Creates an instance of the <code>UpgradeItem</code> to use, without modifying the base data.
   * Also parses in a monetary value to instantly set how much the item was bought for.
   *
   * @param purchasedValue  the value the <code>UpgradeItem</code> was purchased for
   * @return                new <code>UpgradeItem</code> instance, ready for modifying or storing 
   */
  public UpgradeItem createUpgradeInstance(Currency purchasedValue) {
    return new UpgradeItem(itemId, itemName, itemDescription, itemImagePath,
        itemBaseValue, itemType, itemSize, purchasedValue, 
        initialUpgradeTypes, initialUpgradeStats, additive);
  }

  @Override
  public String toString() {
    String output = "ID: " + itemId
                  + "\nName: " + itemName
                  + "\nDescription: " + itemDescription
                  + "\nImage path: " + itemImagePath
                  + "\nBase Value: " + itemBaseValue
                  + "\nType: " + itemType
                  + "\nSize: " + itemSize;
    if (initialUpgradeTypes != null && !initialUpgradeTypes.isEmpty()) {
      output += "\nUpgrade Types:\n";
      for (UpgradeType type : initialUpgradeTypes) {
        output += type + "\n";
      }
      output += "Default upgrade stats:" + initialUpgradeStats.toString()
             +  "\nAdditive multipliers? " + additive;
    }
    return output;
  }
}
