package nz.piromania.gameenvironment.items;

/**
 * This contains all of the types an item can be.
 * {@link #FOOD}
 * {@link #DRINK}
 * {@link #GEM}
 * {@link #UPGRADE}
 */

public enum ItemType {
  //MUST BE ALPHABETICAL FOR SORTING
  /**
   * Drink item.
   */
  DRINK, 
  /**
   * Food item.
   */
  FOOD, 
  /**
   * Gem item.
   */
  GEM, 
  /**
   * Upgrade item.
   */
  UPGRADE
}
