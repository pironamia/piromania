package nz.piromania.gameenvironment.items;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.stream.Collectors;
import nz.piromania.Currency;
import nz.piromania.Stats;
import nz.piromania.reactiveengine.reactivity.Action;
import nz.piromania.reactiveengine.reactivity.Observable;

/**
 * This class is only used for specific types of items called upgrades.
 * These upgrades can be of a certain type,
 *     and will affect the player's <code>Ship</code> in various ways.
 *
 * @see UpgradeType
 * @see Stats
 */

public class UpgradeItem extends Item {
  @SerializedName(value = "upgrade-stats")
  private final Observable<Stats> upgradeStats = new Observable<Stats>(new Stats(0, 0, 0, 0, 0));
  @SerializedName(value = "upgrade-types")
  private final Observable<ArrayList<UpgradeType>> upgradeTypes = 
      new Observable<ArrayList<UpgradeType>>(new ArrayList<>());
  @SerializedName(value = "additive-multipliers")
  private final boolean additive;
  private transient UpgradeType appliedLocation;
  
  /**
   * Calling this constructor sets all the data for the upgrade item,
   *     the amount it was purchased for is 0.
   *
   * @param id              the unique id the item will have
   * @param name            the name of the item
   * @param description     the description of the item
   * @param baseValue       the base value of the item,
   *     before any modifiers are added by shops
   * @param type            the type of item it is,
   *     used for determining what modifiers should be applied by shops
   * @param itemSize        the amount of cargo space this item takes up
   * @param upgradeTypes    what components of the ship the upgrade modifies,
   *     (I.E.: a <code>HULL</code> upgrade can only go in a <code>HULL</code> slot on a ship)
   * @param upgradeStats    the modifiers the <code>UpgradeItem</code> applies. see UpgradeStats
   * @param additive        whether the upgradeStats should use addition for their multipliers,
   *     if true, multipliers will be added, if false, multiplied
   */
  public UpgradeItem(int id, String name, String description, String imagePath, Currency baseValue, 
      ItemType type, int itemSize, 
      ArrayList<UpgradeType> upgradeTypes, Stats upgradeStats, boolean additive) {
    super(id, name, description, imagePath, baseValue, type, itemSize);
    Action.run(() -> {
      this.upgradeStats.set(upgradeStats);
      this.upgradeTypes.set(upgradeTypes);
    });
    this.additive = additive;
  }
  
  /**
   * Calling this constructor sets all the data for the upgrade item.
   *
   * @param id              the unique id the item will have
   * @param name            the name of the item
   * @param description     the description of the item
   * @param baseValue       the base value of the item,
   *     before any modifiers are added by shops
   * @param type            the type of item it is,
   *     used for determining what modifiers should be applied by shops
   * @param itemSize        the amount of cargo space this item takes up
   * @param purchasedValue  the amount the item was purchased for
   * @param upgradeTypes    what components of the ship the upgrade modifies,
   *     (I.E.: a <code>HULL</code> upgrade can only go in a <code>HULL</code> slot on a ship)
   * @param upgradeStats    the modifiers the <code>UpgradeItem</code> applies. see UpgradeStats
   * @param additive     whether or not multipliers should be additive or multiplicative
   *     true for additive, false for multiplicative
   */
  public UpgradeItem(int id, String name, String description, String imagePath,
      Currency baseValue, ItemType type, int itemSize, Currency purchasedValue, 
      ArrayList<UpgradeType> upgradeTypes, Stats upgradeStats, boolean additive) {
    super(id, name, description, imagePath, baseValue, type, itemSize, purchasedValue);
    Action.run(() -> {
      this.upgradeStats.set(upgradeStats);
      this.upgradeTypes.set(upgradeTypes);
    });
    this.additive = additive;
  }
  
  public Stats getUpgradeStats() {
    return upgradeStats.get();
  }
  
  public ArrayList<UpgradeType> getUpgradeTypes() {
    return upgradeTypes.get();
  }
  
  public boolean getAdditive() {
    return additive;
  }
  
  public UpgradeType getAppliedLocation() {
    return appliedLocation;
  }
  
  public void setAppliedLocation(UpgradeType appliedLocation) {
    this.appliedLocation = appliedLocation;
  }
  
  /**
   * Sets the Stats of this Upgrade.
   *
   * @param newUpgradeStats the new Stats
   */
  public void setUpgradeStats(Stats newUpgradeStats) {
    Action.run(() -> {
      upgradeStats.set(newUpgradeStats);
    });
  }
  
  /**
   * Used to add a new set of <code>Stats</code> to the existing <code>Stats</code>.
   *
   * @param statsToAdd   the stats to add to the current existing stats
   * @param additive     whether the multipliers should be additive or multiplicative
   *     see Stats.java
   */
  public void changeUpgradeStats(Stats statsToAdd, boolean additive) {
    getUpgradeStats().changeStats(statsToAdd, additive);
  }
  
  /**
   * Sets the UpgradeTypes of this Upgrade to a new list of UpgradeTypes.
   *
   * @param newUpgradeTypes the new list of UpgradeTypes for this Upgrade
   */
  public void setUpgradeTypes(ArrayList<UpgradeType> newUpgradeTypes) {
    Action.run(() -> {
      upgradeTypes.set(newUpgradeTypes);
    });
  }

  @Override
  public String getGuiSummary() {
    final StringBuilder builder = new StringBuilder(super.getGuiSummary());

    builder.insert(0, "[UPGRADE] ");

    builder.append("\n");
    builder.append(String.format(
        "Upgrade type(s): %s",
        getUpgradeTypes().stream().<String>map(
            (upgradeType -> upgradeType.name().toLowerCase())
        ).collect(Collectors.joining(", "))
    ));

    builder.append("\n");
    builder.append(getUpgradeStats().toString());

    return builder.toString();
  }

  @Override
  public String toString() {
    String output = super.toString() + getUpgradeStats().toString()
        + "\nAdditive? " + additive
        + "\nTypes:";
    for (UpgradeType upgradeType : getUpgradeTypes()) {
      output += "\n" + upgradeType.toString();
    }
    return output;
  }
}
