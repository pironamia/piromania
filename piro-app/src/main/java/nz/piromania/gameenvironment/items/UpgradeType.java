package nz.piromania.gameenvironment.items;

/**
 * This contains all of the types an upgrade can be.
 * {@link #HULL}
 * {@link #STEERING}
 * {@link #WEAPONRY}
 * {@link #CARGO}
 * {@link #SAIL}
 */

public enum UpgradeType {
  /**
   * Hull upgrade.
   */
  HULL, 
  /**
   * Steering upgrade.
   */
  STEERING, 
  /**
   * Weaponry upgrade.
   */
  WEAPONRY, 
  /**
   * Cargo upgrade.
   */
  CARGO, 
  /**
   * Sail upgrade.
   */
  SAIL
}
