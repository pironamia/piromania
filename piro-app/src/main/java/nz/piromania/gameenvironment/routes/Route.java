package nz.piromania.gameenvironment.routes;

import java.util.Random;
import nz.piromania.gameenvironment.islands.Island;
import nz.piromania.gameenvironment.ships.Ship;

/**
 * This class is used to store routes between two islands.
 * It can be used to get the travel time between two islands,
 * and has a random risk assigned to it.
 * Upon starting the game (typically), risks will be around 50%.
 * Play style will impact these risks significantly.
 */
public class Route {
  private static final Random randomGenerator = new Random();
  private final Island startIsland;
  private final Island destinationIsland;
  private final double trueDistance;
  private double distance;
  private final float baseRouteRisk;
  private final Ship ship;
  
  /**
   * Generate a new route between two islands.
   *
   * @param startIsland         the start of the route
   * @param destinationIsland   the destination of the route
   * @param ship                the player's ship
   */
  public Route(Island startIsland, Island destinationIsland, Ship ship) {
    this.startIsland = startIsland;
    this.destinationIsland = destinationIsland;
    trueDistance = Math.sqrt(
        (destinationIsland.getXPosition() - startIsland.getXPosition()) 
        * (destinationIsland.getXPosition() - startIsland.getXPosition())
        + (destinationIsland.getYPosition() - startIsland.getYPosition()) 
        * (destinationIsland.getYPosition() - startIsland.getYPosition())
    );
    baseRouteRisk = randomGenerator.nextFloat();
    this.ship = ship;
  }
  
  /**
   * Calculates how long it takes to use this route.
   * This calculation is performed as such:
   * The distance between the two islands DIVIDED by the ship's travel speed
   * PLUS 3/4 MULTIPLIED by the inverse of the base route risk, ROUNDED UP.
   * Or: (distance / travel speed) + (0.75 / base risk)
   * This creates an effect where inherently more risky routes take
   * a shorter amount of time, which is usually true.
   *
   * @return    the travel time of this route
   */
  public int calculateTravelTime() {
    int travelTime = (int) Math.ceil((trueDistance / ship.getTravelSpeed()) 
        + (0.2f / (baseRouteRisk + 0.0001)));
    //Cap it to 15 days AT MOST.
    if (travelTime >= 10) {
      travelTime = 10;
      distance = trueDistance 
          + (ship.getTravelSpeed() * 10 
              + randomGenerator.nextInt((int) ship.getTravelSpeed() * 2) - ship.getTravelSpeed());
    } else {
      distance = ship.getTravelSpeed() * ((trueDistance / ship.getTravelSpeed()) 
          + (0.2f / (baseRouteRisk + 0.0001)));
    }
    return travelTime;
  }
  
  /**
   * Calculates the risk of the route by looking at the player's
   * ship. This calculation is performed as such:
   * The base risk is a number from 0 - 1.
   * Then, (250 - The ship's damage potential) DIVIDED by 500 is added
   * to the risk.
   * NOTE: 250 is the default starting value of an average ship.
   * Afterwards, the risk is MULTIPLIED by the ship's risk multiplier,
   * and capped up to 0, down to 1 if need be.
   *
   * @return    the risk of this route
   */
  public float calculateRisk() {
    //500 is arbitrary. Graph this to see roughly what value we expect.
    float routeRisk = baseRouteRisk;
    routeRisk += (250 - ship.getShipStats().getDamagePotential()) / 500;
    routeRisk *= ship.getShipStats().getRiskMultiplier();
    //Risk being negative is meaningless effectively here, so just cap it.
    if (routeRisk <= 0) {
      routeRisk = 0.01f;
    } else if (routeRisk > 1) {
      routeRisk = 1;
    }
    return routeRisk;
  }
  
  public Island getStart() {
    return startIsland;
  }
  
  public Island getDestination() {
    return destinationIsland;
  }
  
  public double getDistance() {
    return distance;
  }
  
  @Override
  public String toString() {
    return "Route:"
        +  "\nStarting island:\n" + startIsland.toString()
        +  "\nDestination island:\n" + destinationIsland.toString()
        +  "\nDistance: " + distance
        +  "\nBase risk: " + baseRouteRisk
        +  "\nRoute travel time: " + calculateTravelTime() + " days"
        +  "\nRoute risk: " + calculateRisk();
  }
}
