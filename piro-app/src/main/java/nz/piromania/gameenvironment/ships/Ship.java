package nz.piromania.gameenvironment.ships;

import com.google.gson.annotations.SerializedName;
import java.util.Map;
import nz.piromania.Currency;
import nz.piromania.Inventory;
import nz.piromania.JsonLoadable;
import nz.piromania.Stats;
import nz.piromania.gameenvironment.items.UpgradeItem;
import nz.piromania.gameenvironment.items.UpgradeType;
import nz.piromania.reactiveengine.reactivity.Action;
import nz.piromania.reactiveengine.reactivity.Observable;
import nz.piromania.reactiveengine.reactivity.collections.ObservableMap;

/**
 * This class ties together the essence of a <code>Ship</code>.
 * This class stores and tracks:
 * <ul>
 *   <li>The name of the ship</li>
 *   <li>The description of the ship</li>
 *   <li>The image path to the ship</li>
 *   <li>The amount of crew aboard a ship</li>
 *   <li>The wages the ship takes to run</li>
 *   <li>The inventory of the ship</li>
 *   <li>The statistics of the ship</li>
 *   <li>The current hitpoints</li>
 *   <li>The base travel speed of a ship</li>
 *   <li>The current travel speed of a ship</li>
 *   <li>The upgrade slots of a ship</li>
 * </ul>
 * The amount of crew can be changed (and by extension, wages).
 * The inventory of the ship can be changed.
 * The statistics of the ship can be changed.
 * The hitpoints of the ship can be changed.
 * The travel speed of the ship can be changed.
 * The base travel speed can NOT (and should not) be changed.
 * The name, description, and image path of the ship can NOT be changed.
 * 
 * <p>Ship types should extend this class.</p>
 *
 * @see Stats
 */
public class Ship implements JsonLoadable {
  @SerializedName(value = "id")
  private final int id;
  @SerializedName(value = "name")
  private final String name;
  @SerializedName(value = "description")
  private final String description;
  @SerializedName(value = "image")
  private final String imagePath;
  @SerializedName(value = "crew-count")
  private final Observable<Integer> crewCount;
  private transient Currency wages;
  @SerializedName(value = "inventory")
  //Caution: This is only NOT final due to JSON.
  private Inventory shipInventory;
  @SerializedName(value = "stats")
  private final Stats shipStats;
  //Caution: This is only NOT final due to JSON.
  private transient Observable<Integer> currentHitpoints;
  @SerializedName(value = "base-travel-speed")
  private final int baseTravelSpeed;
  //Caution: This is only NOT final due to JSON.
  private transient Observable<Float> travelSpeed;
  @SerializedName(value = "upgrade-slots")
  private final ObservableMap<UpgradeType, Integer> shipUpgradeSlots;
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void initialize() {
    wages = calculateWages();
    currentHitpoints = new Observable<Integer>(this.shipStats.getMaxHitpoints());
    this.travelSpeed 
        = new Observable<Float>(baseTravelSpeed * this.shipStats.getTravelMultiplier());
    if (shipInventory == null) {
      shipInventory = new Inventory(shipStats.getCargoCapacity());
    } else {
      shipInventory.initialize();
    }
    
  }
  
  /**
   * Constructs a new ship with NO default inventory. The inventory is derived from the
   *     cargo capacity stat.
   *
   * @param id                  the ship's id
   * @param name                the ship's name
   * @param description         the ship's description
   * @param imagePath           the file path of an image of the ship
   * @param crewCount           the amount of crew
   * @param shipStats           the <code>Stats</code> of the ship
   * @param baseTravelSpeed     the initial travel speed of the ship
   * @param upgradeSlots        a map containing the amount of upgrade slots for
   *     each upgrade type, mapping the integer to the type
   */
  public Ship(int id, String name, String description, String imagePath, 
      int crewCount, Stats shipStats, int baseTravelSpeed, Map<UpgradeType, Integer> upgradeSlots) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.imagePath = imagePath;
    this.crewCount = new Observable<Integer>(crewCount);
    wages = calculateWages();
    this.shipStats = shipStats;
    this.baseTravelSpeed = baseTravelSpeed;
    this.currentHitpoints = new Observable<Integer>(this.shipStats.getMaxHitpoints());
    this.travelSpeed = 
        new Observable<Float>(baseTravelSpeed * this.shipStats.getTravelMultiplier());
    shipInventory = new Inventory(shipStats.getCargoCapacity());
    this.shipUpgradeSlots = new ObservableMap<UpgradeType, Integer>();
    Action.run(() -> {
      if (upgradeSlots != null) {
        this.shipUpgradeSlots.clear();
        this.shipUpgradeSlots.putAll(upgradeSlots);
      }
    });
  }
  
  /**
   * Constructs a new ship with a default inventory. WARNING: The cargo capacity stat
   *     MUST be the same as the default inventories capacity.
   *
   * @param id                  the ship's id
   * @param name                the ship's name
   * @param description         the ship's description
   * @param imagePath           the file path of an image of the ship
   * @param crewCount           the amount of crew
   * @param defaultInventory    the inventory to use for the ship
   * @param shipStats           the <code>Stats</code> of the ship
   * @param baseTravelSpeed     the initial travel speed of the ship
   * @param upgradeSlots        a map containing the amount of upgrade slots for
   *     each upgrade type, mapping the integer to the type
   */
  public Ship(int id, String name, String description, String imagePath, 
      int crewCount, Inventory defaultInventory, 
      Stats shipStats, int baseTravelSpeed, Map<UpgradeType, Integer> upgradeSlots) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.imagePath = imagePath;
    this.crewCount = new Observable<Integer>(crewCount);
    wages = calculateWages();
    shipInventory = defaultInventory;
    this.shipStats = shipStats;
    this.baseTravelSpeed = baseTravelSpeed;
    this.currentHitpoints = new Observable<Integer>(this.shipStats.getMaxHitpoints());
    this.travelSpeed = 
        new Observable<Float>(baseTravelSpeed * this.shipStats.getTravelMultiplier());
    this.shipUpgradeSlots = new ObservableMap<UpgradeType, Integer>();
    Action.run(() -> {
      if (upgradeSlots != null) {
        this.shipUpgradeSlots.clear();
        this.shipUpgradeSlots.putAll(upgradeSlots);
      }
    });
  }
  
  /**
   * Calculates the amount of wages for a ship's crew.
   * It is a flat fee of $5 per crew member per day.
   *
   * @return the amount of wages required to run the ship for a day
   */
  private Currency calculateWages() {
    return new Currency(5, 00).multiply(getCrewCount());
  }
  
  /**
   * Used internally to set a new travel speed.
   *
   * @param newTravelSpeed  the new travel speed to set to
   */
  private void setTravelSpeed(float newTravelSpeed) {
    Action.run(() -> {
      travelSpeed.set(newTravelSpeed);
    });
  }
  
  /**
   * TODO: Work out formula for damage.
   *
   * @return the amount it costs to repair the ship
   */
  public Currency calculateDamageCost() {
    // TODO in a balance patch. For now: $1 per HP
    return new Currency(shipStats.getMaxHitpoints() - getCurrentHitpoints(), 00);
  }
  
  /**
   * Handy internal function to reverse an items stats. Used only in removing an upgrade,
   * but has a check just in case.
   *
   * @param upgradeItem the upgrade item to reverse
   * @param upgrade     true if this is an upgrade, false if downgrade
   */
  private void reverseUpgradeItemStats(UpgradeItem upgradeItem, boolean upgrade) {
    if (!upgrade) {
      //Reverse all values of the upgrade item
      upgradeItem.changeUpgradeStats(new Stats(
          -upgradeItem.getUpgradeStats().getMaxHitpoints() * 2,
          -1,
          -upgradeItem.getUpgradeStats().getDamagePotential(),
          -upgradeItem.getUpgradeStats().getCargoCapacity(),
          -1
      ), false);
    }
  }
  
  /**
   * A handy internal function for checking applying an upgrade or downgrade
   * against the thought out edge cases.
   *
   * @param upgradeItem the upgrade item to check against
   * @param upgrade     true if this is an upgrade, false if downgrade
   */
  private void upgradeEdgeCaseChecker(UpgradeItem upgradeItem, boolean upgrade) {
    String grade;
    if (upgrade) {
      grade = "Upgrade";
    } else {
      grade = "Downgrade";
    }
    //If it's a downgrade, temporarily reverse all values of the upgrade item
    reverseUpgradeItemStats(upgradeItem, upgrade);
    //You may set your inventory to 0 capacity. You may not set it lower.
    if (shipInventory.getCapacity() + upgradeItem.getUpgradeStats().getCargoCapacity() < 0) {
      reverseUpgradeItemStats(upgradeItem, upgrade);
      throw new IllegalArgumentException(grade + " cannot set capacity to below 0.");
    }
    //Note: This edge case should always come last, as if it succeeds it'll modify the
    //current HP prematurely otherwise.
    //If the upgrade reduces the amount of hitpoints
    if (upgradeItem.getUpgradeStats().getMaxHitpoints() < 0) {
      //No, you are not allowed to suicide by applying a negative max hitpoint upgrade.
      if (shipStats.getMaxHitpoints() 
          + upgradeItem.getUpgradeStats().getMaxHitpoints() <= 0) {
        reverseUpgradeItemStats(upgradeItem, upgrade);
        throw new IllegalArgumentException(grade + " cannot set max hitpoints to below 0.");
      } else {
        //If your current health goes over what will be the new maximum
        if (getCurrentHitpoints() > shipStats.getMaxHitpoints() 
            + upgradeItem.getUpgradeStats().getMaxHitpoints()) {
          //Set current health to be the new max health. NOTE: This does NOT work vice-versa.
          changeHp(-(getCurrentHitpoints() - (shipStats.getMaxHitpoints() 
              + upgradeItem.getUpgradeStats().getMaxHitpoints())));
        }
      }
      //There might be an edge case here with negative risk / travel speed / damage potential, 
      //but tbh that'll be funny so for now I'm ignoring it.
    }
  }
  
  /**
   * Applies an upgrade to a ship, if it's valid. Note: Upgrades should come from the
   * inventory.
   *
   * @param upgradeToApply              the <code>UpgradeItem</code> to apply
   * @param slotToPutIn                 the slot chosen to put the UpgradeItem in
   * @throws IllegalArgumentException   when the upgrade is not valid
   */
  public void applyUpgrade(UpgradeItem upgradeToApply, 
      UpgradeType slotToPutIn) {
    for (UpgradeType upgradeType : upgradeToApply.getUpgradeTypes()) {
      if (upgradeType == slotToPutIn && getUpgradeSlots().get(upgradeType) > 0) {
        //First, some edge cases.
        upgradeEdgeCaseChecker(upgradeToApply, true);
        //It has survived the edge cases. Apply the upgrade.
        //Make it so there's one less slot available.
        Action.run(() -> {
          //Remove a slot
          putUpgradeSlots(upgradeType, getUpgradeSlots().get(upgradeType) - 1);
          shipStats.changeStats(upgradeToApply.getUpgradeStats(), upgradeToApply.getAdditive());
          setTravelSpeed(baseTravelSpeed * this.shipStats.getTravelMultiplier());
          //Easier to do in two operations
          shipInventory.changeCapacity(-shipInventory.getCapacity());
          shipInventory.changeCapacity(shipStats.getCargoCapacity());
        });
        upgradeToApply.setAppliedLocation(slotToPutIn);
        return;
      }
    }
    throw new IllegalArgumentException(
        "Could not match upgrade with upgrade slot, "
        + "or ship does not have enough upgrade slots of this type."
    );
  }
  
  /**
   * Removes an upgrade from the ship, if it's valid.
   *
   * @param upgradeToRemove             the <code>UpgradeItem</code> to remove
   * @param slotToRemoveFrom            the slot chosen to remove the UpgradeItem from
   * @throws IllegalArgumentException   when removing the upgrade is not valid
   */
  public void removeUpgrade(UpgradeItem upgradeToRemove, 
      UpgradeType slotToRemoveFrom) {
    upgradeEdgeCaseChecker(upgradeToRemove, false);
    Action.run(() -> {
      //Add a slot
      putUpgradeSlots(slotToRemoveFrom, getUpgradeSlots().get(slotToRemoveFrom) + 1);
      shipStats.changeStats(upgradeToRemove.getUpgradeStats(), upgradeToRemove.getAdditive());
      setTravelSpeed(baseTravelSpeed * this.shipStats.getTravelMultiplier());
      //Easier to do in two operations
      shipInventory.changeCapacity(-shipInventory.getCapacity());
      shipInventory.changeCapacity(shipStats.getCargoCapacity());
      //Make sure to reverse it!
      reverseUpgradeItemStats(upgradeToRemove, false);
      upgradeToRemove.setAppliedLocation(null);
    });
  }
  
  /**
   * Sets the ship's health to maximum.
   */
  public void healShipToFull() {
    Action.run(() -> {
      currentHitpoints.set(shipStats.getMaxHitpoints());
    });
  }
  
  /**
   * Adds the HP passed to the current HP. Useful for damage, or healing.
   *
   * @param amountToChange the amount to add to the ship's current hitpoints
   */
  public void changeHp(int amountToChange) {
    Action.run(() -> {
      currentHitpoints.set(getCurrentHitpoints() + amountToChange);
    });
  }
  
  /**
   * This is a generic "get" argument. It can get any value based on the specified
   *     string. To see matches of strings, see ShipVariables.
   *
   * @param valueToGet  the string to match against in ShipVariables
   * @return            the value of the matched variable, or an error if it fails matching
   * @see ShipVariables
   */
  @SuppressWarnings("unchecked")
  public <T> T get(String valueToGet) {
    for (ShipVariables shipVariable : ShipVariables.values()) {
      if (valueToGet.equals(shipVariable.getShipVariable().getVariableAlias())) {
        return (T) shipVariable.getShipVariable().getVariableFunction().getValue(this);
      }
    }
    return (T) "Error: Alias does not exist.";
  }
  
  @Override
  public int getId() {
    return id;
  }
  
  public String getShipName() {
    return name;
  }
  
  public String getDescription() {
    return description;
  }
  
  public String getImagePath() {
    return imagePath;
  }
  
  public int getCrewCount() {
    return crewCount.get();
  }
  
  public Stats getShipStats() {
    return shipStats;
  }
  
  public int getCurrentHitpoints() {
    return currentHitpoints.get();
  }
  
  public float getTravelSpeed() {
    return travelSpeed.get();
  }
  
  public Currency getWages() {
    return wages;
  }
  
  public int getBaseTravelSpeed() {
    return baseTravelSpeed;
  }
  
  public Currency getDamageCost() {
    return calculateDamageCost();
  }
  
  public Inventory getInventory() {
    return shipInventory;
  }
  
  public ObservableMap<UpgradeType, Integer> getUpgradeSlots() {
    return shipUpgradeSlots;
  }
  
  /**
   * Used to put a key and value into the UpgradeSlots map.
   *
   * @param key     the upgrade type
   * @param value   the new value
   */
  public void putUpgradeSlots(UpgradeType key, int value) {
    Action.run(() -> {
      getUpgradeSlots().put(key, value);
    });
  }
  
  /**
   * Changes the <code>Stat</code>s of the <code>Ship</code> by adding them.
   * If cargo capacity changes and becomes too small for the inventory,
   * false will be returned. Otherwise, true will be returned on a success.
   *
   * @param statsToAdd  the stats to add to the existing ship's stats
   * @param additive    whether the multipliers should be additive or multiplicative
   *     see Stats.java
   * @return            true if successful, false otherwise
   */
  public boolean changeStats(Stats statsToAdd, boolean additive) {
    if (shipInventory.changeCapacity(statsToAdd.getCargoCapacity())) {
      shipStats.changeStats(statsToAdd, additive);
      return true;
    } else {
      return false;
    }
  }
  
  @Override
  public String toString() {
    return "Ship name: " + name
      + "\nCrew count: " + getCrewCount()
      + "\nWages (per day): " + wages
      + "\nBase travel speed: " + baseTravelSpeed
      + "\nTravel speed: " + getTravelSpeed()
      + "\nInventory:\n" + shipInventory.toString()
      + "\nShip Stats: " + shipStats.toString()
      + "\nShip upgrade slots: " + getUpgradeSlots().toString();
  }
}