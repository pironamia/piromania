package nz.piromania.gameenvironment.ships;

/**
 * Represents a function with an alias.
 * Used exclusively for ShipVariables currently.
 */
public class ShipVariable {
  private final String variableAlias;
  private final ShipVariableReturner<?> variableFunction;
  
  /**
   * Creates a new <code>ShipVariable</code> with an alias
   *     and a <code>ShipVariableReturner</code>.
   *
   * @param variableAlias       what the variable should be aliased to
   * @param variableFunction    the <code>ShipVariableReturner</code> to
   *     enable access to a ship variable
   */
  public ShipVariable(String variableAlias, 
      ShipVariableReturner<?> variableFunction) {
    this.variableAlias = variableAlias;
    this.variableFunction = variableFunction;
  }
  
  public String getVariableAlias() {
    return variableAlias;
  }
  
  public ShipVariableReturner<?> getVariableFunction() {
    return variableFunction;
  }
}
