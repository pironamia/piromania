package nz.piromania.gameenvironment.ships;

/**
 * This interface is used to get any value from a ship.
 * Currently only used in <code>ShipVariable</code>, 
 * and <code>ShipVariables</code>.
 *
 * @see ShipVariable
 * @see ShipVariables
 */

public interface ShipVariableReturner<T> {
  T getValue(Ship ship);
}
