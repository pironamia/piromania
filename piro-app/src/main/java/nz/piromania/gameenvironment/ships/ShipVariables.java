package nz.piromania.gameenvironment.ships;

/**
 * This contains all the variables that the Ship uses.
 * Also contains an alias, and a method of getting the variable.
 * Used in the TextFormatter, and the generic .get() on a Ship.
 * Calling ShipVariables.{SHIPVARIABLE} will return a <code>ShipVariable</code>
 * that can be used for identification and function.
 * {@link #SHIPNAME}
 * {@link #SHIPDESC}
 * {@link #MAXHITPOINTS}
 * {@link #RISKMULTIPLIER}
 * {@link #DAMAGEPOTENTIAL}
 * {@link #CARGOCAPACITY}
 * {@link #TRAVELMULTIPLIER}
 * {@link #CURRENTHITPOINTS}
 * {@link #TRAVELSPEED}
 * {@link #CREWCOUNT}
 * {@link #WAGES}
 * {@link #BASETRAVELSPEED}
 * {@link #DAMAGECOST}
 */

public enum ShipVariables {
  /**
   * ship-name.
   */
  SHIPNAME(new ShipVariable("ship-name",
      (Ship ship) -> ship.getShipName())),
  /**
   * ship-description.
   */
  SHIPDESC(new ShipVariable("ship-description",
      (Ship ship) -> ship.getDescription())),
  /**
   * max-hitpoints.
   */
  MAXHITPOINTS(new ShipVariable("max-hitpoints", 
      (Ship ship) -> ship.getShipStats().getMaxHitpoints())),
  /**
   * risk-multiplier.
   */
  RISKMULTIPLIER(new ShipVariable("risk-multiplier", 
      (Ship ship) -> ship.getShipStats().getRiskMultiplier())),
  /**
   * damage-potential.
   */
  DAMAGEPOTENTIAL(new ShipVariable("damage-potential", 
      (Ship ship) -> ship.getShipStats().getDamagePotential())),
  /**
   * cargo-capacity.
   */
  CARGOCAPACITY(new ShipVariable("cargo-capacity", 
      (Ship ship) -> ship.getShipStats().getCargoCapacity())),
  /**
   * travel-multiplier.
   */
  TRAVELMULTIPLIER(new ShipVariable("travel-multiplier", 
      (Ship ship) -> ship.getShipStats().getTravelMultiplier())),
  /**
   * current-hitpoints.
   */
  CURRENTHITPOINTS(new ShipVariable("current-hitpoints", 
      (Ship ship) -> ship.getCurrentHitpoints())),
  /**
   * travel-speed.
   */
  TRAVELSPEED(new ShipVariable("travel-speed", 
      (Ship ship) -> ship.getTravelSpeed())),
  /**
   * crew-count.
   */
  CREWCOUNT(new ShipVariable("crew-count", 
      (Ship ship) -> ship.getCrewCount())),
  /**
   * wages.
   */
  WAGES(new ShipVariable("wages", 
      (Ship ship) -> ship.getWages())),
  /**
   * base-travel-speed.
   */
  BASETRAVELSPEED(new ShipVariable("base-travel-speed", 
      (Ship ship) -> ship.getBaseTravelSpeed())),
  /**
   * damage-cost.
   */
  DAMAGECOST(new ShipVariable("damage-cost", 
      (Ship ship) -> ship.getDamageCost()));
  
  private ShipVariable shipVariable;
  ShipVariables(ShipVariable shipVariable) {
    this.shipVariable = shipVariable;
  }

  public ShipVariable getShipVariable() {
    return shipVariable;
  }
}
