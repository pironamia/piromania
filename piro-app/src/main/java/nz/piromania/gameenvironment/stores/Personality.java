package nz.piromania.gameenvironment.stores;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import nz.piromania.JsonLoadable;

/**
 * This class is used to define a personality for a shop.
 * This class should ALWAYS be loaded by a <code>PersonalitySystemLoader</code>.
 *
 * @see Personality
 */
public class Personality implements JsonLoadable {
  private final transient Random randomGenerator = new Random();
  @SerializedName(value = "id")
  private final int personalityId;
  @SerializedName(value = "name")
  private final String personalityName;
  @SerializedName(value = "greeting-dialogues")
  private final ArrayList<String> greetings;
  @SerializedName(value = "leaving-dialogues")
  private final ArrayList<String> farewells;
  @SerializedName(value = "right-type-modifier")
  private final Float rightTypeModifier;
  @SerializedName(value = "bartering-chance")
  private final Float barteringModifier;
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void initialize() {}
  
  /**
   * USED BY GSON ONLY. DO NOT USE!
   */
  //We can ignore this warning because we don't use it.
  @SuppressWarnings("unused")
  private Personality() {
    //This is used for GSON, so when loaded it will still
    //generate important data. It all gets overridden anyways.
    this(-1, null, null, null, null, null);
  }
  
  /**
   * Constructs a new personality system for a store.
   * Includes all dialogue, and some key stats for the personality.
   *
   * @param id                  the ID of the personality
   * @param name                the name of the personality (I.E. 'happy')
   * @param greetings           all greeting dialogues
   * @param farewells           all leaving dialogues
   * @param rightTypeModifier   modifier applied to an item upon selling if it's the right type
   *     NOTE: this is additive
   * @param barteringModifier   chance to haggle down price from player
   *     NOTE: this is additive
   */
  public Personality(Integer id, String name, ArrayList<String> greetings, 
      ArrayList<String> farewells, Float rightTypeModifier, Float barteringModifier) {
    this.personalityId = id;
    this.personalityName = name;
    this.greetings = greetings;
    this.farewells = farewells;
    if (rightTypeModifier == null) {
      //This function DOES allow for values over 1, so it's capped to
      //0.8 for safety of other functions.
      //Also ensures no division by 0.
      float rightTypeModifierValue = (1 / (randomGenerator.nextFloat() + 0.01f)) / 10;
      if (rightTypeModifierValue > 0.8) {
        rightTypeModifierValue = 0.8f;
      }
      this.rightTypeModifier = rightTypeModifierValue;
    } else {
      this.rightTypeModifier = rightTypeModifier;
    }
    if (barteringModifier == null) {
      //Same generation as rightTypeModifier. Effectively does same thing
      //anyways, just when buying FROM the player instead of vice versa.
      float barteringModifierValue = (1 / (randomGenerator.nextFloat() + 0.01f)) / 10;
      if (barteringModifierValue > 0.8) {
        barteringModifierValue = 0.8f;
      }
      this.barteringModifier = barteringModifierValue;
    } else {
      this.barteringModifier = barteringModifier;
    }
  }
  
  /**
   * Used to get a random dialogue from a specified dialogue type.
   * Currently, these dialogue types exist:
   * <ul>
   *     <li>Greeting Dialogue</li>
   *     <li>Leaving Dialogue</li>
   * </ul>
   *
   * @param dialogueToGetFrom   the type of dialogue to get randomly from
   * @return                    a random string from the specified dialogue
   */
  public String getRandomDialogue(String dialogueToGetFrom) {
    ArrayList<String> dialogueType;
    switch (dialogueToGetFrom) {
      case "Greeting Dialogue":
        dialogueType = greetings;
        break;
      case "Leaving Dialogue":
        dialogueType = farewells;
        break;
      default:
        dialogueType = new ArrayList<String>(Arrays.asList("ERROR: Dialogue Type not found"));
        break;
    }
    return dialogueType.get(randomGenerator.nextInt(dialogueType.size()));
  }
  
  public Float getRightTypeModifier() {
    return rightTypeModifier;
  }
  
  public Float getBarteringModifier() {
    return barteringModifier;
  }
  
  @Override
  public int getId() {
    return personalityId;
  }
  
  public String getName() {
    return personalityName;
  }
  
  public ArrayList<String> getGreetings() {
    return greetings;
  }
  
  public ArrayList<String> getFarewells() {
    return farewells;
  }
  
  @Override
  public String toString() {
    String output = "ID: " + personalityId
        + "\nName: " + personalityName
        + "\nRight type modifier: " + rightTypeModifier
        + "\nBartering modifier: " + barteringModifier
        + "\nGreeting dialogues:\n";
    for (String dialogue : greetings) {
      output += dialogue + "\n";
    }
    output += "Leaving dialogues:";
    for (String dialogue : farewells) {
      output += "\n" + dialogue;
    }
    return output;
  }
}
