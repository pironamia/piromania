package nz.piromania.gameenvironment.stores;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import nz.piromania.Currency;
import nz.piromania.Inventory;
import nz.piromania.JsonLoadable;
import nz.piromania.JsonManager;
import nz.piromania.gameenvironment.items.Item;
import nz.piromania.gameenvironment.items.ItemData;
import nz.piromania.gameenvironment.items.ItemType;
import nz.piromania.gameenvironment.items.UpgradeItem;

/**
 * Defines the store class. This is used to handle transactions
 * between the player and the game, generating realistic buying
 * and selling prices, as well as catering to demand and type.
 * 
 * <p>Note: Store names should be generated before initializing the store.</p>
 */
public class Store implements JsonLoadable {
  //BUYING TYPE IS PREFERRED TO BUY. STORES WILL BUY ANYTHING,
  //BUT WILL OFFER MORE FOR THOSE OF BUYING TYPE
  //AS IN, THE STORE *WANTS* TO BUY BUYING TYPE
  //(Trust me, this gets confusing fast.)
  @SerializedName(value = "id")
  private final int id;
  @SerializedName(value = "name")
  private final String storeName;
  @SerializedName(value = "buying-type")
  private final ItemType buyingType;
  @SerializedName(value = "selling-type")
  private final ItemType sellingType;
  @SerializedName(value = "personality")
  //Caution: This is only NOT final due to JSON.
  private Personality personality;
  //Caution: This is only NOT final due to JSON.
  @SerializedName(value = "inventory")
  private Inventory storeInventory;
  private transient Inventory storeSellingInventory;
  private transient Inventory storeBuyingInventory;
  private transient Random randomGenerator = new Random();
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void initialize() {}
  
  /**
   * Should be used only after a JSON store is loaded.
   * Sets all the variable dependents. Currently, this only
   * gives a random personality if none is found in the JSON.
   *
   * @param personalities   in case there is no personality with this store,
   *     give it a random personality
   */
  public void initialize(JsonManager<Personality> personalities) {
    if (personality == null) {
      personality = personalities.getRandomData();
    }
    if (storeInventory == null) {
      storeInventory = new Inventory();
    }
    randomGenerator = new Random();
  }
  
  /**
   * Defines a new Store with a random personality, and no base inventory.
   *
   * @param personalities   a <code>PersonalitySystemManager</code> 
   *     to generate a random personality with
   * @param buyingType      the kind of items this store wants to buy from the player
   * @param sellingType     the kind of items this store wants to sell to the player
   */
  public Store(int id, String name, JsonManager<Personality> personalities, 
      ItemType buyingType, ItemType sellingType) {
    this.id = id;
    this.storeName = name;
    this.storeInventory = new Inventory();
    this.buyingType = buyingType;
    this.sellingType = sellingType;
    this.personality = personalities.getRandomData();
  }
  
  /**
   * Defines a new Store with a random personality and a base inventory.
   *
   * @param personalities   a <code>PersonalitySystemManager</code> 
   *     to generate a random personality with
   * @param inventory       the base inventory of the store
   * @param buyingType      the kind of items this store wants to buy from the player
   * @param sellingType     the kind of items this store wants to sell to the player
   */
  public Store(int id, String name, JsonManager<Personality> personalities, 
      Inventory inventory, ItemType buyingType, ItemType sellingType) {
    this.id = id;
    this.storeName = name;
    this.storeInventory = inventory;
    this.buyingType = buyingType;
    this.sellingType = sellingType;
    this.personality = personalities.getRandomData();
  }
  
  /**
   * Defines a new Store with no base inventory.
   *
   * @param personality     the kind of personality this store has
   * @param buyingType      the kind of items this store wants to buy from the player
   * @param sellingType     the kind of items this store wants to sell to the player
   */
  public Store(int id, String name, Personality personality, 
      ItemType buyingType, ItemType sellingType) {
    this.id = id;
    this.storeName = name;
    this.storeInventory = new Inventory();
    this.buyingType = buyingType;
    this.sellingType = sellingType;
    this.personality = personality;
  }
  
  /**
   * Defines a new Store with a base inventory.
   *
   * @param personality     the kind of personality this store has
   * @param inventory       the base inventory of the store
   * @param buyingType      the kind of items this store wants to buy from the player
   * @param sellingType     the kind of items this store wants to sell to the player
   */
  public Store(int id, String name, Personality personality, Inventory inventory, 
      ItemType buyingType, ItemType sellingType) {
    this.id = id;
    this.storeName = name;
    this.storeInventory = inventory;
    this.buyingType = buyingType;
    this.sellingType = sellingType;
    this.personality = personality;
  }
   
  /**
   * Generates a random inventory for this shop.
   * Prioritizes items of sellingType.
   * Deprioritizes items of buyingType.
   * Will USUALLY generate 10 - 41 items.
   * Can generate 0-inf items in reality.
   * ALSO generates new random prices for every item to be bought or sold.
   *
   * @param itemsToGenerateFrom an <code>ItemManager</code> to get random item data from
   */
  public void randomizeInventory(JsonManager<ItemData> itemsToGenerateFrom) {
    //First, remove some random items.
    ArrayList<Item> itemsToRemove = new ArrayList<Item>();
    for (Item item : storeInventory.getItems()) {
      float removeChance = 0.5f;
      //Items of sellingType will be removed more frequently
      if (item.getItemType().equals(sellingType)) {
        removeChance += 0.25f;
      }
      //Ones of buyingType will be removed less frequently
      if (item.getItemType().equals(buyingType)) {
        removeChance -= 0.25f;
      }
      //Arbitrarily saying anything over $100 is classified 'expensive'
      //Expensive things should stay for longer to encourage saving
      if (item.getItemBaseValue().getCents() >= 10000) {
        removeChance -= 0.2f;
      } else {
        removeChance += 0.2f;
      }
      if (randomGenerator.nextFloat() <= removeChance) {
        itemsToRemove.add(item);
      }
    }
    
    for (Item item : itemsToRemove) {
      storeInventory.removeItem(item);
    }
    
    //Next, add random items.
    int randomNum = randomGenerator.nextInt(32);
    for (int i = 0; i < (randomNum + 10); i++) {
      //Get any random item
      ItemData itemData = itemsToGenerateFrom.getRandomData();
      //If it's the kind of item the Store should be selling, make it more frequent
      if (itemData.getItemType().equals(sellingType) && randomGenerator.nextFloat() < 0.2f) {
        i -= 1;
      }
      //Note: two if statements here as buyingType could == sellingType.
      //If it's the kind of item the Store should be buying, make it rarer
      if (itemData.getItemType().equals(buyingType) && randomGenerator.nextFloat() < 0.2f) {
        continue;
      }
      //Make sure the Item gets instantiated correctly, then add it.
      if (itemData.getItemType().equals(ItemType.UPGRADE)) {
        UpgradeItem item = itemData.createUpgradeInstance();
        storeInventory.addItem(item);
      } else {
        Item item = itemData.createItemInstance();
        storeInventory.addItem(item);
      }
    }
    generateSellingToStorePrices(itemsToGenerateFrom);
    generateBuyingFromStorePrices(itemsToGenerateFrom);
  }
  
  /**
   * Buys an Item from the store.
   *
   * @param itemToBuy   the item to buy from the store
   * @param payingMoney the amount of money the object purchasing has
   * @param soldLedger  the ledger the player uses to keep track of all sold items
   * @return            the Item the player bought, null if player cannot afford item
   */
  public Item buyFromStore(Item itemToBuy, Currency payingMoney, 
      Map<Store, Map<Item, Currency>> soldLedger) {
    if (payingMoney.getCents() 
        >= storeBuyingInventory.getItem(itemToBuy.getId()).getPurchasedValue().getCents()) {
      //Item must've been sold from the player, as NO item in the store's Inventory
      //should have a sold value EXCEPT those bought by the player.
      if (itemToBuy.getSoldValue().getCents() > 0) {
        soldLedger.get(this).remove(itemToBuy);
      }
      if (storeInventory.removeItem(itemToBuy)) {
        itemToBuy.setSoldValue(new Currency(0));
        itemToBuy.setPurchasedValue(
            storeBuyingInventory.getItem(itemToBuy.getId()).getPurchasedValue());
        return itemToBuy;  
      } else {
        return null;
      }
    } else {
      throw new IllegalArgumentException("Not enough money to purchase item.");
    }
  }  
  
  /**
   * Sells an Item to the store.
   *
   * @param itemToSell              the item to sell to the store
   * @param inventoryToRemoveFrom   the inventory to remove the item from
   * @param soldLedger              the player's ledger for all sold items
   * @return                        the amount of money the item was sold for
   */
  public Currency sellToStore(Item itemToSell, 
      Inventory inventoryToRemoveFrom, 
      Map<Store, Map<Item, Currency>> soldLedger) {

    if (inventoryToRemoveFrom.removeItem(itemToSell)) {
      //Something has gone drastically wrong if we
      //need the result of .addItem().
      itemToSell.setSoldValue(
          storeSellingInventory.getItem(itemToSell.getId()).getPurchasedValue()
      );
      storeInventory.addItem(itemToSell);
      //Item was sold. Best to keep a copy in the sold ledger. 
      //Maps the amount it was purchased for to the item.
      soldLedger.get(this).put(itemToSell, new Currency(itemToSell.getPurchasedValue().getCents()));
      return itemToSell.getSoldValue();
    } else {
      return new Currency(0);
    }
    
  }

  /**
   * Generates the amount a store would buy items for.
   * Each item from an itemManager is generated, and added to the store's
   * selling inventory. This means that items with the same ID will be sold for
   * the same amount. This also allows for the player to see what ANY store
   * will buy items as. The value of the item is stored in PurchasedValue,
   * which is dependent on the personality of the store.
   *
   * @param itemManager  the manager of all the items in this game
   */
  public void generateSellingToStorePrices(JsonManager<ItemData> itemManager) {
    storeSellingInventory = new Inventory();
    for (int i : itemManager.getAllIds()) {
      //Yes, this initializes UpgradeItems as Items.
      //Doesn't matter, these are only used for storing prices.
      Item item = itemManager.getData(i).createItemInstance();
      storeSellingInventory.addItem(item);
      Float sellingModifier = 0.9f + (randomGenerator.nextFloat() / 4);
      if (item.getItemType().equals(buyingType)) {
        sellingModifier += personality.getRightTypeModifier();
      } else {
        //This places huge emphasis on going to stores with the correct types
        //for what you want to sell.
        sellingModifier -= personality.getRightTypeModifier();
      }
      //Yes, this function looks scary as it looks like it could set Currency to be negative.
      //Don't worry, this is handled in personality by capping the modifier.
      item.setPurchasedValue(item.getItemBaseValue().multiply(sellingModifier));
    }
  }
  
  /**
   * Generates the amount a store would sell items for.
   * Each item from an itemManager is generated, and added to the store's
   * buying inventory. This means that items with the same ID will be bought for
   * the same amount. This also allows for the player to see what ANY store
   * will sell items as. The value of the item is stored in PurchasedValue,
   * which is dependent on the personality of the store.
   *
   * @param itemManager  the manager of all the items in this game
   */
  public void generateBuyingFromStorePrices(JsonManager<ItemData> itemManager) {
    storeBuyingInventory = new Inventory();
    for (int i : itemManager.getAllIds()) {
      //Yes, this initializes UpgradeItems as Items.
      //Doesn't matter, these are only used for storing prices.
      Item item = itemManager.getData(i).createItemInstance();
      storeBuyingInventory.addItem(item);
      Float buyingModifier = 1.1f + (randomGenerator.nextFloat() / 4);
      if (item.getItemType().equals(sellingType)) {
        buyingModifier -= personality.getRightTypeModifier();
      } else {
        //This places huge emphasis on going to stores with the correct types
        //for what you want to buy.
        buyingModifier += personality.getRightTypeModifier();
      }
      //Yes, this function looks scary as it looks like it could set Currency to be negative.
      //Don't worry, this is handled in personality by capping the modifier.
      item.setPurchasedValue(item.getItemBaseValue().multiply(buyingModifier));
    }
  }

  @Override
  public int getId() {
    return id;
  }
  
  public String getStoreName() {
    return storeName;
  }
  
  public Inventory getStoreInventory() {
    return storeInventory;
  }
  
  public Inventory getSellingInventory() {
    return storeSellingInventory;
  }
  
  public Inventory getBuyingInventory() {
    return storeBuyingInventory;
  }
  
  public ItemType getBuyingType() {
    return buyingType;
  }
  
  public ItemType getSellingType() {
    return sellingType;
  }
  
  public Personality getPersonality() {
    return personality;
  }
  
  @Override
  public String toString() {
    String output = "Store:"
        + "\nName: " + storeName
        + "\nInventory:\n" + storeInventory.toString()
        + "\nBuying inventory:\n" + storeBuyingInventory.toString()
        + "\nSelling inventory:\n" + storeSellingInventory.toString()
        + "\nPersonality:\n" + personality.toString()
        + "\nBuying type: " + buyingType
        + "\nSelling type: " + sellingType;
    return output;
  }
}
