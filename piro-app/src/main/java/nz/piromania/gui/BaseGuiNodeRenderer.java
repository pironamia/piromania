package nz.piromania.gui;

import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.component.DirectlyRenderableComponent;

/**
 * A very basic implementation of a GuiNodeRenderer.
 */
public abstract class BaseGuiNodeRenderer<T extends Component & DirectlyRenderableComponent> {

  private T currentComponent;
  private GuiNodeRendererParent parent;

  /**
   * Provides the parent of this GuiNodeRenderer to subclasses.
   *
   * @return the GuiNodeRendererParent that contains this GuiNodeRenderer instance
   */
  public final GuiNodeRendererParent getParent() {
    return parent;
  }

  /**
   * Provides the current component of this GuiNodeRenderer to subclasses.
   *
   * @return the current ReactiveEngine component that is bound to this GuiNodeRenderer
   */
  protected final T getCurrentComponent() {
    return currentComponent;
  }

  /**
   * Updates this gui element to conform to this component without disposing the parent that the gui
   * children are bound to.
   *
   * @param newComponent  to update this GuiNodeRenderer instance with
   * @param parent        of this GuiNodeRenderer
   * @param indexInParent of this component
   */
  public final void updateComponent(
      T newComponent,
      GuiNodeRendererParent parent,
      int indexInParent
  ) {
    if (
        currentComponent == null
            || !currentComponent.equals(newComponent)
            || !parent.equals(this.parent)
    ) {
      updateComponentImpl(newComponent, parent, indexInParent);
    }
    this.parent = parent;
    this.currentComponent = newComponent;
  }

  /**
   * The implementation to be provided by subclasses of the updateComponent method.
   * <p>
   * This will be called every time the ReactiveEngine emits a new component tree (that still
   * contains a Component of this type).
   * </p>
   *
   * @param newComponent to update this GuiNodeRenderer instance with
   * @param parent       of this GuiNodeRenderer
   */
  protected abstract void updateComponentImpl(
      T newComponent,
      GuiNodeRendererParent parent,
      int indexInParent
  );

  /**
   * Determines whether this GuiNodeRenderer is compatible with the given
   * <code>otherComponent</code>.
   *
   * @param otherComponent to check for compatibility with
   * @return whether this GuiNodeRenderer instance is compatible with <code>otherComponent</code>
   */
  public final boolean compatibleWithComponent(Component otherComponent) {
    return currentComponent.getClass().equals(otherComponent.getClass());
  }
}
