package nz.piromania.gui;

import java.util.ArrayList;

/**
 * Interface for a GuiNodeRenderer that may want to run operations on its children once they have
 * renderer.
 */
public interface ChildAwareGuiNodeRenderer {

  /**
   * Called after the children of this GuiNodeRenderer have been built.
   *
   * @param children GuiNodeRenderers that are immediate descendants of this GuiNodeRenderer.
   */
  void postRenderChildren(ArrayList<GuiNodeRenderer> children);
}
