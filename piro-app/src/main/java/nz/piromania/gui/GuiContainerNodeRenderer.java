package nz.piromania.gui;

import java.awt.LayoutManager;
import java.util.ArrayList;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.component.DirectlyRenderableComponent;

/**
 * In charge of rendering a DirectlyRenderableComponent to the screen.
 */
public abstract class GuiContainerNodeRenderer<T extends Component & DirectlyRenderableComponent>
    extends GuiNodeRenderer<T> implements GuiNodeRendererParent {

  @Override
  public abstract java.awt.Container getContainer();

  @Override
  public abstract LayoutManager getLayoutManager();

  @Override
  protected abstract void updateComponentImpl(T newComponent, GuiNodeRendererParent parent,
      int indexInParent);

  /**
   * Called immediately after children are bound and updated (i.e. after children are rendered to
   * the screen). This shouldn't be necessary to override in the vast majority of cases. However, is
   * required to implement e.g. {@link nz.piromania.gui.renderer.TooltipNodeRenderer}.
   *
   * @param children ArrayList of the GuiNodeRenderers direct children to this one (although may
   *                 pass through layers of GuiNodes that have no GuiNodeRenderer attached).
   */
  @Override
  public abstract void postRenderChildren(ArrayList<GuiNodeRenderer> children);
}
