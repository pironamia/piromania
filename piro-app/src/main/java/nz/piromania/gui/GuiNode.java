package nz.piromania.gui;

import java.util.ArrayList;
import nz.piromania.gui.exception.IllegalChildOfLeafRenderer;
import nz.piromania.gui.renderer.AlignmentNodeRenderer;
import nz.piromania.gui.renderer.BackgroundNodeRenderer;
import nz.piromania.gui.renderer.ButtonNodeRenderer;
import nz.piromania.gui.renderer.CentreNodeRenderer;
import nz.piromania.gui.renderer.ClickableNodeRenderer;
import nz.piromania.gui.renderer.ContainerNodeRenderer;
import nz.piromania.gui.renderer.FlexibleNodeRenderer;
import nz.piromania.gui.renderer.ImageNodeRenderer;
import nz.piromania.gui.renderer.ListNodeRenderer;
import nz.piromania.gui.renderer.PaddingNodeRenderer;
import nz.piromania.gui.renderer.ScrollingNodeRenderer;
import nz.piromania.gui.renderer.StackNodeRenderer;
import nz.piromania.gui.renderer.TextNodeRenderer;
import nz.piromania.gui.renderer.TextboxNodeRenderer;
import nz.piromania.gui.renderer.TooltipNodeRenderer;
import nz.piromania.gui.util.GuiDebugHelper;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.RenderNode;
import nz.piromania.reactiveengine.component.AlignmentComponent;
import nz.piromania.reactiveengine.component.BackgroundComponent;
import nz.piromania.reactiveengine.component.ButtonComponent;
import nz.piromania.reactiveengine.component.CentreComponent;
import nz.piromania.reactiveengine.component.ClickableComponent;
import nz.piromania.reactiveengine.component.ContainerComponent;
import nz.piromania.reactiveengine.component.DirectlyRenderableComponent;
import nz.piromania.reactiveengine.component.ImageComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.ScrollingComponent;
import nz.piromania.reactiveengine.component.StackComponent;
import nz.piromania.reactiveengine.component.TooltipComponent;
import nz.piromania.reactiveengine.component.input.TextboxComponent;
import nz.piromania.reactiveengine.component.list.FlexibleComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.util.ArrayListBuilder;

/**
 * Node that connects RenderNodes (in ReactiveEngine build artifacts) to GuiNodeRenderer instances.
 *
 * <p>
 * The GuiNode tree (after processing) will exactly match the RenderNode tree, however only nodes
 * that are attached to Components that implement {@link DirectlyRenderableComponent} will be
 * connected to a GuiNodeRenderer instance (and thus rendered to the screen). Other Component types
 * are ignored, and no GuiNodeRenderer is generated.
 * </p>
 */
public class GuiNode {

  private final ArrayList<GuiNode> children = new ArrayList<>();
  /**
   * The current GuiNodeRenderer instance for this GuiNode. Will be null if this GuiNode currently
   * corresponds with a RenderNode that houses a Component that is not directly renderable.
   */
  private BaseGuiNodeRenderer nodeRenderer;

  /**
   * Gets the current nodeRenderer.
   *
   * @return current nodeRenderer
   * @throws RuntimeException if no <code>nodeRenderer</code> is bound.
   */
  public BaseGuiNodeRenderer getNodeRenderer() {
    if (nodeRenderer == null) {
      throw new RuntimeException("Attempted to get GuiNodeRenderer on GuiNode that has none.");
    }

    return nodeRenderer;
  }

  /**
   * Creates a GuiNodeRenderer instance for the given Component (if the Component is directly
   * renderable). Otherwise, will return null.
   *
   * @param component to build GuiNodeRenderer for (if applicable)
   * @return GuiNodeRenderer instance or null
   */
  private BaseGuiNodeRenderer createRendererForComponent(Component component) {
    if (!(component instanceof DirectlyRenderableComponent)) {
      return null;
    }

    if (component instanceof TextSpan) {
      return new TextNodeRenderer();
    } else if (component instanceof ContainerComponent) {
      return new ContainerNodeRenderer();
    } else if (component instanceof ButtonComponent) {
      return new ButtonNodeRenderer();
    } else if (component instanceof CentreComponent) {
      return new CentreNodeRenderer();
    } else if (component instanceof ListComponent) {
      return new ListNodeRenderer();
    } else if (component instanceof TextboxComponent) {
      return new TextboxNodeRenderer();
    } else if (component instanceof TooltipComponent) {
      return new TooltipNodeRenderer();
    } else if (component instanceof StackComponent) {
      return new StackNodeRenderer();
    } else if (component instanceof AlignmentComponent) {
      return new AlignmentNodeRenderer();
    } else if (component instanceof FlexibleComponent) {
      return new FlexibleNodeRenderer();
    } else if (component instanceof ImageComponent) {
      return new ImageNodeRenderer();
    } else if (component instanceof ScrollingComponent) {
      return new ScrollingNodeRenderer();
    } else if (component instanceof PaddingComponent) {
      return new PaddingNodeRenderer();
    } else if (component instanceof ClickableComponent) {
      return new ClickableNodeRenderer();
    } else if (component instanceof BackgroundComponent) {
      return new BackgroundNodeRenderer();
    } else {
      return null;
    }
  }

  /**
   * Updates this GuiNode with the RenderNode that it now corresponds with in the GuiNode tree.
   *
   * <p>
   * Automatically changes the type of GuiNodeRenderer (if necessary), updates the current
   * GuiNodeRenderer, and creates, updates, and/or destroys children as required.
   * </p>
   *
   * @param renderNode containing the Component to attach this GuiNode to
   * @param parent     renderer above this GuiNode in the tree (may not be direct parent to this
   *                   GuiNode since parent may not be directly renderable)
   * @return list of the next GuiNodeRenderers in the tree that has a <code>renderNode</code>
   *         attached (including this one)
   */
  public ArrayList<GuiNodeRenderer> updateComponent(RenderNode renderNode,
      GuiNodeRendererParent parent, int indexInParent) {
    final Component component = renderNode.getComponent();

    boolean passParentFrame = true;

    if (nodeRenderer != null && nodeRenderer.compatibleWithComponent(component)) {
      nodeRenderer.updateComponent(component, parent, indexInParent);
      passParentFrame = !(nodeRenderer instanceof GuiNodeRendererParent);
    } else {
      disposeNodeRenderer();

      final var renderer = createRendererForComponent(component);

      if (renderer != null) {
        nodeRenderer = renderer;
        nodeRenderer.updateComponent(component, parent, indexInParent);
        passParentFrame = !(nodeRenderer instanceof GuiNodeRendererParent);
      }
    }

    final var renderNodeChildren = renderNode.getChildren();

    if (
        nodeRenderer != null && !(nodeRenderer instanceof ChildAwareGuiNodeRenderer)
            && !renderNodeChildren.isEmpty()
    ) {
      // Ensures that if this node is supposed to be a leaf node, we don't try to mount children on
      // it.
      throw new IllegalChildOfLeafRenderer();
    }

    if (renderNodeChildren.size() < children.size()) {
      // There are more GuiNode children than there are component children, so remove some.
      for (int i = children.size() - 1; i >= renderNodeChildren.size(); i--) {
        children.remove(i).dispose();
      }
    }

    final ArrayList<GuiNodeRenderer> childRenderers = new ArrayList<>();

    for (int i = 0; i < renderNodeChildren.size(); i++) {
      final var renderNodeChild = renderNodeChildren.get(i);

      GuiNode childNode;

      if (i < children.size()) {
        childNode = children.get(i);
      } else {
        childNode = new GuiNode();
        children.add(childNode);
      }

      final int childIndexInParent =
          (passParentFrame) ? indexInParent + childRenderers.size() : childRenderers.size();

      if (GuiDebugHelper.isDebug) {
        System.out.printf(
            "%d Building %s%n",
            childIndexInParent,
            renderNodeChild.getComponent().getClass().getSimpleName()
        );
      }

      childRenderers.addAll(childNode.updateComponent(
          renderNodeChild,
          (passParentFrame) ? parent : (GuiNodeRendererParent) nodeRenderer,
          childIndexInParent
      ));
    }

    if (nodeRenderer instanceof ChildAwareGuiNodeRenderer) {
      ((ChildAwareGuiNodeRenderer) nodeRenderer).postRenderChildren(childRenderers);
    }

    if (passParentFrame || nodeRenderer instanceof GuiNodeRenderer) {
      return !(nodeRenderer instanceof GuiNodeRenderer) ? childRenderers
          : ArrayListBuilder.build((GuiNodeRenderer) nodeRenderer);
    } else {
      return new ArrayList<>();
    }
  }

  /**
   * Disposes the currently bound nodeRenderer (if any) and sets it to null.
   */
  public void disposeNodeRenderer() {
    if (nodeRenderer != null) {
      if (nodeRenderer instanceof GuiNodeRenderer) {
        ((GuiNodeRenderer<?>) nodeRenderer).unmount();
      }

      nodeRenderer = null;
    }
  }

  /**
   * Disposes this GuiNode and all of its children.
   */
  public void dispose() {
    disposeNodeRenderer();

    for (GuiNode child : children) {
      child.dispose();
    }
    children.clear();
  }
}
