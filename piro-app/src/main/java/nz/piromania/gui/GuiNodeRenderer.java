package nz.piromania.gui;

import javax.annotation.OverridingMethodsMustInvokeSuper;
import javax.swing.JComponent;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.RenderNode;
import nz.piromania.reactiveengine.component.DirectlyRenderableComponent;

/**
 * Abstract class defining all the necessary methods to be implemented by a renderer that will have
 * no children (aka are leaf nodes).
 *
 * <p>The {@link GuiNode} will enforce this.</p>
 */
public abstract class GuiNodeRenderer<T extends Component & DirectlyRenderableComponent>
    extends BaseGuiNodeRenderer<T> {

  /**
   * Returns the Swing Component that is the root of this GuiNodeRenderer. This is used by the
   * default implementation of {@link GuiNodeRenderer#unmount()} to automatically remove this
   * component from the GUI.
   */
  public abstract JComponent getThisRootGuiComponent();

  /**
   * Automatically manages error-prone and common parts of binding the component to its parent at
   * the appropriate index.
   *
   * <p>
   * Implementers should make sure to call this method in their {@link
   * #updateComponentImpl(Component, GuiNodeRendererParent, int)} methods.
   * </p>
   *
   * @param parent        to bind the component to
   * @param component     to bind
   * @param indexInParent at which the component must be bound
   */
  protected final void bindToParent(
      GuiNodeRendererParent parent,
      java.awt.Component component,
      int indexInParent
  ) {
    boolean mustAddToParent = true;

    final var oldParent = getParent();
    final var newParentContainer = parent.getContainer();

    if (parent.equals(oldParent) && newParentContainer.getComponent(indexInParent)
        .equals(component)) {
      // We have the same parent and the same component bound at the same index.
      mustAddToParent = false;
    } else if (oldParent != null) {
      oldParent.getContainer().remove(component);
    }

    bindToParentImpl(parent, component, indexInParent, mustAddToParent);
  }

  /**
   * Implementation method where the child can choose how to bind to the parent container.
   *
   * @param parent          to bind to
   * @param component       to bind
   * @param indexInParent   to insert the component in to
   * @param mustAddToParent true if the component is currently not bound to the parent
   */
  protected void bindToParentImpl(
      GuiNodeRendererParent parent,
      java.awt.Component component,
      int indexInParent,
      boolean mustAddToParent
  ) {
    if (mustAddToParent) {
      parent.getContainer().add(component, indexInParent);
    }
  }

  /**
   * Called when this GuiNodeRenderer is no longer needed and should be removed from view (and
   * deallocated from memory).
   *
   * <p>This can be caused by a variety of different circumstances.
   * See {@link GuiNode#updateComponent(RenderNode, GuiNodeRendererParent, int)} for details.</p>
   *
   * <p>
   *   NOTE: If this method is overridden in a subclass, this implementation must also be called.
   * </p>
   */
  @OverridingMethodsMustInvokeSuper
  public void unmount() {
    if (getParent() != null) {
      final var parentContainer = getParent().getContainer();
      parentContainer.remove(getThisRootGuiComponent());
      parentContainer.revalidate();
      parentContainer.repaint();
    }
  }
}
