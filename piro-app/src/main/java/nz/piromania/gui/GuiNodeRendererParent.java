package nz.piromania.gui;

import java.awt.Container;
import java.awt.LayoutManager;

/**
 * Methods required to be provided by GuiNodeRenderers that will act as parents to child
 * GuiNodeRenderers.
 */
public interface GuiNodeRendererParent extends ChildAwareGuiNodeRenderer {

  Container getContainer();

  LayoutManager getLayoutManager();
}
