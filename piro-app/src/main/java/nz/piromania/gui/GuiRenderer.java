package nz.piromania.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.LayoutManager;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.function.Consumer;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.ToolTipManager;
import nz.piromania.gui.util.GuiDebugHelper;
import nz.piromania.reactiveengine.RenderNode;
import nz.piromania.reactiveengine.util.VoidCallback;

/**
 * Receives RenderNode trees (through {@link GuiRenderer#accept(RenderNode)}) and renders them into
 * a Swing GUI.
 */
public class GuiRenderer implements Consumer<RenderNode>, GuiNodeRendererParent {

  private final LayoutManager rootFrameLayout = new BorderLayout();
  private final JFrame rootFrame;

  private final Dimension size;
  private final VoidCallback onExit;
  private final GuiNode rootNode;
  private JLabel loadingText;

  private boolean hasExited = false;

  /**
   * Constructs the GuiRenderer with the given window size, and binding the onExit callback when the
   * window is closed.
   *
   * @param windowSize initial window size
   * @param onExit     called when the window is closed
   */
  public GuiRenderer(String title, BufferedImage icon, Dimension windowSize, VoidCallback onExit) {
    if (GuiDebugHelper.isDebug) {
      System.out.println("GUI debugging is enabled.");
    }

    ToolTipManager.sharedInstance().setInitialDelay(0);
    ToolTipManager.sharedInstance().setDismissDelay(200000);

    this.rootFrame = new JFrame();
    this.size = windowSize;
    this.onExit = onExit;

    rootFrame.getContentPane().setLayout(rootFrameLayout);
    loadingText = new JLabel("Loading. Please wait...");
    rootFrame.add(loadingText, BorderLayout.CENTER);

    rootFrame.setTitle(title);
    rootFrame.setIconImage(icon);

    rootFrame.setSize(size);
    rootFrame.setMaximumSize(size);
    rootFrame.setMinimumSize(size);
    rootFrame.setResizable(false);

    rootFrame.setVisible(true);
    rootFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    rootFrame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        hasExited = true;
        onExit.call();
        e.getWindow().dispose();
        super.windowClosing(e);
      }
    });

    this.rootNode = new GuiNode();
  }

  /**
   * Loads a TrueType font from the given <code>fontfile</code> and returns it.
   *
   * <p>
   *     This Font can then be registered for use by using {@link GuiRenderer#registerFont(Font)}.
   * </p>
   *
   * @param fontFile to load
   * @return font in the given fontFile
   */
  public Font loadTrueTypeFont(InputStream fontFile) throws IOException, FontFormatException {
    return Font.createFont(Font.TRUETYPE_FONT, fontFile);
  }

  /**
   * Registers the given <code>font</code> with the GuiRenderer's Swing environment.
   * After registration, font should be able available for use by name.
   *
   * @param font to register
   */
  public void registerFont(Font font) {
    GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(font);
  }

  private void clearLoadingTextIfNecessary() {
    if (loadingText != null) {
      loadingText.setVisible(false);
      rootFrame.remove(loadingText);
      loadingText = null;
    }
  }

  /**
   * Receives render information from the Engine and renders it.
   *
   * @param rootRenderNode of the component tree to render
   * @see nz.piromania.reactiveengine.Engine {@link nz.piromania.reactiveengine.Engine}
   */
  public void accept(RenderNode rootRenderNode) {
    clearLoadingTextIfNecessary();

    rootNode.updateComponent(rootRenderNode, this, 0);

    if (GuiDebugHelper.isDebug) {
      rootFrame.list(System.out, 2);
    }

    rootFrame.repaint();

    System.out.println("Render complete.");
  }

  /**
   * Closes the GUI (if necessary).
   */
  public void exit() {
    if (!hasExited) {
      this.rootFrame.dispose();
    }
  }

  @Override
  public Container getContainer() {
    return rootFrame;
  }

  @Override
  public LayoutManager getLayoutManager() {
    return rootFrameLayout;
  }

  @Override
  public void postRenderChildren(ArrayList<GuiNodeRenderer> children) {

  }
}
