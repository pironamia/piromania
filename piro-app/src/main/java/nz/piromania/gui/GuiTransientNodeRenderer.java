package nz.piromania.gui;

import java.util.ArrayList;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.component.DirectlyRenderableComponent;

/**
 * A NodeRenderer for a Component that draws nothing of its own, instead modifying the frame(s) of
 * its child(ren).
 */
public abstract class GuiTransientNodeRenderer<T extends Component & DirectlyRenderableComponent>
    extends BaseGuiNodeRenderer<T>
    implements ChildAwareGuiNodeRenderer {

  @Override
  protected void updateComponentImpl(T newComponent, GuiNodeRendererParent parent,
      int indexInParent) {

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public abstract void postRenderChildren(ArrayList<GuiNodeRenderer> children);
}
