package nz.piromania.gui.adapter;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.security.InvalidParameterException;
import nz.piromania.reactiveengine.util.Alignment;

/**
 * Used to convert 'alignment' objects between ReactiveEngine representations and Swing
 * representations.
 */
public class AlignmentAdapter {

  /**
   * Converts {@link Alignment} to a {@link GridBagConstraints} alignment value.
   *
   * @param alignment to convert
   * @return GridBagConstraints-supported alignment value
   */
  public static int alignmentToGridBagAnchor(Alignment alignment) {
    return switch (alignment) {
      case TOP_LEFT -> GridBagConstraints.FIRST_LINE_START;
      case TOP_CENTRE -> GridBagConstraints.PAGE_START;
      case TOP_RIGHT -> GridBagConstraints.FIRST_LINE_END;
      case CENTRE_LEFT -> GridBagConstraints.LINE_START;
      case CENTRE -> GridBagConstraints.CENTER;
      case CENTRE_RIGHT -> GridBagConstraints.LINE_END;
      case BOTTOM_LEFT -> GridBagConstraints.LAST_LINE_START;
      case BOTTOM_CENTRE -> GridBagConstraints.PAGE_END;
      case BOTTOM_RIGHT -> GridBagConstraints.LAST_LINE_END;
      default -> throw new InvalidParameterException("A valid alignment must be specified");
    };
  }

  /**
   * Converts {@link Alignment} to a {@link Component} alignment value in the Y direction.
   *
   * @param alignment to convert
   * @return Component-supported alignment value
   */
  @SuppressWarnings("checkstyle:AbbreviationAsWordInName")
  public static float alignmentToYAlignment(Alignment alignment) {
    return switch (alignment) {
      case TOP_LEFT -> Component.TOP_ALIGNMENT;
      case TOP_CENTRE -> Component.TOP_ALIGNMENT;
      case TOP_RIGHT -> Component.TOP_ALIGNMENT;
      case CENTRE_LEFT -> Component.CENTER_ALIGNMENT;
      case CENTRE -> Component.CENTER_ALIGNMENT;
      case CENTRE_RIGHT -> Component.CENTER_ALIGNMENT;
      case BOTTOM_LEFT -> Component.BOTTOM_ALIGNMENT;
      case BOTTOM_CENTRE -> Component.BOTTOM_ALIGNMENT;
      case BOTTOM_RIGHT -> Component.BOTTOM_ALIGNMENT;
      default -> throw new InvalidParameterException("A valid alignment must be specified");
    };
  }

  /**
   * Converts {@link Alignment} to a {@link Component} alignment value in the X direction.
   *
   * @param alignment to convert
   * @return Component-supported alignment value
   */
  @SuppressWarnings("checkstyle:AbbreviationAsWordInName")
  public static float alignmentToXAlignment(Alignment alignment) {
    return switch (alignment) {
      case TOP_LEFT -> Component.LEFT_ALIGNMENT;
      case TOP_CENTRE -> Component.CENTER_ALIGNMENT;
      case TOP_RIGHT -> Component.RIGHT_ALIGNMENT;
      case CENTRE_LEFT -> Component.LEFT_ALIGNMENT;
      case CENTRE -> Component.CENTER_ALIGNMENT;
      case CENTRE_RIGHT -> Component.RIGHT_ALIGNMENT;
      case BOTTOM_LEFT -> Component.LEFT_ALIGNMENT;
      case BOTTOM_CENTRE -> Component.CENTER_ALIGNMENT;
      case BOTTOM_RIGHT -> Component.RIGHT_ALIGNMENT;
      default -> throw new InvalidParameterException("A valid alignment must be specified");
    };
  }
}
