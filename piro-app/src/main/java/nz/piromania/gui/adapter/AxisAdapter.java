package nz.piromania.gui.adapter;

import java.security.InvalidParameterException;
import javax.swing.BoxLayout;
import nz.piromania.reactiveengine.layout.Axis;

/**
 * Used to convert 'axis' objects between ReactiveEngine representations and Swing representations.
 */
public class AxisAdapter {

  /**
   * Converts {@link Axis} to a {@link BoxLayout} axis value.
   *
   * @param axis to convert
   * @return BoxLayout-supported axis value
   */
  public static int axisToBoxLayoutAxis(Axis axis) {
    return switch (axis) {
      case VERTICAL -> BoxLayout.Y_AXIS;
      case HORIZONTAL -> BoxLayout.X_AXIS;
      default -> throw new InvalidParameterException("A valid Axis must be specified");
    };
  }

  /**
   * Converts a {@link BoxLayout} axis value, to a {@link Axis}.
   *
   * @param axis to convert
   * @return {@link Axis} equivalent value
   */
  public static Axis boxLayoutAxisToAxis(int axis) {
    return switch (axis) {
      case BoxLayout.X_AXIS -> Axis.HORIZONTAL;
      case BoxLayout.LINE_AXIS -> Axis.HORIZONTAL;

      case BoxLayout.Y_AXIS -> Axis.VERTICAL;
      case BoxLayout.PAGE_AXIS -> Axis.VERTICAL;

      default -> throw new InvalidParameterException("A valid BoxLayout axis must be specified");
    };
  }
}
