package nz.piromania.gui.adapter;

import java.awt.Dimension;
import nz.piromania.reactiveengine.util.BoxSize;

/**
 * Used to convert 'box size' objects between ReactiveEngine representations and Swing
 * representations.
 */
public class BoxSizeAdapter {
  /**
   * Converts {@link BoxSize} to a {@link Dimension} alignment value.
   *
   * @param size to convert
   * @param fallback value to use for a dimention if <code>size</code> is null for that dimension
   * @return equivalent representation of <code>size</code> (with fallback values taken where
   *         necessary
   */
  public static Dimension boxSizeToDimension(BoxSize size, Dimension fallback) {
    return new Dimension(
        (size.getWidth() != null) ? size.getWidth() : fallback.width,
        (size.getHeight() != null) ? size.getHeight() : fallback.height
    );
  }
}
