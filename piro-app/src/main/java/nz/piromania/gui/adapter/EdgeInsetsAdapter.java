package nz.piromania.gui.adapter;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import nz.piromania.reactiveengine.util.Alignment;
import nz.piromania.reactiveengine.util.EdgeInsets;

/**
 * Used to convert 'edge inset' objects between ReactiveEngine representations and Swing
 * representations.
 */
public class EdgeInsetsAdapter {
  /**
   * Converts {@link EdgeInsets} to a {@link Insets} alignment value.
   *
   * @param padding to convert
   * @return equivalent representation of <code>size</code>
   */
  public static Insets edgeInsetsToInsets(EdgeInsets padding) {
    return new Insets(padding.getTop(), padding.getLeft(), padding.getBottom(), padding.getRight());
  }
}
