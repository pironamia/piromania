package nz.piromania.gui.component;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JTextField;

/**
 * A small extension to the Swing JTextField that adds support for placeholder text.
 */
public class PlaceholderTextField extends JTextField {
  private String placeholder;

  public void setPlaceholder(String placeholder) {
    this.placeholder = placeholder;
  }

  public String getPlaceholder() {
    return placeholder;
  }

  /**
   * Draws the JTextField as usual, then adds the placeholder as an overlay if appropriate.
   * <p>
   *   Credit to https://stackoverflow.com/a/16229082 (viewed 2021-05-20 20:47:00) for inspiration.
   * </p>
   *
   * @param g the current graphics context.
   */
  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);

    if (placeholder != null && placeholder.length() != 0 && getText().length() == 0) {
      final var g2 = (Graphics2D) g;
      final var insets = getInsets();
      g2.setRenderingHint(
          RenderingHints.KEY_ANTIALIASING,
          RenderingHints.VALUE_ANTIALIAS_ON
      );
      g.setColor(getDisabledTextColor());
      g.drawString(
          placeholder,
          insets.left,
          insets.top + g.getFontMetrics().getMaxAscent()
      );
    }
  }
}
