package nz.piromania.gui.component;

import java.awt.Graphics;
import java.awt.LayoutManager;
import javax.swing.JPanel;
import nz.piromania.gui.util.GuiDebugHelper;

/**
 * A JPanel that is transparent by default, but can have a transparent background colour applied.
 *
 * <p>
 * Credit to https://tips4java.wordpress.com/2009/05/31/backgrounds-with-transparency/ for this
 * solution.
 * </p>
 */
@SuppressWarnings("checkstyle:AbbreviationAsWordInName")
public class SemiTransparentJPanel extends JPanel {

  /**
   * Initialises a JPanel that is transparent.
   */
  public SemiTransparentJPanel() {
    super();
    this.setOpaque(false);

    GuiDebugHelper.paintForDebugIfNecessary(this);
  }

  /**
   * Initialises a JPanel that is transparent.
   *
   * @param layout LayoutManager instance to attach
   */
  public SemiTransparentJPanel(LayoutManager layout) {
    super(layout);
    this.setOpaque(false);

    GuiDebugHelper.paintForDebugIfNecessary(this);
  }

  @Override
  protected void paintComponent(Graphics g) {
    g.setColor(getBackground());
    g.fillRect(0, 0, getWidth(), getHeight());
    super.paintComponent(g);
  }
}
