package nz.piromania.gui.component;

import java.awt.LayoutManager;
import javax.swing.JPanel;
import nz.piromania.gui.util.GuiDebugHelper;

/**
 * A JPanel that is transparent by default.
 */
@SuppressWarnings("checkstyle:AbbreviationAsWordInName")
public class TransparentJPanel extends JPanel {

  /**
   * Initialises a JPanel that is transparent.
   */
  public TransparentJPanel() {
    super();
    this.setOpaque(false);

    GuiDebugHelper.paintForDebugIfNecessary(this);
  }

  /**
   * Initialises a JPanel that is transparent.
   *
   * @param layout LayoutManager instance to attach
   */
  public TransparentJPanel(LayoutManager layout) {
    super(layout);
    this.setOpaque(false);

    GuiDebugHelper.paintForDebugIfNecessary(this);
  }
}
