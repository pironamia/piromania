package nz.piromania.gui.exception;

/**
 * Raised when getRenderFrame() is called on a GuiLeafNodeRenderer (which shouldn't have children).
 */
public class IllegalChildOfLeafRenderer extends RuntimeException {
  public IllegalChildOfLeafRenderer() {
    super("Attempted to mount a child on a GuiLeafRendererNode.");
  }
}
