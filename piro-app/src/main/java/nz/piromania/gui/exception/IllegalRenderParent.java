package nz.piromania.gui.exception;

/**
 * Raised when a NodeRenderer is attached as the child of an unsupported parent NodeRenderer.
 */
public class IllegalRenderParent extends RuntimeException {

  public IllegalRenderParent(String message) {
    super(message);
  }
}