package nz.piromania.gui.layout;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.ArrayList;

/**
 * Swing LayoutManager that positions every child to fill the screen. Thus 'stacking' them on top of
 * each other.
 */
public class StackLayoutManager implements LayoutManager {

  private final ArrayList<Component> children = new ArrayList<>();

  /**
   * {@inheritDoc}
   */
  @Override
  public void addLayoutComponent(String name, Component comp) {
    children.add(comp);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void removeLayoutComponent(Component comp) {
    children.remove(comp);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Dimension preferredLayoutSize(Container parent) {
    final Insets insets = parent.getInsets();
    final Dimension dimension = new Dimension();

    for (var child : children) {
      final Dimension childPreferredSize = child.getPreferredSize();

      if (childPreferredSize.width > dimension.width) {
        dimension.width = childPreferredSize.width;
      }
      if (childPreferredSize.height > dimension.height) {
        dimension.height = childPreferredSize.height;
      }
    }

    dimension.width += insets.left + insets.right;
    dimension.height += insets.top + insets.bottom;

    return dimension;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Dimension minimumLayoutSize(Container parent) {
    final Insets insets = parent.getInsets();
    final Dimension dimension = new Dimension();

    for (var child : children) {
      final Dimension childMinimumSize = child.getMinimumSize();

      if (childMinimumSize.width > dimension.width) {
        dimension.width = childMinimumSize.width;
      }
      if (childMinimumSize.height > dimension.height) {
        dimension.height = childMinimumSize.height;
      }
    }

    dimension.width += insets.left + insets.right;
    dimension.height += insets.top + insets.bottom;

    return dimension;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void layoutContainer(Container parent) {
    final Insets insets = parent.getInsets();
    final int maxWidth = parent.getWidth() - (insets.left + insets.right);
    final int maxHeight = parent.getHeight() - (insets.top + insets.bottom);

    for (var component : parent.getComponents()) {
      if (component.isVisible()) {
        component.setLocation(0, 0);
        component.setSize(maxWidth, maxHeight);
      }
    }
  }
}
