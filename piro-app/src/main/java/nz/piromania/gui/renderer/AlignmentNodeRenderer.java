package nz.piromania.gui.renderer;

import java.awt.BorderLayout;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JPanel;
import nz.piromania.gui.GuiNodeRenderer;
import nz.piromania.gui.GuiTransientNodeRenderer;
import nz.piromania.gui.adapter.AlignmentAdapter;
import nz.piromania.reactiveengine.component.AlignmentComponent;
import nz.piromania.reactiveengine.util.Alignment;

/**
 * Renderer for {@link AlignmentComponent}. Sets the X and Y alignment of its child.
 */
public class AlignmentNodeRenderer extends GuiTransientNodeRenderer<AlignmentComponent> {
  private void updateAlignmentIfNecessary(JComponent renderComponent, Alignment alignment) {
    final float xAlignment = AlignmentAdapter.alignmentToXAlignment(alignment);
    final float yAlignment = AlignmentAdapter.alignmentToYAlignment(alignment);

    renderComponent.setAlignmentX(xAlignment);
    renderComponent.setAlignmentY(yAlignment);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void postRenderChildren(ArrayList<GuiNodeRenderer> children) {

    if (getCurrentComponent() != null) {
      for (var child : children) {
        updateAlignmentIfNecessary(
            child.getThisRootGuiComponent(),
            getCurrentComponent().getAlignment()
        );
      }
    }
  }
}
