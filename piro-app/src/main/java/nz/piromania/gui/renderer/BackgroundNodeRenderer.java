package nz.piromania.gui.renderer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.LayoutManager;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JPanel;
import nz.piromania.gui.GuiContainerNodeRenderer;
import nz.piromania.gui.GuiNodeRenderer;
import nz.piromania.gui.GuiNodeRendererParent;
import nz.piromania.gui.component.SemiTransparentJPanel;
import nz.piromania.gui.util.GuiDebugHelper;
import nz.piromania.reactiveengine.component.BackgroundComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;

/**
 * Renderer for {@link PaddingComponent}. Applies a border around the outside of a frame, and forces
 * children to fit that frame.
 */
public class BackgroundNodeRenderer extends GuiContainerNodeRenderer<BackgroundComponent> {

  private final JPanel frame = new SemiTransparentJPanel(new BorderLayout());
  private Color lastBackground;

  /**
   * Initialises a ButtonNodeRenderer.
   */
  public BackgroundNodeRenderer() {
    GuiDebugHelper.paintForDebugIfNecessary(frame);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Container getContainer() {
    return frame;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public LayoutManager getLayoutManager() {
    return frame.getLayout();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public JComponent getThisRootGuiComponent() {
    return frame;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void updateComponentImpl(
      BackgroundComponent newComponent,
      GuiNodeRendererParent parent,
      int indexInParent
  ) {
    bindToParent(parent, frame, indexInParent);

    if (!newComponent.getBackgroundColour().equals(lastBackground)) {
      frame.setBackground(newComponent.getBackgroundColour());
      lastBackground = newComponent.getBackgroundColour();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void postRenderChildren(ArrayList<GuiNodeRenderer> children) {
    frame.revalidate();
    frame.repaint();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void unmount() {
    super.unmount();

    frame.setVisible(false);
  }
}
