package nz.piromania.gui.renderer;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JPanel;
import nz.piromania.gui.GuiContainerNodeRenderer;
import nz.piromania.gui.GuiNodeRenderer;
import nz.piromania.gui.GuiNodeRendererParent;
import nz.piromania.gui.component.TransparentJPanel;
import nz.piromania.reactiveengine.component.CentreComponent;

/**
 * Renderer for the {@link CentreComponent}. Positions its children in the centre of its parent.
 */
public class CentreNodeRenderer extends GuiContainerNodeRenderer<CentreComponent> {

  private final GridBagLayout layout = new GridBagLayout();
  private final JPanel frame = new TransparentJPanel();

  private final LayoutManager childLayout = new GridBagLayout();
  private final JPanel childFrame = new TransparentJPanel();

  /**
   * Constructs the CentreNodeRenderer. Initialising its internal layout.
   */
  public CentreNodeRenderer() {
    frame.setLayout(layout);
    childFrame.setLayout(childLayout);

    final var childConstraints = new GridBagConstraints();
    childConstraints.anchor = GridBagConstraints.CENTER;
    frame.add(childFrame, childConstraints);
  }

  @Override
  public java.awt.Container getContainer() {
    return childFrame;
  }

  @Override
  public LayoutManager getLayoutManager() {
    return childLayout;
  }

  @Override
  public JComponent getThisRootGuiComponent() {
    return frame;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void bindToParentImpl(GuiNodeRendererParent parent, Component component,
      int indexInParent,
      boolean mustAddToParent) {
    if (mustAddToParent) {
      if (parent.getLayoutManager() instanceof GridBagLayout) {
        final var layoutConstraints = new GridBagConstraints();
        layoutConstraints.anchor = GridBagConstraints.CENTER;
        parent.getContainer().add(frame, layoutConstraints, indexInParent);
      } else {
        frame.setAlignmentX(0.5f);
        frame.setAlignmentY(0.5f);
        parent.getContainer().add(frame, indexInParent);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void updateComponentImpl(CentreComponent newComponent, GuiNodeRendererParent parent,
      int indexInParent) {
    bindToParent(parent, frame, indexInParent);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void postRenderChildren(ArrayList<GuiNodeRenderer> children) {
    frame.setVisible(true);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void unmount() {
    super.unmount();
    frame.setVisible(false);
  }
}
