package nz.piromania.gui.renderer;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import nz.piromania.gui.GuiContainerNodeRenderer;
import nz.piromania.gui.GuiNodeRenderer;
import nz.piromania.gui.GuiNodeRendererParent;
import nz.piromania.gui.util.GuiDebugHelper;
import nz.piromania.reactiveengine.component.ButtonComponent;
import nz.piromania.reactiveengine.component.ClickableComponent;

/**
 * Renderer for the {@link ClickableComponent}.
 */
public class ClickableNodeRenderer extends GuiContainerNodeRenderer<ClickableComponent>
    implements ActionListener {

  private final JButton button = new JButton();

  /**
   * Initialises a ButtonNodeRenderer.
   */
  public ClickableNodeRenderer() {
    button.setLayout(new FlowLayout());
    button.addActionListener(this);
    button.setActionCommand("click");
    button.setBorder(BorderFactory.createEmptyBorder());
    button.setContentAreaFilled(false);

    button.setAlignmentX(0);
    GuiDebugHelper.paintForDebugIfNecessary(button);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Container getContainer() {
    return button;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public LayoutManager getLayoutManager() {
    return button.getLayout();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public JComponent getThisRootGuiComponent() {
    return button;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void updateComponentImpl(
      ClickableComponent newComponent,
      GuiNodeRendererParent parent,
      int indexInParent
  ) {
    bindToParent(parent, button, indexInParent);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void postRenderChildren(ArrayList<GuiNodeRenderer> children) {
    button.revalidate();
    button.repaint();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    // Called when the button is clicked.
    if (e.getActionCommand().equals("click")) {
      getCurrentComponent().getOnClick().call();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void unmount() {
    super.unmount();

    button.setVisible(false);
  }
}
