package nz.piromania.gui.renderer;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JPanel;
import nz.piromania.gui.GuiContainerNodeRenderer;
import nz.piromania.gui.GuiNodeRenderer;
import nz.piromania.gui.GuiNodeRendererParent;
import nz.piromania.gui.adapter.AlignmentAdapter;
import nz.piromania.gui.adapter.BoxSizeAdapter;
import nz.piromania.gui.adapter.EdgeInsetsAdapter;
import nz.piromania.gui.component.SemiTransparentJPanel;
import nz.piromania.gui.component.TransparentJPanel;
import nz.piromania.gui.util.GuiDebugHelper;
import nz.piromania.reactiveengine.component.ContainerComponent;

/**
 * Renderer for the {@link ContainerComponent}.
 */
public class ContainerNodeRenderer extends GuiContainerNodeRenderer<ContainerComponent> {

  private final GridBagLayout layout = new GridBagLayout();
  private final JPanel frame = new SemiTransparentJPanel(layout);
  private final GridBagConstraints layoutConstraints = new GridBagConstraints();

  private final GridBagLayout childLayout = new GridBagLayout();
  private final JPanel childContainer = new TransparentJPanel(childLayout);
  private final GridBagConstraints childContainerConstraints = new GridBagConstraints();

  /**
   * Initialises a ContainerNodeRenderer.
   */
  public ContainerNodeRenderer() {
    frame.add(childContainer);
    childContainer.setVisible(true);

    GuiDebugHelper.paintForDebugIfNecessary(frame);
  }

  @Override
  public java.awt.Container getContainer() {
    return childContainer;
  }

  @Override
  public GridBagLayout getLayoutManager() {
    return childLayout;
  }

  @Override
  public JComponent getThisRootGuiComponent() {
    return frame;
  }

  @Override
  protected void updateComponentImpl(ContainerComponent newComponent,
      GuiNodeRendererParent parent, int indexInParent) {
    bindToParent(parent, frame, indexInParent);

    final var newPadding = EdgeInsetsAdapter.edgeInsetsToInsets(newComponent.padding);
    final var newAlignment = AlignmentAdapter.alignmentToGridBagAnchor(newComponent.alignment);

    if (
        !newPadding.equals(childContainerConstraints.insets)
            || newAlignment != childContainerConstraints.anchor
    ) {
      // Time for a new layout constraints...
      childContainerConstraints.insets = newPadding;
      childContainerConstraints.anchor = newAlignment;

      childContainer.setAlignmentX(AlignmentAdapter.alignmentToXAlignment(newComponent.alignment));
      childContainer.setAlignmentY(AlignmentAdapter.alignmentToYAlignment(newComponent.alignment));

      layout.setConstraints(childContainer, childContainerConstraints);
    }

    final var currentComponent = getCurrentComponent();

    if (currentComponent == null
        || currentComponent.backgroundColour != newComponent.backgroundColour) {
      frame.setBackground(
          (newComponent.backgroundColour != null) ? newComponent.backgroundColour
              : new Color(255, 0, 0, 0)
      );
    }

    if (currentComponent == null
        || (currentComponent.size != null && !currentComponent.size.equals(newComponent.size))) {
      frame.setPreferredSize(
          (newComponent.size == null) ? null
              : BoxSizeAdapter.boxSizeToDimension(newComponent.size, frame.getPreferredSize())
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void postRenderChildren(ArrayList<GuiNodeRenderer> children) {
    frame.setVisible(true);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void unmount() {
    super.unmount();

    frame.setVisible(false);
  }
}
