package nz.piromania.gui.renderer;

import java.util.ArrayList;
import nz.piromania.gui.GuiNodeRenderer;
import nz.piromania.gui.GuiTransientNodeRenderer;
import nz.piromania.gui.exception.IllegalRenderParent;
import nz.piromania.gui.layout.RelativeLayout;
import nz.piromania.reactiveengine.component.list.FlexibleComponent;

/**
 * A transient node renderer that ensures that its children (there <i>should</i> only ever be one)
 * is assigned the correct flex factor.
 */
public class FlexibleNodeRenderer extends GuiTransientNodeRenderer<FlexibleComponent> {

  private void updateFlexIfNecessary(GuiNodeRenderer child, int flex) {
    if (child.getParent().getLayoutManager() instanceof RelativeLayout) {
      final RelativeLayout layout = (RelativeLayout) child.getParent().getLayoutManager();
      final var currentConstraints = layout.getConstraints(child.getThisRootGuiComponent());

      if (currentConstraints == null || currentConstraints != (float) flex) {
        // Flex constraints need updating
        layout.addLayoutComponent(child.getThisRootGuiComponent(), (float) flex);
      }
    } else {
      throw new IllegalRenderParent("FlexibleComponent must be a direct child of a ListComponent.");
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void postRenderChildren(ArrayList<GuiNodeRenderer> children) {

    if (getCurrentComponent() != null) {
      for (var child : children) {
        updateFlexIfNecessary(
            child,
            getCurrentComponent().getFlex()
        );
      }
    }
  }
}
