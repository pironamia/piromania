package nz.piromania.gui.renderer;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import net.coobird.thumbnailator.Thumbnails;
import nz.piromania.gui.GuiNodeRenderer;
import nz.piromania.gui.GuiNodeRendererParent;
import nz.piromania.gui.adapter.BoxSizeAdapter;
import nz.piromania.gui.util.GuiDebugHelper;
import nz.piromania.reactiveengine.component.ImageComponent;

/**
 * Renderer for {@link ImageComponent}.
 */
public class ImageNodeRenderer extends GuiNodeRenderer<ImageComponent> {

  private final JLabel label;

  public ImageNodeRenderer() {
    this.label = new JLabel();
    GuiDebugHelper.paintForDebugIfNecessary(label);
  }

  @Override
  protected void updateComponentImpl(ImageComponent newComponent, GuiNodeRendererParent parent,
      int indexInParent) {
    bindToParent(parent, label, indexInParent);

    if (
        getCurrentComponent() == null
            || getCurrentComponent().getImage() != newComponent.getImage()
            || (getCurrentComponent().getSize() == null && newComponent.getSize() != null)
            || (
                getCurrentComponent().getSize() != null
                    && !getCurrentComponent().getSize().equals(newComponent.getSize())
            )
    ) {
      BufferedImage imageToWrite = newComponent.getImage();

      if (newComponent.getSize() != null) {
        // Resize the image.
        if (
            newComponent.getSize().getWidth() == null
                && newComponent.getSize().getHeight() == null
        ) {
          throw new IllegalArgumentException(
              "ImageComponent requires that at least one of height and width are specified"
                  + ", or size is null."
          );
        }

        final Integer width = newComponent.getSize().getWidth();
        final Integer height = newComponent.getSize().getHeight();

        try {
          imageToWrite = Thumbnails.of(newComponent.getImage())
              .size(
                  (width == null) ? Integer.MAX_VALUE : width,
                  (height == null) ? Integer.MAX_VALUE : height
              )
              .asBufferedImage();
        } catch (IOException ex) {
          System.out.println("Failed to resize image. Got error:");
          ex.printStackTrace();
        }

        label.setPreferredSize(new Dimension(imageToWrite.getWidth(), imageToWrite.getHeight()));
      } else {
        label.setPreferredSize(null);
      }

      final ImageIcon icon = new ImageIcon(imageToWrite);
      label.setIcon(icon);
    }

    label.setVisible(true);
    label.revalidate();
    label.repaint();
  }

  @Override
  public JComponent getThisRootGuiComponent() {
    return label;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void unmount() {
    super.unmount();

    label.setVisible(false);
  }
}
