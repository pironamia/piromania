package nz.piromania.gui.renderer;

import java.awt.Container;
import java.awt.LayoutManager2;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JPanel;
import nz.piromania.gui.GuiContainerNodeRenderer;
import nz.piromania.gui.GuiNodeRenderer;
import nz.piromania.gui.GuiNodeRendererParent;
import nz.piromania.gui.adapter.AxisAdapter;
import nz.piromania.gui.component.TransparentJPanel;
import nz.piromania.gui.layout.RelativeLayout;
import nz.piromania.gui.util.GuiDebugHelper;
import nz.piromania.reactiveengine.component.list.ListComponent;

/**
 * Renderer for the {@link ListComponent}. Lays children in a row (or column) along its main axis.
 */
public class ListNodeRenderer extends GuiContainerNodeRenderer<ListComponent> {

  private final JPanel rootPanel = new TransparentJPanel();
  private RelativeLayout rootLayout;

  /**
   * Instantiates a new ListNodeRenderer, configuring the BoxLayout to vertical.
   */
  public ListNodeRenderer() {
    rootLayout = new RelativeLayout(RelativeLayout.Y_AXIS);
    rootLayout.setAlignment(RelativeLayout.COMPONENT);
    rootLayout.setFill(true);
    //rootLayout.setFillGap(7);
    rootPanel.setAlignmentY(0);
    rootPanel.setAlignmentX(0.5f);

    rootPanel.setLayout(rootLayout);

    GuiDebugHelper.paintForDebugIfNecessary(rootPanel);
  }

  @Override
  public Container getContainer() {
    return rootPanel;
  }

  @Override
  public LayoutManager2 getLayoutManager() {
    return rootLayout;
  }

  @Override
  public JComponent getThisRootGuiComponent() {
    return rootPanel;
  }

  @Override
  protected void updateComponentImpl(ListComponent newComponent, GuiNodeRendererParent parent,
      int indexInParent) {
    bindToParent(parent, rootPanel, indexInParent);

    final int newMainAxis = AxisAdapter.axisToBoxLayoutAxis(newComponent.getMainAxis());

    if (newMainAxis != rootLayout.getAxis()) {
      rootLayout.setAxis(newMainAxis);
      rootLayout.setFill(true);

      // Left align on main axis, centre align on cross axis
      switch (newComponent.getMainAxis()) {
        case HORIZONTAL -> {
          rootPanel.setAlignmentX(0);
          rootPanel.setAlignmentY(0.5f);
        }
        case VERTICAL -> {
          rootPanel.setAlignmentX(0.5f);
          rootPanel.setAlignmentY(0);
        }
        default -> throw new RuntimeException("Illegal null value for ListNode.mainAxis found.");
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void postRenderChildren(ArrayList<GuiNodeRenderer> children) {
    if (rootPanel.isVisible()) {
      rootPanel.revalidate();
      rootPanel.repaint();
    } else {
      rootPanel.setVisible(true);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void unmount() {
    super.unmount();

    rootPanel.setVisible(false);
  }
}
