package nz.piromania.gui.renderer;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.LayoutManager;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import nz.piromania.gui.GuiContainerNodeRenderer;
import nz.piromania.gui.GuiNodeRenderer;
import nz.piromania.gui.GuiNodeRendererParent;
import nz.piromania.gui.component.TransparentJPanel;
import nz.piromania.gui.util.GuiDebugHelper;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.util.EdgeInsets;

/**
 * Renderer for {@link PaddingComponent}. Applies a border around the outside of a frame, and forces
 * children to fit that frame.
 */
public class PaddingNodeRenderer extends GuiContainerNodeRenderer<PaddingComponent> {

  private final JPanel frame = new TransparentJPanel(new BorderLayout());
  private EdgeInsets lastPadding;

  /**
   * Initialises a ButtonNodeRenderer.
   */
  public PaddingNodeRenderer() {
    GuiDebugHelper.paintForDebugIfNecessary(frame);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Container getContainer() {
    return frame;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public LayoutManager getLayoutManager() {
    return frame.getLayout();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public JComponent getThisRootGuiComponent() {
    return frame;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void updateComponentImpl(
      PaddingComponent newComponent,
      GuiNodeRendererParent parent,
      int indexInParent
  ) {
    bindToParent(parent, frame, indexInParent);

    final EdgeInsets padding = newComponent.getPadding();

    if (!padding.equals(lastPadding)) {
      frame.setBorder(BorderFactory.createEmptyBorder(
          padding.getTop(),
          padding.getLeft(),
          padding.getBottom(),
          padding.getRight()
      ));
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void postRenderChildren(ArrayList<GuiNodeRenderer> children) {
    frame.revalidate();
    frame.repaint();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void unmount() {
    super.unmount();

    frame.setVisible(false);
  }
}
