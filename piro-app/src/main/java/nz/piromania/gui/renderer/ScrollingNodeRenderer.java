package nz.piromania.gui.renderer;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.LayoutManager;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import nz.piromania.gui.GuiContainerNodeRenderer;
import nz.piromania.gui.GuiNodeRenderer;
import nz.piromania.gui.GuiNodeRendererParent;
import nz.piromania.gui.component.TransparentJPanel;
import nz.piromania.gui.util.GuiDebugHelper;
import nz.piromania.reactiveengine.component.ScrollingComponent;

/**
 * Renderer for {@link ScrollingComponent}.
 */
public class ScrollingNodeRenderer extends GuiContainerNodeRenderer<ScrollingComponent> {

  private final JPanel contentFrame = new TransparentJPanel(new BorderLayout());
  private final JScrollPane scrollPane = new JScrollPane();

  /**
   * Initialises a ScrollingNodeRenderer.
   */
  public ScrollingNodeRenderer() {
    scrollPane.setViewportView(contentFrame);
    scrollPane.setOpaque(false);
    scrollPane.getViewport().setOpaque(false);
    scrollPane.setBorder(BorderFactory.createEmptyBorder());

    scrollPane.getVerticalScrollBar().setUnitIncrement(50);

    GuiDebugHelper.paintForDebugIfNecessary(contentFrame);
    GuiDebugHelper.paintForDebugIfNecessary(scrollPane);
  }

  @Override
  public Container getContainer() {
    return contentFrame;
  }

  @Override
  public LayoutManager getLayoutManager() {
    return contentFrame.getLayout();
  }

  @Override
  public JComponent getThisRootGuiComponent() {
    return scrollPane;
  }

  @Override
  protected void updateComponentImpl(ScrollingComponent newComponent,
      GuiNodeRendererParent parent, int indexInParent) {
    bindToParent(parent, scrollPane, indexInParent);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void postRenderChildren(ArrayList<GuiNodeRenderer> children) {
    contentFrame.revalidate();
    contentFrame.repaint();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void unmount() {
    super.unmount();

    contentFrame.setVisible(false);
  }
}
