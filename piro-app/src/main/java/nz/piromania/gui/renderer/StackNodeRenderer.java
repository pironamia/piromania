package nz.piromania.gui.renderer;

import java.awt.Container;
import java.awt.LayoutManager;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JPanel;
import nz.piromania.gui.GuiContainerNodeRenderer;
import nz.piromania.gui.GuiNodeRenderer;
import nz.piromania.gui.GuiNodeRendererParent;
import nz.piromania.gui.component.TransparentJPanel;
import nz.piromania.gui.layout.StackLayoutManager;
import nz.piromania.reactiveengine.component.StackComponent;

/**
 * Renderer for {@link StackComponent}. Lays children out in a 'stack' on top of each other using
 * {@link StackLayoutManager}.
 */
public class StackNodeRenderer extends GuiContainerNodeRenderer<StackComponent> {

  private final LayoutManager layout = new StackLayoutManager();
  private final JPanel frame = new TransparentJPanel(layout);

  public StackNodeRenderer() {
  }

  @Override
  public Container getContainer() {
    return frame;
  }

  @Override
  public LayoutManager getLayoutManager() {
    return frame.getLayout();
  }

  @Override
  public JComponent getThisRootGuiComponent() {
    return frame;
  }

  @Override
  protected void updateComponentImpl(StackComponent newComponent, GuiNodeRendererParent parent,
      int indexInParent) {
    bindToParent(parent, frame, indexInParent);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void postRenderChildren(ArrayList<GuiNodeRenderer> children) {
    frame.revalidate();
    frame.repaint();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void unmount() {
    super.unmount();
  }
}
