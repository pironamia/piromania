package nz.piromania.gui.renderer;

import java.awt.Font;
import javax.swing.JComponent;
import javax.swing.JLabel;
import nz.piromania.gui.GuiNodeRenderer;
import nz.piromania.gui.GuiNodeRendererParent;
import nz.piromania.gui.util.GuiDebugHelper;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.component.text.style.FontStyle;
import nz.piromania.reactiveengine.component.text.style.FontWeight;
import nz.piromania.reactiveengine.component.text.style.TextStyle;

/**
 * Renderer for {@link TextSpan} (and, indirectly, the more commonly-used {@link
 * nz.piromania.reactiveengine.component.text.TextComponent}).
 *
 * <p>
 * Note: Custom fonts specified in the TextStyle are looked up by family name. If the specified font
 * is not available, the font used in the previous render may continue to be used.
 * </p>
 */
public class TextNodeRenderer extends GuiNodeRenderer<TextSpan> {

  private final JLabel label;
  private TextStyle lastStyle;

  public TextNodeRenderer() {
    this.label = new JLabel();
    GuiDebugHelper.paintForDebugIfNecessary(label);
  }

  @Override
  protected void updateComponentImpl(TextSpan newComponent, GuiNodeRendererParent parent,
      int indexInParent) {
    bindToParent(parent, label, indexInParent);

    if (!label.getText().equals(newComponent.getText())) {
      String text = newComponent.getText();
      if (newComponent.isMultiLine()) {
        text = String.format(
            "<html>%s</html>",
            newComponent.getText().replaceAll("\n", "<br/>")
        );
      }
      label.setText(text);
    }

    final var style = newComponent.getStyle();

    if (!style.equalsExcludingColour(lastStyle)) {
      Font font = label.getFont();
      if (!style.getFontFamily().equals(TextStyle.DEFAULT_FAMILY)) {
        font = new Font(style.getFontFamily(), Font.PLAIN, style.getFontSize());
      }

      int fontStyle = Font.PLAIN;

      if (style.getFontStyle() == FontStyle.ITALIC) {
        fontStyle |= Font.ITALIC;
      }
      if (style.getFontWeight() == FontWeight.BOLD) {
        fontStyle |= Font.BOLD;
      }

      font = font.deriveFont(fontStyle, style.getFontSize());
      label.setFont(font);
    }

    if (lastStyle == null || !style.getColour().equals(lastStyle.getColour())) {
      label.setForeground(style.getColour());
    }

    lastStyle = style;

    label.setVisible(true);
    label.revalidate();
    label.repaint();
  }

  @Override
  public JComponent getThisRootGuiComponent() {
    return label;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void unmount() {
    super.unmount();

    label.setVisible(false);
  }
}
