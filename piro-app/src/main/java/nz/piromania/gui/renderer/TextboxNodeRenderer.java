package nz.piromania.gui.renderer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import nz.piromania.gui.GuiNodeRenderer;
import nz.piromania.gui.GuiNodeRendererParent;
import nz.piromania.gui.component.PlaceholderTextField;
import nz.piromania.gui.util.GuiDebugHelper;
import nz.piromania.reactiveengine.component.input.TextboxComponent;

/**
 * Renderer for {@link TextboxComponent}.
 */
public class TextboxNodeRenderer extends GuiNodeRenderer<TextboxComponent> implements DocumentListener,
    ActionListener {

  private final PlaceholderTextField field = new PlaceholderTextField();

  /**
   * Initialises a TextboxRenderer.
   */
  public TextboxNodeRenderer() {
    field.getDocument().addDocumentListener(this);
    field.addActionListener(this);

    GuiDebugHelper.paintForDebugIfNecessary(field);
  }

  @Override
  public JComponent getThisRootGuiComponent() {
    return field;
  }

  @Override
  protected void updateComponentImpl(TextboxComponent newComponent, GuiNodeRendererParent parent,
      int indexInParent) {
    bindToParent(parent, field, indexInParent);

    if (!newComponent.getValue().equals(field.getText())) {
      field.setText(newComponent.getValue());
    }

    final String placeholder = newComponent.getPlaceholder();
    if (placeholder == null || !placeholder.equals(field.getPlaceholder())) {
      field.setPlaceholder(placeholder);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void unmount() {
    super.unmount();

    field.setVisible(false);
  }

  /**
   * Notifies via the current component's onChange method when the value changes.
   */
  private void notifyComponentOfValueUpdate() {
    if (getCurrentComponent() != null) {
      getCurrentComponent().getOnChange().accept(field.getText());
    }
  }

  @Override
  public void insertUpdate(DocumentEvent documentEvent) {
    this.notifyComponentOfValueUpdate();
  }

  @Override
  public void removeUpdate(DocumentEvent documentEvent) {
    this.notifyComponentOfValueUpdate();
  }

  @Override
  public void changedUpdate(DocumentEvent documentEvent) {
    this.notifyComponentOfValueUpdate();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    if (getCurrentComponent() != null && getCurrentComponent().getOnSubmit() != null) {
      getCurrentComponent().getOnSubmit().accept(field.getText());
    }
  }
}
