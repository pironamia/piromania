package nz.piromania.gui.renderer;

import java.util.ArrayList;
import javax.swing.JComponent;
import nz.piromania.gui.GuiNodeRenderer;
import nz.piromania.gui.GuiTransientNodeRenderer;
import nz.piromania.reactiveengine.component.TooltipComponent;

/**
 * Renderer for {@link TooltipComponent}. Displays a tooltip on all GuiNodeRenderers that are
 * attached as direct children.
 */
public class TooltipNodeRenderer extends GuiTransientNodeRenderer<TooltipComponent> {

  private void updateTooltipIfNecessary(JComponent renderComponent, String tooltipText) {
    if (renderComponent.getToolTipText() == null
        || !renderComponent.getToolTipText().equals(tooltipText)) {
      renderComponent.setToolTipText(tooltipText);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void postRenderChildren(ArrayList<GuiNodeRenderer> children) {
    if (getCurrentComponent() != null) {
      final String tooltipText = getCurrentComponent().getTooltip().getHtml();

      for (var child : children) {
        updateTooltipIfNecessary(child.getThisRootGuiComponent(), tooltipText);
      }
    }
  }
}
