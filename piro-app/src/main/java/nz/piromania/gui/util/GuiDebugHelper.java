package nz.piromania.gui.util;

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JComponent;

/**
 * A helper class that contains common GUI debugging functions. E.g. {@link
 * #paintForDebugIfNecessary(JComponent)} which paints a colourful border around the GUI component
 * when in debugging mode.
 *
 * <p>
 * To enable debugging, pass <code>-Ddebug_ui=true</code> to the JVM.
 * </p>
 */
public class GuiDebugHelper {

  private static final Color[] colours = {Color.BLUE, Color.CYAN, Color.GREEN, Color.MAGENTA,
      Color.ORANGE, Color.PINK, Color.RED, Color.YELLOW};
  private static int nextColourIndex = 0;

  public static final boolean isDebug = Boolean.getBoolean("debug_ui");

  /**
   * Paints the given component with a colourful border if debugging is enabled.
   *
   * @param component to paint border around
   */
  public static void paintForDebugIfNecessary(JComponent component) {
    if (isDebug) {
      component.setBorder(BorderFactory.createCompoundBorder(
          BorderFactory.createLineBorder(colours[nextColourIndex]),
          component.getBorder()));

      nextColourIndex++;
      nextColourIndex %= colours.length;
    }
  }
}
