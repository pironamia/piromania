package nz.piromania.reactiveengine;

import java.util.HashMap;
import nz.piromania.reactiveengine.exception.KeyNotFoundException;

/**
 * Context for the current build as inherited from a ComponentState's parents.
 */
public class BuildContext {
  private final HashMap<Class<?>, Object> availableData;

  protected BuildContext(HashMap<Class<?>, Object> parentAvailableData) {
    this.availableData = parentAvailableData;
  }

  public BuildContext() {
    this.availableData = new HashMap<>();
  }

  /**
   * Get data of the given type if it is available in this BuildContext.
   *
   * @param <T> type to get
   * @param type to get
   * @return object stored in this <code>BuildContext</code> of that exact type
   * @throws KeyNotFoundException when there is nothing of that exact type stored in this
   *                             <code>BuildContext</code>
   */
  public <T> T getInheritedDataOfExactType(Class<T> type) throws KeyNotFoundException {
    if (this.availableData.containsKey(type)) {
      return (T) this.availableData.get(type);
    } else {
      throw new KeyNotFoundException("type", type, "There is no inherited data of this type.");
    }
  }

  /**
   * Insert data (potentially overriding existing data) into the BuildContext.
   *
   * @param data The data to insert. May overwrite existing data if they have the same exact class.
   */
  public void provideInheritedData(Object data) {
    this.availableData.put(data.getClass(), data);
  }

  protected BuildContext copy() {
    return new BuildContext(new HashMap<>(this.availableData));
  }
}
