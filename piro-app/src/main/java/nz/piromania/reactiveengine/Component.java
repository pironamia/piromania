package nz.piromania.reactiveengine;

/**
 * An <b>immutable</b> logical UI element.
 *
 * <p>
 *     These correspond 1:1 with a ComponentState, which is built into many components to
 *     describe a component tree. See the reactiveengine README for details.
 * </p>
 */
public abstract class Component {
  public ComponentKey getKey() {
    return null;
  }

  /**
   * Called to create the <code>ComponentState</code> that represents this <code>Component</code>.
   * this should be an instance of <code>ComponentState</code> with T as the subclass of Component
   * that uses this ComponentState.
   * <p>
   *     <b>Every subclass, <code>S</code>, MUST return instances of
   *     <code>ComponentState&lt;S&gt;</code></b>.
   * </p>
   *
   * @return component state for this component
   */
  @SuppressWarnings("rawtypes")
  protected abstract ComponentState createState();
}