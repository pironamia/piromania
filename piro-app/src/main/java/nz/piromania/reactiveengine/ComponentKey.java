package nz.piromania.reactiveengine;

/**
 * A key that can be used to identify a Component.
 * <p>
 *     Available to resolve conflicts when components of the same class don't want to be mounted
 *     to the same ComponentState. As a drawbreaker, the keys of the components are compared.
 * </p>
 *
 * @see ComponentState#shouldMountWithComponent for details of how <code>ComponentKeys</code> are
 *     used for drawbreaking.
 */
public interface ComponentKey  {
  boolean equals(ComponentKey other);
}
