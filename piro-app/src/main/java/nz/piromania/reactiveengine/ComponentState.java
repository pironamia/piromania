package nz.piromania.reactiveengine;

import java.util.ArrayList;

/**
 * A <b>mutable</b> object that can contain Component-local state, and 'builds' into many
 * components at build time.
 *
 * <p>
 *     The ComponentState for a component may live across multiple builds and state changes,
 *     whereas Components are created fresh every build.<br>
 *     ComponentStates are connected to the appropriate Component before being built (note that the
 *     data stored inside the 'current component' may change as the component changes - see
 *     didChangeDependencies for details).
 * </p>
 *
 * <div>
 *     <p>Lifecycle of a ComponentState:</p>
 *     <ol>
 *         <li>Constructed.</li>
 *         <li>Component connected.</li>
 *         <li>initState() is called.</li>
 *         <li>Lifecycle Loop is entered:</li>
 *         <li><ol>
 *             <li>build() is called.</li>
 *             <li>didChangeDependencies() is be called if appropriate.</li>
 *         </ol></li>
 *         <li>dispose() is called.</li>
 *     </ol>
 * </div>
 */
public abstract class ComponentState<T extends Component> {
  private boolean isDirty = true;
  private T component;

  /**
   * Sets the component, potentially calling didChangeDependencies if necessary.
   * <p>Called by the <code>Engine</code> when the Component is changed.</p>
   *
   * @param component to be bound to this <code>ComponentState</code>
   */
  final void setComponent(T component) {
    final T oldComponent = this.component;
    this.component = component;

    if (oldComponent != null && oldComponent != component) {
      this.didChangeDependencies(oldComponent);
    }
  }

  /**
   * Gets the current component, so e.g. parameters passed into it can be used in the build.
   *
   * @return the component that is currently bound to this state
   */
  public final T getComponent() {
    return this.component;
  }

  final boolean getIsDirty() {
    return this.isDirty;
  }

  /**
   * Called by the <code>Engine</code>
   * after this <code>ComponentState</code>'s update has been processed.
   */
  final void clearDirt() {
    this.isDirty = false;
  }

  /**
   * Called by <code>setState</code> to notify the <code>Engine</code> that this
   * <code>ComponentState</code> needs an update.
   */
  private void setDirty() {
    this.isDirty = true;
  }

  /**
   * Returns whether or not this state should mount with the new component.
   * <p>
   *   This takes the following heuristics into account:
   *   <ul>
   *     <li>
   *         Component type (note that it should be guaranteed that <code>newComponent</code> is
   *         of the type <code>&lt;T&gt;</code>, which should be the same as
   *         <code>this.component</code>.
   *     </li>
   *     <li>
   *         Component key (if specified in old component - if specified in exactly one of 'old' or
   *         'new' components these are considered incompatible).
   *     </li>
   *   </ul>
   * </p>
   */
  final boolean shouldMountWithComponent(Component newComponent) {
    if (component == null) {
      return true;
    } else if (component.getClass().equals(newComponent.getClass())) {
      final ComponentKey oldKey = component.getKey();
      final ComponentKey newKey = newComponent.getKey();

      return (oldKey != null && newKey != null && oldKey.equals(newKey))
          || (newKey == null && oldKey == null);
    }

    return false;
  }

  /**
   * Calls the setter and <b>then queues a rebuild</b>.
   * <p>Make sure to call this every time to modify state that could affect the build</p>
   *
   * @param setter the function called to modify the state
   */
  public final void setState(ComponentStateSetter setter) {
    setter.setState();
    this.setDirty();
  }

  /**
   * Called exactly once when this state is instantiated, after mounting it to a component.
   */
  public void initState() {}

  /**
   * Called exactly once when this state is going to be forgotten.
   * <p>Override dispose() to clean up references that might otherwise leak.</p>
   * <p>This method should not call setState().</p>
   */
  public void dispose() {}

  /**
   * Called when a new Component is attached to this state after the first component is mounted.
   *
   * @param oldComponent component that was replaced
   */
  public void didChangeDependencies(T oldComponent) {}

  /**
   * Builds this <code>ComponentState</code> into an array of child <code>Component</code>s.
   * <p>
   *     This method may be called at any time after <code>initState()</code> and before
   *     <code>dispose()</code> and should not synchronously make unchecked calls to
   *     <code>setState()</code>.
   * </p>
   *
   * @param context provided by components higher up the tree
   * @see BuildContext
   * @return list of components to be rendered as children of this state
   */
  public abstract ArrayList<Component> build(BuildContext context);
}
