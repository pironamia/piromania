package nz.piromania.reactiveengine;

/**
 * The type of the setter parameter passed into ComponentState::setState().
 * <p>
 * Doesn't need to be used in practice, since Java will automatically convert lambdas of the
 * form: <code>() -&gt; {...}</code> to conform to the appropriate interface.
 * </p>
 */
public interface ComponentStateSetter {
  void setState();
}
