package nz.piromania.reactiveengine;

/**
 * Interface for an object that exposes a dispose() method that should be called when the consumer
 * is finished using that object.
 */
public interface Disposable {
  void dispose();
}
