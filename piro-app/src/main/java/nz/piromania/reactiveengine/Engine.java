package nz.piromania.reactiveengine;


import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Takes a component (tree) and continuously (when required) builds it and emits the built
 * <code>RenderNode</code> tree to the <code>renderConsumer</code>.
 */
public class Engine {

  private StateNode rootStateNode;
  private RenderNode rootRenderNode;
  private Consumer<RenderNode> renderConsumer;

  /**
   * Builds a component, attaching appropriate <code>ComponentState</code>, and inserting it into
   * its <code>parentContext</code> (as the child at <code>index</code>) and
   * <code>renderParent</code> (indexing is ignored because it is assumed that an extant
   * <code>RenderNode</code> will have it's children cleared before building).
   *
   * @param <T>           type of the <code>Component</code> to be built
   * @param component     current component to be built
   * @param parentContext <code>StateNode</code> parent context of this <code>ComponentState</code>
   * @param renderParent  parent <code>RenderNode</code> of this <code>Component</code>
   * @param childIndex    index where the <code>StateNode</code> we're looking for should be in
   *                      the <code>parentContext</code>
   */
  @SuppressWarnings("rawtypes")
  <T extends Component> void buildComponent(T component, StateNode parentContext,
      RenderNode renderParent, int childIndex) {
    final RenderNode currentRenderContext = new RenderNode(component);

    StateNode<T> currentContext;
    if (parentContext.getChildren().size() > childIndex) {
      // We have an existing StateNode to try to match against.
      final StateNode contender = (StateNode) parentContext.getChildren().get(childIndex);

      if (contender.getComponentState().shouldMountWithComponent(component)) {
        currentContext = (StateNode<T>) contender;
        currentContext.getComponentState().setComponent(component);
      } else {
        // The component tree has changed, throw out all of the state from here down, and rebuild.
        final StateNode<T> newNode = new StateNode<T>(component.createState(),
            parentContext.copyBuildContext(), parentContext, currentRenderContext);
        newNode.getComponentState().setComponent(component);
        newNode.getComponentState().initState();

        parentContext.replaceChild(childIndex, newNode);

        currentContext = newNode;
      }
    } else {
      // We don't have an existing StateNode to match against. So make a new one.
      final StateNode<T> newNode = new StateNode<T>(component.createState(),
          parentContext.copyBuildContext(), parentContext, currentRenderContext);
      newNode.getComponentState().setComponent(component);
      newNode.getComponentState().initState();

      parentContext.insertChild(childIndex, newNode);

      currentContext = newNode;
    }

    // Copy new BuildContext from parent.
    currentContext.setBuildContext(parentContext.getBuildContext().copy());

    // Insert into RenderNode tree
    currentContext.setCurrentRenderNode(currentRenderContext);
    currentRenderContext.setComponent(component);

    renderParent.addChild(currentRenderContext);

    buildComponentState(currentContext, currentRenderContext);
  }

  /**
   * Builds a componentState, and calls buildComponent on each child.
   *
   * @param currentContext       current StateNode context, referring to the
   *                             <code>ComponentState</code> to be built
   * @param currentRenderContext current RenderNode, referring to the <code>Component</code>
   *                             that is being built
   * @param <T>                  type of the <code>Component</code> that the
   *                             <code>ComponentState</code> is for (should match the type of
   *                             <code>currentRenderContext.getComponent()</code>)
   */
  <T extends Component> void buildComponentState(StateNode<T> currentContext,
      RenderNode currentRenderContext) {
    // We're about to rebuild the component state, so it shouldn't be dirty any more.
    // NB: This is set *before* we build, just in case the ComponentState wants to rebuild
    // immediately (i.e. calls setState() in the build method).
    currentContext.getComponentState().clearDirt();

    final ArrayList<Component> subComponents = currentContext.getComponentState().build(
        currentContext.getBuildContext()
    );

    for (int i = 0; i < subComponents.size(); i++) {
      buildComponent(subComponents.get(i), currentContext, currentRenderContext, i);
    }

    // Dispose and remove StateNodes that are no longer in use.
    for (int i = currentContext.getChildren().size() - 1; i >= subComponents.size(); i--) {
      currentContext.removeChild(i);
    }
  }

  public void setRenderConsumer(Consumer<RenderNode> consumer) {
    this.renderConsumer = consumer;
  }

  /**
   * Initialises the Engine using <code>rootComponent</code> as the root of the component tree. Runs
   * an initial build and emits the result to the <code>renderConsumer</code> (if any).
   *
   * @param rootComponent to be built to generate the initial component tree
   */
  public void initialise(Component rootComponent) {
    rootRenderNode = new RenderNode(rootComponent);
    rootStateNode = new StateNode<>(
        rootComponent.createState(),
        new BuildContext(),
        null,
        rootRenderNode
    );

    rootStateNode.getComponentState().setComponent(rootComponent);
    rootStateNode.getComponentState().initState();

    buildComponentState(rootStateNode, rootRenderNode);

    this.emitRender();
  }

  /**
   * Traverses the state tree looking for 'dirty' states. Once it finds one, it triggers a rebuild
   * from that component down.
   *
   * @param currentContext <code>StateNode</code> at the root of the tree to traverse. May rebuild
   *                       nothing, the entire tree, or arbitrary subtree(s).
   * @return boolean representing whether a rebuild occurred
   */
  private boolean rebuildIfNecessary(StateNode<?> currentContext) {
    if (currentContext.getComponentState().getIsDirty()) {
      currentContext.getCurrentRenderNode()
          .clearChildren();  // Just throw out the child Component tree. We're rebuilding everything
      buildComponentState(currentContext, currentContext.getCurrentRenderNode());

      return true;
    } else {
      boolean didAnyRebuilds = false;
      for (StateNode<?> childContext : currentContext.getChildren()) {
        if (rebuildIfNecessary(childContext)) {
          didAnyRebuilds = true;
        }
      }

      return didAnyRebuilds;
    }
  }

  /**
   * Emits <b>a copy of</b> the current tree below <code>rootRenderNode</code> to the
   * <code>renderConsumer</code>, if any is attached.
   *
   * @throws IllegalStateException if there is no rootRenderNode
   */
  private void emitRender() {
    if (rootRenderNode == null) {
      throw new IllegalStateException("Cannot tick until after the Engine has been initialised.");
    }

    if (renderConsumer != null) {
      // Pass the renderer the new render tree.
      renderConsumer.accept(rootRenderNode.copy());
    }
  }

  /**
   * Scans the current render tree and rebuilds components as required. If a rebuild occurs, emits
   * the entire component tree as an artifact to the <code>renderConsumer</code> (if any).
   *
   * @return boolean representing whether a rebuild occurred during this tick
   * @throws IllegalStateException If called before the Engine has completed initialisation
   */
  public boolean tick() {
    if (rootStateNode == null) {
      throw new IllegalStateException("Cannot tick until after the Engine has been initialised.");
    }

    final boolean didRebuild = rebuildIfNecessary(rootStateNode);
    if (didRebuild) {
      this.emitRender();
    }
    return didRebuild;
  }

  /**
   * Initialises the engine, and then continuously ticks until exit.
   *
   * @param rootComponent to use to initialise the engine
   */
  public void run(Component rootComponent) {
    run(rootComponent, null);
  }

  /**
   * Initialises the engine, and then continuously ticks until exit (or the <code>ticks</code> tick
   * count is reached).
   *
   * @param rootComponent to use to initialise the engine
   * @param ticks         number of ticks to execute
   */
  public void run(Component rootComponent, Integer ticks) {
    initialise(rootComponent);

    AtomicInteger currentTick = new AtomicInteger();

    this.runUntil(rootComponent, () -> {
      if (ticks != null) {
        currentTick.getAndIncrement();
        return currentTick.get() == ticks;
      }
      return false;
    });
  }

  /**
   * Initialises the engine, and then continuously ticks until exit <code>shouldExitSupplier</code>
   * returns true.
   *
   * @param rootComponent The component to use to initialise the engine
   * @param shouldExitSupplier function that returns true when the engine should exit
   */
  public void runUntil(Component rootComponent, Supplier<Boolean> shouldExitSupplier) {
    initialise(rootComponent);

    while (!shouldExitSupplier.get()) {
      tick();
    }
  }

  protected StateNode getRootStateNode() {
    return rootStateNode;
  }

  protected RenderNode getRootRenderNode() {
    return rootRenderNode;
  }
}
