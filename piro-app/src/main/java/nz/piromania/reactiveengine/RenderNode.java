package nz.piromania.reactiveengine;

import java.util.ArrayList;

/**
 * A tree node that contains a Component, and links to its children.
 */
public class RenderNode {

  private final ArrayList<RenderNode> children;
  private Component component;

  RenderNode(Component component) {
    this.component = component;
    this.children = new ArrayList<>();
  }

  public Component getComponent() {
    return this.component;
  }

  public void setComponent(Component newComponent) {
    this.component = newComponent;
  }

  public ArrayList<RenderNode> getChildren() {
    return this.children;
  }

  public void addChild(RenderNode child) {
    children.add(child);
  }

  /**
   * Makes a deep copy of this RenderNode and its children, returning it.
   *
   * @return an identical deep copy of this render node and its children
   */
  public RenderNode copy() {
    final RenderNode thisCopy = new RenderNode(this.component);
    for (RenderNode child : children) {
      thisCopy.addChild(child.copy());
    }
    return thisCopy;
  }

  /**
   * Disposes all children and clears the children array.
   */
  public void clearChildren() {
    for (RenderNode childNode : this.children) {
      childNode.dispose();
    }
    this.children.clear();
  }

  public void dispose() {
    this.clearChildren();
  }
}