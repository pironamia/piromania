package nz.piromania.reactiveengine;

import java.util.ArrayList;

/**
 * A tree node that exists one-to-one with a specific ComponentState.
 * Allows read-only access to both the parent node (if any), and read/write access to many
 * child nodes.
 * Also links to the corresponding RenderNode, if any exists.
 */
public class StateNode<T extends Component> {
  private final StateNode parent;

  private BuildContext buildContext;
  private final ComponentState<T> state;
  private final ArrayList<StateNode> children = new ArrayList<>();

  /**
   * The RenderNode for the current component connected to this ComponentState.
   */
  private RenderNode currentRenderNode;

  /**
   * Constructs a StateNode for a specific ComponentState state.
   *
   * @param state ComponentState that corresponds to this StateNode
   * @param parent StateNode in the state tree (should be null for the root node)
   * @param currentRenderNode RenderNode that is currently inflating its Component using this state
   */
  public StateNode(ComponentState<T> state, BuildContext buildContext, StateNode<?> parent,
      RenderNode currentRenderNode) {
    this.state = state;
    this.parent = parent;
    this.currentRenderNode = currentRenderNode;
    this.buildContext = buildContext;
  }

  public void setBuildContext(BuildContext buildContext) {
    this.buildContext = buildContext;
  }

  public BuildContext getBuildContext() {
    return this.buildContext;
  }

  public BuildContext copyBuildContext() {
    return this.buildContext.copy();
  }

  public StateNode<?> getParent() {
    return this.parent;
  }

  public ComponentState<T> getComponentState() {
    return this.state;
  }

  public ArrayList<StateNode> getChildren() {
    return children;
  }

  /**
   * Removes and automatically disposes the child at the given index.
   *
   * @param index of the child to remove in the children array
   * @throws IndexOutOfBoundsException when the provided <code>index</code> is invalid
   */
  public void removeChild(int index) {
    children.remove(index).disposeSubtree();
  }

  /**
   * Removes and automatically disposes the child at the given index, replacing it with a new
   * child at the same index.
   *
   * @param index of the child to remove in the children array
   * @param newChild to insert at <code>index</code>
   * @throws IndexOutOfBoundsException when the provided index is invalid
   */
  public void replaceChild(int index, StateNode<? extends Component> newChild) {
    children.set(index, newChild).disposeSubtree();
  }

  /**
   * Inserts the given child at the given index.
   *
   * @param index at which to place the child in the children array
   * @param child to insert at <code>index</code>
   * @throws IndexOutOfBoundsException when the provided index is invalid
   */
  public void insertChild(int index, StateNode<? extends Component> child) {
    children.add(index, child);
  }

  public RenderNode getCurrentRenderNode() {
    return this.currentRenderNode;
  }

  public void setCurrentRenderNode(RenderNode newCurrentRenderNode) {
    this.currentRenderNode = newCurrentRenderNode;
  }

  /**
   * Disposes the entire subtree including all children of this node, as well as the node itself
   * (and the state attached to it).
   */
  public void disposeSubtree() {
    state.dispose();
    for (StateNode<?> childNode : children) {
      childNode.disposeSubtree();
    }
  }
}
