package nz.piromania.reactiveengine.component;

import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.util.Alignment;
import nz.piromania.reactiveengine.util.ChildComponentSupplier;
import nz.piromania.reactiveengine.util.SingleChildComponentState;

/**
 * A component that aligns its child in the parent box.
 */
public class AlignmentComponent extends Component implements ChildComponentSupplier,
    DirectlyRenderableComponent {

  private final Component child;
  private final Alignment alignment;

  public AlignmentComponent(Alignment alignment, Component child) {
    this.child = child;
    this.alignment = alignment;
  }

  @Override
  protected SingleChildComponentState<AlignmentComponent> createState() {
    return new SingleChildComponentState<AlignmentComponent>();
  }

  @Override
  public Component getChild() {
    return child;
  }

  public Alignment getAlignment() {
    return alignment;
  }
}
