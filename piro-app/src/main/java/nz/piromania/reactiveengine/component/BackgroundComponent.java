package nz.piromania.reactiveengine.component;

import java.awt.Color;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.util.ChildComponentSupplier;
import nz.piromania.reactiveengine.util.SingleChildComponentState;

/**
 * A component that wraps its child in a background colour.
 */
public class BackgroundComponent extends Component implements DirectlyRenderableComponent,
    ChildComponentSupplier {

  private final Color backgroundColour;
  private final Component child;

  public BackgroundComponent(Color backgroundColour, Component child) {
    this.backgroundColour = backgroundColour;
    this.child = child;
  }

  @Override
  protected SingleChildComponentState<BackgroundComponent> createState() {
    return new SingleChildComponentState<>();
  }


  public Component getChild() {
    return child;
  }

  public Color getBackgroundColour() {
    return backgroundColour;
  }
}