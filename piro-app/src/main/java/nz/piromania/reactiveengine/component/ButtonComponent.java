package nz.piromania.reactiveengine.component;

import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.util.ChildComponentSupplier;
import nz.piromania.reactiveengine.util.SingleChildComponentState;
import nz.piromania.reactiveengine.util.VoidCallback;

/**
 * Component that represents a button with <code>child</code> placed inside the button, and
 * <code>onClick</code> called when the button is clicked.
 */
public class ButtonComponent extends Component implements DirectlyRenderableComponent,
    ChildComponentSupplier {
  private final boolean disabled;
  private final VoidCallback onClick;
  private final Component child;

  /**
   * Construcst a ButtonComponent that triggers <code>onClick</code> when clicked.
   *
   * @param onClick callback to call when the button is clicked
   * @param disabled if the button should be displayed as disabled (clicks will not be registered)
   * @param child component to display inside this button
   */
  public ButtonComponent(VoidCallback onClick, boolean disabled, Component child) {
    this.onClick = onClick;
    this.disabled = disabled;
    this.child = child;
  }

  public ButtonComponent(VoidCallback onClick, Component child) {
    this(onClick, false, child);
  }

  @Override
  protected SingleChildComponentState<ButtonComponent> createState() {
    return new SingleChildComponentState<ButtonComponent>();
  }

  public VoidCallback getOnClick() {
    return onClick;
  }

  public Component getChild() {
    return child;
  }

  public boolean isDisabled() {
    return disabled;
  }
}
