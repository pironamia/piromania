package nz.piromania.reactiveengine.component;

import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.util.ChildComponentSupplier;
import nz.piromania.reactiveengine.util.SingleChildComponentState;

/**
 * Component that centres its children in its parent's view.
 */
public class CentreComponent extends Component implements
    ChildComponentSupplier, DirectlyRenderableComponent {
  private final Component child;

  public CentreComponent(Component child) {
    this.child = child;
  }

  @Override
  public Component getChild() {
    return child;
  }

  @Override
  protected SingleChildComponentState<CentreComponent> createState() {
    return new SingleChildComponentState<>();
  }
}
