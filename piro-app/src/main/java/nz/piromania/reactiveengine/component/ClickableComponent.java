package nz.piromania.reactiveengine.component;

import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.util.ChildComponentSupplier;
import nz.piromania.reactiveengine.util.SingleChildComponentState;
import nz.piromania.reactiveengine.util.VoidCallback;

/**
 * A component much like a button, that accepts a child and an onClick callback. But doesn't provide
 * and wrapping styles like a button does.
 */
public class ClickableComponent extends Component implements DirectlyRenderableComponent,
    ChildComponentSupplier {

  private final VoidCallback onClick;
  private final Component child;

  public ClickableComponent(VoidCallback onClick, Component child) {
    this.onClick = onClick;
    this.child = child;
  }

  public VoidCallback getOnClick() {
    return onClick;
  }

  public Component getChild() {
    return child;
  }

  @Override
  protected SingleChildComponentState<ClickableComponent> createState() {
    return new SingleChildComponentState<>();
  }
}
