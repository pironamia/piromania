package nz.piromania.reactiveengine.component;

import java.awt.Color;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.util.Alignment;
import nz.piromania.reactiveengine.util.BoxSize;
import nz.piromania.reactiveengine.util.EdgeInsets;

/**
 * A box of an optionally specified size, that optionally contains a child, has padding around the
 * outside, and a background colour. May align its child based on the given Alignment.
 */
public class ContainerComponent extends Component implements DirectlyRenderableComponent {
  final Component child;
  public final EdgeInsets padding;
  public final Color backgroundColour;  // null denotes no background colour.
  public final BoxSize size;
  public final Alignment alignment;

  /**
   * Constructs a Container.
   *
   * @param size             to prefer for this box
   * @param alignment        for the child inside this box
   * @param padding          for this box
   * @param backgroundColour to be displayed behind the content of this box (null for transparent)
   * @param child            component to be inside this container
   */
  public ContainerComponent(
      BoxSize size,
      Alignment alignment,
      EdgeInsets padding,
      Color backgroundColour,
      Component child
  ) {
    this.size = size;
    this.alignment = alignment;
    this.backgroundColour = backgroundColour;
    this.padding = padding;
    this.child = child;
  }

  public ContainerComponent(BoxSize size, Component child) {
    this(size, Alignment.TOP_LEFT, EdgeInsets.zero, null, child);
  }

  @Override
  protected ComponentState<ContainerComponent> createState() {
    return new ContainerComponentState();
  }
}
