package nz.piromania.reactiveengine.component;

import java.awt.Color;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.util.Alignment;
import nz.piromania.reactiveengine.util.BoxSize;
import nz.piromania.reactiveengine.util.EdgeInsets;

/**
 * A builder class for the ContainerComponent, to reduce the burden and chance of misconfiguration
 * caused by its complicated constructors.
 */
public class ContainerComponentBuilder {

  private EdgeInsets padding = EdgeInsets.zero;
  private Color backgroundColour = null;
  private BoxSize size;
  private Alignment alignment = Alignment.TOP_LEFT;

  public ContainerComponentBuilder setPadding(EdgeInsets padding) {
    this.padding = padding;
    return this;
  }

  public ContainerComponentBuilder setBackgroundColour(Color backgroundColour) {
    this.backgroundColour = backgroundColour;
    return this;
  }

  public ContainerComponentBuilder setSize(BoxSize size) {
    this.size = size;
    return this;
  }

  public ContainerComponentBuilder setAlignment(Alignment alignment) {
    this.alignment = alignment;
    return this;
  }

  /**
   * Takes the current state of the builder and configures a new ContainerComponent accordingly,
   * attaching <code>child</code> as its child.
   *
   * @param child to attach to the component
   * @return the ContainerComponent instance
   */
  public ContainerComponent build(Component child) {
    return new ContainerComponent(
        size,
        alignment,
        padding,
        backgroundColour,
        child
    );
  }

  /**
   * Takes the current state of the builder and configures a new ContainerComponent accordingly,
   * attaching no child.
   *
   * @return the ContainerComponent instance
   */
  public ContainerComponent build() {
    return new ContainerComponent(
        size,
        alignment,
        padding,
        backgroundColour,
        null
    );
  }

  /**
   * Return the builder to its default configuration.
   */
  public void reset() {
    size = null;
    alignment = Alignment.TOP_LEFT;
    padding = EdgeInsets.zero;
    backgroundColour = null;
  }
}
