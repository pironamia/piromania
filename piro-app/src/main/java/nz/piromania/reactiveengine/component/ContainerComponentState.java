package nz.piromania.reactiveengine.component;

import java.util.ArrayList;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.util.ArrayListBuilder;

/**
 * The ComponentState for a Container. Allows for the Container to be given either 1 or no child.
 *
 * @see ContainerComponent {@link ContainerComponent}
 */
public class ContainerComponentState extends ComponentState<ContainerComponent> {
  @Override
  public ArrayList<Component> build(BuildContext context) {
    if (getComponent().child == null) {
      return new ArrayList<>();
    }

    return ArrayListBuilder.build(getComponent().child);
  }
}
