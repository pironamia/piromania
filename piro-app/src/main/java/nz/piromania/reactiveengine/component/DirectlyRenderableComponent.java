package nz.piromania.reactiveengine.component;

/**
 * Interface to be added to components to mark that they should have an implementation in the
 * renderer.
 */
public interface DirectlyRenderableComponent {

}
