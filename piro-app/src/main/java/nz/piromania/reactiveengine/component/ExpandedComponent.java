package nz.piromania.reactiveengine.component;

import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;

/**
 * Component that expands to fill the extra space in its parent ListComponent.
 */
public class ExpandedComponent extends Component {
  private final Component child;

  public ExpandedComponent(Component child) {
    this.child = child;
  }

  public ExpandedComponent() {
    this(null);
  }

  @Override
  protected ExpandedComponentState createState() {
    return new ExpandedComponentState();
  }

  public Component getChild() {
    return child;
  }
}
