package nz.piromania.reactiveengine.component;

import java.util.ArrayList;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.list.FlexibleComponent;
import nz.piromania.reactiveengine.util.ArrayListBuilder;

/**
 * ComponentState for {@link ExpandedComponent}.
 */
public class ExpandedComponentState extends ComponentState<ExpandedComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final Component child = getComponent().getChild();

    return ArrayListBuilder.build(
        new FlexibleComponent(
            1,
            (child == null) ? new ContainerComponentBuilder().build() : child
        )
    );
  }
}
