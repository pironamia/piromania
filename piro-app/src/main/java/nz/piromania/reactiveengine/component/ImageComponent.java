package nz.piromania.reactiveengine.component;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.util.BoxSize;
import nz.piromania.reactiveengine.util.LeafComponentState;

/**
 * A component that displays an image, optionally constraining it to the given size.
 */
public class ImageComponent extends Component implements DirectlyRenderableComponent {

  private final BufferedImage image;
  private final BoxSize size;

  /**
   * Constructs an ImageComponent.
   *
   * @param image to display
   * @param size  to scale the image to (null disable scaling)
   */
  public ImageComponent(BoxSize size, BufferedImage image) {
    this.size = size;
    this.image = image;
  }

  /**
   * Constructs an ImageComponent with no scaling.
   *
   * @param image to display
   */
  public ImageComponent(BufferedImage image) {
    this(null, image);
  }

  /**
   * Loads a BufferedImage for an image file stored at <code>filePath</code> in the application
   * resources.
   *
   * @param filePath the path of the image to load
   * @return BufferedImage for the image at this location
   * @throws IllegalArgumentException when the file does not exist at that location
   * @throws IOException              if an error occurs during reading or when not able to create
   *                                  required ImageInputStream
   */
  public static BufferedImage loadImageFromResourcesFile(String filePath) throws IOException {
    final var imgStream = ImageComponent.class.getClassLoader().getResourceAsStream(filePath);

    if (imgStream == null) {
      throw new IllegalArgumentException(String.format(
          "Resource file at path %s does not exist.",
          filePath
      ));
    }

    return ImageIO.read(imgStream);
  }

  /**
   * Loads a BufferedImage for an image file stored at <code>filePath</code> in the application
   * resources. If the image does not exist, emits a placeholder image instead of raising an error.
   *
   * @param filePath          the path of the image to load
   * @param placeholderWidth  width of the placeholder image (does not constrain the loaded image)
   * @param placeholderHeight height of the placeholder image (does not constrain the loaded image)
   * @return BufferedImage for the image at this location
   */
  public static BufferedImage tryLoadImageFromResourcesFile(String filePath, int placeholderWidth,
      int placeholderHeight) {
    try {
      return loadImageFromResourcesFile(filePath);
    } catch (IOException e) {
      if (!Boolean.getBoolean("image_component_hide_errors")) {
        System.out.println("Failed to load image at path " + filePath);
        e.printStackTrace();
      }
      return getPlaceholderImage(placeholderWidth, placeholderHeight);
    } catch (NullPointerException e) {
      if (!Boolean.getBoolean("image_component_hide_errors")) {
        System.out.println("Failed to load image at path " + filePath);
        e.printStackTrace();
      }
      return getPlaceholderImage(placeholderWidth, placeholderHeight);
    } catch (IllegalArgumentException e) {
      if (!Boolean.getBoolean("image_component_hide_errors")) {
        System.out.println("Failed to load image at path " + filePath);
        e.printStackTrace();
      }
      return getPlaceholderImage(placeholderWidth, placeholderHeight);
    }
  }

  /**
   * Generates a placeholder image with the given dimensions. The image consists of a red background
   * with a white cross.
   *
   * @param width the width of the image
   * @param height the height of the image
   * @return BufferedImage containing the placeholder image
   */
  public static BufferedImage getPlaceholderImage(int width, int height) {
    BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

    final Graphics2D g = img.createGraphics();
    g.setColor(Color.RED);
    g.fillRect(0, 0, width, height);

    g.setColor(Color.WHITE);
    g.setStroke(new BasicStroke(3));
    g.drawLine(0, 0, width, height);
    g.drawLine(width, 0, 0, height);

    g.dispose();

    return img;
  }

  @Override
  protected LeafComponentState createState() {
    return new LeafComponentState();
  }

  public BufferedImage getImage() {
    return image;
  }

  public BoxSize getSize() {
    return size;
  }
}
