package nz.piromania.reactiveengine.component;

import java.util.function.Consumer;
import nz.piromania.reactiveengine.Component;


/**
 * A component used to detect keyboard input.
 * Upon keyboard input in the context of it or its children, renderer will call onKeyboardCallback()
 * with appropriate event data.
 */
public class KeyboardDetector extends Component {

  private final Component child;

  private final Consumer<String> onKeyboardCallback;

  public KeyboardDetector(Consumer<String> onKeyboardCallback, Component child) {
    this.onKeyboardCallback = onKeyboardCallback;
    this.child = child;
  }

  /**
   * Callback triggered when a key is detected.
   */
  public Consumer<String> getOnKeyboardCallback() {
    return this.onKeyboardCallback;
  }

  public Component getChild() {
    return child;
  }

  @Override
  protected KeyboardDetectorState createState() {
    return new KeyboardDetectorState();
  }
}
