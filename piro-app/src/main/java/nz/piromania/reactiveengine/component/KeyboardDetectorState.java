package nz.piromania.reactiveengine.component;

import java.util.ArrayList;
import java.util.List;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;

/**
 * State for the KeyboardDetector component.
 * Simply returns the given child.
 */
public class KeyboardDetectorState extends ComponentState<KeyboardDetector> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    return new ArrayList<>(List.of(this.getComponent().getChild()));
  }
}
