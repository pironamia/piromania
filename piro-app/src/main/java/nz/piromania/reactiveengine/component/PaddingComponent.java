package nz.piromania.reactiveengine.component;

import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.util.ChildComponentSupplier;
import nz.piromania.reactiveengine.util.EdgeInsets;
import nz.piromania.reactiveengine.util.SingleChildComponentState;

/**
 * A component that surrounds its child in padding.
 */
public class PaddingComponent extends Component implements DirectlyRenderableComponent,
    ChildComponentSupplier {

  private final EdgeInsets padding;
  private final Component child;

  public PaddingComponent(EdgeInsets padding, Component child) {
    this.padding = padding;
    this.child = child;
  }

  @Override
  protected SingleChildComponentState<PaddingComponent> createState() {
    return new SingleChildComponentState<>();
  }

  public EdgeInsets getPadding() {
    return padding;
  }

  public Component getChild() {
    return child;
  }
}
