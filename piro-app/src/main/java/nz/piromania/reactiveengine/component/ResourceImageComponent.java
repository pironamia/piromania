package nz.piromania.reactiveengine.component;

import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.util.BoxSize;

/**
 * ImageComponent that loads and automatically caches images from the resources directory.
 */
public class ResourceImageComponent extends Component {
  private final String path;
  private final BoxSize size;

  public ResourceImageComponent(BoxSize size, String path) {
    this.size = size;
    this.path = path;
  }

  @Override
  protected ResourceImageComponentState createState() {
    return new ResourceImageComponentState();
  }

  public String getPath() {
    return path;
  }

  public BoxSize getSize() {
    return size;
  }
}
