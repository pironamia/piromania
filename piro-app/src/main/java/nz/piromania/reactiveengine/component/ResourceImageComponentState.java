package nz.piromania.reactiveengine.component;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.BoxSize;

/**
 * ComponentState for {@link ResourceImageComponent}. Loads the specified image and saves it until
 * forced to update (e.g. by a new component with a different image path being attached).
 *
 * <p>
 * Does not resize the image to the specified dimensions (if any are specified). Instead this is
 * deferred to the {@link nz.piromania.gui.renderer.ImageNodeRenderer} implementation. This could be
 * a performance improvement in the future if necessary.
 * </p>
 */
public class ResourceImageComponentState extends ComponentState<ResourceImageComponent> {

  private BufferedImage image;

  private void loadImage() {
    final BoxSize placeholderSize =
        (getComponent().getSize() != null) ? getComponent().getSize()
            : new BoxSize(100, 100);

    image = ImageComponent.tryLoadImageFromResourcesFile(
        getComponent().getPath(),
        (placeholderSize.getWidth() == null) ? 100 : placeholderSize.getWidth(),
        (placeholderSize.getHeight() == null) ? 100 : placeholderSize.getHeight()
    );
  }

  @Override
  public void initState() {
    super.initState();

    loadImage();
  }

  @Override
  public void didChangeDependencies(ResourceImageComponent oldComponent) {
    super.didChangeDependencies(oldComponent);

    if (!oldComponent.getPath().equals(getComponent().getPath())) {
      setState(this::loadImage);
    }
  }

  @Override
  public ArrayList<Component> build(BuildContext context) {
    return ArrayListBuilder.build(new ImageComponent(
        getComponent().getSize(),
        image
    ));
  }
}
