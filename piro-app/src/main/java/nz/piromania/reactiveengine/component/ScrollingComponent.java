package nz.piromania.reactiveengine.component;

import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.util.ChildComponentSupplier;
import nz.piromania.reactiveengine.util.SingleChildComponentState;


/**
 * A component that allows its child to be scrolled if it exceeds the available height.
 */
public class ScrollingComponent extends Component implements DirectlyRenderableComponent,
    ChildComponentSupplier {

  private final Component child;

  public ScrollingComponent(Component child) {
    this.child = child;
  }

  @Override
  public Component getChild() {
    return child;
  }

  @Override
  protected SingleChildComponentState<ScrollingComponent> createState() {
    return new SingleChildComponentState<ScrollingComponent>();
  }
}
