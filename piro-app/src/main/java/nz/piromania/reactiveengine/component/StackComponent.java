package nz.piromania.reactiveengine.component;

import java.util.ArrayList;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.ChildrenComponentSupplier;
import nz.piromania.reactiveengine.util.MultipleChildrenComponentState;

/**
 * A component that allows components to be stacked on top of each other in the Z-axis.
 */
public class StackComponent extends Component implements DirectlyRenderableComponent,
    ChildrenComponentSupplier {
  private final ArrayList<Component> children;

  public StackComponent(ArrayList<Component> children) {
    this.children = children;
  }

  public StackComponent(Component... children) {
    this(ArrayListBuilder.build(children));
  }

  @Override
  public ArrayList<Component> getChildren() {
    return children;
  }

  @Override
  protected MultipleChildrenComponentState<StackComponent> createState() {
    return new MultipleChildrenComponentState<StackComponent>();
  }
}
