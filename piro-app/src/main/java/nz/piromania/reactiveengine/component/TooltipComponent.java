package nz.piromania.reactiveengine.component;

import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.util.ChildComponentSupplier;
import nz.piromania.reactiveengine.util.SingleChildComponentState;

/**
 * A Component that shows a tooltip with text to provide supplementary information to its child.
 */
public class TooltipComponent extends Component implements DirectlyRenderableComponent,
    ChildComponentSupplier {

  private final TextComponent tooltip;
  private final Component child;

  public TooltipComponent(TextComponent tooltip, Component child) {
    this.tooltip = tooltip;
    this.child = child;
  }

  public TooltipComponent(String tooltip, Component child) {
    this(new TextComponent(tooltip), child);
  }

  @Override
  protected SingleChildComponentState<TooltipComponent> createState() {
    return new SingleChildComponentState<TooltipComponent>();
  }

  public TextComponent getTooltip() {
    return tooltip;
  }

  @Override
  public Component getChild() {
    return child;
  }
}
