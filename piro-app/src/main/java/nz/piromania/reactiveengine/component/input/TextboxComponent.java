package nz.piromania.reactiveengine.component.input;

import java.util.function.Consumer;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.component.DirectlyRenderableComponent;
import nz.piromania.reactiveengine.util.LeafComponentState;

/**
 * A component that displays a text input.
 */
public class TextboxComponent extends Component implements DirectlyRenderableComponent {

  private final String placeholder;
  private final String value;
  private final Consumer<String> onChange;
  private final Consumer<String> onSubmit;

  /**
   * Constructs the TextboxComponent.
   *
   * @param value       in the text box
   * @param placeholder value to show in the text box when there is no other content
   * @param onChange    called when the value is updated
   * @param onSubmit    called when the box is submitted
   */
  public TextboxComponent(String value, String placeholder, Consumer<String> onChange,
      Consumer<String> onSubmit) {
    this.value = value;
    this.placeholder = placeholder;
    this.onChange = onChange;
    this.onSubmit = onSubmit;
  }

  /**
   * Constructs the TextboxComponent.
   *
   * @param value    in the text box
   * @param onChange called when the value is updated
   */
  public TextboxComponent(String value, Consumer<String> onChange) {
    this(value, null, onChange, null);
  }

  /**
   * Constructs the TextboxComponent.
   *
   * @param value       in the text box
   * @param placeholder value to show in the text box when there is no other content
   * @param onChange    called when the value is updated
   */
  public TextboxComponent(String value, String placeholder, Consumer<String> onChange) {
    this(value, placeholder, onChange, null);
  }

  @Override
  protected LeafComponentState createState() {
    return new LeafComponentState();
  }

  public String getValue() {
    return value;
  }

  public Consumer<String> getOnChange() {
    return onChange;
  }

  public Consumer<String> getOnSubmit() {
    return onSubmit;
  }

  public String getPlaceholder() {
    return placeholder;
  }
}

