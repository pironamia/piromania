package nz.piromania.reactiveengine.component.list;

import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.component.DirectlyRenderableComponent;
import nz.piromania.reactiveengine.util.ChildComponentSupplier;
import nz.piromania.reactiveengine.util.SingleChildComponentState;


/**
 * A component that 'flexes' inside its parent {@link ListComponent} to take up spare space,
 * the amount of spare space it takes vs how much other FlexibleComponents in the same ListComponent
 * is determine by the 'flex' value of each.
 */
public class FlexibleComponent extends Component implements DirectlyRenderableComponent,
    ChildComponentSupplier {
  private final int flex;
  private final Component child;

  public FlexibleComponent(int flex, Component child) {
    this.flex = flex;
    this.child = child;
  }

  @Override
  protected SingleChildComponentState<FlexibleComponent> createState() {
    return new SingleChildComponentState<>();
  }

  public int getFlex() {
    return flex;
  }

  public Component getChild() {
    return child;
  }
}
