package nz.piromania.reactiveengine.component.list;

import java.util.ArrayList;
import java.util.List;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.component.DirectlyRenderableComponent;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.ChildrenComponentSupplier;
import nz.piromania.reactiveengine.util.MultipleChildrenComponentState;

/**
 * A Component that represents a list of child components in either the Horizontal or Vertical
 * directions.
 */
public class ListComponent extends Component implements DirectlyRenderableComponent,
    ChildrenComponentSupplier {

  private final ArrayList<Component> children;
  private final Axis mainAxis;

  public ListComponent(Axis mainAxis, Component... children) {
    this(mainAxis, ArrayListBuilder.buildFilteringNulls(children));
  }

  public ListComponent(Axis mainAxis, List<Component> children) {
    this.mainAxis = mainAxis;
    this.children = new ArrayList<>(children);
  }

  @Override
  protected MultipleChildrenComponentState<ListComponent> createState() {
    return new MultipleChildrenComponentState<>();
  }

  public ArrayList<Component> getChildren() {
    return children;
  }

  public Axis getMainAxis() {
    return mainAxis;
  }
}
