package nz.piromania.reactiveengine.component.provider;

import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.exception.KeyNotFoundException;

/**
 * Component that provides data to the components in the tree beneath it.
 */
public class Provider extends Component {
  protected final Object data;
  protected final Component child;

  /**
   * Constructs a Provider component. All components below this provider will be able to access the
   * provided <code>data</code>.
   *
   * @see Provider#of(Class, BuildContext) for details on how to look up the data in child
   *                                       components.
   */
  public Provider(Object data, Component child) {
    this.data = data;
    this.child = child;
  }

  /**
   * Set this component as depending on data inherited from a parent Provider (or other user of
   * <code>BuildContext.provideInheritedDataOfExactType()</code>).
   *
   * <p>
   *   This data can then be looked up by using e.g.
   *   <code>Provider.of(String.class, context)</code>, which will return the String
   *   that has been passed down the tree to this context.
   * </p>
   *
   * @param <T> type of the data that is being looked up
   * @param type of the data that is being looked up
   * @param context BuildContext to look up the data in
   */
  public static <T> T of(Class<T> type, BuildContext context) throws KeyNotFoundException {
    return context.getInheritedDataOfExactType(type);
  }

  @Override
  protected ProviderState createState() {
    return new ProviderState();
  }
}
