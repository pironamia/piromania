package nz.piromania.reactiveengine.component.provider;

import java.util.ArrayList;
import java.util.List;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;

/**
 * State for the <code>Provider</code> component.
 * Simply injects the data into the BuildContext and returns the Provider's child.
 *
 * @see Provider for more details
 */
public class ProviderState extends ComponentState<Provider> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    context.provideInheritedData(this.getComponent().data);

    return new ArrayList<>(List.of(this.getComponent().child));
  }
}
