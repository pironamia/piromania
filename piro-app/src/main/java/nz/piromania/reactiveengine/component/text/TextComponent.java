package nz.piromania.reactiveengine.component.text;

import java.util.ArrayList;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.util.ArrayListBuilder;

/**
 * A component that contains a string of (optionally styled) text.
 * Can contain multiple TextSpans, representing areas of text with their own styles.
 */
public class TextComponent extends Component {

  private final ArrayList<TextSpan> text;

  /**
   * Constructs a TextComponent, with plain styling.
   *
   * @param text to display
   */
  public TextComponent(String text) {
    this.text = ArrayListBuilder.build(new TextSpan(
        TextStyle.plain,
        text
    ));
  }

  /**
   * Constructs a TextComponent with custom styling.
   *
   * @param style to apply to the text
   * @param text to display
   */
  public TextComponent(TextStyle style, String text) {
    this.text = ArrayListBuilder.build(new TextSpan(
        style,
        text
    ));
  }

  public TextComponent(TextSpan... text) {
    this.text = ArrayListBuilder.build(text);
  }

  public TextComponent(ArrayList<TextSpan> text) {
    this.text = text;
  }

  /**
   * Renders this TextComponent into an HTML 4 string with the specified styling.
   */
  public String getHtml() {
    final StringBuilder builder = new StringBuilder("<html>");

    for (var span : text) {
      builder.append(span.getHtml().replaceAll("\n", "<br/>"));
    }

    builder.append("</html>");

    return builder.toString();
  }

  @Override
  protected TextComponentState createState() {
    return new TextComponentState();
  }

  public ArrayList<TextSpan> getText() {
    return text;
  }
}
