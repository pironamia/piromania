package nz.piromania.reactiveengine.component.text;

import java.util.ArrayList;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;

/**
 * ComponentState for the TextComponent.
 */
public class TextComponentState extends ComponentState<TextComponent> {

  /**
   * {@inheritDoc}
   */
  @Override
  public ArrayList<Component> build(BuildContext context) {
    return ArrayListBuilder.build(
        new ListComponent(
            Axis.HORIZONTAL,
            new ArrayList<>(getComponent().getText())
        )
    );
  }
}
