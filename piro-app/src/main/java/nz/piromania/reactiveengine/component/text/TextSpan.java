package nz.piromania.reactiveengine.component.text;

import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.component.DirectlyRenderableComponent;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.util.LeafComponentState;
import org.apache.commons.text.StringEscapeUtils;

/**
 * Directly renderable component that represents a string of text with styles applied to it.
 */
public class TextSpan extends Component implements DirectlyRenderableComponent {

  private final TextStyle style;
  private final String text;
  private final boolean multiLine;

  /**
   * Constructs a TextSpan with a given style and text.
   *
   * @param style     the type to apply to the text
   * @param multiLine whether or not to allow the text to appear on multiple lines
   * @param text      the text to show
   */
  public TextSpan(TextStyle style, boolean multiLine, String text) {
    this.style = style;
    this.multiLine = multiLine;
    this.text = text;
  }

  public TextSpan(TextStyle style, String text) {
    this(style, false, text);
  }

  public TextSpan(String text) {
    this(TextStyle.plain, text);
  }

  @Override
  protected LeafComponentState createState() {
    return new LeafComponentState();
  }

  public TextStyle getStyle() {
    return style;
  }

  public String getText() {
    return text;
  }

  /**
   * Renders this TextSpan (and its styles) into an HTML 4 string.
   */
  public String getHtml() {
    return style.wrapWithStyleHtml(
        StringEscapeUtils.escapeHtml4(text)
    );
  }

  public boolean isMultiLine() {
    return multiLine;
  }
}
