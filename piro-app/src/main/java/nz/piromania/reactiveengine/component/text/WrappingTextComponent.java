package nz.piromania.reactiveengine.component.text;

import java.util.ArrayList;
import nz.piromania.reactiveengine.Component;

/**
 * A Text Component that wraps its content and renders it.
 *
 * <p>
 * Wraps its children to ensure that no line has more characters than maxLineLength - ignores font
 * size etc. during this calculation.
 * </p>
 */
public class WrappingTextComponent extends Component {

  private final ArrayList<TextSpan> children;
  private final int maxLineLength;

  public WrappingTextComponent(ArrayList<TextSpan> children, int maxLineLength) {
    this.children = children;
    this.maxLineLength = maxLineLength;
  }

  @Override
  protected WrappingTextComponentState createState() {
    return new WrappingTextComponentState();
  }

  public ArrayList<TextSpan> getChildren() {
    return children;
  }

  public int getMaxLineLength() {
    return maxLineLength;
  }
}
