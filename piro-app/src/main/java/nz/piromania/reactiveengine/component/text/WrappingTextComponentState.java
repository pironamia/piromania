package nz.piromania.reactiveengine.component.text;

import java.util.ArrayList;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.util.TextSpanWrapper;

/**
 * ComponentState for {@link WrappingTextComponent}.
 */
public class WrappingTextComponentState extends ComponentState<WrappingTextComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final ArrayList<ArrayList<TextSpan>> wrappedRows = new TextSpanWrapper(
        getComponent().getChildren(),
        getComponent().getMaxLineLength()
    ).wrap();

    final ArrayList<Component> textRows = new ArrayList<>();
    for (final var row : wrappedRows) {
      textRows.add(new TextComponent(
          row
      ));
    }

    return ArrayListBuilder.build(
        new ListComponent(
            Axis.VERTICAL,
            textRows
        )
    );
  }
}
