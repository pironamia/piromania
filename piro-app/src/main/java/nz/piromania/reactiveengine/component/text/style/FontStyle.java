package nz.piromania.reactiveengine.component.text.style;

/**
 * Enum representing the possible styles of the font.
 */
public enum FontStyle {
  NORMAL,
  ITALIC
}
