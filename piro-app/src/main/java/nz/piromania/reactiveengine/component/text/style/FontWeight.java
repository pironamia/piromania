package nz.piromania.reactiveengine.component.text.style;

/**
 * Enum representing the possible weight of the font.
 */
public enum FontWeight {
  NORMAL,
  BOLD
}
