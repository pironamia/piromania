package nz.piromania.reactiveengine.component.text.style;

import java.awt.Color;
import java.security.InvalidParameterException;
import java.util.Stack;

/**
 * Class that contains all styling information for a string of text.
 */
public class TextStyle {
  public static final String DEFAULT_FAMILY = "TextStyle__DEFAULT_FONT_VALUE";

  private final FontStyle fontStyle;
  private final FontWeight fontWeight;
  private final String fontFamily;
  private final int fontSize;
  private final Color colour;

  /**
   * Constructs a TextStyle, ensuring that all arguments are valid.
   */
  public TextStyle(
      FontStyle fontStyle, FontWeight fontWeight, String fontFamily, int fontSize, Color colour
  ) {
    if (fontStyle == null) {
      throw new InvalidParameterException("A fontStyle must be specified");
    }

    if (fontWeight == null) {
      throw new InvalidParameterException("A fontWeight must be specified");
    }

    if (fontFamily == null) {
      throw new InvalidParameterException(
          "A fontFamily must be provided. To use default, specify TextStyle.DEFAULT_FAMILY"
      );
    }

    if (fontSize <= 0) {
      throw new InvalidParameterException("Font size must be greater than 0");
    }

    if (colour == null) {
      throw new InvalidParameterException("A colour must be specified");
    }

    this.fontStyle = fontStyle;
    this.fontWeight = fontWeight;
    this.fontFamily = fontFamily;
    this.fontSize = fontSize;
    this.colour = colour;
  }

  /**
   * Copies this instance of the TextStyle, overriding the properties specified (properties to
   * inherit should be supplied with null.
   */
  public TextStyle copyWith(
      FontStyle fontStyle, FontWeight fontWeight, String fontFamily, Integer fontSize, Color colour
  ) {
    return new TextStyle(
        (fontStyle == null) ? this.fontStyle : fontStyle,
        (fontWeight == null) ? this.fontWeight : fontWeight,
        (fontFamily == null) ? this.fontFamily : fontFamily,
        (fontSize == null) ? this.fontSize : fontSize,
        (colour == null) ? this.colour : colour
    );
  }


  public static final TextStyle plain = new TextStyle(
      FontStyle.NORMAL,
      FontWeight.NORMAL,
      DEFAULT_FAMILY,
      14,
      Color.BLACK
  );

  public static final TextStyle plainWhite = plain.copyWith(null, null, null, null, Color.WHITE);

  public FontStyle getFontStyle() {
    return fontStyle;
  }

  public FontWeight getFontWeight() {
    return fontWeight;
  }

  public String getFontFamily() {
    return fontFamily;
  }

  public int getFontSize() {
    return fontSize;
  }

  /**
   * Computes whether this TextStyle and the other object are equivalent, if text colour is ignored.
   */
  public boolean equalsExcludingColour(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TextStyle textStyle = (TextStyle) o;
    return getFontSize() == textStyle.getFontSize() && getFontStyle() == textStyle.getFontStyle()
        && getFontWeight() == textStyle.getFontWeight() && getFontFamily()
        .equals(textStyle.getFontFamily());
  }

  public Color getColour() {
    return colour;
  }

  /**
   * Renders styles into an HTML 4 string, putting <code>content</code> between the opening and
   * closing tags.
   *
   * @param content to style (should have HTML tags escaped)
   * @return HTML 4 string with <code>content</code> styled by this style
   */
  public String wrapWithStyleHtml(String content) {
    final StringBuilder builder = new StringBuilder();
    final Stack<String> closingTags = new Stack<>();

    builder.append(
        String.format(
            "<font size=\"%dpx\" color=\"#%02x%02x%02x\" face=\"%s\">",
            fontSize,
            colour.getRed(),
            colour.getGreen(),
            colour.getBlue(),
            fontFamily.replaceAll("\"", "")
        )
    );
    closingTags.push("</font>");

    if (fontWeight == FontWeight.BOLD) {
      builder.append("<b>");
      closingTags.push("</b>");
    }

    if (fontStyle == FontStyle.ITALIC) {
      builder.append("<i>");
      closingTags.push("</i>");
    }

    builder.append(content);

    for (String closeTag : closingTags) {
      builder.append(closeTag);
    }

    return builder.toString();
  }
}
