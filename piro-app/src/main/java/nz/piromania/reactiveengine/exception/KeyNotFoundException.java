package nz.piromania.reactiveengine.exception;

/**
 * A RuntimeException that is triggered when a lookup fails due to a key that is not found.
 */
public class KeyNotFoundException extends RuntimeException {
  public KeyNotFoundException(String message) {
    super(message);
  }

  /**
   * Constructs a descriptive KeyNotFoundException.
   *
   * @param keyName name of the variable the key is stored in
   * @param keyValue value of the key used
   * @param message general further message to the user
   */
  public KeyNotFoundException(String keyName, Object keyValue, String message) {
    super(String.format(
        "Cannot look up using key %s with value %s: %s",
        keyName, keyValue, message
    ));
  }
}
