package nz.piromania.reactiveengine.key;

import nz.piromania.reactiveengine.ComponentKey;

/**
 * A ComponentKey that compares based on equality of the 'value' it contains.
 */
public final class ValueKey implements ComponentKey {

  private final Object value;

  public ValueKey(Object value) {
    this.value = value;
  }

  public Object getValue() {
    return this.value;
  }

  @Override
  public boolean equals(ComponentKey other) {
    return other instanceof ValueKey && this.value.equals(((ValueKey) other).getValue());
  }
}
