package nz.piromania.reactiveengine.layout;

/**
 * Represents a screen Axis. Either vertical or horizontal.
 */
public enum Axis {
    VERTICAL,
    HORIZONTAL
}
