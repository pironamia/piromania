package nz.piromania.reactiveengine.layout;

/**
 * Immutable class that contains padding information.
 */
public class Padding {
  private final double left;
  private final double right;
  private final double top;
  private final double bottom;

  Padding(double top, double right, double bottom, double left) {
    this.top = top;
    this.right = right;
    this.bottom = bottom;
    this.left = left;
  }

  static Padding symmetric(double vertical, double horizontal) {
    return new Padding(
        vertical,
        horizontal,
        vertical,
        horizontal
    );
  }

  public double getBottom() {
    return bottom;
  }

  public double getLeft() {
    return left;
  }

  public double getRight() {
    return right;
  }

  public double getTop() {
    return top;
  }

  /**
   * Creates a new <code>Padding</code> with all properties from this and other summed together.
   */
  public Padding add(Padding other) {
    return new Padding(
        this.top + other.getTop(),
        this.right + other.getRight(),
        this.bottom + other.getBottom(),
        this.left + other.getLeft()
    );
  }
}
