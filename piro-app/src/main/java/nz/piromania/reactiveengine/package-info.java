/**
 * Package containing the reactive UI framework.
 *
 * <p>
 *   The overarching design of this package is heavily inspired by other reactive user interface
 *   frameworks, like <a href="https://reactjs.org/">React</a> and
 *   <a href="https://flutter.dev">Flutter</a>, and <a href="https://mobx.js.org/">MobX</a>.
 *   Except, for the sake of development speed, this implementation is more limited in scope.
 * </p>
 *
 * <p>
 *   Another key difference is that this implemention was designed to be renderer agnostic, with the
 *   aim to be able to connect a Swing GUI and a text command-line renderer to the same
 *   implementation. See {@link nz.piromania.gui} for the GUI renderer implementation.
 * </p>
 *
 *   <p>Overview:</p>
 *
 *   <ul>
 *     <li>
 *       Components are immutable and short-lived.
 *     </li>
 *     <li>
 *       Components are attached to ComponentState instances, which are mutable and long-lived.
 *     </li>
 *     <li>
 *       ComponentStates are built into more components by the Engine.
 *     </li>
 *     <li>
 *       ComponentStates notify the engine that they need rebuilding, e.g. when their state changes
 *       and they might want to pass different parameters into their child components.
 *     </li>
 *     <li>
 *       The engine starts at the changed component and recursively rebuilds its children.
 *     </li>
 *     <li>
 *       The engine emits the completed build to a renderer as a tree of Components.
 *     </li>
 *     <li>
 *       The engine will then wait until the next ComponentState needs updating and repeat.
 *     </li>
 *   </ul>
 *
 *   <p>This package has a few distinct parts and sub-modules:</p>
 *
 *   <ul>
 *     <li>
 *       Core classes - defined in the root package, provide the base implementation of the
 *       engine and classes, such as {@link nz.piromania.reactiveengine.Component} and
 *       {@link nz.piromania.reactiveengine.ComponentState} for consumers to subclass.
 *     </li>
 *     <li>
 *       Core components - defined in the component package, provides base components from which
 *       most user interfaces and components will be constructed. Many of these components are
 *       {@link nz.piromania.reactiveengine.component.DirectlyRenderableComponent} and so have
 *       corresponding GUI renderer implementations.
 *     </li>
 *     <li>
 *       Provider - defined in the {@link nz.piromania.reactiveengine.component.provider} package,
 *       provides a component that has no effect on rendering, but instead facilitates passing of
 *       data from a parent component to all of its children through the
 *       {@link nz.piromania.reactiveengine.BuildContext}.
 *     </li>
 *     <li>
 *       Reactivity - defined in the reactivity package, provides a suite of classes that can be
 *       used to automatically and implicitly update the GUI (or any function) in response to data
 *       changes. Using {@link nz.piromania.reactiveengine.reactivity.Observable} value wrappers and
 *       {@link nz.piromania.reactiveengine.reactivity.component.Observer} or
 *       {@link nz.piromania.reactiveengine.reactivity.AutoRunner}.
 *     </li>
 *   </ul>
 *
 * @author Thomas Finlay
 */
package nz.piromania.reactiveengine;