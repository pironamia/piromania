package nz.piromania.reactiveengine.reactivity;

import java.util.function.Function;
import java.util.function.Supplier;
import nz.piromania.reactiveengine.util.VoidCallback;

/**
 * Manages calling an action function that takes a parameter of type T and returns
 * a value of type R.
 *
 * <p>
 *   Warning: observables that are observed in actions are not added to the parent ObserverContext
 *   (if any), since actions should only be used as setters, not as getters.
 * </p>
 *
 * @see SupplierAction {@link SupplierAction}
 * @see VoidAction {@link VoidAction}
 */
public class Action<T, R> {
  private final Function<T, R> action;
  private final TransactionTracker transactionTracker;
  private final ObserverTracker observerTracker;

  /**
   * Constructs an Action that can run the given action multiple times, tracking
   * observable updates and absorbing observations using the given Transaction and
   * Observer trackers.
   *
   * @param action function to call when when run is called.
   * @param transactionTracker to track the observables that are updated
   * @param observerTracker to prevent observations from leaking into the parent ObserverTracker
   *                        (if any)
   */
  public Action(
      Function<T, R> action,
      TransactionTracker transactionTracker,
      ObserverTracker observerTracker
  ) {
    this.action = action;
    this.transactionTracker = transactionTracker;
    this.observerTracker = observerTracker;
  }

  public Action(Function<T, R> action) {
    this(action, TransactionTracker.main, ObserverTracker.main);
  }

  /**
   * Runs this action in a transactionTracker transaction.
   */
  public final R run(T params) {
    transactionTracker.beginTransaction();
    observerTracker.pushObservationContext();

    try {
      return this.action.apply(params);
    } finally {
      observerTracker.popAndForgetObservationContext();
      transactionTracker.commitTransaction();
    }
  }

  static void run(
      VoidCallback action,
      TransactionTracker transactionTracker,
      ObserverTracker observerTracker
  ) {
    VoidAction.run(action, transactionTracker, observerTracker);
  }

  static <R> R run(
      Supplier<R> action,
      TransactionTracker transactionTracker,
      ObserverTracker observerTracker
  ) {
    return SupplierAction.run(action, transactionTracker, observerTracker);
  }

  public static void run(VoidCallback action) {
    VoidAction.run(action);
  }

  public static <R> R run(Supplier<R> action) {
    return SupplierAction.run(action);
  }
}
