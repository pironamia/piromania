package nz.piromania.reactiveengine.reactivity;

import java.util.HashSet;
import java.util.function.Consumer;
import java.util.function.Supplier;
import nz.piromania.reactiveengine.Disposable;
import nz.piromania.reactiveengine.util.VoidCallback;

/**
 * Automatically runs a function that depends on observables whenever those observables change.
 */
public class AutoRunner<T> implements VoidCallback, Disposable {
  private final Supplier<T> mainFunction;
  private final Consumer<T> onUpdate;
  private final HashSet<Observable> observablesToIgnore = new HashSet<>();
  private final ObserverTracker observerTracker;

  private MutableMultiObserverListenerDisposerDisposer disposer;
  private T lastValue;

  /**
   * Construct an AutoRunner to automatically run <code>mainFunction</code> whenever the observables
   * used within it are changed.
   * <p>
   *   When the return value of the <code>mainFunction</code> changes, <code>onUpdate</code> is
   *   called with this new value.
   * </p>
   *
   * @param observerTracker The ObserverTracker to use. It is assumed that all relevant observables
   *                        are tracked with this tracker.
   * @param mainFunction function that depends on observables and is to be automatically updated.
   * @param onUpdate function that is called with deduplicated return values
   *                 from <code>mainFunction</code>.
   */
  public AutoRunner(
      ObserverTracker observerTracker,
      Supplier<T> mainFunction,
      Consumer<T> onUpdate
  ) {
    this.observerTracker = observerTracker;
    this.mainFunction = mainFunction;
    this.onUpdate = onUpdate;
  }

  /**
   * Constructs an AutoRunner with the main ObserverTracker instance.
   *
   * @see AutoRunner#AutoRunner(ObserverTracker, Supplier, Consumer)
   *      {@link AutoRunner#AutoRunner(ObserverTracker, Supplier, Consumer)}
   */
  public AutoRunner(
      Supplier<T> mainFunction,
      Consumer<T> onUpdate
  ) {
    this(ObserverTracker.main, mainFunction, onUpdate);
  }

  /**
   * Runs mainFunction for the first time and emits its result into onUpdate.
   */
  public void initialise() {
    if (disposer != null) {
      throw new UnsupportedOperationException("Cannot initialise AutoRunner more than once.");
    }

    // Run mainFunction for the first time to get everything initialised.
    observerTracker.pushObservationContext();
    this.lastValue = mainFunction.get();
    // So we don't re-subscribe to these observables next time.
    observablesToIgnore.addAll(
        observerTracker.getCurrentObservationContext().getObservedObservables()
    );
    this.disposer = observerTracker.popObservationContextMutable(this);

    this.onUpdate.accept(this.lastValue);
  }

  /**
   * When an update occurs, we call the mainFunction again and emit its value to onUpdate.
   * We also automatically subscribe to observables that are referenced for the first time in this
   * call to mainFunction.
   */
  @Override
  public void call() {
    if (disposer == null) {
      throw new UnsupportedOperationException("Cannot call AutoRunner before initialising it.");
    }

    observerTracker.pushObservationContext(this.observablesToIgnore);
    final T res = this.mainFunction.get();

    // So we don't re-subscribe to these observables next time.
    observablesToIgnore.addAll(
        observerTracker.getCurrentObservationContext().getObservedObservables()
    );
    observerTracker.popObservationContextMutable(this.disposer, this);

    if (!res.equals(this.lastValue)) {
      this.onUpdate.accept(res);
      this.lastValue = res;
    }
  }

  @Override
  public void dispose() {
    this.observablesToIgnore.clear();

    if (this.disposer != null) {
      this.disposer.dispose();
    }
  }
}
