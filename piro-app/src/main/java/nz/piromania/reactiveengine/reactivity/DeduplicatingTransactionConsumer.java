package nz.piromania.reactiveengine.reactivity;

import java.util.function.Consumer;
import nz.piromania.reactiveengine.util.VoidCallback;

/**
 * Acts as a Consumer of Transactions that only calls the <code>callback</code> when it is
 * called with a different Transaction.
 */
public class DeduplicatingTransactionConsumer implements Consumer<Transaction> {
  private final VoidCallback callback;
  private Transaction lastTransaction;

  public DeduplicatingTransactionConsumer(VoidCallback callback) {
    this.callback = callback;
  }

  @Override
  public void accept(Transaction transaction) {
    if (transaction != lastTransaction) {
      lastTransaction = transaction;
      this.callback.call();
    }
  }
}
