package nz.piromania.reactiveengine.reactivity;

import nz.piromania.reactiveengine.util.VoidCallback;

/**
 * An ObservableListenerDisposer that calls a callback when disposed.
 */
class FunctionalObservableListenerDisposer extends ObservableListenerDisposer {
  private final VoidCallback disposeCallback;

  public FunctionalObservableListenerDisposer(VoidCallback disposeCallback) {
    this.disposeCallback = disposeCallback;
  }

  @Override
  protected void doDispose() {
    this.disposeCallback.call();
  }
}
