package nz.piromania.reactiveengine.reactivity;

import java.util.HashSet;
import java.util.function.Consumer;

/**
 * Generic class that manages subscribing, unsubscribing, and notifying listeners of events.
 */
class ListenersManager<T> {

  private HashSet<Consumer<T>> listeners = new HashSet<>();

  public ObservableListenerDisposer subscribe(Consumer<T> listener) {
    listeners.add(listener);

    return new FunctionalObservableListenerDisposer(() -> {
      this.listeners.remove(listener);
    });
  }

  public void notifyListeners(T param) {
    for (Consumer<T> listener : listeners) {
      listener.accept(param);
    }
  }
}
