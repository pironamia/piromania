package nz.piromania.reactiveengine.reactivity;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Consolidates many ObservableListenerDisposers into one disposer that disposes all of
 * them at once.
 */
public class MutableMultiObserverListenerDisposerDisposer extends ObservableListenerDisposer {
  private final ArrayList<ObservableListenerDisposer> disposers = new ArrayList<>();

  public MutableMultiObserverListenerDisposerDisposer() {}

  public MutableMultiObserverListenerDisposerDisposer(
      Collection<ObservableListenerDisposer> disposers
  ) {
    this.disposers.addAll(disposers);
  }

  public void addDisposer(ObservableListenerDisposer disposer) {
    this.disposers.add(disposer);
  }

  public void addAllDisposers(Collection<ObservableListenerDisposer> disposers) {
    this.disposers.addAll(disposers);
  }

  @Override
  protected void doDispose() {
    for (ObservableListenerDisposer disposer : disposers) {
      disposer.dispose();
    }
  }
}
