package nz.piromania.reactiveengine.reactivity;

import com.google.gson.annotations.SerializedName;
import java.util.function.Consumer;

/**
 * Contains a mutable value, which can be read and written.
 *
 * <p>
 *   Observers are subscribed to receive updates whenever an Observable has been written to.
 *   This subscription is automatically applied (when using e.g. {@link AutoRunner}) on
 *   calls to <code>get</code> on an instance.
 * </p>
 *
 * <p>
 *   Observer updates are batched into atomic 'transactions', and are only notified when
 *   all pending transactions are completed. To enforce this rule, it is an error to set an
 *   observable outside of a transaction. For simple ways to manage this, see {@link Action}.
 * </p>
 */
public class Observable<T> {
  @SerializedName(value = "val")
  private T value;
  private final transient ListenersManager<Transaction> listenersManager = new ListenersManager<>();
  private final transient ObserverTracker observerTracker;
  private final transient TransactionTracker transactionTracker;

  /**
   * Constructs a new observable with the given value, inside the given Observer and Transaction
   * trackers.
   *
   * @param initialValue stored in this observable
   * @param observerTracker ObserverTracker used to track observations of this observable.
   *                        In order for things to work as expected, all parts of the system
   *                        (including Observers and Actions) should use the same ObserverTracker.
   * @param transactionTracker TransactionTracker used to associate writes to this observable
   *                           with the correct 'transaction'. In order for things to work as
   *                           expected, all parts of the system (including Observers and Actions)
   *                           should use the same ObserverTracker.
   */
  public Observable(
      T initialValue,
      ObserverTracker observerTracker,
      TransactionTracker transactionTracker
  ) {
    this.value = initialValue;
    this.observerTracker = observerTracker;
    this.transactionTracker = transactionTracker;
  }

  public Observable(T value) {
    this(value, ObserverTracker.main, TransactionTracker.main);
  }

  /**
   * Gets the value of this observable, and subscribes the caller to updates if necessary.
   */
  public T get() {
    if (observerTracker.hasCurrentObservationContext()) {
      observerTracker.getCurrentObservationContext().notifyObserved(this);
    }
    return this.value;
  }

  /**
   * Sets the new value of this observable.
   *
   * <p><b>Must be called from inside a transaction (see {@link Action}).</b></p>
   */
  public void set(T newValue) {
    if (!transactionTracker.hasCurrentTransaction()) {
      throw new UnsupportedOperationException("Cannot set Observable value outside of an action.");
    }

    final T oldValue = this.value;
    this.value = newValue;

    if (
        (oldValue == null && newValue != null)
            || (oldValue != null && !oldValue.equals(newValue))
    ) {
      transactionTracker.getCurrentTransaction().notifyChanged(this);
    }
  }

  protected void notifyListeners(Transaction transaction) {
    this.listenersManager.notifyListeners(transaction);
  }

  protected ListenersManager getListenersManager() {
    return this.listenersManager;
  }

  public ObservableListenerDisposer subscribeListener(Consumer<Transaction> listener) {
    return this.listenersManager.subscribe(listener);
  }
}
