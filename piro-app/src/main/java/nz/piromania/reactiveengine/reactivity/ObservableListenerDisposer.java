package nz.piromania.reactiveengine.reactivity;

import nz.piromania.reactiveengine.Disposable;

/**
 * Represents a class that can be used to dispose an observable listener subscription.
 * Enforces that disposes may only occur once.
 */
public abstract class ObservableListenerDisposer implements Disposable {
  private boolean disposed = false;

  protected abstract void doDispose();

  @Override
  public final void dispose() {
    if (this.disposed) {
      throw new UnsupportedOperationException(
          "Cannot dispose ObservableListenerDisposer that has already been disposed."
      );
    }
    this.disposed = true;

    this.doDispose();
  }
}
