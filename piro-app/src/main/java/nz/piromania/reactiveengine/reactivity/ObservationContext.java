package nz.piromania.reactiveengine.reactivity;

import java.util.HashSet;

/**
 * Stores the observables that have been observed inside a given 'context' (read function).
 *
 * <p>
 *   This data is then used by the ObserverTracker to subscribe to the observed observables
 *   so that the function may receive updates when these are updated.
 * </p>
 *
 * <p>
 *   This class shouldn't be necessary for API consumers, instead use {@link AutoRunner}.
 * </p>
 */
public class ObservationContext {
  private HashSet<Observable> observedObservables = new HashSet<>();
  private final HashSet<Observable> alreadySubscribedObservables;

  /**
   * Instantiates an ObservationContext that ignores notifications that observables within the
   * <code>alreadySubscribedObservables</code> set have been observed.
   */
  public ObservationContext(HashSet<Observable> alreadySubscribedObservables) {
    this.alreadySubscribedObservables = alreadySubscribedObservables;
  }

  public ObservationContext() {
    this.alreadySubscribedObservables = new HashSet<>(0);
  }

  /**
   * Saves observables that have been observed.
   */
  public void notifyObserved(Observable observable) {
    if (!this.alreadySubscribedObservables.contains(observable)) {
      this.observedObservables.add(observable);
    }
  }

  public HashSet<Observable> getObservedObservables() {
    return this.observedObservables;
  }
}
