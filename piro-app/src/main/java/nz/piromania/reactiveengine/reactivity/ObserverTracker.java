package nz.piromania.reactiveengine.reactivity;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.HashSet;
import java.util.Stack;
import java.util.function.Consumer;
import java.util.function.Supplier;
import nz.piromania.reactiveengine.util.VoidCallback;

/**
 * Tracks and manages the ObservationContext, and manages implicit observation bundling for
 * observables that are 'observed' inside an ObservationContext.
 */
public class ObserverTracker {
  public static final ObserverTracker main = new ObserverTracker();
  private final Stack<ObservationContext> observationContextStack = new Stack<>();

  public void pushObservationContext() {
    this.observationContextStack.push(new ObservationContext());
  }

  public void pushObservationContext(HashSet<Observable> ignoredObservables) {
    this.observationContextStack.push(new ObservationContext(ignoredObservables));
  }

  /**
   * Pops the most recent ObservationContext from the stack.
   * Automatically subscribes <code>onUpdate</code> to be called whenever one of the observables
   * observed within this ObservationContext is changed.
   *
   * @param onUpdate is called when an observable that was read in the ObservationContext that is
   *                 being popped are updated.
   * @return a disposer to be called when the reactions are no longer needed. Note that this
   *         <b>must</b> be called before references are lost.
   * @throws EmptyStackException when there is currently no ObservationContext on the stack
   */
  public ObservableListenerDisposer popObservationContext(VoidCallback onUpdate)
      throws EmptyStackException {
    final ObservationContext context = this.observationContextStack.pop();

    // Ensures that onUpdate is only called once for each transaction.
    final Consumer<Transaction> updateConsumer = new DeduplicatingTransactionConsumer(onUpdate);

    // Subscribe onUpdate to all of the listeners read inside this context.
    final ArrayList<ObservableListenerDisposer> disposers = new ArrayList<>();
    for (Observable observable : context.getObservedObservables()) {
      disposers.add(observable.subscribeListener(updateConsumer));
    }

    // Return a Disposer for that disposes every listener in this context.
    return new FunctionalObservableListenerDisposer(() -> {
      for (ObservableListenerDisposer disposer : disposers) {
        disposer.dispose();
      }
    });
  }

  /**
   * Pops the most recent ObservationContext from the stack and ignores all observations inside it.
   *
   * @throws EmptyStackException when there is currently no ObservationContext on the stack
   */
  public void popAndForgetObservationContext() throws EmptyStackException {
    this.observationContextStack.pop();
  }

  /**
   * Pops the most recent ObservationContext from the stack.
   * Automatically subscribes <code>onUpdate</code> to be called whenever one of the observables
   * observed within this ObservationContext is changed.
   *
   * @param onUpdate is called when an observable that was read in the ObservationContext that is
   *                 being popped are updated.
   * @return a mutable disposer to be called when the reactions are no longer needed. Note that this
   *         <b>must</b> be called before references are lost. This disposer can then be extended to
   *         dispose more observables if extended in future.
   * @see AutoRunner
   * @throws EmptyStackException when there is currently no ObservationContext on the stack
   */
  public MutableMultiObserverListenerDisposerDisposer popObservationContextMutable(
      VoidCallback onUpdate
  ) throws EmptyStackException {
    final ObservationContext context = this.observationContextStack.pop();

    /*
     * Do some naive transaction deduplication since we want onUpdate to only be called once,
     * regardless of the number of Observables updated.
     * NB: This is not entirely effective when using popObservationContextMutable() since a new
     * DeduplicatingTransactionConsumer is created with each call, so the consumer may want to do
     * their own deduplication.
     */
    final Consumer<Transaction> updateConsumer = new DeduplicatingTransactionConsumer(onUpdate);

    // Subscribe onUpdate to all of the listeners read inside this context.
    final ArrayList<ObservableListenerDisposer> disposers = new ArrayList<>();
    for (Observable observable : context.getObservedObservables()) {
      disposers.add(observable.subscribeListener(updateConsumer));
    }

    // Return a mutable Disposer for that disposes every listener in this context.
    return new MutableMultiObserverListenerDisposerDisposer(disposers);
  }

  /**
   * Pops the most recent ObservationContext from the stack.
   * Automatically subscribes <code>onUpdate</code> to be called whenever one of the observables
   * observed within this ObservationContext is changed.
   *
   * <p>
   *   Adds the disposers to the <code>disposerToExtend</code> instead of creating a new one.
   * </p>
   *
   * @param onUpdate is called when an observable that was read in the ObservationContext that is
   *                 being popped are updated.
   * @see AutoRunner
   * @throws EmptyStackException when there is currently no ObservationContext on the stack
   */
  public void popObservationContextMutable(
      MutableMultiObserverListenerDisposerDisposer disposerToExtend,
      VoidCallback onUpdate
  ) throws EmptyStackException {
    final ObservationContext context = this.observationContextStack.pop();

    /*
     * Do some naive transaction deduplication since we want onUpdate to only be called once,
     * regardless of the number of Observables updated.
     * NB: This is not entirely effective when using popObservationContextMutable() since a new
     * DeduplicatingTransactionConsumer is created with each call, so the consumer may want to do
     * their own deduplication.
     */
    final Consumer<Transaction> updateConsumer = new DeduplicatingTransactionConsumer(onUpdate);

    // Subscribe onUpdate to all of the listeners read inside this context.
    final ArrayList<ObservableListenerDisposer> disposers = new ArrayList<>();
    for (Observable observable : context.getObservedObservables()) {
      disposers.add(observable.subscribeListener(updateConsumer));
    }

    // Extend the existing Disposer to also dispose these listeners.
    disposerToExtend.addAllDisposers(disposers);
  }

  public ObservationContext getCurrentObservationContext() {
    return this.observationContextStack.peek();
  }

  public boolean hasCurrentObservationContext() {
    return !this.observationContextStack.empty();
  }

  /**
   * Automatically runs mainFunction (feeding its result into onUpdate) when necessary.
   */
  public <T> ObservableListenerDisposer autoRun(Supplier<T> mainFunction, Consumer<T> onUpdate) {
    final AutoRunner<T> runner = new AutoRunner<>(this, mainFunction, onUpdate);

    runner.initialise();

    return new FunctionalObservableListenerDisposer(runner::dispose);
  }
}
