# Reactivity

The reactiveengine only knows to rebuild components when they call setState(), however for larger applications, state needs to be stored externally (e.g. in a `GameEnvironment` class), and could be modified be other Components or processes without the consumer component's knowledge.

The `reactivity` package provides a solution to this problem. By wrapping variables that may be set in the `GameEnvironment` class (or in classes beneath it, e.g. `Ship`) in `Observable`s (which are never re-set after initialisation), the `reactivity` system can track reads and writes to specific `Observable`s and notify the Components that depend on that `Observable` that they need to rebuild.

In order to ensure that these notifications are delivered efficiently - and not when the `GameEnvironment` state is inconsistent (e.g. if multiple updates should occur at once, we don't want to notify after the first one is updated, since the later updates may be needed to make the state consistent) - we batch updates inside 'transactions' (or `Action`s). The system will only notify listeners that the observables updated in an Action once the last Action in the call stack has completed (e.g. for nested Actions, the first one in the call stack will notify observable listeners (such as `Observers`). `Observables` that were modified in `Action`s further up the stack simply push the record of `Observable`s that were updated down the stack to the root Action).

Another key caveat with Actions is that any observations (reads of `Observable`s) that occur inside the Action will be prevented from subscribing the `Action`'s caller to updates to these `Observable`s (enforcing the idea that `Action` should only be used for modifying state, not to wrap getters). Non-`Action` functions will subscribe the caller as expected, however.

This module is inspired by [mobx](https://mobx.netlify.app/getting-started) - see their high-level documentation for a brief overview of how things are 'supposed' to work.

## Code Example

```java
import java.util.ArrayList;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.Text;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.reactivity.Observable;
import nz.piromania.reactiveengine.reactivity.component.Observer;

class GameEnvironment {

  private final Observable<String> playerName = new Observable<>(null);

  public String getPlayerName() {
    return playerName.get();
  }

  public void setPlayerName(String newValue) {
    Action.run(() -> {
      playerName.set(newValue);
    });
  }
}

class SomeComponent extends Component {
  // Implementation is irrelevant
}

class SomeComponentState extends ComponentState<SomeComponent> {
  @Override
  public ArrayList<Component> build(BuildContext context) {  // Will only rebuild according to local setState() calls, or rebuild of parent Component.
    final GameEnvironment environment = Provider.of(GameEnvironment.class, context);

    return new ArrayList<>(List.of(
        new Observer(
            (BuildContext context) -> {  // Will automatically rebuild its children when the observables read inside it are updated.
              return new ArrayList<>(List.of(
                  new Text(environment.getPlayerName())  // getPlayerName calls Observable.get()
              ));
            })
    ));
  }
}
```