package nz.piromania.reactiveengine.reactivity;

import java.util.function.Supplier;

/**
 * Manages calling an action function that takes no parameters and returns a value of type T.
 *
 * <p>
 *   Warning: observables that are observed in actions are not added to the parent ObserverContext
 *   (if any), since actions should only be used as setters, not as getters.
 * </p>
 *
 * @see Action {@link Action}
 * @see VoidAction {@link VoidAction}
 */
public class SupplierAction<R> {
  private final Supplier<R> action;
  private final TransactionTracker transactionTracker;
  private final ObserverTracker observerTracker;

  /**
   * Constructs a SupplierAction that can run the given action multiple times, tracking
   * observable updates and absorbing observations using the given Transaction and
   * Observer trackers.
   *
   * @param action function to call when when run is called.
   * @param transactionTracker to track the observables that are updated
   * @param observerTracker to prevent observations from leaking into the parent ObserverTracker
   *                        (if any).
   */
  public SupplierAction(
      Supplier<R> action,
      TransactionTracker transactionTracker,
      ObserverTracker observerTracker
  ) {
    this.action = action;
    this.transactionTracker = transactionTracker;
    this.observerTracker = observerTracker;
  }

  public SupplierAction(Supplier<R> action) {
    this(action, TransactionTracker.main, ObserverTracker.main);
  }

  /**
   * Runs this action in a transactionTracker transaction.
   */
  public final R run() {
    transactionTracker.beginTransaction();
    observerTracker.pushObservationContext();

    try {
      return this.action.get();
    } finally {
      observerTracker.popAndForgetObservationContext();
      transactionTracker.commitTransaction();
    }
  }

  static <R> R run(
      Supplier<R> action,
      TransactionTracker transactionTracker,
      ObserverTracker observerTracker
  ) {
    return new SupplierAction<R>(action, transactionTracker, observerTracker).run();
  }

  static <R> R run(Supplier<R> action) {
    return new SupplierAction<R>(action).run();
  }
}
