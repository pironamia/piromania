package nz.piromania.reactiveengine.reactivity;

import java.util.HashSet;

/**
 * Stores the observables that have been changed inside a given transaction (read Action).
 *
 * <p>
 *   This data is then used by the TransactionTracker to notify those depending on the observables
 *   when the transaction has been completed.
 * </p>
 *
 * <p>
 *   This class shouldn't be necessary for API consumers, instead use {@link Action}.
 * </p>
 */
class Transaction {
  private HashSet<Observable> changedObservables = new HashSet<>();

  public void notifyChanged(Observable observable) {
    this.changedObservables.add(observable);
  }

  public HashSet<Observable> getChangedObservables() {
    return this.changedObservables;
  }
}
