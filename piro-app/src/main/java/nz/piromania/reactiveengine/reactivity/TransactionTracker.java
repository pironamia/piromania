package nz.piromania.reactiveengine.reactivity;

import java.util.EmptyStackException;
import java.util.HashSet;
import java.util.Stack;

/**
 * Tracks the observables that have been updated in the current 'transaction' (read Action).
 *
 * <p>This class shouldn't be necessary for API consumers, instead use {@link Action}.</p>
 */
class TransactionTracker {
  public static final TransactionTracker main = new TransactionTracker();

  private final Stack<Transaction> transactionStack = new Stack<>();
  private final HashSet<Observable> changedObservables = new HashSet<>();

  public void beginTransaction() {
    this.transactionStack.push(new Transaction());
  }

  /**
   * The transaction is complete, so notify all of the listeners of the affected observables.
   * <p>
   *   We only do this after the transaction completes to prevent unnecessary GUI rebuilds, and to
   *   prevent the GUI from rebuilding when the state is inconsistent (mid-transaction).
   * </p>
   *
   * @throws EmptyStackException when there is no Transaction currently under way
   */
  public void commitTransaction() throws EmptyStackException {
    final Transaction trans = this.transactionStack.pop();

    // Save the updated observables for later (or now)
    this.changedObservables.addAll(trans.getChangedObservables());

    if (this.transactionStack.empty()) {
      // Notify all listeners.
      for (Observable observable : this.changedObservables) {
        observable.notifyListeners(trans);
      }
      this.changedObservables.clear();
    }
  }

  public Transaction getCurrentTransaction() {
    return this.transactionStack.peek();
  }

  public boolean hasCurrentTransaction() {
    return !this.transactionStack.empty();
  }
}
