package nz.piromania.reactiveengine.reactivity;

import nz.piromania.reactiveengine.util.VoidCallback;

/**
 * Manages calling an action function that takes no parameters and returns void.
 *
 * <p>
 *   Warning: observables that are observed in actions are not added to the parent ObserverContext
 *   (if any), since actions should only be used as setters, not as getters.
 * </p>
 *
 * @see Action {@link Action}
 * @see SupplierAction {@link SupplierAction}
 */
public class VoidAction {
  private final VoidCallback action;
  private final TransactionTracker transactionTracker;
  private final ObserverTracker observerTracker;

  /**
   * Constructs a VoidAction that can run the given action multiple times, tracking
   * observable updates and absorbing observations using the given Transaction and
   * Observer trackers.
   *
   * @param action function to call when when run is called
   * @param transactionTracker to track the observables that are updated
   * @param observerTracker to prevent observations from leaking into the parent ObserverTracker
   *                        (if any)
   */
  public VoidAction(
      VoidCallback action,
      TransactionTracker transactionTracker,
      ObserverTracker observerTracker
  ) {
    this.action = action;
    this.transactionTracker = transactionTracker;
    this.observerTracker = observerTracker;
  }

  public VoidAction(VoidCallback action) {
    this(action, TransactionTracker.main, ObserverTracker.main);
  }

  /**
   * Runs this action in a transactionTracker transaction.
   */
  public final void run() {
    transactionTracker.beginTransaction();
    observerTracker.pushObservationContext();

    try {
      this.action.call();
    } finally {
      observerTracker.popAndForgetObservationContext();
      transactionTracker.commitTransaction();
    }
  }

  static void run(
      VoidCallback action,
      TransactionTracker transactionTracker,
      ObserverTracker observerTracker
  ) {
    new VoidAction(action, transactionTracker, observerTracker).run();
  }

  static void run(VoidCallback action) {
    new VoidAction(action).run();
  }
}
