package nz.piromania.reactiveengine.reactivity.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import nz.piromania.reactiveengine.reactivity.Observable;

/**
 * An ArrayList implementation that automatically notifies listeners of changes to its attributes
 * and its elements' attributes.
 */
public class ObservableArrayList<E> implements List<E> {

  private final List<Observable<E>> internalList;

  // Internal Observables
  private final Observable<Integer> size = new Observable<>(0);

  // toggled every time internal state changes to trigger re-run of containsKey etc.
  private final Observable<Boolean> internalUpdateObservable = new Observable<>(false);

  public ObservableArrayList() {
    this.internalList = new ArrayList<>();
  }

  private void signalInternalUpdate() {
    internalUpdateObservable.set(!internalUpdateObservable.get());
  }

  @Override
  public int size() {
    return size.get();
  }

  @Override
  public boolean isEmpty() {
    return size.get() == 0;
  }

  @Override
  public boolean contains(Object o) {
    internalUpdateObservable.get();
    return internalList.stream().anyMatch((ob) -> {
      return (o == null && ob.get() == null) || (o != null && o.equals(ob.get()));
    });
  }

  @Override
  public Iterator<E> iterator() {
    internalUpdateObservable.get();
    return internalList.stream().map(Observable::get).iterator();
  }

  @Override
  public Object[] toArray() {
    internalUpdateObservable.get();
    return internalList.stream().map(Observable::get).toArray();
  }

  @Override
  public <T> T[] toArray(T[] a) {
    internalUpdateObservable.get();
    for (int i = 0; i < size(); i++) {
      a[i] = (T) get(i);
    }
    return a;
  }

  @Override
  public E remove(int index) {
    signalInternalUpdate();
    size.set(size.get() - 1);
    return internalList.remove(index).get();
  }

  @Override
  public boolean remove(Object o) {
    final var index = indexOf(o);
    if (index != -1) {
      internalList.remove(index);
      size.set(size.get() - 1);
      signalInternalUpdate();
      return true;
    } else {
      return false;
    }
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    internalUpdateObservable.get();

    for (var element : c) {
      if (!contains(element)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean addAll(Collection<? extends E> c) {
    for (var element : c) {
      add(element);
    }
    return true;
  }

  @Override
  public boolean addAll(int index, Collection<? extends E> c) {
    int i = index;
    for (var element : c) {
      add(i, element);
      i++;
    }
    return i != index;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    final int initialSize = size.get();
    for (var element : c) {
      remove(element);
    }
    return size.get() != initialSize;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    final int initialSize = size.get();
    for (int i = size.get() - 1; i >= 0; i--) {
      if (!c.contains(get(i))) {
        remove(i);
      }
    }
    return initialSize != size.get();
  }

  @Override
  public void clear() {
    size.set(0);
    signalInternalUpdate();
    internalList.clear();
  }

  @Override
  public E get(int index) {
    return internalList.get(index).get();
  }

  @Override
  public E set(int index, E element) {
    internalList.get(index).set(element);
    return internalList.get(index).get();
  }

  @Override
  public boolean add(E e) {
    size.set(size.get() + 1);
    signalInternalUpdate();
    return internalList.add(new Observable<>(e));
  }

  @Override
  public void add(int index, E element) {
    size.set(size.get() + 1);

    E currentElement = element;

    for (int i = index; i < internalList.size(); i++) {
      final E intermediateCurrentElement = internalList.get(i).get();
      internalList.get(i).set(currentElement);
      currentElement = intermediateCurrentElement;
    }
    internalList.add(new Observable<>(currentElement));
  }

  @Override
  public int indexOf(Object o) {
    internalUpdateObservable.get();

    for (int i = 0; i < size.get(); i++) {
      final var el = internalList.get(i).get();
      if ((el == null && o == null) || (el != null && el.equals(o))) {
        return i;
      }
    }

    return -1;
  }

  @Override
  public int lastIndexOf(Object o) {
    internalUpdateObservable.get();

    int lastIndex = -1;

    for (int i = 0; i < size.get(); i++) {
      final var element = internalList.get(i).get();
      if ((element == null && o == null) || (element != null && element.equals(o))) {
        lastIndex = i;
      }
    }

    return lastIndex;
  }

  @Override
  public ListIterator<E> listIterator() {
    internalUpdateObservable.get();
    return new ObservableArrayListIterator(0);
  }

  @Override
  public ListIterator<E> listIterator(int index) {
    internalUpdateObservable.get();
    return new ObservableArrayListIterator(index);
  }

  @Override
  public List<E> subList(int fromIndex, int toIndex) {
    throw new UnsupportedOperationException(
        "ObservableArrayList does not support creating subLists at this time."
    );
  }

  /**
   * A custom ListIterator for ObservableArrayLists.
   */
  public class ObservableArrayListIterator implements ListIterator<E> {

    private int currentIndex = 0;

    ObservableArrayListIterator(int startIndex) {
      currentIndex = startIndex;
    }

    @Override
    public boolean hasNext() {
      return currentIndex == size.get();
    }

    @Override
    public E next() {
      if (!hasNext()) {
        throw new NoSuchElementException();
      } else {
        currentIndex++;
        return get(currentIndex);
      }
    }

    @Override
    public boolean hasPrevious() {
      return currentIndex != 0;
    }

    @Override
    public E previous() {
      if (!hasPrevious()) {
        throw new NoSuchElementException();
      } else {
        currentIndex--;
        return get(currentIndex);
      }
    }

    @Override
    public int nextIndex() {
      return currentIndex + 1;
    }

    @Override
    public int previousIndex() {
      return currentIndex - 1;
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException(
          "ObservableArrayListIterator does not support the remove operation."
      );
    }

    @Override
    public void set(E e) {
      throw new UnsupportedOperationException(
          "ObservableArrayListIterator does not support the set operation."
      );
    }

    @Override
    public void add(E e) {
      throw new UnsupportedOperationException(
          "ObservableArrayListIterator does not support the add operation."
      );
    }
  }
}
