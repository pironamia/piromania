package nz.piromania.reactiveengine.reactivity.collections;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import nz.piromania.reactiveengine.reactivity.Observable;

/**
 * A Map implementation that automatically notifies listeners of changes to its attributes and its
 * elements' attributes.
 */
public class ObservableMap<K, V> implements Map<K, V> {

  private final Map<K, Observable<V>> internalMap;

  // Internal Observables
  private final Observable<Integer> size = new Observable<>(0);

  // toggled every time internal state changes to trigger re-run of containsKey etc.
  private final Observable<Boolean> internalUpdateObservable = new Observable<>(false);

  public ObservableMap() {
    internalMap = new HashMap<>();
  }

  private void signalInternalUpdate() {
    internalUpdateObservable.set(!internalUpdateObservable.get());
  }

  @Override
  public int size() {
    return size.get();
  }

  @Override
  public boolean isEmpty() {
    return size.get() == 0;
  }

  @Override
  public boolean containsKey(Object key) {
    internalUpdateObservable.get();
    return internalMap.containsKey(key);
  }

  @Override
  public boolean containsValue(Object value) {
    internalUpdateObservable.get();
    return internalMap.containsValue(value);
  }

  @Override
  public V get(Object key) {
    return internalMap.get(key).get();
  }

  @Override
  public V put(K key, V value) {
    if (internalMap.containsKey(key)) {
      final V originalVal = internalMap.get(key).get();
      internalMap.get(key).set(value);
      return originalVal;
    } else {
      final var val = new Observable<>(value);
      internalMap.put(key, val);

      size.set(size.get() + 1);
      signalInternalUpdate();

      return null;
    }
  }

  @Override
  public V remove(Object key) {
    if (internalMap.containsKey(key)) {
      final V val = internalMap.get(key).get();
      internalMap.remove(key);

      size.set(size.get() - 1);
      signalInternalUpdate();

      return val;
    } else {
      internalUpdateObservable.get();
      return null;
    }
  }

  @Override
  public void putAll(Map<? extends K, ? extends V> m) {
    for (var entry : m.entrySet()) {
      put(entry.getKey(), entry.getValue());
    }
  }

  @Override
  public void clear() {
    internalMap.clear();
    signalInternalUpdate();
    size.set(0);
  }

  @Override
  public Set<K> keySet() {
    internalUpdateObservable.get();
    return internalMap.keySet();
  }

  @Override
  public Collection<V> values() {
    internalUpdateObservable.get();
    return internalMap.values().stream().map(Observable::get).collect(Collectors.toList());
  }

  @Override
  public Set<Entry<K, V>> entrySet() {
    internalUpdateObservable.get();
    return internalMap.entrySet().stream().map((entry) -> {
      return new SimpleImmutableEntry<K, V>(entry.getKey(), entry.getValue().get());
    }).collect(Collectors.toSet());
  }
}
