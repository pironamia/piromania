package nz.piromania.reactiveengine.reactivity.component;

import java.util.ArrayList;
import java.util.function.Function;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;

/**
 * A Component that automatically runs the given builder to produce it's children.
 *
 * <p>
 *   If that builder relies on observables, then it automatically rebuilds whenever the values of
 *  those observables change.
 * </p>
 */
public class Observer extends Component {
  private final Function<BuildContext, ArrayList<Component>> builder;

  /**
   * Constructs a component that calls the <code>builder</code> method to automatically rebuild
   * it's children, reacting to changes in observables that are observed in the given builder.
   */
  public Observer(Function<BuildContext, ArrayList<Component>> builder) {
    this.builder = builder;
  }

  public Function<BuildContext, ArrayList<Component>> getBuilder() {
    return builder;
  }

  @Override
  protected ObserverState createState() {
    return new ObserverState();
  }
}
