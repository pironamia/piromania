package nz.piromania.reactiveengine.reactivity.component;

import java.util.ArrayList;
import java.util.HashSet;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.reactivity.MutableMultiObserverListenerDisposerDisposer;
import nz.piromania.reactiveengine.reactivity.Observable;
import nz.piromania.reactiveengine.reactivity.ObserverTracker;

/**
 * State for the {@link Observer} component.
 * <p>
 *   Operates similarly to {@link nz.piromania.reactiveengine.reactivity.AutoRunner} to
 *   automatically keep the child builder up-to-date with changes in its subscribed observables.
 * </p>
 *
 * <p>
 *   It would be nice if AutoRunner could be used here, however there is no way (except for
 *   storing the last BuildContext in the state - yuck) to synchronise AutoRunner with the
 *   reactiveengine build system.
 * </p>
 */
public class ObserverState extends ComponentState<Observer> {
  final ObserverTracker observerTracker = ObserverTracker.main;

  MutableMultiObserverListenerDisposerDisposer disposer;
  private final HashSet<Observable> observablesToIgnore = new HashSet<>();

  /**
   * Receives update calls from ObserverTracker.
   */
  public void onUpdate() {
    if (disposer == null) {
      throw new UnsupportedOperationException("Cannot call ObserverState before initialising it.");
    }

    // Trigger a re-render with no actual state updates (since the state is external)
    setState(() -> {});
  }

  private void reset() {
    this.observablesToIgnore.clear();
    if (disposer != null) {
      disposer.dispose();
      disposer = null;
    }
  }

  @Override
  public void didChangeDependencies(Observer oldComponent) {
    super.didChangeDependencies(oldComponent);

    if (! oldComponent.getBuilder().equals(getComponent().getBuilder())) {
      // The builder has changed, so forget everything we know about the old builder.
      reset();
    }
  }

  @Override
  public void dispose() {
    reset();

    super.dispose();
  }

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var builder = getComponent().getBuilder();

    observerTracker.pushObservationContext(observablesToIgnore);

    final ArrayList<Component> res = builder.apply(context);

    final var observableObservablesCopy = new HashSet<Observable>();
    for (var observable : observerTracker.getCurrentObservationContext().getObservedObservables()) {
      observableObservablesCopy.add(observable);
    }

    observablesToIgnore.addAll(
        observableObservablesCopy
    );

    if (disposer == null) {
      // Initialise a new disposer
      this.disposer = observerTracker.popObservationContextMutable(this::onUpdate);
    } else {
      // Extend the existing disposer to collect the extra observations.
      observerTracker.popObservationContextMutable(this.disposer, this::onUpdate);
    }

    return res;
  }
}
