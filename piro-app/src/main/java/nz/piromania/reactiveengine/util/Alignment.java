package nz.piromania.reactiveengine.util;

/**
 * Enumeration for alignment in a box.
 */
public enum Alignment {
  TOP_LEFT,
  TOP_CENTRE,
  TOP_RIGHT,
  CENTRE_LEFT,
  CENTRE,
  CENTRE_RIGHT,
  BOTTOM_LEFT,
  BOTTOM_CENTRE,
  BOTTOM_RIGHT
}
