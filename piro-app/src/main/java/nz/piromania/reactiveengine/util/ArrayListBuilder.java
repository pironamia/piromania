package nz.piromania.reactiveengine.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class to help with building ArrayLists.
 */
public class ArrayListBuilder {

  protected ArrayListBuilder() {}

  /**
   * Builds an ArrayList with the given elements.
   */
  @SafeVarargs
  public static <T> ArrayList<T> build(T... elements) {
    return new ArrayList<>(List.of(elements));
  }

  /**
   * Builds and ArrayList from the given elements (filtering out nulls).
   */
  @SafeVarargs
  public static <T> ArrayList<T> buildFilteringNulls(T... elements) {
    final var res = new ArrayList<T>();

    for (var el : elements) {
      if (el != null) {
        res.add(el);
      }
    }

    return res;
  }
}
