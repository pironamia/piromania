package nz.piromania.reactiveengine.util;

import java.security.InvalidParameterException;
import java.util.Objects;

/**
 * Represents the size of a box.
 */
public class BoxSize {
  private final Integer width;
  private final Integer height;

  /**
   * Defines the size of a box with width and height.
   *
   * <p>
   *   The BoxSize can be unbounded on at most one axis (pass null).
   * </p>
   *
   * @throws InvalidParameterException when width and height are both null
   */
  public BoxSize(Integer width, Integer height) {
    if (width == null && height == null) {
      throw new InvalidParameterException("Cannot have a BoxSize with both width and height null.");
    }

    this.width = width;
    this.height = height;
  }

  public static BoxSize fromWidth(int width) {
    return new BoxSize(width, null);
  }

  public static BoxSize fromHeight(int height) {
    return new BoxSize(null, height);
  }

  public Integer getWidth() {
    return width;
  }

  public Integer getHeight() {
    return height;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BoxSize boxSize = (BoxSize) o;
    return Objects.equals(getWidth(), boxSize.getWidth()) && Objects
        .equals(getHeight(), boxSize.getHeight());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getWidth(), getHeight());
  }
}
