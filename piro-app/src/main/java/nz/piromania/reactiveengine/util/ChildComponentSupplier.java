package nz.piromania.reactiveengine.util;

import nz.piromania.reactiveengine.Component;

/**
 * Interface for a class that supplies a Component through a getChild method. Used by {@link
 * SingleChildComponentState}.
 */
public interface ChildComponentSupplier {

  Component getChild();
}
