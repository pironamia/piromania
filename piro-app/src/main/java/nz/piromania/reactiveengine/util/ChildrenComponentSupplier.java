package nz.piromania.reactiveengine.util;

import java.util.ArrayList;
import nz.piromania.reactiveengine.Component;

/**
 * Interface for a class that supplies a multiple Components through a getChildren method.
 * Used by {@link MultipleChildrenComponentState}.
 */
public interface ChildrenComponentSupplier {
  ArrayList<Component> getChildren();
}
