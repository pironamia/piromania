package nz.piromania.reactiveengine.util;

import java.util.Objects;

/**
 * Insets to be applied to the edges of a box.
 */
public class EdgeInsets {
  private final int top;
  private final int left;
  private final int bottom;
  private final int right;

  /**
   * Creates and initializes a new {@code EdgeInsets} object with the specified top, left, bottom,
   * and right insets.
   *
   * @param top    the inset from the top.
   * @param left   the inset from the left.
   * @param bottom the inset from the bottom.
   * @param right  the inset from the right.
   */
  public EdgeInsets(int top, int left, int bottom, int right) {
    this.top = top;
    this.left = left;
    this.bottom = bottom;
    this.right = right;
  }

  public EdgeInsets(int vertical, int horizontal) {
    this(vertical, horizontal, vertical, horizontal);
  }

  public EdgeInsets(int all) {
    this(all, all, all, all);
  }

  public static final EdgeInsets zero = new EdgeInsets(0, 0, 0, 0);

  public int getTop() {
    return top;
  }

  public int getLeft() {
    return left;
  }

  public int getBottom() {
    return bottom;
  }

  public int getRight() {
    return right;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EdgeInsets that = (EdgeInsets) o;
    return getTop() == that.getTop() && getLeft() == that.getLeft() && getBottom() == that
        .getBottom()
        && getRight() == that.getRight();
  }

  @Override
  public int hashCode() {
    return Objects.hash(getTop(), getLeft(), getBottom(), getRight());
  }
}
