package nz.piromania.reactiveengine.util;

import java.util.ArrayList;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;

/**
 * ComponentState for a leaf component
 * (i.e. one that has no children and is handled purely by the renderer).
 */
public class LeafComponentState extends ComponentState<Component> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    return new ArrayList<Component>();
  }
}
