package nz.piromania.reactiveengine.util;

import java.util.ArrayList;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;

/**
 * ComponentState for Components that contain no logic in their state and have multiple
 * children.
 */
public class MultipleChildrenComponentState<T extends Component & ChildrenComponentSupplier>
    extends ComponentState<T> {
  @Override
  public ArrayList<Component> build(BuildContext context) {
    return getComponent().getChildren();
  }
}
