package nz.piromania.reactiveengine.util;

/**
 * Type for a function that takes no parameters and returns void.
 */
@FunctionalInterface
public interface VoidCallback {
  void call();
}
