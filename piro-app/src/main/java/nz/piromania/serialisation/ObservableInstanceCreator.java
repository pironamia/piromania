package nz.piromania.serialisation;

import com.google.gson.InstanceCreator;
import java.lang.reflect.Type;
import nz.piromania.reactiveengine.reactivity.Observable;

/**
 * This class is used to serialize observable components from a JSON file.
 */
@SuppressWarnings("rawtypes")
public class ObservableInstanceCreator implements InstanceCreator<Observable> {
    
  /**
   * {@inheritDoc}
   */
  @SuppressWarnings("unchecked")
  @Override
  public Observable createInstance(Type type) {
    return new Observable(null);
  }
}
