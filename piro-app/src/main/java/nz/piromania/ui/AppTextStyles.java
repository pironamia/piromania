package nz.piromania.ui;

import java.awt.Color;
import nz.piromania.reactiveengine.component.text.style.FontStyle;
import nz.piromania.reactiveengine.component.text.style.FontWeight;
import nz.piromania.reactiveengine.component.text.style.TextStyle;

/**
 * Container class to hold all of the text styles used in this application.
 */
public class AppTextStyles {

  public static final TextStyle headingStyle = new TextStyle(
      FontStyle.NORMAL,
      FontWeight.BOLD,
      "Pirata One",
      40,
      Color.BLACK
  );

  public static final TextStyle headingStyleWhite = headingStyle.copyWith(
      null, null, null, null, Color.WHITE);

  public static final TextStyle errorStyle = new TextStyle(
      FontStyle.NORMAL,
      FontWeight.NORMAL,
      TextStyle.DEFAULT_FAMILY,
      12,
      Color.RED
  );

  public static final TextStyle detailStyle = new TextStyle(
      FontStyle.NORMAL,
      FontWeight.NORMAL,
      TextStyle.DEFAULT_FAMILY,
      12,
      Color.BLACK
  );

  public static final TextStyle detailStyleWhite = detailStyle.copyWith(
      null, null, null, null, Color.WHITE);

  public static final TextStyle boldStyle = new TextStyle(
      FontStyle.NORMAL,
      FontWeight.BOLD,
      TextStyle.DEFAULT_FAMILY,
      14,
      Color.BLACK
  );
  public static final TextStyle boldStyleWhite = boldStyle.copyWith(
      null, null, null, null, Color.WHITE);
}
