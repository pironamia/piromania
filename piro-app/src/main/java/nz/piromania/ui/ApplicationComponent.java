package nz.piromania.ui;

import nz.piromania.reactiveengine.Component;

/**
 * The root component for this application.
 */
public class ApplicationComponent extends Component {
  @Override
  protected ApplicationComponentState createState() {
    return new ApplicationComponentState();
  }
}
