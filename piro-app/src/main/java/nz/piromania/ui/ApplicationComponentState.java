package nz.piromania.ui;

import java.util.ArrayList;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.reactivity.component.Observer;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.ui.screen.EventScreenWrapperComponent;
import nz.piromania.ui.screen.GameOverScreenComponent;
import nz.piromania.ui.screen.IslandScreenComponent;
import nz.piromania.ui.screen.IslandTradingScreenComponent;
import nz.piromania.ui.screen.JourneyPlannerScreenComponent;
import nz.piromania.ui.screen.MapScreenComponent;
import nz.piromania.ui.screen.PlayerDetailEntryScreenComponent;
import nz.piromania.ui.screen.PlayerShipSelectionScreenComponent;
import nz.piromania.ui.screen.ShipInventoryScreenComponent;
import nz.piromania.ui.screen.ShipScreenComponent;
import nz.piromania.ui.screen.ShipUpgradeScreenComponent;
import nz.piromania.ui.screen.WelcomeScreenComponent;

/**
 * The root ComponentState for this application. Contains the GameEnvironment and passes it to its
 * children.
 */
public class ApplicationComponentState extends ComponentState<ApplicationComponent> {

  private final GameEnvironment environment;

  /**
   * Constructs a new ApplicationComponentState with a GameEnvironment.
   */
  public ApplicationComponentState() {
    try {
      environment = new GameEnvironment("");
    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException("Failed to construct ApplicationComponentState", e);
    }
  }

  @Override
  public ArrayList<Component> build(BuildContext context) {

    return ArrayListBuilder.build(
        new Provider(
            environment,
            new Observer((context1) -> {
              return ArrayListBuilder.build(
                  switch (environment.getScreenManager().getCurrentScreen()) {
                    case WELCOME -> new WelcomeScreenComponent();
                    case PLAYER_DETAILS_1 -> new PlayerDetailEntryScreenComponent();
                    case PLAYER_DETAILS_2 -> new PlayerShipSelectionScreenComponent();
                    case ISLAND_OVERVIEW -> new IslandScreenComponent();
                    case ISLAND_MAP -> new MapScreenComponent();
                    case ISLAND_TRADE -> new IslandTradingScreenComponent();
                    case SHIP_OVERVIEW -> new ShipScreenComponent();
                    case SHIP_INVENTORY -> new ShipInventoryScreenComponent();
                    case EVENT -> new EventScreenWrapperComponent();
                    case JOURNEY_PLANNER -> new JourneyPlannerScreenComponent();
                    case SHIP_UPGRADE -> new ShipUpgradeScreenComponent();
                    case GAME_OVER -> new GameOverScreenComponent();
                  }
              );
            })
        )
    );
  }
}
