package nz.piromania.ui;

/**
 * Enumeration of all 'screens' in the application. Used by ApplicationComponent to determine which
 * screen component to show.
 */
public enum Screen {
  WELCOME,
  PLAYER_DETAILS_1,   // PlayerDetailEntryScreen
  PLAYER_DETAILS_2,   // PlayerShipSelectionScreen

  // In Game
  ISLAND_OVERVIEW,  // main screen
  ISLAND_MAP,
  ISLAND_TRADE,     // trading screen
  SHIP_OVERVIEW,
  SHIP_INVENTORY,
  SHIP_UPGRADE,
  EVENT,
  JOURNEY_PLANNER,

  GAME_OVER
}
