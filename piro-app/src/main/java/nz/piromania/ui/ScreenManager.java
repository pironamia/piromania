package nz.piromania.ui;

import nz.piromania.reactiveengine.reactivity.Action;
import nz.piromania.reactiveengine.reactivity.Observable;

/**
 * Manages the current screen and its (optional) arguments.
 */
public class ScreenManager {

  private final Observable<Screen> currentScreen;
  private final Observable<Object> currentScreenArgs;

  /**
   * Instantiates a new screen manager,
   * with a default start screen and the starting arguments.
   *
   * @param startScreen the screen to start on
   * @param startArgs   the arguments to call on start
   */
  public ScreenManager(Screen startScreen, Object startArgs) {
    currentScreen = new Observable<>(startScreen);
    currentScreenArgs = new Observable<>(startArgs);
  }

  public Screen getCurrentScreen() {
    return currentScreen.get();
  }

  public Object getCurrentScreenArgs() {
    return currentScreenArgs.get();
  }

  /**
   * Replaces the current screen with the given screen, saving args for it to look up if required.
   */
  public void show(Screen screen, Object args) {
    Action.run(() -> {
      currentScreen.set(screen);
      currentScreenArgs.set(args);
    });
  }

  /**
   * Replaces the current screen with the given screen, saving args for it to look up if required.
   */
  public void show(Screen screen) {
    this.show(screen, null);
  }
}
