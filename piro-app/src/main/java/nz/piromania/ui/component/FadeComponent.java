package nz.piromania.ui.component;

import java.awt.Color;
import nz.piromania.reactiveengine.Component;

/**
 * Component that provides a faded background to its child.
 */
public class FadeComponent extends Component {

  private final Component child;
  private final Color backgroundColour;

  public FadeComponent(Color backgroundColour, Component child) {
    this.backgroundColour = backgroundColour;
    this.child = child;
  }

  public FadeComponent(Component child) {
    this.backgroundColour = null;
    this.child = child;
  }

  @Override
  protected FadeComponentState createState() {
    return new FadeComponentState();
  }

  public Component getChild() {
    return child;
  }

  public Color getBackgroundColour() {
    return backgroundColour;
  }
}
