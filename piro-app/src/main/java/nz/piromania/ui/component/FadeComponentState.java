package nz.piromania.ui.component;

import java.awt.Color;
import java.util.ArrayList;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.BackgroundComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.EdgeInsets;

/**
 * ComponentState for {@link FadeComponent}.
 */
public class FadeComponentState extends ComponentState<FadeComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    return ArrayListBuilder.build(
        new BackgroundComponent(
            (getComponent().getBackgroundColour() == null) ? new Color(0, 0, 0, 127)
                : getComponent().getBackgroundColour(),
            new PaddingComponent(
                new EdgeInsets(10),
                getComponent().getChild()
            )
        )
    );
  }
}
