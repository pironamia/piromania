package nz.piromania.ui.component;

import nz.piromania.reactiveengine.Component;

/**
 * Helper component that creates the basic structure for a page.
 */
public class PageWrapperComponent extends Component {
  private final String imagePath;
  private final Component topNav;
  private final Component child;
  private final Component bottomNav;

  private final Integer bottomNavHeight;

  /**
   * Constructs a PageWrapperComponent. All parameters can be null.
   *
   * @param imagePath path to image for the background
   * @param topNav    component to display in 50px at the top of the screen
   * @param child     component to fill the space between topNav and bottomNav
   * @param bottomNav component to display in the bottomNavHeight (default 50px) at the bottom of
   *                  the screen
   */
  public PageWrapperComponent(String imagePath, Component topNav, Component child,
      Component bottomNav,
      Integer bottomNavHeight) {
    this.imagePath = imagePath;
    this.topNav = topNav;
    this.child = child;
    this.bottomNav = bottomNav;
    this.bottomNavHeight = bottomNavHeight;
  }

  /**
   * Constructs a PageWrapperComponent. All parameters can be null.
   *
   * @param imagePath path to image for the background
   * @param topNav    component to display in 50px at the top of the screen
   * @param child     component to fill the space between topNav and bottomNav
   * @param bottomNav component to display in the bottomNavHeight (default 50px) at the bottom of
   *                  the screen
   */
  public PageWrapperComponent(String imagePath, Component topNav, Component child,
      Component bottomNav) {
    this(imagePath, topNav, child, bottomNav, 50);
  }

  @Override
  protected PageWrapperComponentState createState() {
    return new PageWrapperComponentState();
  }

  public Component getChild() {
    return child;
  }

  public String getImagePath() {
    return imagePath;
  }

  public Component getTopNav() {
    return topNav;
  }

  public Component getBottomNav() {
    return bottomNav;
  }

  public Integer getBottomNavHeight() {
    return bottomNavHeight;
  }
}
