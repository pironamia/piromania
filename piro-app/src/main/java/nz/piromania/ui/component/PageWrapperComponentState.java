package nz.piromania.ui.component;

import java.awt.Color;
import java.util.ArrayList;
import nz.piromania.App;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ContainerComponentBuilder;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.ResourceImageComponent;
import nz.piromania.reactiveengine.component.StackComponent;
import nz.piromania.reactiveengine.component.list.FlexibleComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.Alignment;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.BoxSize;
import nz.piromania.reactiveengine.util.EdgeInsets;

/**
 * ComponentState for {@link PageWrapperComponent}.
 */
public class PageWrapperComponentState extends ComponentState<PageWrapperComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    return ArrayListBuilder.build(
        new StackComponent(
            new ListComponent(
                Axis.VERTICAL,

                ArrayListBuilder.buildFilteringNulls(
                    ((getComponent().getTopNav() != null) ? (
                        new FadeComponent(
                            new Color(125, 125, 0, 127),
                            new ContainerComponentBuilder()
                                .setSize(new BoxSize(null, 50))
                                .setAlignment(Alignment.TOP_LEFT)
                                .build(getComponent().getTopNav())
                        )
                    )
                        : null
                    ),

                    new FlexibleComponent(
                        2,
                        new PaddingComponent(
                            new EdgeInsets(20, 20),
                            getComponent().getChild()
                        )
                    ),

                    ((getComponent().getBottomNav() != null) ? (
                        new FadeComponent(
                            new Color(125, 125, 0, 127),
                            new ContainerComponentBuilder()
                                .setSize(new BoxSize(null, getComponent().getBottomNavHeight()))
                                .setAlignment(Alignment.TOP_LEFT)
                                .build(getComponent().getBottomNav())
                        )
                    )
                        : null
                    )
                )
            ),

            new ResourceImageComponent(
                new BoxSize(App.getScreenWidth(), App.getScreenHeight()),
                getComponent().getImagePath()
            )
        )
    );
  }
}
