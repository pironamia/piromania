package nz.piromania.ui.component;

import nz.piromania.reactiveengine.Component;

/**
 * Wraps child in components that ensure that it is expanded to fill the full screen width
 * when nested inside a PageWrapperComponent.
 */
public class PageWrapperFullWidthContainerComponent extends Component {
  private final Component child;

  public PageWrapperFullWidthContainerComponent(Component child) {
    this.child = child;
  }

  public Component getChild() {
    return child;
  }

  @Override
  protected PageWrapperFullWidthContainerComponentState createState() {
    return new PageWrapperFullWidthContainerComponentState();
  }
}
