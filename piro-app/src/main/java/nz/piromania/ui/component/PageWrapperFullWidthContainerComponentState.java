package nz.piromania.ui.component;

import java.util.ArrayList;
import nz.piromania.App;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ContainerComponentBuilder;
import nz.piromania.reactiveengine.component.list.FlexibleComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.EdgeInsets;

/**
 * ComponentState for {@link PageWrapperFullWidthContainerComponent}.
 */
public class PageWrapperFullWidthContainerComponentState
    extends ComponentState<PageWrapperFullWidthContainerComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var app = Provider.of(App.class, context);

    return ArrayListBuilder.build(
        new ListComponent(
            Axis.VERTICAL,
            new ContainerComponentBuilder()
                .setPadding(new EdgeInsets(1, app.getScreenWidth() / 2 - 10))
                .build(),
            new FlexibleComponent(
                1,
                getComponent().getChild()
            )
        )
    );
  }
}
