package nz.piromania.ui.component;

import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.util.VoidCallback;

/**
 * A PageWrapperComponent nav bar with a back button and a title.
 */
public class PageWrapperTitleBarWithBackButtonComponent extends Component {
  private final VoidCallback onBack;
  private final Component title;

  public PageWrapperTitleBarWithBackButtonComponent(VoidCallback onBack, Component title) {
    this.onBack = onBack;
    this.title = title;
  }

  @Override
  protected PageWrapperTitleBarWithBackButtonComponentState createState() {
    return new PageWrapperTitleBarWithBackButtonComponentState();
  }

  public VoidCallback getOnBack() {
    return onBack;
  }

  public Component getTitle() {
    return title;
  }
}
