package nz.piromania.ui.component;

import java.util.ArrayList;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ButtonComponent;
import nz.piromania.reactiveengine.component.ContainerComponent;
import nz.piromania.reactiveengine.component.ContainerComponentBuilder;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.EdgeInsets;

/**
 * ComponentState for {@link PageWrapperTitleBarWithBackButtonComponent}.
 */
public class PageWrapperTitleBarWithBackButtonComponentState
    extends ComponentState<PageWrapperTitleBarWithBackButtonComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    return ArrayListBuilder.build(
        new PageWrapperFullWidthContainerComponent(
            new ListComponent(
                Axis.HORIZONTAL,
                new ContainerComponentBuilder()
                    .setPadding(new EdgeInsets(0, 10))
                    .build(
                        new ButtonComponent(
                            getComponent().getOnBack(),
                            new TextComponent("< Back")
                        )
                    ),
                new ExpandedComponent(),

                new PaddingComponent(
                    new EdgeInsets(0, 0, 0, 95),
                    getComponent().getTitle()
                ),
                new ExpandedComponent()
            )
        )
    );
  }
}
