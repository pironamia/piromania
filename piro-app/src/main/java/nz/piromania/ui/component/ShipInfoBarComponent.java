package nz.piromania.ui.component;

import nz.piromania.reactiveengine.Component;

/**
 * A bar that is shown at the bottom of many ship-related screens. Contains basic info that may be
 * relevant to the user, including player name, time remaining, and balance.
 */
public class ShipInfoBarComponent extends Component {

  @Override
  protected ShipInfoBarComponentState createState() {
    return new ShipInfoBarComponentState();
  }
}
