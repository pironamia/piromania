package nz.piromania.ui.component;

import java.awt.Color;
import java.util.ArrayList;
import nz.piromania.Currency;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.style.FontWeight;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.reactivity.component.Observer;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.EdgeInsets;

/**
 * ComponentState for {@link ShipInfoBarComponent}.
 */
public class ShipInfoBarComponentState extends ComponentState<ShipInfoBarComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    return ArrayListBuilder.build(
        new Observer((context1) -> {
          final var gameEnvironment = Provider.of(GameEnvironment.class, context1);
          final int remainingDays =
              gameEnvironment.getGameDuration() - gameEnvironment.getCurrentDay();
          final Currency damageCost = gameEnvironment.getShip().calculateDamageCost();

          return ArrayListBuilder.build(
              new PageWrapperFullWidthContainerComponent(
                  new ListComponent(
                      Axis.HORIZONTAL,
                      ArrayListBuilder.buildFilteringNulls(
                          new PaddingComponent(
                              new EdgeInsets(0, 8, 0, 0),
                              new TextComponent(
                                  TextStyle.plainWhite
                                      .copyWith(null, FontWeight.BOLD, null, null, null),
                                  gameEnvironment.getTraderName()
                              )
                          ),
                          new PaddingComponent(
                              new EdgeInsets(0, 10),
                              new TextComponent(
                                  TextStyle.plainWhite,
                                  String.format(
                                      "%d day%s remaining",
                                      remainingDays,
                                      (remainingDays == 1) ? "" : "s"
                                  )
                              )
                          ),
                          new ExpandedComponent(),

                          ((damageCost.equals(new Currency(0))) ? null : (
                              new PaddingComponent(
                                  new EdgeInsets(0, 0, 0, 8),
                                  new TextComponent(
                                      TextStyle.plainWhite
                                          .copyWith(null, FontWeight.BOLD, null, null, Color.RED),
                                      String.format(
                                          "Your ship is damaged, repairs will cost %s",
                                          damageCost
                                      )
                                  )
                              )
                          )),

                          new PaddingComponent(
                              new EdgeInsets(0, 0, 0, 16),
                              new TextComponent(
                                  TextStyle.plainWhite,
                                  gameEnvironment.getTraderMoney().toString()
                              )
                          )
                      )
                  )
              )
          );
        })
    );
  }
}
