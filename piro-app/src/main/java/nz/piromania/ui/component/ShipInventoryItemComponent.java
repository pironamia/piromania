package nz.piromania.ui.component;

import nz.piromania.Currency;
import nz.piromania.gameenvironment.items.Item;
import nz.piromania.gameenvironment.stores.Store;
import nz.piromania.reactiveengine.Component;

/**
 * Component that display and item in the ship's inventory, and data associated with it. This data
 * includes: name, purchase price, and (if applicable) sold price and sold location.
 */
public class ShipInventoryItemComponent extends Component {

  private final Item item;
  private final Store soldStore;
  private final Currency originalPurchasePrice;

  /**
   * A component in the ship's inventory.
   *
   * @param item                  the item
   * @param soldStore             the store at which the item was sold (if any)
   * @param originalPurchasePrice the price that this item was originally purchased for
   */
  public ShipInventoryItemComponent(Item item, Store soldStore, Currency originalPurchasePrice) {
    this.item = item;
    this.soldStore = soldStore;
    this.originalPurchasePrice = originalPurchasePrice;
  }

  @Override
  protected ShipInventoryItemComponentState createState() {
    return new ShipInventoryItemComponentState();
  }

  public Item getItem() {
    return item;
  }

  public Store getSoldStore() {
    return soldStore;
  }

  public Currency getOriginalPurchasePrice() {
    return originalPurchasePrice;
  }

  public boolean isSold() {
    return soldStore != null;
  }
}
