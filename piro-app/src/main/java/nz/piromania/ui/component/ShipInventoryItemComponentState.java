package nz.piromania.ui.component;

import java.util.ArrayList;
import nz.piromania.App;
import nz.piromania.gameenvironment.items.Item;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.ResourceImageComponent;
import nz.piromania.reactiveengine.component.TooltipComponent;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.component.text.style.FontWeight;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.BoxSize;
import nz.piromania.reactiveengine.util.EdgeInsets;

/**
 * ComponentState for {@link ShipInventoryItemComponent}.
 */
public class ShipInventoryItemComponentState extends ComponentState<ShipInventoryItemComponent> {
  private static final int size = (App.getScreenWidth() - 40) / 6 - 8;

  private TextComponent buildTooltip() {
    final Item item = getComponent().getItem();

    final var valueStyle = TextStyle.plain.copyWith(null, FontWeight.BOLD, null, null, null);

    final ArrayList<TextSpan> text = ArrayListBuilder.build(
        new TextSpan(
            valueStyle,
            String.format(
                "%s%s%n",
                item.getItemName(),
                (getComponent().isSold()) ? " (sold)" : ""
            )
        ),
        new TextSpan(
            TextStyle.plain,
            "Bought for: "
        ),
        new TextSpan(
            valueStyle,
            String.format(
                "%s%n",
                getComponent().getOriginalPurchasePrice()
            )
        )
    );

    if (getComponent().isSold()) {
      text.addAll(
          ArrayListBuilder.build(
              new TextSpan(
                  TextStyle.plain,
                  "Sold for: "
              ),
              new TextSpan(
                  valueStyle,
                  String.format(
                      "%s%n",
                      item.getSoldValue()
                  )
              ),
              new TextSpan(
                  TextStyle.plain,
                  "Sold at: "
              ),
              new TextSpan(
                  valueStyle,
                  getComponent().getSoldStore().getStoreName()
              )
          )
      );
    }

    return new TextComponent(text);
  }

  @Override
  public ArrayList<Component> build(BuildContext context) {
    return ArrayListBuilder.build(
        new PaddingComponent(
            new EdgeInsets(4, 4),
            new TooltipComponent(
                buildTooltip(),
                new ResourceImageComponent(
                    new BoxSize(size, size),
                    getComponent().getItem().getImagePath()
                )
            )
        )
    );
  }
}
