package nz.piromania.ui.component;

import java.util.function.Consumer;
import nz.piromania.gameenvironment.ships.Ship;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;

/**
 * A button that contains a single ship that can be selected.
 */
public class ShipSelectionComponent extends Component {
  private final Ship ship;
  private final Consumer<Ship> onClick;

  public ShipSelectionComponent(Ship ship, Consumer<Ship> onClick) {
    this.ship = ship;
    this.onClick = onClick;
  }

  @Override
  protected ComponentState createState() {
    return new ShipSelectionComponentState();
  }

  public Ship getShip() {
    return ship;
  }

  public Consumer<Ship> getOnClick() {
    return onClick;
  }
}
