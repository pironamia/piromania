package nz.piromania.ui.component;

import java.awt.Color;
import java.util.ArrayList;
import nz.piromania.gameenvironment.ships.Ship;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ButtonComponent;
import nz.piromania.reactiveengine.component.ContainerComponentBuilder;
import nz.piromania.reactiveengine.component.ResourceImageComponent;
import nz.piromania.reactiveengine.component.TooltipComponent;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.component.text.style.FontWeight;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.BoxSize;
import nz.piromania.reactiveengine.util.EdgeInsets;

/**
 * ComponentState for {@link ShipSelectionComponent}.
 */
public class ShipSelectionComponentState extends ComponentState<ShipSelectionComponent> {
  private TextComponent buildShipTooltipText() {
    final Ship ship = getComponent().getShip();

    final TextStyle statStyle = TextStyle.plain.copyWith(
        null,
        FontWeight.BOLD,
        null,
        null,
        Color.RED
    );

    return new TextComponent(
        new TextSpan(
            TextStyle.plain.copyWith(null, null, "Pirata One", 24, null),
            String.format("%s\n", ship.getShipName())
        ),

        new TextSpan(
            statStyle,
            ship.getWages().toString()
        ),
        new TextSpan(
            TextStyle.plain,
            " wages\n"
        ),

        new TextSpan(
            statStyle,
            Integer.toString(ship.getShipStats().getCargoCapacity())
        ),
        new TextSpan(
            TextStyle.plain,
            " cargo capacity\n"
        ),

        new TextSpan(
            statStyle,
            Integer.toString(ship.getBaseTravelSpeed())
        ),
        new TextSpan(
            TextStyle.plain,
            " base travel speed\n"
        ),

        new TextSpan(
            statStyle,
            Integer.toString(ship.getCurrentHitpoints())
        ),
        new TextSpan(
            TextStyle.plain,
            " hit points"
        )
    );
  }

  @Override
  public ArrayList<Component> build(BuildContext context) {
    return ArrayListBuilder.build(
        new ContainerComponentBuilder()
            .setPadding(new EdgeInsets(5, 5))
            .build(new TooltipComponent(
                buildShipTooltipText(),
                new ButtonComponent(
                    () -> {
                      getComponent().getOnClick().accept(getComponent().getShip());
                    },
                    new ContainerComponentBuilder()
                        .setSize(new BoxSize(150, 150))
                        .build(
                            new ResourceImageComponent(
                                new BoxSize(150, 150),
                                getComponent().getShip().getImagePath()
                            )
                        )
                )
            ))
    );
  }
}
