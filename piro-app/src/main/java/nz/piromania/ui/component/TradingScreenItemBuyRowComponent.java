package nz.piromania.ui.component;

import nz.piromania.gameenvironment.items.Item;
import nz.piromania.reactiveengine.Component;

/**
 * A row representing an item that can be bought in the 
 * {@link nz.piromania.ui.screen.IslandTradingScreenComponent}.
 */
public class TradingScreenItemBuyRowComponent extends Component {

  private final Item item;

  public TradingScreenItemBuyRowComponent(Item item) {
    this.item = item;
  }

  @Override
  protected TradingScreenItemBuyRowComponentState createState() {
    return new TradingScreenItemBuyRowComponentState();
  }

  public Item getItem() {
    return item;
  }

}
