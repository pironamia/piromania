package nz.piromania.ui.component;

import java.util.ArrayList;
import nz.piromania.Currency;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ButtonComponent;
import nz.piromania.reactiveengine.component.ContainerComponentBuilder;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.TooltipComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.reactivity.Action;
import nz.piromania.reactiveengine.reactivity.component.Observer;
import nz.piromania.reactiveengine.util.ArrayListBuilder;

/**
 * ComponentState for {@link TradingScreenItemBuyRowComponent}.
 */
public class TradingScreenItemBuyRowComponentState
    extends ComponentState<TradingScreenItemBuyRowComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var gameEnvironment = Provider.of(GameEnvironment.class, context);
    final var item = getComponent().getItem();

    final Currency itemPrice = gameEnvironment.getCurrentIsland().getStore().getBuyingInventory()
        .getItem(item.getId()).getPurchasedValue();

    return ArrayListBuilder.build(
        new TooltipComponent(
            new TextComponent(
                gameEnvironment.getTextFormatterGui().formatText(item.getGuiSummary())
            ),
            new ListComponent(
                Axis.HORIZONTAL,
                new ExpandedComponent(
                    new ListComponent(
                        Axis.VERTICAL,
                        new ExpandedComponent(),
                        new TextComponent(
                            TextStyle.plainWhite,
                            item.getItemName()
                        ),
                        new ExpandedComponent()
                    )
                ),
                new ContainerComponentBuilder()
                    .build(
                        new Observer((context1) -> {
                          // Disable if it's too expensive or there's not enough space.
                          final boolean isDisabled =
                              itemPrice.compareTo(gameEnvironment.getTraderMoney()) == 1
                                  || item.getItemSize() > gameEnvironment.getShip().getInventory()
                                  .getCapacity();

                          return ArrayListBuilder.build(
                              new ButtonComponent(
                                  () -> {
                                    Action.run(() -> {
                                      gameEnvironment.buyItem(item);
                                    });
                                  },
                                  isDisabled,
                                  new TextComponent(
                                      String.format(
                                          "Buy for %s",
                                          itemPrice
                                      )
                                  )
                              )
                          );
                        })
                    )
            )
        )
    );
  }
}
