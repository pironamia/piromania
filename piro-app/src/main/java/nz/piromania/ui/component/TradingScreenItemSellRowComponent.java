package nz.piromania.ui.component;

import nz.piromania.gameenvironment.items.Item;
import nz.piromania.reactiveengine.Component;

/**
 * A row representing an item that can be sold in the 
 * {@link nz.piromania.ui.screen.IslandTradingScreenComponent}.
 */
public class TradingScreenItemSellRowComponent extends Component {

  private final Item item;

  public TradingScreenItemSellRowComponent(Item item) {
    this.item = item;
  }

  @Override
  protected TradingScreenItemSellRowComponentState createState() {
    return new TradingScreenItemSellRowComponentState();
  }

  public Item getItem() {
    return item;
  }

}
