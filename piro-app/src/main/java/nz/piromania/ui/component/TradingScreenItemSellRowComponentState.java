package nz.piromania.ui.component;

import java.util.ArrayList;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ButtonComponent;
import nz.piromania.reactiveengine.component.ContainerComponentBuilder;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.TooltipComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.ui.AppTextStyles;

/**
 * ComponentState for {@link TradingScreenItemSellRowComponent}.
 */
public class TradingScreenItemSellRowComponentState
    extends ComponentState<TradingScreenItemSellRowComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var gameEnvironment = Provider.of(GameEnvironment.class, context);
    final var item = getComponent().getItem();

    final var sellingPrice = gameEnvironment.getCurrentIsland().getStore().getSellingInventory()
        .getItem(item.getId()).getPurchasedValue();

    return ArrayListBuilder.build(
        new TooltipComponent(
            new TextComponent(
                gameEnvironment.getTextFormatterGui().formatText(item.getGuiSummary())
            ),
            new ListComponent(
                Axis.HORIZONTAL,
                new ExpandedComponent(
                    new ListComponent(
                        Axis.VERTICAL,
                        new ExpandedComponent(),
                        new TextComponent(
                            TextStyle.plainWhite,
                            item.getItemName()
                        ),
                        new TextComponent(
                            AppTextStyles.detailStyleWhite,
                            String.format(
                                "Purchased for %s",
                                item.getPurchasedValue()
                            )
                        ),
                        new ExpandedComponent()
                    )
                ),
                new ContainerComponentBuilder()
                    .build(
                        new ButtonComponent(
                            () -> {
                              gameEnvironment.sellItem(item);
                            },
                            new ListComponent(
                                Axis.VERTICAL,
                                new TextComponent(
                                    String.format(
                                        "Sell for %s",
                                        sellingPrice
                                    )
                                ),
                                new TextComponent(
                                    AppTextStyles.detailStyle,
                                    String.format(
                                        "(%s profit)",
                                        sellingPrice.subtract(item.getPurchasedValue(), true)
                                    )
                                )
                            )
                        )
                    )
            )
        )
    );
  }
}
