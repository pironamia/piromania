package nz.piromania.ui.component.journeyplanner;

import nz.piromania.gameenvironment.islands.Island;
import nz.piromania.gameenvironment.routes.Route;
import nz.piromania.reactiveengine.Component;

/**
 * Component that contains information about the currently selected island and (optionally) route in
 * the journey planner screen.
 */
public class JourneyPlannerScreenIslandInfoPaneComponent extends Component {

  private final Island island;
  private final Route route;

  public JourneyPlannerScreenIslandInfoPaneComponent(Island island, Route route) {
    this.island = island;
    this.route = route;
  }

  @Override
  protected JourneyPlannerScreenIslandInfoPaneComponentState createState() {
    return new JourneyPlannerScreenIslandInfoPaneComponentState();
  }

  public Island getIsland() {
    return island;
  }

  public Route getRoute() {
    return route;
  }
}
