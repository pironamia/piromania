package nz.piromania.ui.component.journeyplanner;

import java.awt.Color;
import java.util.ArrayList;
import nz.piromania.Currency;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ButtonComponent;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.TooltipComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.component.text.style.FontWeight;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.EdgeInsets;

/**
 * ComponentState for {@link JourneyPlannerScreenIslandInfoPaneComponent}.
 */
public class JourneyPlannerScreenIslandInfoPaneComponentState extends
    ComponentState<JourneyPlannerScreenIslandInfoPaneComponent> {

  private JourneyPlannerScreenIslandInfoPaneComponentView currentView;

  private void setView(JourneyPlannerScreenIslandInfoPaneComponentView view) {
    setState(() -> {
      currentView = view;
    });
  }

  @Override
  public void initState() {
    super.initState();

    currentView = JourneyPlannerScreenIslandInfoPaneComponentView.OVERVIEW;
  }

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var gameEnvironment = Provider.of(GameEnvironment.class, context);
    final var island = getComponent().getIsland();
    final var route = getComponent().getRoute();

    Component childComponent = new ListComponent(
        Axis.VERTICAL,
        new TextComponent("Select an island to view details."),
        new ExpandedComponent()
    );

    Currency sailingCost = null;
    Integer sailingTime = null;
    if (route != null) {
      sailingCost = gameEnvironment.calculateSailingCost(route);
      sailingTime = route.calculateTravelTime();
    }

    boolean canSailMoney =
        sailingCost != null && gameEnvironment.getTraderMoney().compareTo(sailingCost) != -1;
    //It's reasonable to be on 0 days.
    boolean canSailTime = sailingTime != null
        && (gameEnvironment.getGameDuration()
        - gameEnvironment.getCurrentDay() - route.calculateTravelTime()
    ) >= 0;

    if (island != null) {
      childComponent = switch (currentView) {
        case OVERVIEW -> new JourneyPlannerScreenIslandInfoPaneOverviewComponent();
        case STORE_BUYS -> new JourneyPlannerScreenIslandInfoPaneIslandInventoryComponent(
            JourneyPlannerScreenIslandInfoPaneIslandInventoryComponentView.STORE_BUYING);
        case STORE_INVENTORY -> new JourneyPlannerScreenIslandInfoPaneIslandInventoryComponent(
            JourneyPlannerScreenIslandInfoPaneIslandInventoryComponentView.STORE_SELLING);
      };

      if (route != null) {
        childComponent = new Provider(
            route,
            childComponent
        );
      }

      childComponent = new Provider(
          island,
          childComponent
      );
    }

    return ArrayListBuilder.build(
        new ListComponent(
            Axis.VERTICAL,
            ArrayListBuilder.buildFilteringNulls(
                new ListComponent(
                    Axis.HORIZONTAL,
                    new ButtonComponent(
                        () -> {
                          setView(JourneyPlannerScreenIslandInfoPaneComponentView.OVERVIEW);
                        },
                        island == null
                            || currentView
                            == JourneyPlannerScreenIslandInfoPaneComponentView.OVERVIEW,
                        new TextComponent("Overview")
                    ),
                    new PaddingComponent(
                        new EdgeInsets(0, 8, 0, 0),
                        new ButtonComponent(
                            () -> {
                              setView(
                                  JourneyPlannerScreenIslandInfoPaneComponentView.STORE_INVENTORY);
                            },
                            island == null || currentView
                                == JourneyPlannerScreenIslandInfoPaneComponentView.STORE_INVENTORY,
                            new TextComponent("Store selling prices")
                        )
                    ),
                    new PaddingComponent(
                        new EdgeInsets(0, 8, 0, 0),
                        new ButtonComponent(
                            () -> {
                              setView(JourneyPlannerScreenIslandInfoPaneComponentView.STORE_BUYS);
                            },
                            island == null || currentView
                                == JourneyPlannerScreenIslandInfoPaneComponentView.STORE_BUYS,
                            new TextComponent("Store buying prices")
                        )
                    ),
                    new ExpandedComponent()
                ),
                new ExpandedComponent(
                    childComponent
                ),

                ((route == null) ? null : (
                    new PaddingComponent(
                        new EdgeInsets(10, 0, 0, 0),
                        new TooltipComponent(
                            (canSailMoney && canSailTime) ? new TextComponent(
                                new TextSpan(
                                    TextStyle.plain,
                                    "Onwards into the great unknown!\n"
                                ),
                                new TextSpan(
                                    TextStyle.plain,
                                    "Cost of this journey: "
                                ),
                                new TextSpan(
                                    TextStyle.plain.copyWith(null, FontWeight.BOLD, null,
                                        null, Color.GREEN),
                                    gameEnvironment.calculateSailingCost(route).toString()
                                ))
                                : (!canSailMoney) ? new TextComponent(
                                    new TextSpan(
                                        TextStyle.plain,
                                        true,
                                        "You don't have enough money to set sail:\n"
                                    ),
                                    new TextSpan(
                                        TextStyle.plain
                                            .copyWith(null, FontWeight.BOLD, null,
                                                null, null),
                                        gameEnvironment.getTraderMoney().toString()
                                    ),
                                    new TextSpan(
                                        TextStyle.plain,
                                        true,
                                        " - your current balance\n"
                                    ),
                                    new TextSpan(
                                        TextStyle.plain
                                            .copyWith(null, FontWeight.BOLD, null,
                                                null, Color.RED),
                                        gameEnvironment.getShip().getWages()
                                            .multiply(route.calculateTravelTime()).toString()
                                    ),
                                    new TextSpan(
                                        TextStyle.plain,
                                        true,
                                        " - route wage costs\n"
                                    ),
                                    new TextSpan(
                                        TextStyle.plain
                                            .copyWith(null, FontWeight.BOLD, null,
                                                null, Color.RED),
                                        gameEnvironment.getShip().calculateDamageCost().toString()
                                    ),
                                    new TextSpan(
                                        TextStyle.plain,
                                        true,
                                        " - ship repairs\n"
                                    ),
                                    new TextSpan(
                                        TextStyle.plain
                                            .copyWith(null, FontWeight.BOLD, null,
                                                null, Color.RED),
                                        sailingCost.toString()
                                    ),
                                    new TextSpan(
                                        TextStyle.plain,
                                        " - total sailing cost"
                                    )
                                ) : new TextComponent(
                                    new TextSpan(
                                        TextStyle.plain,
                                        "You don't have enough time to set sail:\n"
                                    ),
                                    new TextSpan(
                                        TextStyle.plain,
                                        "You have: "
                                    ),
                                    new TextSpan(
                                        TextStyle.plain
                                            .copyWith(null, FontWeight.BOLD, null,
                                                null, null),
                                        Integer.toString(
                                            gameEnvironment.getGameDuration() - gameEnvironment
                                                .getCurrentDay())
                                    ),
                                    new TextSpan(
                                        TextStyle.plain,
                                        " days left.\n"
                                    ),
                                    new TextSpan(
                                        TextStyle.plain,
                                        "This route takes: "
                                    ),
                                    new TextSpan(
                                        TextStyle.plain
                                            .copyWith(null, FontWeight.BOLD, null,
                                                null, Color.RED),
                                        Integer.toString(route.calculateTravelTime())
                                    ),
                                    new TextSpan(
                                        TextStyle.plain,
                                        " days to sail."
                                    )
                                ),

                            new ButtonComponent(
                                () -> {
                                  gameEnvironment.takeRoute(route);
                                },
                                !(canSailMoney && canSailTime),
                                new TextComponent(
                                    (canSailMoney && canSailTime) ? "Set sail"
                                        : "Can't set sail - try a different route."
                                )
                            )
                        )
                    )
                ))
            )
        )
    );
  }
}
