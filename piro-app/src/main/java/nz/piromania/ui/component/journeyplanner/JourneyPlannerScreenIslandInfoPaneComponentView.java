package nz.piromania.ui.component.journeyplanner;

/**
 * Enum for the possible tabs (or 'view's) that can be selected in the {@link
 * JourneyPlannerScreenIslandInfoPaneComponent}.
 */
public enum JourneyPlannerScreenIslandInfoPaneComponentView {
  OVERVIEW,
  STORE_INVENTORY,
  STORE_BUYS
}
