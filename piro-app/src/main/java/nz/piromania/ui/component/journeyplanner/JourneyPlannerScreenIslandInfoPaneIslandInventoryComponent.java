package nz.piromania.ui.component.journeyplanner;

import nz.piromania.reactiveengine.Component;

/**
 * A component that displays either the items that an island is selling, or that it is buying, and
 * the prices for each. The view is determined by the provided view.
 */
public class JourneyPlannerScreenIslandInfoPaneIslandInventoryComponent extends Component {

  private final JourneyPlannerScreenIslandInfoPaneIslandInventoryComponentView view;

  public JourneyPlannerScreenIslandInfoPaneIslandInventoryComponent(
      JourneyPlannerScreenIslandInfoPaneIslandInventoryComponentView view
  ) {
    this.view = view;
  }

  @Override
  protected JourneyPlannerScreenIslandInfoPaneIslandInventoryComponentState createState() {
    return new JourneyPlannerScreenIslandInfoPaneIslandInventoryComponentState();
  }

  public JourneyPlannerScreenIslandInfoPaneIslandInventoryComponentView getView() {
    return view;
  }
}
