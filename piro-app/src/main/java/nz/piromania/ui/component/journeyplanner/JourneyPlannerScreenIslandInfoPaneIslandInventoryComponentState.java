package nz.piromania.ui.component.journeyplanner;

import java.util.ArrayList;
import nz.piromania.Inventory;
import nz.piromania.gameenvironment.islands.Island;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ScrollingComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.ui.component.FadeComponent;

/**
 * ComponentState for {@link JourneyPlannerScreenIslandInfoPaneIslandInventoryComponent}.
 */
public class JourneyPlannerScreenIslandInfoPaneIslandInventoryComponentState extends
    ComponentState<JourneyPlannerScreenIslandInfoPaneIslandInventoryComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final Island island = Provider.of(Island.class, context);
    final Inventory inventory = switch (getComponent().getView()) {
      case STORE_BUYING -> island.getStore().getSellingInventory();
      case STORE_SELLING -> island.getStore().getBuyingInventory();
    };

    final ArrayList<Component> itemRows = new ArrayList<>();
    for (final var item : inventory.getItems()) {
      itemRows.add(new JourneyPlannerScreenIslandInfoPaneIslandInventoryItemRowComponent(
          item,
          getComponent().getView()
      ));
    }

    return ArrayListBuilder.build(
        new ScrollingComponent(
            new FadeComponent(
                new ListComponent(
                    Axis.VERTICAL,
                    itemRows
                )
            )
        )
    );
  }
}
