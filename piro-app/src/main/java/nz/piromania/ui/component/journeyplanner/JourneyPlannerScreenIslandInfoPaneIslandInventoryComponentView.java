package nz.piromania.ui.component.journeyplanner;

/**
 * View mode for the {@link JourneyPlannerScreenIslandInfoPaneIslandInventoryComponent} to be in.
 * Either showing the items that the island sells, or that it buys.
 */
public enum JourneyPlannerScreenIslandInfoPaneIslandInventoryComponentView {
  STORE_SELLING,
  STORE_BUYING
}
