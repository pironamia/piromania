package nz.piromania.ui.component.journeyplanner;

import nz.piromania.gameenvironment.items.Item;
import nz.piromania.reactiveengine.Component;

/**
 * Components representing a single in the shop's inventory, as viewed through the Journey Planner.
 */
public class JourneyPlannerScreenIslandInfoPaneIslandInventoryItemRowComponent extends Component {

  private final Item item;
  private final JourneyPlannerScreenIslandInfoPaneIslandInventoryComponentView view;

  public JourneyPlannerScreenIslandInfoPaneIslandInventoryItemRowComponent(
      Item item,
      JourneyPlannerScreenIslandInfoPaneIslandInventoryComponentView view
  ) {
    this.item = item;
    this.view = view;
  }

  @Override
  protected JourneyPlannerScreenIslandInfoPaneIslandInventoryItemRowComponentState createState() {
    return new JourneyPlannerScreenIslandInfoPaneIslandInventoryItemRowComponentState();
  }

  public Item getItem() {
    return item;
  }

  public JourneyPlannerScreenIslandInfoPaneIslandInventoryComponentView getView() {
    return view;
  }
}
