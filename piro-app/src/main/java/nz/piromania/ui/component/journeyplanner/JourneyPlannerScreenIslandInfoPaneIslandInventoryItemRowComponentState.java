package nz.piromania.ui.component.journeyplanner;

import java.awt.Color;
import java.util.ArrayList;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.TooltipComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.EdgeInsets;
import nz.piromania.ui.AppTextStyles;

/**
 * ComponentState for {@link JourneyPlannerScreenIslandInfoPaneIslandInventoryItemRowComponent}.
 */
public class JourneyPlannerScreenIslandInfoPaneIslandInventoryItemRowComponentState extends
    ComponentState<JourneyPlannerScreenIslandInfoPaneIslandInventoryItemRowComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var gameEnv = Provider.of(GameEnvironment.class, context);
    final var item = getComponent().getItem();
    final var view = getComponent().getView();

    return ArrayListBuilder.build(
        new ListComponent(
            Axis.HORIZONTAL,
            new ExpandedComponent(
                new PaddingComponent(
                    new EdgeInsets(0, 5, 0, 0),
                    new TooltipComponent(
                        new TextComponent(
                            gameEnv.getTextFormatterGui().formatText(item.getGuiSummary(),
                                Color.WHITE)
                        ),
                        new TextComponent(
                            TextStyle.plainWhite,
                            item.getItemName()
                        )
                    )
                )
            ),

            new PaddingComponent(
                new EdgeInsets(5, 5),
                new TextComponent(
                    new TextSpan(
                        TextStyle.plainWhite,
                        (view == JourneyPlannerScreenIslandInfoPaneIslandInventoryComponentView
                            .STORE_SELLING)
                            ? "Selling for: " : "Buying for: "
                    ),
                    new TextSpan(
                        AppTextStyles.boldStyleWhite,
                        item.getPurchasedValue().toString()
                    )
                )
            )
        )
    );
  }
}
