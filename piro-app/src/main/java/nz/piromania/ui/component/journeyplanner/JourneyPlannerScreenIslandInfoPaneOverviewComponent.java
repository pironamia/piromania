package nz.piromania.ui.component.journeyplanner;

import nz.piromania.reactiveengine.Component;

/**
 * Overview of the currently selected island (and optionally route) in the journey planner screen.
 */
public class JourneyPlannerScreenIslandInfoPaneOverviewComponent extends Component {

  @Override
  protected JourneyPlannerScreenIslandInfoPaneOverviewComponentState createState() {
    return new JourneyPlannerScreenIslandInfoPaneOverviewComponentState();
  }
}
