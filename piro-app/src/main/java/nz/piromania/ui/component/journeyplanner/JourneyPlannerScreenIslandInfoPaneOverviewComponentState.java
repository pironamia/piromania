package nz.piromania.ui.component.journeyplanner;

import java.util.ArrayList;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.gameenvironment.islands.Island;
import nz.piromania.gameenvironment.routes.Route;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ContainerComponentBuilder;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.ResourceImageComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.component.text.style.FontWeight;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.exception.KeyNotFoundException;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.BoxSize;
import nz.piromania.reactiveengine.util.EdgeInsets;
import nz.piromania.ui.AppTextStyles;
import nz.piromania.ui.component.FadeComponent;

/**
 * ComponentState for {@link JourneyPlannerScreenIslandInfoPaneOverviewComponent}.
 */
public class JourneyPlannerScreenIslandInfoPaneOverviewComponentState extends
    ComponentState<JourneyPlannerScreenIslandInfoPaneOverviewComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var gameEnv = Provider.of(GameEnvironment.class, context);
    final var island = Provider.of(Island.class, context);

    Route route;
    try {
      route = Provider.of(Route.class, context);
    } catch (KeyNotFoundException e) {
      route = null;
    }

    final ArrayList<Component> overviewText = ArrayListBuilder.build(
        new TextSpan(
            AppTextStyles.headingStyleWhite,
            true,
            island.getName()
        ),
        new TextSpan(
            AppTextStyles.detailStyleWhite,
            true,
            island.getDescription()
        ),
        new TextSpan(
            AppTextStyles.detailStyleWhite,
            true,
            String.format("The shop on this island is called %s."
                    + "\nThis island prefers to sell items of type: %s"
                    + "\nThis island prefers to buy items of type: %s",
                island.getStore().getStoreName(),
                island.getStore().getSellingType().toString().substring(0, 1).toUpperCase()
                    + island.getStore().getSellingType().toString().substring(1).toLowerCase(),
                island.getStore().getBuyingType().toString().substring(0, 1).toUpperCase()
                    + island.getStore().getBuyingType().toString().substring(1).toLowerCase())
        ));

    if (route != null) {
      overviewText.addAll(ArrayListBuilder.build(
          new PaddingComponent(
              new EdgeInsets(10, 0, 0, 0),
              new TextSpan(
                  TextStyle.plainWhite.copyWith(null, FontWeight.BOLD, null, null, null),
                  "Route information"
              )
          ),
          new TextSpan(
              AppTextStyles.detailStyleWhite,
              String.format(
                  "Distance: %.0f miles%n",
                  route.getDistance()
              )
          ),
          new TextSpan(
              AppTextStyles.detailStyleWhite,
              String.format(
                  "Travel Cost: %s%n",
                  gameEnv.getShip().getWages().multiply(route.calculateTravelTime())
              )
          ),
          new TextSpan(
              AppTextStyles.detailStyleWhite,
              String.format(
                  "Travel time: %d days%n",
                  route.calculateTravelTime()
              )
          ),
          new TextSpan(
              AppTextStyles.detailStyleWhite,
              String.format(
                  "Travel Risk: %.0f%%",
                  route.calculateRisk() * 100f
              )
          )
      ));
    }

    return ArrayListBuilder.build(
        new PaddingComponent(
            new EdgeInsets(10, 0, 0, 0),
            new ListComponent(
                Axis.HORIZONTAL,
                new ExpandedComponent(
                    new FadeComponent(new ListComponent(
                        Axis.VERTICAL,
                        overviewText
                    ))
                ),

                new PaddingComponent(
                    new EdgeInsets(0, 5),
                    new ContainerComponentBuilder().build()
                ),

                new ExpandedComponent(
                    new ListComponent(
                        Axis.VERTICAL,
                        new ResourceImageComponent(
                            new BoxSize(250, 250),
                            island.getImagePath()
                        ),
                        new ExpandedComponent()
                    )
                )
            )
        )
    );
  }
}
