package nz.piromania.ui.component.journeyplanner;

import java.util.function.Consumer;
import nz.piromania.reactiveengine.Component;

/**
 * An island that can be visited from the journey planner.
 */
public class JourneyPlannerScreenIslandRowComponent extends Component {

  private final int islandIndex;
  private final Consumer<Integer> onSelected;


  public JourneyPlannerScreenIslandRowComponent(int islandIndex, Consumer<Integer> onSelected) {
    this.islandIndex = islandIndex;
    this.onSelected = onSelected;
  }


  @Override
  protected JourneyPlannerScreenIslandRowComponentState createState() {
    return new JourneyPlannerScreenIslandRowComponentState();
  }

  public int getIslandIndex() {
    return islandIndex;
  }

  public Consumer<Integer> getOnSelected() {
    return onSelected;
  }
}
