package nz.piromania.ui.component.journeyplanner;

import java.util.ArrayList;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ButtonComponent;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.EdgeInsets;
import nz.piromania.ui.AppTextStyles;

/**
 * ComponentState for {@link JourneyPlannerScreenIslandRowComponent}.
 */
public class JourneyPlannerScreenIslandRowComponentState extends
    ComponentState<JourneyPlannerScreenIslandRowComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var gameEnv = Provider.of(GameEnvironment.class, context);
    final var island = gameEnv.getAllIslands().get(getComponent().getIslandIndex());

    return ArrayListBuilder.build(
        new ListComponent(
            Axis.HORIZONTAL,
            new PaddingComponent(
                new EdgeInsets(0, 5, 0, 0),
                new ListComponent(
                    Axis.VERTICAL,
                    new TextSpan(
                        TextStyle.plainWhite,
                        false,
                        island.getName()
                    ),
                    new TextSpan(
                        AppTextStyles.detailStyleWhite,
                        String.format(
                            "%.0f miles away",
                            gameEnv.getCurrentIsland().calculateDistance(island.getXPosition(),
                                island.getYPosition())
                        )
                    )
                )
            ),

            new ExpandedComponent(),

            new ButtonComponent(
                () -> {
                  getComponent().getOnSelected().accept(getComponent().getIslandIndex());
                },
                new TextComponent(
                    ">"
                )
            )
        )
    );
  }
}
