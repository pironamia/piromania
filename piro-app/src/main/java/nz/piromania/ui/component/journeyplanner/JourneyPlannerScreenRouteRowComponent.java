package nz.piromania.ui.component.journeyplanner;

import java.util.function.Consumer;
import nz.piromania.reactiveengine.Component;

/**
 * Represents a route from the current island to another island in the journey planner screen.
 */
public class JourneyPlannerScreenRouteRowComponent extends Component {

  private final int islandIndex;
  private final int routeIndex;
  private final Consumer<Integer> onSelected;

  /**
   * Construct a JourneyPlannerScreenRouteRowComponent.
   *
   * @param islandIndex index of the island
   * @param routeIndex  index of the route from this island to that island
   * @param onSelected  callback to trigger when the route is selected
   */
  public JourneyPlannerScreenRouteRowComponent(int islandIndex, int routeIndex,
      Consumer<Integer> onSelected) {
    this.islandIndex = islandIndex;
    this.routeIndex = routeIndex;
    this.onSelected = onSelected;
  }

  @Override
  protected JourneyPlannerScreenRouteRowComponentState createState() {
    return new JourneyPlannerScreenRouteRowComponentState();
  }

  public int getRouteIndex() {
    return routeIndex;
  }

  public Consumer<Integer> getOnSelected() {
    return onSelected;
  }

  public int getIslandIndex() {
    return islandIndex;
  }
}
