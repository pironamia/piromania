package nz.piromania.ui.component.journeyplanner;

import java.util.ArrayList;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ButtonComponent;
import nz.piromania.reactiveengine.component.CentreComponent;
import nz.piromania.reactiveengine.component.ContainerComponentBuilder;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.EdgeInsets;
import nz.piromania.ui.AppTextStyles;

/**
 * ComponentState for {@link JourneyPlannerScreenRouteRowComponent}.
 */
public class JourneyPlannerScreenRouteRowComponentState extends
    ComponentState<JourneyPlannerScreenRouteRowComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var gameEnv = Provider.of(GameEnvironment.class, context);
    final var route = gameEnv.getCurrentRoutes().get(getComponent().getIslandIndex()).get(
        getComponent().getRouteIndex());

    return ArrayListBuilder.build(
        new PaddingComponent(
            new EdgeInsets(5, 0),
            new ListComponent(
                Axis.HORIZONTAL,
                new PaddingComponent(
                    new EdgeInsets(0, 5, 0, 0),
                    new ListComponent(
                        Axis.VERTICAL,
                        new TextSpan(
                            TextStyle.plainWhite,
                            String.format(
                                "Route %d: %d Days",
                                getComponent().getRouteIndex() + 1,
                                route.calculateTravelTime()
                            )
                        ),
                        new TextSpan(
                            AppTextStyles.detailStyleWhite,
                            true,
                            String.format(
                                "%.0f miles%n%.0f%% risk%n%s cost",
                                route.getDistance(),
                                route.calculateRisk() * 100f,
                                gameEnv.calculateSailingCost(route)
                            )
                        )
                    )
                ),

                new ExpandedComponent(),

                new ContainerComponentBuilder()
                    .build(
                        new ButtonComponent(
                            () -> {
                              getComponent().getOnSelected().accept(getComponent().getRouteIndex());
                            },
                            new CentreComponent(
                                new TextComponent(
                                    ">"
                                )
                            )
                        )
                    )
            )
        )
    );
  }
}
