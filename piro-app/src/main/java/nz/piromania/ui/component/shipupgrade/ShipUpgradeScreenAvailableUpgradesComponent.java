package nz.piromania.ui.component.shipupgrade;

import java.util.function.Consumer;
import nz.piromania.gameenvironment.items.UpgradeItem;
import nz.piromania.reactiveengine.Component;

/**
 * Represents the list of available upgrades in the ship upgrades screen.
 */
public class ShipUpgradeScreenAvailableUpgradesComponent extends Component {

  private final Consumer<UpgradeItem> onUpgrade;

  public ShipUpgradeScreenAvailableUpgradesComponent(Consumer<UpgradeItem> onUpgrade) {
    this.onUpgrade = onUpgrade;
  }

  @Override
  protected ShipUpgradeScreenAvailableUpgradesComponentState createState() {
    return new ShipUpgradeScreenAvailableUpgradesComponentState();
  }

  public Consumer<UpgradeItem> getOnUpgrade() {
    return onUpgrade;
  }
}
