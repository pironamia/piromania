package nz.piromania.ui.component.shipupgrade;

import java.util.ArrayList;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.gameenvironment.items.UpgradeItem;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ContainerComponentBuilder;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.ScrollingComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.EdgeInsets;
import nz.piromania.ui.component.PageWrapperFullWidthContainerComponent;

/**
 * ComponentState for {@link ShipUpgradeScreenAvailableUpgradesComponent}.
 */
public class ShipUpgradeScreenAvailableUpgradesComponentState extends
    ComponentState<ShipUpgradeScreenAvailableUpgradesComponent> {

  private RuntimeException lastError;

  private void setLastError(RuntimeException error) {
    setState(() -> {
      lastError = error;
    });
  }

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var gameEnv = Provider.of(GameEnvironment.class, context);

    final ArrayList<Component> components = new ArrayList<>();

    for (final var item : gameEnv.getShip().getInventory().getItems()) {
      if (item instanceof UpgradeItem) {
        final UpgradeItem upgradeItem = (UpgradeItem) item;

        if (upgradeItem.getAppliedLocation() == null) {
          components.add(new ShipUpgradeScreenUpgradeComponent(
              upgradeItem,
              (x) -> {
                getComponent().getOnUpgrade().accept(x);
              }
          ));
        }
      }
    }

    return ArrayListBuilder.build(
        new PageWrapperFullWidthContainerComponent(
            new ListComponent(
                Axis.HORIZONTAL,
                new ContainerComponentBuilder()
                    .setPadding(new EdgeInsets(45, 0))
                    .build(),

                new ExpandedComponent(
                    new ScrollingComponent(
                        new ListComponent(
                            Axis.HORIZONTAL,
                            components
                        )
                    )
                )
            )
        )
    );
  }
}
