package nz.piromania.ui.component.shipupgrade;

import java.util.function.Consumer;
import nz.piromania.gameenvironment.items.UpgradeItem;
import nz.piromania.reactiveengine.Component;

/**
 * A single upgrade item in the ship upgrade screen. Could be either applied in a specific slot, or
 * not applied.
 */
public class ShipUpgradeScreenUpgradeComponent extends Component {

  private final UpgradeItem item;
  private final Consumer<UpgradeItem> onClick;

  public ShipUpgradeScreenUpgradeComponent(UpgradeItem item, Consumer<UpgradeItem> onClick) {
    this.item = item;
    this.onClick = onClick;
  }

  @Override
  protected ShipUpgradeScreenUpgradeComponentState createState() {
    return new ShipUpgradeScreenUpgradeComponentState();
  }

  public UpgradeItem getItem() {
    return item;
  }

  public Consumer<UpgradeItem> getOnClick() {
    return onClick;
  }
}
