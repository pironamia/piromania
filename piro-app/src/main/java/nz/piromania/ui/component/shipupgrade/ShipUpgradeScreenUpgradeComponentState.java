package nz.piromania.ui.component.shipupgrade;

import java.util.ArrayList;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ClickableComponent;
import nz.piromania.reactiveengine.component.ResourceImageComponent;
import nz.piromania.reactiveengine.component.TooltipComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.BoxSize;

/**
 * ComponentState for {@link ShipUpgradeScreenUpgradeComponent}.
 */
public class ShipUpgradeScreenUpgradeComponentState extends
    ComponentState<ShipUpgradeScreenUpgradeComponent> {

  private static final int size = 62;

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var gameEnv = Provider.of(GameEnvironment.class, context);
    final boolean isAdding = getComponent().getItem().getAppliedLocation() == null;

    final var item = getComponent().getItem();

    final ArrayList<TextSpan> tooltipText = ArrayListBuilder.build(
        new TextSpan(
            TextStyle.plain,
            true,
            (isAdding) ? "Click to add upgrade to ship.\n" : "Click to remove upgrade from ship\n"
        )
    );
    tooltipText.addAll(gameEnv.getTextFormatterGui().formatText(item.getGuiSummary()));

    return ArrayListBuilder.build(
        new TooltipComponent(
            new TextComponent(
                tooltipText
            ),
            new ClickableComponent(
                () -> {
                  getComponent().getOnClick().accept(item);
                },
                new ResourceImageComponent(
                    new BoxSize(size, size),
                    item.getImagePath()
                )
            )
        )
    );
  }
}
