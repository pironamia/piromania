package nz.piromania.ui.component.shipupgrade;

import java.util.function.Consumer;
import nz.piromania.gameenvironment.items.UpgradeItem;
import nz.piromania.gameenvironment.items.UpgradeType;
import nz.piromania.reactiveengine.Component;
import nz.piromania.util.DualConsumer;

/**
 * A single column in the ship upgrade screen, containing all applied upgrades of the given type.
 */
public class ShipUpgradeScreenUpgradeTypeColumnComponent extends Component {

  private final UpgradeType type;
  private final DualConsumer<UpgradeItem, UpgradeType> onDowngrade;

  private final boolean showingAddButton;
  private final Consumer<UpgradeType> onAddButtonClicked;

  /**
   * Constructs a ShipUpgradeScreenUpgradeTypeColumnComponent.
   *
   * @param type               the type of upgrades that this column contains
   * @param onDowngrade        called when the item is clicked to remove it from the column
   * @param showingAddButton   boolean passed to determine whether to show the '+' button at the
   *                           bottom of the column. The player presses this when selecting which
   *                           column to add an item to if that item can be applied to multiple
   *                           slots.
   * @param onAddButtonClicked callback when this component is <code>showingAddButton</code> and the
   *                           button is clicked.
   */
  public ShipUpgradeScreenUpgradeTypeColumnComponent(
      UpgradeType type,
      DualConsumer<UpgradeItem, UpgradeType> onDowngrade,
      boolean showingAddButton,
      Consumer<UpgradeType> onAddButtonClicked
  ) {
    this.type = type;
    this.onDowngrade = onDowngrade;
    this.showingAddButton = showingAddButton;
    this.onAddButtonClicked = onAddButtonClicked;
  }

  @Override
  protected ShipUpgradeScreenUpgradeTypeColumnComponentState createState() {
    return new ShipUpgradeScreenUpgradeTypeColumnComponentState();
  }

  public UpgradeType getType() {
    return type;
  }

  public DualConsumer<UpgradeItem, UpgradeType> getOnDowngrade() {
    return onDowngrade;
  }

  public boolean isShowingAddButton() {
    return showingAddButton;
  }

  public Consumer<UpgradeType> getOnAddButtonClicked() {
    return onAddButtonClicked;
  }
}
