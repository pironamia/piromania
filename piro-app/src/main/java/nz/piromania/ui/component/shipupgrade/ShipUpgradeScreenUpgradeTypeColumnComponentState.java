package nz.piromania.ui.component.shipupgrade;

import java.util.ArrayList;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.gameenvironment.items.UpgradeItem;
import nz.piromania.gameenvironment.items.UpgradeType;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ButtonComponent;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.ScrollingComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.EdgeInsets;
import nz.piromania.ui.AppTextStyles;
import nz.piromania.ui.component.FadeComponent;

/**
 * ComponentState for {@link ShipUpgradeScreenUpgradeTypeColumnComponent}.
 */
public class ShipUpgradeScreenUpgradeTypeColumnComponentState extends
    ComponentState<ShipUpgradeScreenUpgradeTypeColumnComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var gameEnv = Provider.of(GameEnvironment.class, context);

    final UpgradeType type = getComponent().getType();

    final ArrayList<Component> components = new ArrayList<>();

    for (final var item : gameEnv.getShip().getInventory().getItems()) {
      if (item instanceof UpgradeItem) {
        final UpgradeItem upgradeItem = (UpgradeItem) item;

        if (upgradeItem.getAppliedLocation() == type) {
          components.add(new ShipUpgradeScreenUpgradeComponent(
              upgradeItem,
              (x) -> {
                getComponent().getOnDowngrade().accept(x, type);
              }
          ));
        }
      }
    }

    return ArrayListBuilder.build(
        new PaddingComponent(
            new EdgeInsets(0, 20, 0, 0),
            new ListComponent(
                Axis.VERTICAL,
                ArrayListBuilder.buildFilteringNulls(
                    new TextComponent(
                        new TextSpan(
                            TextStyle.plain,
                            type.name()
                        ),
                        new TextSpan(
                            AppTextStyles.detailStyle,
                            String.format(
                                " (%d / %d)",
                                components.size(),
                                gameEnv.getShip().getUpgradeSlots().get(type) + components.size()
                            )
                        )
                    ),
                    new ExpandedComponent(
                        new PaddingComponent(
                            new EdgeInsets(0, 0, 0, 20),
                            new ListComponent(
                                Axis.VERTICAL,
                                new ExpandedComponent(
                                    new ScrollingComponent(
                                        new FadeComponent(
                                            new ListComponent(
                                                Axis.VERTICAL,
                                                components
                                            )
                                        )
                                    )
                                ),

                                ((getComponent().isShowingAddButton()) ? (
                                    new ButtonComponent(
                                        () -> {
                                          getComponent().getOnAddButtonClicked().accept(type);
                                        },
                                        new TextComponent("+")
                                    )
                                ) : null)
                            )
                        )
                    )
                )
            )
        )
    );
  }
}
