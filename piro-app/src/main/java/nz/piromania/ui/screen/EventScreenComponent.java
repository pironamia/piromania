package nz.piromania.ui.screen;

import nz.piromania.gameenvironment.eventsandchoices.Event;
import nz.piromania.reactiveengine.Component;

/**
 * Screen that shows an event.
 */
public class EventScreenComponent extends Component {
  private final Event event;

  public EventScreenComponent(Event event) {
    this.event = event;
  }

  @Override
  protected EventScreenComponentState createState() {
    return new EventScreenComponentState();
  }

  public Event getEvent() {
    return event;
  }
}
