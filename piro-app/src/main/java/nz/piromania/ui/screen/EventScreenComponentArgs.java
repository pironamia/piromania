package nz.piromania.ui.screen;

import java.util.Objects;
import nz.piromania.gameenvironment.eventsandchoices.Event;

/**
 * Arguments passed to the event screen component.
 */
public class EventScreenComponentArgs {
  private final Event event;

  public EventScreenComponentArgs(Event event) {
    this.event = event;
  }

  public Event getEvent() {
    return event;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EventScreenComponentArgs that = (EventScreenComponentArgs) o;
    return getEvent().equals(that.getEvent());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getEvent());
  }
}
