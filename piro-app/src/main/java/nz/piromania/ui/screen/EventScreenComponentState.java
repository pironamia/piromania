package nz.piromania.ui.screen;

import java.awt.Color;
import java.util.ArrayList;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.gameenvironment.eventsandchoices.Choice;
import nz.piromania.gameenvironment.eventsandchoices.Event;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ButtonComponent;
import nz.piromania.reactiveengine.component.CentreComponent;
import nz.piromania.reactiveengine.component.ContainerComponentBuilder;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.ResourceImageComponent;
import nz.piromania.reactiveengine.component.TooltipComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.WrappingTextComponent;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.BoxSize;
import nz.piromania.reactiveengine.util.EdgeInsets;
import nz.piromania.ui.AppTextStyles;
import nz.piromania.ui.component.FadeComponent;
import nz.piromania.ui.component.PageWrapperComponent;

/**
 * ComponentState for {@link EventScreenComponent}.
 */
public class EventScreenComponentState extends ComponentState<EventScreenComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var gameEnv = Provider.of(GameEnvironment.class, context);
    final Event event = getComponent().getEvent();

    final ArrayList<Component> choiceComponents = new ArrayList<>();

    for (Choice choice : event.getChoices()) {
      choiceComponents.add(new PaddingComponent(
          new EdgeInsets(8, 0, 0, 0),
          new TooltipComponent(
              choice.getTooltip(),
              new ButtonComponent(
                  choice::pickChoice,
                  new TextComponent(choice.getTitle())
              )
          )
      ));
    }

    return ArrayListBuilder.build(
        new PageWrapperComponent(
            "Islands/Island.jpg",
            null,
            new CentreComponent(
                new FadeComponent(
                    new ListComponent(
                        Axis.VERTICAL,
                        new ListComponent(
                            Axis.HORIZONTAL,
                            new ExpandedComponent(),
                            new TextComponent(
                                AppTextStyles.headingStyle
                                    .copyWith(null, null, null, null,
                                        Color.WHITE),
                                event.getTitle()
                            ),
                            new ExpandedComponent()
                        ),

                        new ListComponent(
                            Axis.HORIZONTAL,
                            new ExpandedComponent(),
                            new ResourceImageComponent(
                                new BoxSize(300, 200),
                                event.getImagePath()
                            ),
                            new ExpandedComponent()
                        ),

                        new ContainerComponentBuilder()
                            .build(
                                new WrappingTextComponent(
                                    gameEnv.getTextFormatterGui().formatText(
                                        event.getDescription(),
                                        Color.WHITE
                                    ),
                                    70
                                )
                            ),

                        new PaddingComponent(
                            new EdgeInsets(10, 0, 0, 0),
                            new ContainerComponentBuilder()
                                .setPadding(new EdgeInsets(1, 10, 0, 10))
                                .setBackgroundColour(Color.WHITE)
                                .build()
                        ),

                        new ListComponent(
                            Axis.VERTICAL,
                            choiceComponents
                        )
                    )
                )
            ),

            null
        )
    );
  }
}
