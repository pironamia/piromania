package nz.piromania.ui.screen;

import nz.piromania.reactiveengine.Component;

/**
 * Component that wraps the EventScreenComponent, automatically rebuilding it as necessary.
 */
public class EventScreenWrapperComponent extends Component {

  @Override
  protected EventScreenWrapperComponentState createState() {
    return new EventScreenWrapperComponentState();
  }
}
