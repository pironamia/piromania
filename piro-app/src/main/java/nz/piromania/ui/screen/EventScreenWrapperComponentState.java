package nz.piromania.ui.screen;

import java.util.ArrayList;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.reactivity.component.Observer;
import nz.piromania.reactiveengine.util.ArrayListBuilder;

/**
 * ComponentState for {@link EventScreenWrapperComponent}.
 */
public class EventScreenWrapperComponentState extends ComponentState<EventScreenWrapperComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    return ArrayListBuilder.build(
        new Observer((BuildContext context1) -> {
          final var args = (EventScreenComponentArgs) Provider.of(GameEnvironment.class, context1)
              .getScreenManager().getCurrentScreenArgs();
          return ArrayListBuilder.build(new EventScreenComponent(args.getEvent()));
        })
    );
  }
}
