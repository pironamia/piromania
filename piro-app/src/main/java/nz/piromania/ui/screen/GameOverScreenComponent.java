package nz.piromania.ui.screen;

import nz.piromania.reactiveengine.Component;

/**
 * Screen that shows the statistics for the last game and allows the player to exit.
 */
public class GameOverScreenComponent extends Component {

  @Override
  protected GameOverScreenComponentState createState() {
    return new GameOverScreenComponentState();
  }
}
