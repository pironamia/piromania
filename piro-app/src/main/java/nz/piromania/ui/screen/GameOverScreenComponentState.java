package nz.piromania.ui.screen;

import java.awt.Color;
import java.util.ArrayList;
import nz.piromania.App;
import nz.piromania.Currency;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ButtonComponent;
import nz.piromania.reactiveengine.component.CentreComponent;
import nz.piromania.reactiveengine.component.ContainerComponentBuilder;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.component.text.style.FontStyle;
import nz.piromania.reactiveengine.component.text.style.FontWeight;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.EdgeInsets;
import nz.piromania.ui.AppTextStyles;
import nz.piromania.ui.component.PageWrapperComponent;

/**
 * ComponentState for {@link GameOverScreenComponent}.
 */
public class GameOverScreenComponentState extends ComponentState<GameOverScreenComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var app = Provider.of(App.class, context);
    final var gameEnv = Provider.of(GameEnvironment.class, context);

    final TextStyle bodyStyle = TextStyle.plain.copyWith(null, null, null,
        null, Color.WHITE);

    final Currency profit = gameEnv.getTraderMoney()
        .add(gameEnv.getShip().getInventory().getWorth());
    final double finalScore =
        profit.getCents() * ((double) gameEnv.getCurrentDay() / (double) gameEnv.getGameDuration());

    return ArrayListBuilder.build(
        new PageWrapperComponent(
            "Islands/Island.jpg",
            null,
            new CentreComponent(
                new ContainerComponentBuilder()
                    .setPadding(new EdgeInsets(10))
                    .setBackgroundColour(new Color(0, 0, 0, 127))
                    .build(new ListComponent(
                        Axis.VERTICAL,
                        new ListComponent(
                            Axis.HORIZONTAL,
                            new ExpandedComponent(),
                            new TextComponent(
                                AppTextStyles.headingStyle
                                    .copyWith(null, null, null, null,
                                        Color.WHITE),
                                "Game over"
                            ),
                            new ExpandedComponent()
                        ),

                        new TextComponent(
                            new TextSpan(
                                bodyStyle,
                                "That's it "
                            ),
                            new TextSpan(
                                bodyStyle.copyWith(FontStyle.ITALIC, null, null, null, null),
                                String.format(
                                    "%s.",
                                    gameEnv.getTraderName()
                                )
                            )
                        ),

                        new TextSpan(
                            bodyStyle,
                            String.format(
                                "You survived %d / %d chosen days.",
                                gameEnv.getCurrentDay(),
                                gameEnv.getGameDuration()
                            )
                        ),

                        new TextSpan(
                            bodyStyle,
                            String.format(
                                "You finished with %s cash.",
                                gameEnv.getTraderMoney()
                            )
                        ),

                        new TextSpan(
                            bodyStyle,
                            String.format(
                                "Your assets brought %s at auction.",
                                gameEnv.getShip().getInventory().getWorth()
                            )
                        ),

                        new TextSpan(
                            bodyStyle.copyWith(null, FontWeight.BOLD, null, 20, null),
                            String.format(
                                "Final score %.0f",
                                finalScore
                            )
                        ),

                        new PaddingComponent(
                            new EdgeInsets(10, 0, 0, 0),
                            new ContainerComponentBuilder()
                                .setPadding(new EdgeInsets(1, 10, 0, 10))
                                .setBackgroundColour(Color.WHITE)
                                .build()
                        ),

                        new PaddingComponent(
                            new EdgeInsets(8, 0, 0, 0),
                            new ButtonComponent(
                                app::exit,
                                new TextComponent(
                                    "Exit game"
                                )
                            )
                        )
                    ))
            ),

            null
        )
    );
  }
}
