package nz.piromania.ui.screen;

import nz.piromania.reactiveengine.Component;

/**
 * Island screen component that acts as the main screen the user is delivered to when they
 * first arrive at an island.
 */
public class IslandScreenComponent extends Component {

  @Override
  protected IslandScreenComponentState createState() {
    return new IslandScreenComponentState();
  }
}
