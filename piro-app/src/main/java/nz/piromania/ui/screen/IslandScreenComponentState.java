package nz.piromania.ui.screen;

import java.util.ArrayList;
import nz.piromania.App;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ButtonComponent;
import nz.piromania.reactiveengine.component.CentreComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.EdgeInsets;
import nz.piromania.ui.AppTextStyles;
import nz.piromania.ui.Screen;
import nz.piromania.ui.component.FadeComponent;
import nz.piromania.ui.component.PageWrapperComponent;
import nz.piromania.ui.component.ShipInfoBarComponent;

/**
 * ComponentState for {@link IslandScreenComponent}.
 */
public class IslandScreenComponentState extends ComponentState<IslandScreenComponent> {
  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var app = Provider.of(App.class, context);
    final var gameEnvironment = Provider.of(GameEnvironment.class, context);

    return ArrayListBuilder.build(new PageWrapperComponent(
        "Islands/Island.jpg",
        null,
        new CentreComponent(
            new FadeComponent(
                new ListComponent(
                    Axis.VERTICAL,
                    new TextComponent(
                        AppTextStyles.headingStyleWhite,
                        gameEnvironment.getCurrentIsland().getName()
                    ),

                    new PaddingComponent(
                        new EdgeInsets(5, 0),
                        new ButtonComponent(
                            () -> {
                              gameEnvironment.getScreenManager().show(Screen.JOURNEY_PLANNER);
                            },
                            new TextComponent("Plot next journey")
                        )
                    ),
                    new PaddingComponent(
                        new EdgeInsets(5, 0),
                        new ButtonComponent(
                            () -> {
                              gameEnvironment.getScreenManager().show(Screen.ISLAND_MAP);
                            },
                            new TextComponent("Island map")
                        )
                    ),
                    new PaddingComponent(
                        new EdgeInsets(5, 0),
                        new ButtonComponent(
                            () -> {
                              gameEnvironment.getScreenManager().show(Screen.ISLAND_TRADE);
                            },
                            new TextComponent("Trade")
                        )
                    ),
                    new PaddingComponent(
                        new EdgeInsets(5, 0),
                        new ButtonComponent(
                            () -> {
                              gameEnvironment.getScreenManager().show(Screen.SHIP_OVERVIEW);
                            },
                            new TextComponent("Ship information")
                        )
                    ),
                    new PaddingComponent(
                        new EdgeInsets(5, 0),
                        new ButtonComponent(
                            () -> {
                              gameEnvironment.getScreenManager().show(Screen.GAME_OVER);
                            },
                            new TextComponent("Retire")
                        )
                    )
                )
            )
        ),

        // Bottom nav
        new ShipInfoBarComponent()
    ));
  }
}
