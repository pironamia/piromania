package nz.piromania.ui.screen;

import nz.piromania.reactiveengine.Component;

/**
 * Screen where the user may trade with the island they are currently docked at.
 */
public class IslandTradingScreenComponent extends Component {

  @Override
  protected IslandTradingScreenComponentState createState() {
    return new IslandTradingScreenComponentState();
  }
}
