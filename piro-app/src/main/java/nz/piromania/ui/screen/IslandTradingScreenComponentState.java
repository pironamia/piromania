package nz.piromania.ui.screen;

import java.util.ArrayList;
import nz.piromania.App;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ContainerComponentBuilder;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.ScrollingComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.reactivity.component.Observer;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.EdgeInsets;
import nz.piromania.ui.AppTextStyles;
import nz.piromania.ui.Screen;
import nz.piromania.ui.component.FadeComponent;
import nz.piromania.ui.component.PageWrapperComponent;
import nz.piromania.ui.component.PageWrapperTitleBarWithBackButtonComponent;
import nz.piromania.ui.component.ShipInfoBarComponent;
import nz.piromania.ui.component.TradingScreenItemBuyRowComponent;
import nz.piromania.ui.component.TradingScreenItemSellRowComponent;

/**
 * Component state for {@link IslandTradingScreenComponent}.
 */
public class IslandTradingScreenComponentState
    extends ComponentState<IslandTradingScreenComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var gameEnvironment = Provider.of(GameEnvironment.class, context);

    return ArrayListBuilder.build(new PageWrapperComponent(
        "Islands/Island.jpg",
        // Top navigation
        new PageWrapperTitleBarWithBackButtonComponent(
            () -> {
              gameEnvironment.getScreenManager().show(Screen.ISLAND_OVERVIEW);
            },
            new TextComponent(
                AppTextStyles.headingStyleWhite.copyWith(null, null, null, 30, null),
                String.format(
                    "Trade with %s on %s",
                    gameEnvironment.getCurrentIsland().getStore().getStoreName(),
                    gameEnvironment.getCurrentIsland().getName()
                )
            )
        ),

        // Main panel
        new ListComponent(
            Axis.VERTICAL,
            new ContainerComponentBuilder()
                .setPadding(new EdgeInsets(1, App.getScreenWidth() / 2 - 10))
                .build(),

            new ExpandedComponent(
                new ListComponent(
                    Axis.HORIZONTAL,
                    new ContainerComponentBuilder()
                        .setPadding(new EdgeInsets(App.getScreenHeight() / 2 - 75, 1))
                        .build(),

                    new ExpandedComponent(
                        new ListComponent(
                            Axis.VERTICAL,
                            new FadeComponent(
                                new Observer((context1) -> {
                                  final var ship = gameEnvironment.getShip();

                                  return ArrayListBuilder.build(
                                      new TextComponent(
                                          new TextSpan(
                                              TextStyle.plainWhite,
                                              "Your inventory. Capacity:"
                                          ),
                                          new TextSpan(
                                              AppTextStyles.detailStyleWhite,
                                              String.format(
                                                  " (%d/%d)",
                                                  //Max capacity - Current capacity
                                                  ship.getShipStats().getCargoCapacity()
                                                      - ship.getInventory().getCapacity(),
                                                  ship.getShipStats().getCargoCapacity()
                                              )
                                          )
                                      )
                                  );
                                })
                            ),
                            new ExpandedComponent(
                                new ScrollingComponent(
                                    new Observer((context1) -> {
                                      final ArrayList<Component> itemRows = new ArrayList<>();

                                      for (var item : gameEnvironment.getShip().getInventory()
                                          .getItems()) {
                                        itemRows.add(new TradingScreenItemSellRowComponent(item));
                                      }

                                      return ArrayListBuilder.build(
                                          new FadeComponent(
                                              new ListComponent(
                                                  Axis.VERTICAL,
                                                  itemRows
                                              )
                                          )
                                      );
                                    })
                                )
                            )

                        )
                    ),

                    new PaddingComponent(
                        new EdgeInsets(0, 10),
                        new ContainerComponentBuilder().build()
                    ),

                    new ExpandedComponent(
                        new ListComponent(
                            Axis.VERTICAL,
                            new FadeComponent(
                                new TextComponent(TextStyle.plainWhite, "Store inventory")
                            ),
                            new ExpandedComponent(
                                new ScrollingComponent(
                                    new Observer((context1) -> {
                                      final ArrayList<Component> itemRows = new ArrayList<>();

                                      for (var item : gameEnvironment.getCurrentIsland().getStore()
                                          .getStoreInventory().getItems()) {
                                        itemRows.add(new TradingScreenItemBuyRowComponent(item));
                                      }

                                      return ArrayListBuilder.build(
                                          new FadeComponent(
                                              new ListComponent(
                                                  Axis.VERTICAL,
                                                  itemRows
                                              )
                                          )
                                      );
                                    })
                                )
                            )
                        )

                    )
                )
            )
        ),

        new ShipInfoBarComponent()
    ));
  }
}
