package nz.piromania.ui.screen;

import nz.piromania.reactiveengine.Component;

/**
 * Screen where the player can choose which route they want to sail to another island.
 */
public class JourneyPlannerScreenComponent extends Component {

  @Override
  protected JourneyPlannerScreenComponentState createState() {
    return new JourneyPlannerScreenComponentState();
  }
}
