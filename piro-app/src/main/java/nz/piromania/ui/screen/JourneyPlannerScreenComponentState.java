package nz.piromania.ui.screen;

import java.util.ArrayList;
import nz.piromania.App;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.gameenvironment.islands.Island;
import nz.piromania.gameenvironment.routes.Route;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ContainerComponentBuilder;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.ScrollingComponent;
import nz.piromania.reactiveengine.component.list.FlexibleComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.EdgeInsets;
import nz.piromania.ui.AppTextStyles;
import nz.piromania.ui.Screen;
import nz.piromania.ui.component.FadeComponent;
import nz.piromania.ui.component.PageWrapperComponent;
import nz.piromania.ui.component.PageWrapperTitleBarWithBackButtonComponent;
import nz.piromania.ui.component.ShipInfoBarComponent;
import nz.piromania.ui.component.journeyplanner.JourneyPlannerScreenIslandInfoPaneComponent;
import nz.piromania.ui.component.journeyplanner.JourneyPlannerScreenIslandRowComponent;
import nz.piromania.ui.component.journeyplanner.JourneyPlannerScreenRouteRowComponent;

/**
 * ComponentState for {@link JourneyPlannerScreenComponent}.
 */
public class JourneyPlannerScreenComponentState extends
    ComponentState<JourneyPlannerScreenComponent> {

  private Integer currentIslandIndex;
  private Integer currentRouteIndex;

  private void onIslandSelected(int islandIndex) {
    setState(() -> {
      currentIslandIndex = islandIndex;
      currentRouteIndex = null;
    });
  }

  private void onRouteSelected(int routeIndex) {
    if (currentIslandIndex == null) {
      throw new RuntimeException("Cannot select route index for unknown island.");
    }

    setState(() -> {
      currentRouteIndex = routeIndex;
    });
  }

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var gameEnvironment = Provider.of(GameEnvironment.class, context);

    final var islandRows = new ArrayList<Component>();
    int playerIslandIndex = 0;
    for (int i = 0; i < gameEnvironment.getAllIslands().size(); i++) {
      final var island = gameEnvironment.getAllIslands().get(i);
      if (gameEnvironment.getCurrentIsland() != island) {
        islandRows.add(new JourneyPlannerScreenIslandRowComponent(
            i,
            this::onIslandSelected
        ));
      } else {
        playerIslandIndex = i;
      }
    }

    final Island currentIsland = (currentIslandIndex == null) ? null
        : gameEnvironment.getAllIslands().get(currentIslandIndex);
    final Integer islandRouteIndex = (currentIslandIndex == null) ? null
        : ((currentIslandIndex < playerIslandIndex) ? currentIslandIndex : currentIslandIndex - 1);
    final Route currentRoute = (currentRouteIndex == null) ? null
        : gameEnvironment.getCurrentRoutes().get(islandRouteIndex).get(currentRouteIndex);

    final var routeRows = new ArrayList<Component>();
    if (islandRouteIndex != null) {
      final var routes = gameEnvironment.getCurrentRoutes().get(islandRouteIndex);
      for (int i = 0; i < routes.size(); i++) {
        routeRows.add(new JourneyPlannerScreenRouteRowComponent(
            islandRouteIndex,
            i,
            this::onRouteSelected
        ));
      }
    }

    return ArrayListBuilder.build(
        new PageWrapperComponent(
            "Islands/Island.jpg",
            new PageWrapperTitleBarWithBackButtonComponent(
                () -> {
                  gameEnvironment.getScreenManager().show(Screen.ISLAND_OVERVIEW);
                },
                new TextComponent(
                    AppTextStyles.headingStyleWhite,
                    "Route planner"
                )
            ),

            new ListComponent(
                Axis.VERTICAL,
                new ContainerComponentBuilder()
                    .setPadding(new EdgeInsets(1, App.getScreenWidth() / 2 - 10))
                    .build(),

                new ExpandedComponent(
                    new ListComponent(
                        Axis.HORIZONTAL,
                        new ContainerComponentBuilder()
                            .setPadding(new EdgeInsets(App.getScreenHeight() / 2 - 75, 1))
                            .build(),

                        new FlexibleComponent(
                            2,
                            new ListComponent(
                                Axis.VERTICAL,
                                ArrayListBuilder.buildFilteringNulls(
                                    new FlexibleComponent(
                                        2,
                                        new ListComponent(
                                            Axis.VERTICAL,
                                            new FadeComponent(
                                                new PaddingComponent(
                                                    new EdgeInsets(5, 5),
                                                    new TextComponent(
                                                        TextStyle.plainWhite,
                                                        "Islands:"
                                                    )
                                                )
                                            ),
                                            new ExpandedComponent(
                                                new ScrollingComponent(
                                                    new FadeComponent(
                                                        new ListComponent(
                                                            Axis.VERTICAL,
                                                            islandRows
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    ),
                                    ((currentIslandIndex == null) ? null : (
                                        new FlexibleComponent(
                                            3,
                                            new PaddingComponent(
                                                new EdgeInsets(10, 0, 0, 0),
                                                new ListComponent(
                                                    Axis.VERTICAL,
                                                    new FadeComponent(
                                                        new PaddingComponent(
                                                            new EdgeInsets(5, 5),
                                                            new TextComponent(
                                                                TextStyle.plainWhite,
                                                                "Routes:"
                                                            )
                                                        )
                                                    ),
                                                    new ExpandedComponent(
                                                        new ScrollingComponent(
                                                            new FadeComponent(
                                                                new ListComponent(
                                                                    Axis.VERTICAL,
                                                                    routeRows
                                                                )
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    ))
                                )
                            )
                        ),

                        new ContainerComponentBuilder()
                            .setPadding(new EdgeInsets(0, 5))
                            .build(),

                        new FlexibleComponent(
                            5,
                            new JourneyPlannerScreenIslandInfoPaneComponent(
                                currentIsland,
                                currentRoute
                            )
                        )
                    )
                )
            ),

            new ShipInfoBarComponent()
        )
    );
  }
}
