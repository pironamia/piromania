package nz.piromania.ui.screen;

import nz.piromania.reactiveengine.Component;

/**
 * Screen that shows the map.
 */
public class MapScreenComponent extends Component {

  @Override
  protected MapScreenComponentState createState() {
    return new MapScreenComponentState();
  }
}