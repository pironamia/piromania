package nz.piromania.ui.screen;

import java.util.ArrayList;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.ImageComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.ui.AppTextStyles;
import nz.piromania.ui.Screen;
import nz.piromania.ui.component.FadeComponent;
import nz.piromania.ui.component.PageWrapperTitleBarWithBackButtonComponent;

/**
 * ComponentState for {@link MapScreenComponent}.
 */
public class MapScreenComponentState extends ComponentState<MapScreenComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final GameEnvironment gameEnvironment = Provider.of(GameEnvironment.class, context);
    return ArrayListBuilder.build(
        new ListComponent(
            Axis.VERTICAL,
            new FadeComponent(
                new PageWrapperTitleBarWithBackButtonComponent(
                    () -> {
                      gameEnvironment.getScreenManager().show(Screen.ISLAND_OVERVIEW);
                    },
                    new TextComponent(
                        AppTextStyles.headingStyleWhite,
                        "Island Map"
                    )
                )
            ),

            new ExpandedComponent(
                new ImageComponent(gameEnvironment.getMap())
            )
        )
    );
  }
}
