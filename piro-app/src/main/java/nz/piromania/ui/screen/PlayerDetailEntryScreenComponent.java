package nz.piromania.ui.screen;

import nz.piromania.reactiveengine.Component;

/**
 * Screen where the player is prompted to enter their name and game duration.
 */
public class PlayerDetailEntryScreenComponent extends Component {

  @Override
  protected PlayerDetailEntryScreenComponentState createState() {
    return new PlayerDetailEntryScreenComponentState();
  }
}
