package nz.piromania.ui.screen;

import java.awt.Color;
import java.util.ArrayList;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.AlignmentComponent;
import nz.piromania.reactiveengine.component.ButtonComponent;
import nz.piromania.reactiveengine.component.CentreComponent;
import nz.piromania.reactiveengine.component.ContainerComponent;
import nz.piromania.reactiveengine.component.ContainerComponentBuilder;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.StackComponent;
import nz.piromania.reactiveengine.component.input.TextboxComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.reactivity.component.Observer;
import nz.piromania.reactiveengine.util.Alignment;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.BoxSize;
import nz.piromania.reactiveengine.util.EdgeInsets;
import nz.piromania.ui.AppTextStyles;
import nz.piromania.ui.Screen;
import nz.piromania.ui.component.PageWrapperComponent;

/**
 * Component state for the {@link PlayerDetailEntryScreenComponent}.
 */
public class PlayerDetailEntryScreenComponentState
    extends ComponentState<PlayerDetailEntryScreenComponent> {

  private static final int entryBoxHalfWidth = 170;

  private final PlayerDetailEntryScreenStore store = new PlayerDetailEntryScreenStore();

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final GameEnvironment gameEnvironment = Provider.of(GameEnvironment.class, context);

    return ArrayListBuilder.build(
        new StackComponent(
            new CentreComponent(
                new Observer((BuildContext context1) ->
                    ArrayListBuilder.build(
                        new ListComponent(
                            Axis.VERTICAL,
                            new AlignmentComponent(
                                // Pushes parent to be 300 wide.
                                Alignment.TOP_LEFT,
                                new ContainerComponentBuilder()
                                    .setPadding(new EdgeInsets(0, entryBoxHalfWidth))
                                    .build()
                            ),

                            // Player name
                            new TextComponent("Trader name:"),
                            new TextboxComponent(
                                store.getPlayerName(),
                                store::setPlayerName
                            ),
                            new ContainerComponentBuilder()
                                .setSize(new BoxSize(null, 15))
                                .build(
                                    new TextSpan(
                                        AppTextStyles.errorStyle,
                                        store.getPlayerNameError()
                                    )
                                ),

                            // Game Duration
                            new TextComponent("Game duration (days):"),
                            new TextboxComponent(
                                store.getGameDuration(),
                                "Please enter a number between 20 and 50",
                                store::setGameDuration
                            ),
                            new ContainerComponentBuilder()
                                .setSize(new BoxSize(null, 15))
                                .build(
                                    new TextSpan(
                                        AppTextStyles.errorStyle,
                                        store.getGameDurationError()
                                    )
                                ),

                            new ContainerComponentBuilder()
                                .setPadding(new EdgeInsets(5, 0))
                                .build(),

                            // Proceed Button
                            new ListComponent(
                                Axis.HORIZONTAL,
                                new ButtonComponent(
                                    () -> {
                                      gameEnvironment.getScreenManager().show(Screen.WELCOME);
                                    },
                                    new TextComponent("Cancel")
                                ),
                                new ExpandedComponent(),

                                new ButtonComponent(
                                    () -> {
                                      if (store.isValid()) {
                                        gameEnvironment.getScreenManager().show(
                                            Screen.PLAYER_DETAILS_2,
                                            new PlayerShipSelectionScreenArgs(
                                                store.getPlayerName(),
                                                store.getGameDurationInt()
                                            )
                                        );
                                      }
                                    },
                                    !store.isValid(),
                                    new TextComponent("Continue")
                                )

                            )
                        )
                    )
                )
            ),
            new ContainerComponent(
                null,
                Alignment.CENTRE,
                EdgeInsets.zero,
                Color.ORANGE,
                null
            )
        )
    );
  }
}
