package nz.piromania.ui.screen;

import java.util.regex.Pattern;
import nz.piromania.reactiveengine.reactivity.Action;
import nz.piromania.reactiveengine.reactivity.Observable;

/**
 * Stores and validates data for the PlayerDetailEntryScreen.
 */
public class PlayerDetailEntryScreenStore {

  /**
   * Regex that accepts between 3 and 15 alphabet characters, enforcing that the string must start
   * with a letter (not a space), can have no more than one space in a row, and cannot end in a
   * space.
   */
  private static final Pattern playerNamePattern =
      Pattern.compile("^[a-zA-Z]([a-zA-Z]|( (?!( |$)))){2,14}$");

  private final Observable<String> playerName = new Observable<>(null);
  private final Observable<String> playerNameError = new Observable<>("");

  private final Observable<String> gameDuration = new Observable<>(null);
  private final Observable<String> gameDurationError = new Observable<>("");

  public PlayerDetailEntryScreenStore() {
  }

  private String validatePlayerName(String name) {
    if (name == null) {
      // User hasn't started editing yet.
      return "";
    }

    if (name.length() < 3 || name.length() > 15) {
      return "Name must be at least 3 and at most 15 characters.";
    }

    final var matcher = playerNamePattern.matcher(name);

    if (!matcher.matches()) {
      if (name.contains("  ")) {
        return "Name cannot contain multiple consecutive spaces.";
      } else if (name.startsWith(" ") || name.endsWith(" ")) {
        return "Name may not start or end with a space.";
      } else {
        return "Name may only contain alphabet characters and spaces.";
      }
    } else {
      return "";
    }
  }

  private String validateGameDuration(String duration) {
    if (duration == null) {
      return "";  // Don't show an error if the user hasn't entered anything yet.
    }

    try {
      final int durationVal = Integer.parseInt(duration, 10);

      if (20 <= durationVal && durationVal <= 50) {
        return "";
      } else {
        return "Please specify a number of days from 20 to 50.";
      }
    } catch (NumberFormatException ex) {
      return "Invalid number format. Please enter a base 10 integer.";
    }
  }

  /**
   * Determines whether the current playerName and gameDuration values are valid.
   */
  public boolean isValid() {
    final var name = playerName.get();
    final var duration = gameDuration.get();
    return name != null && duration != null && validatePlayerName(playerName.get()).equals("")
        && validateGameDuration(gameDuration.get()).equals("");
  }

  public String getPlayerName() {
    final var name = playerName.get();
    return (name == null) ? "" : name;
  }

  /**
   * Sets the player name.
   */
  public void setPlayerName(String newValue) {
    Action.run(() -> {
      playerName.set(newValue);
      playerNameError.set(validatePlayerName(newValue));
    });
  }

  public String getPlayerNameError() {
    return playerNameError.get();
  }

  public String getGameDuration() {
    final var duration = gameDuration.get();
    return (duration == null) ? "" : duration;
  }

  /**
   * Returns the value of gameDuration parsed and validated as an integer.
   * If the value is not valid, throws a RuntimeException
   *
   * @return gameDuration as an integer
   * @throws RuntimeException if the gameDuration string is invalid
   */
  public int getGameDurationInt() {
    final String duration = gameDuration.get();

    if (duration == null || !validateGameDuration(duration).equals("")) {
      throw new RuntimeException("Cannot get game duration before validating that it is valid.");
    }

    return Integer.parseInt(gameDuration.get(), 10);
  }

  /**
   * Sets the game duration.
   */
  public void setGameDuration(String newValue) {
    Action.run(() -> {
      gameDuration.set(newValue);
      gameDurationError.set(validateGameDuration(newValue));
    });
  }

  public String getGameDurationError() {
    return gameDurationError.get();
  }
}
