package nz.piromania.ui.screen;

/**
 * Class representing the arguments that the PlayerShipSelectionScreenComponent expects to receive.
 */
public class PlayerShipSelectionScreenArgs {
  private final String playerName;
  private final int gameDuration;

  public PlayerShipSelectionScreenArgs(String playerName, int gameDuration) {
    this.playerName = playerName;
    this.gameDuration = gameDuration;
  }

  public String getPlayerName() {
    return playerName;
  }

  public int getGameDuration() {
    return gameDuration;
  }
}
