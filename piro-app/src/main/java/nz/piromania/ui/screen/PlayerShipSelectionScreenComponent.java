package nz.piromania.ui.screen;

import nz.piromania.reactiveengine.Component;

/**
 * Screen where the player is prompted to choose their ship.
 */
public class PlayerShipSelectionScreenComponent extends Component {

  @Override
  protected PlayerShipSelectionScreenComponentState createState() {
    return new PlayerShipSelectionScreenComponentState();
  }
}
