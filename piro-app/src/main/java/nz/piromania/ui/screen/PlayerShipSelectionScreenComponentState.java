package nz.piromania.ui.screen;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.function.Consumer;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.gameenvironment.ships.Ship;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.CentreComponent;
import nz.piromania.reactiveengine.component.ContainerComponent;
import nz.piromania.reactiveengine.component.StackComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.Alignment;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.EdgeInsets;
import nz.piromania.ui.AppTextStyles;
import nz.piromania.ui.component.PageWrapperComponent;
import nz.piromania.ui.component.ShipSelectionComponent;

/**
 * ComponentState for {@link PlayerShipSelectionScreenComponent}.
 */
public class PlayerShipSelectionScreenComponentState
    extends ComponentState<PlayerShipSelectionScreenComponent> {

  private final Random randomGenerator = new Random();

  private final Ship[] availableShips = new Ship[4];
  boolean hasPopulatedAvailableShips = false;

  private void populateShipChoices(GameEnvironment env) {
    if (env.getShipManager().getDataSize() < 4) {
      throw new RuntimeException(
          "PlayerShipSelectionScreen requires at least 4 ships to choose from.");
    }

    for (int i = 0; i < 4; i++) {
      final var container = new Object() {
        Ship currentship;
      };

      do {
        container.currentship = env.getShipManager().getRandomData();
      } while (Arrays.stream(availableShips).anyMatch((ship -> ship == container.currentship)));

      availableShips[i] = container.currentship;
    }
  }

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final GameEnvironment gameEnvironment = Provider.of(GameEnvironment.class, context);
    final PlayerShipSelectionScreenArgs args =
        (PlayerShipSelectionScreenArgs) gameEnvironment.getScreenManager().getCurrentScreenArgs();

    if (!hasPopulatedAvailableShips) {
      populateShipChoices(gameEnvironment);
    }

    final Consumer<Ship> onShipSelected = (ship) -> {
      gameEnvironment.startGame(
          args.getPlayerName(),
          args.getGameDuration(),
          ship
      );
    };

    return ArrayListBuilder.build(new StackComponent(
        new CentreComponent(
            new ListComponent(
                Axis.VERTICAL,
                new TextComponent(
                    AppTextStyles.headingStyle,
                    "Select ye vessel"
                ),

                new ListComponent(
                    Axis.HORIZONTAL,
                    new ShipSelectionComponent(
                        availableShips[0],
                        onShipSelected
                    ),
                    new ShipSelectionComponent(
                        availableShips[1],
                        onShipSelected
                    )
                ),

                new ListComponent(
                    Axis.HORIZONTAL,
                    new ShipSelectionComponent(
                        availableShips[2],
                        onShipSelected
                    ),
                    new ShipSelectionComponent(
                        availableShips[3],
                        onShipSelected
                    )
                )
            )
        ),
        new ContainerComponent(
            null,
            Alignment.CENTRE,
            EdgeInsets.zero,
            Color.ORANGE,
            null
        )
    ));
  }
}
