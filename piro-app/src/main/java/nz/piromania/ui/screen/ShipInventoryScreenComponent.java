package nz.piromania.ui.screen;

import nz.piromania.reactiveengine.Component;

/**
 * Screen that displays the ship's current inventory and its past inventory changes.
 */
public class ShipInventoryScreenComponent extends Component {

  @Override
  protected ShipInventoryScreenComponentState createState() {
    return new ShipInventoryScreenComponentState();
  }
}
