package nz.piromania.ui.screen;

import java.util.ArrayList;
import nz.piromania.App;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ButtonComponent;
import nz.piromania.reactiveengine.component.ContainerComponentBuilder;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.ScrollingComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.reactivity.component.Observer;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.EdgeInsets;
import nz.piromania.ui.AppTextStyles;
import nz.piromania.ui.Screen;
import nz.piromania.ui.component.FadeComponent;
import nz.piromania.ui.component.PageWrapperComponent;
import nz.piromania.ui.component.PageWrapperTitleBarWithBackButtonComponent;
import nz.piromania.ui.component.ShipInventoryItemComponent;

/**
 * ComponentState for {@link ShipInventoryScreenComponent}.
 */
public class ShipInventoryScreenComponentState extends
    ComponentState<ShipInventoryScreenComponent> {

  private ShipInventoryScreenFilter currentFilter = ShipInventoryScreenFilter.UNSOLD;

  private void setFilter(ShipInventoryScreenFilter filter) {
    setState(() -> {
      currentFilter = filter;
    });
  }

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var gameEnvironment = Provider.of(GameEnvironment.class, context);

    final var itemRows = new ArrayList<Component>();
    var currentRow = new ArrayList<Component>();
    int totalUsedSpace = 0;  // sum of the sizes of the items.

    if (currentFilter == ShipInventoryScreenFilter.UNSOLD) {
      // Show the trader's inventory
      final var items = gameEnvironment.getShip().getInventory().getItems();

      // Build the items into rows of 6, recording useful metadata as we go
      for (int i = 0; i < items.size(); i++) {
        if (i % 6 == 0) {
          itemRows.add(
              new ListComponent(
                  Axis.HORIZONTAL,
                  currentRow
              )
          );

          currentRow = new ArrayList<>();
        }

        final var item = items.get(i);
        currentRow.add(new ShipInventoryItemComponent(
            item,
            null,
            item.getPurchasedValue()
        ));
        totalUsedSpace += item.getItemSize();
      }
    } else {
      // Show the sold inventory.
      int i = 0;

      for (final var storeEntry : gameEnvironment.getSoldLedger().entrySet()) {
        final var store = storeEntry.getKey();
        final var items = storeEntry.getValue();
        for (final var itemEntry : items.entrySet()) {
          final var item = itemEntry.getKey();
          final var originalPurchaseValue = itemEntry.getValue();

          if (i % 6 == 0) {
            itemRows.add(
                new ListComponent(
                    Axis.HORIZONTAL,
                    currentRow
                )
            );

            currentRow = new ArrayList<>();
          }

          currentRow.add(new ShipInventoryItemComponent(
              item,
              store,
              originalPurchaseValue
          ));
          totalUsedSpace += item.getItemSize();

          i++;
        }
      }
    }

    if (!currentRow.isEmpty()) {
      itemRows.add(new ListComponent(
          Axis.HORIZONTAL,
          currentRow
      ));
    }

    final int totalSpaceUsed = totalUsedSpace;

    return ArrayListBuilder.build(
        new PageWrapperComponent(
            "Islands/Island.jpg",
            new PageWrapperTitleBarWithBackButtonComponent(
                () -> {
                  gameEnvironment.getScreenManager().show(Screen.SHIP_OVERVIEW);
                },
                new Observer((context1) -> {
                  final String currentFilterTitle =
                      (currentFilter == ShipInventoryScreenFilter.SOLD) ? "Ledger" : "Cargo";

                  return ArrayListBuilder.build(
                      new TextComponent(
                          ArrayListBuilder.buildFilteringNulls(
                              new TextSpan(AppTextStyles.headingStyleWhite, currentFilterTitle),
                              ((currentFilter == ShipInventoryScreenFilter.SOLD) ? null : (
                                  new TextSpan(
                                      AppTextStyles.detailStyleWhite,
                                      String.format(
                                          " (%d/%d)",
                                          //Max capacity - Current capacity
                                          gameEnvironment.getShip().getShipStats()
                                              .getCargoCapacity() - gameEnvironment.getShip()
                                              .getInventory().getCapacity(),
                                          gameEnvironment.getShip().getShipStats()
                                              .getCargoCapacity()
                                      )
                                  )
                              ))
                          )
                      )
                  );
                })
            ),

            new ListComponent(
                Axis.VERTICAL,
                new ContainerComponentBuilder()
                    .setPadding(new EdgeInsets(1, App.getScreenWidth() / 2 - 10))
                    .build(),

                new ListComponent(
                    Axis.HORIZONTAL,
                    new PaddingComponent(
                        new EdgeInsets(0, 5, 0, 0),
                        new TextComponent("Filter:")
                    ),
                    new PaddingComponent(
                        new EdgeInsets(5, 5),
                        new ButtonComponent(
                            () -> {
                              setFilter(ShipInventoryScreenFilter.UNSOLD);
                            },
                            currentFilter == ShipInventoryScreenFilter.UNSOLD,
                            new TextComponent("Unsold")
                        )
                    ),
                    new PaddingComponent(
                        new EdgeInsets(5, 5),
                        new ButtonComponent(
                            () -> {
                              setFilter(ShipInventoryScreenFilter.SOLD);
                            },
                            currentFilter == ShipInventoryScreenFilter.SOLD,
                            new TextComponent("Sold")
                        )
                    )
                ),

                new ExpandedComponent(
                    new ListComponent(
                        Axis.HORIZONTAL,
                        new ContainerComponentBuilder()
                            .setPadding(new EdgeInsets(App.getScreenHeight() / 2 - 70, 1))
                            .build(),

                        new ExpandedComponent(
                            new ScrollingComponent(
                                new FadeComponent(
                                    new ListComponent(
                                        Axis.VERTICAL,
                                        itemRows
                                    )
                                )
                            )
                        )
                    )
                )
            ),

            null
        )
    );
  }
}
