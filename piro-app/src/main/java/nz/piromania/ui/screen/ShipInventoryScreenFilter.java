package nz.piromania.ui.screen;

/**
 * Defines the types of filters available in the ShipInventoryScreen.
 */
public enum ShipInventoryScreenFilter {
  UNSOLD,
  SOLD
}
