package nz.piromania.ui.screen;

import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;

/**
 * Screen that shows details about the ship.
 */
public class ShipScreenComponent extends Component {

  @Override
  protected ShipScreenComponentState createState() {
    return new ShipScreenComponentState();
  }
}
