package nz.piromania.ui.screen;

import java.util.ArrayList;
import nz.piromania.App;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.AlignmentComponent;
import nz.piromania.reactiveengine.component.ButtonComponent;
import nz.piromania.reactiveengine.component.ContainerComponentBuilder;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.ResourceImageComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.Alignment;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.BoxSize;
import nz.piromania.reactiveengine.util.EdgeInsets;
import nz.piromania.ui.AppTextStyles;
import nz.piromania.ui.Screen;
import nz.piromania.ui.component.FadeComponent;
import nz.piromania.ui.component.PageWrapperComponent;
import nz.piromania.ui.component.PageWrapperTitleBarWithBackButtonComponent;

/**
 * ComponentState for {@link ShipScreenComponent}.
 */
public class ShipScreenComponentState extends ComponentState<ShipScreenComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var app = Provider.of(App.class, context);
    final var gameEnvironment = Provider.of(GameEnvironment.class, context);
    final var ship = gameEnvironment.getShip();

    return ArrayListBuilder.build(
        new PageWrapperComponent(
            "Islands/Island.jpg",

            new PageWrapperTitleBarWithBackButtonComponent(
                () -> {
                  gameEnvironment.getScreenManager().show(Screen.ISLAND_OVERVIEW);
                },
                new TextComponent(
                    AppTextStyles.headingStyleWhite,
                    ship.getShipName()
                )
            ),

            new ListComponent(
                Axis.VERTICAL,
                new ContainerComponentBuilder()
                    .setPadding(new EdgeInsets(1, App.getScreenWidth() / 2 - 10))
                    .build(),

                new ListComponent(
                    Axis.HORIZONTAL,
                    new ContainerComponentBuilder()
                        .setPadding(new EdgeInsets(100, 1))
                        .build(),

                    new FadeComponent(
                        new ListComponent(
                            Axis.VERTICAL,

                            new PaddingComponent(
                                new EdgeInsets(10, 10),
                                new TextComponent(
                                    TextStyle.plainWhite,
                                    String.format(
                                        "Wage Cost: %s / day",
                                        ship.getWages()
                                    )
                                )
                            ),
                            new PaddingComponent(
                                new EdgeInsets(10, 10),
                                new TextComponent(
                                    TextStyle.plainWhite,
                                    String.format(
                                        "Crew: %d crew members",
                                        ship.getCrewCount()
                                    )
                                )
                            ),
                            new PaddingComponent(
                                new EdgeInsets(10, 10),
                                new TextComponent(
                                    TextStyle.plainWhite,
                                    String.format(
                                        "Health: %d / %d",
                                        ship.getCurrentHitpoints(),
                                        ship.getShipStats().getMaxHitpoints()
                                    )
                                )
                            ),
                            new PaddingComponent(
                                new EdgeInsets(10, 10),
                                new TextComponent(
                                    TextStyle.plainWhite,
                                    String.format(
                                        "Repair cost: %s",
                                        ship.getDamageCost()
                                    )
                                )
                            )
                        )
                    ),

                    new ExpandedComponent(),

                    new AlignmentComponent(
                        Alignment.TOP_LEFT,
                        new ResourceImageComponent(
                            new BoxSize(450, 350),
                            ship.getImagePath()
                        )
                    )
                ),

                new ListComponent(
                    Axis.HORIZONTAL,
                    new ContainerComponentBuilder()
                        .setPadding(new EdgeInsets(50, 1))
                        .build(),

                    new PaddingComponent(
                        new EdgeInsets(30, 30),
                        new ButtonComponent(
                            () -> {
                              gameEnvironment.getScreenManager().show(Screen.SHIP_INVENTORY);
                            },
                            new TextComponent("Cargo")
                        )
                    ),

                    new ExpandedComponent(),

                    new PaddingComponent(
                        new EdgeInsets(30, 30),
                        new ButtonComponent(
                            () -> {
                              gameEnvironment.getScreenManager().show(Screen.SHIP_UPGRADE);
                            },
                            new TextComponent("Upgrades")
                        )
                    )
                )
            ),

            null
        )
    );
  }
}
