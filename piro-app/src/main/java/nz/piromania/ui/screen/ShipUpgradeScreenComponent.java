package nz.piromania.ui.screen;

import nz.piromania.reactiveengine.Component;

/**
 * Screen in which the player can manage the upgrades that are applied to their ship.
 */
public class ShipUpgradeScreenComponent extends Component {

  @Override
  protected ShipUpgradeScreenComponentState createState() {
    return new ShipUpgradeScreenComponentState();
  }
}
