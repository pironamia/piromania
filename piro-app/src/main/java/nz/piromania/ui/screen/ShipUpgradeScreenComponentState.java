package nz.piromania.ui.screen;

import java.awt.Color;
import java.util.ArrayList;
import java.util.function.Consumer;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.gameenvironment.items.UpgradeItem;
import nz.piromania.gameenvironment.items.UpgradeType;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.ContainerComponentBuilder;
import nz.piromania.reactiveengine.component.ExpandedComponent;
import nz.piromania.reactiveengine.component.list.FlexibleComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.BoxSize;
import nz.piromania.ui.AppTextStyles;
import nz.piromania.ui.Screen;
import nz.piromania.ui.component.PageWrapperComponent;
import nz.piromania.ui.component.PageWrapperTitleBarWithBackButtonComponent;
import nz.piromania.ui.component.shipupgrade.ShipUpgradeScreenAvailableUpgradesComponent;
import nz.piromania.ui.component.shipupgrade.ShipUpgradeScreenUpgradeTypeColumnComponent;
import nz.piromania.util.DualConsumer;
import nz.piromania.util.TextSpanWrapper;

/**
 * ComponentState for {@link ShipUpgradeScreenComponent}.
 */
public class ShipUpgradeScreenComponentState extends ComponentState<ShipUpgradeScreenComponent> {

  private RuntimeException lastError;
  private UpgradeItem pendingUpgrade;

  private void setLastError(RuntimeException error) {
    setState(() -> {
      lastError = error;
    });
  }

  private void setPendingUpgrade(UpgradeItem item) {
    setState(() -> {
      lastError = null;
      pendingUpgrade = item;
    });
  }

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final var gameEnv = Provider.of(GameEnvironment.class, context);

    final DualConsumer<UpgradeItem, UpgradeType> onApplyDowngrade = (item, type) -> {
      setPendingUpgrade(null);

      try {
        gameEnv.getShip().removeUpgrade(item, type);
        setLastError(null);
      } catch (IllegalArgumentException e) {
        setLastError(e);
      }
    };

    final Consumer<UpgradeItem> onApplyUpgrade = (item) -> {

      if (item.getUpgradeTypes().size() == 1) {
        setPendingUpgrade(null);
        try {
          gameEnv.getShip().applyUpgrade(item, item.getUpgradeTypes().get(0));
          setLastError(null);
        } catch (IllegalArgumentException e) {
          setLastError(e);
        }
      } else {
        setPendingUpgrade(item);
      }
    };

    final Consumer<UpgradeType> onUpgradeTypeSelected = (type) -> {
      if (pendingUpgrade != null) {
        try {
          gameEnv.getShip().applyUpgrade(pendingUpgrade, type);
          setLastError(null);
          setPendingUpgrade(null);
        } catch (IllegalArgumentException e) {
          setLastError(e);
        }
      }
    };

    final ArrayList<Component> columns = new ArrayList<>();

    for (final var type : UpgradeType.values()) {
      columns.add(
          new FlexibleComponent(
              1,
              new ShipUpgradeScreenUpgradeTypeColumnComponent(
                  type,
                  onApplyDowngrade,
                  pendingUpgrade != null && pendingUpgrade.getUpgradeTypes().contains(type),
                  onUpgradeTypeSelected
              )
          )
      );
    }

    final var errorRows = new ArrayList<Component>();
    final var wrappedRows = new TextSpanWrapper(
        ArrayListBuilder.build(
            new TextSpan(
                AppTextStyles.errorStyle.copyWith(null, null, null, 20, Color.BLUE),
                (lastError == null) ? "" : String.format(
                    "Error: %s",
                    lastError.getMessage()
                )
            )
        ),
        78
    ).wrap();
    for (final var row : wrappedRows) {
      errorRows.add(new TextComponent(row));
    }

    return ArrayListBuilder.build(
        new PageWrapperComponent(
            "Islands/Island.jpg",
            new PageWrapperTitleBarWithBackButtonComponent(
                () -> {
                  gameEnv.getScreenManager().show(Screen.SHIP_OVERVIEW);
                },
                new TextComponent(
                    AppTextStyles.headingStyleWhite,
                    "Upgrade Ship"
                )
            ),

            new ListComponent(
                Axis.VERTICAL,

                new ContainerComponentBuilder()
                    .setSize(new BoxSize(null, 25))
                    .build(
                        new ListComponent(
                            Axis.VERTICAL,
                            errorRows
                        )
                    ),

                new ExpandedComponent(
                    new ListComponent(
                        Axis.HORIZONTAL,
                        columns
                    )
                )
            ),

            new ShipUpgradeScreenAvailableUpgradesComponent(onApplyUpgrade),
            100
        )
    );
  }
}
