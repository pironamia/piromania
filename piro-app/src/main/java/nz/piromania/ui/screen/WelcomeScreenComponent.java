package nz.piromania.ui.screen;

import nz.piromania.reactiveengine.Component;

/**
 * The start screen of the application.
 *
 * <p>
 * Allows the user to start a new game or quit.
 * </p>
 */
public class WelcomeScreenComponent extends Component {

  @Override
  protected WelcomeScreenComponentState createState() {
    return new WelcomeScreenComponentState();
  }
}
