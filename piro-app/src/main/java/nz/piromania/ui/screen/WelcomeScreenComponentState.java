package nz.piromania.ui.screen;

import java.awt.Color;
import java.util.ArrayList;
import nz.piromania.App;
import nz.piromania.gameenvironment.GameEnvironment;
import nz.piromania.reactiveengine.BuildContext;
import nz.piromania.reactiveengine.Component;
import nz.piromania.reactiveengine.ComponentState;
import nz.piromania.reactiveengine.component.AlignmentComponent;
import nz.piromania.reactiveengine.component.ButtonComponent;
import nz.piromania.reactiveengine.component.CentreComponent;
import nz.piromania.reactiveengine.component.ContainerComponent;
import nz.piromania.reactiveengine.component.PaddingComponent;
import nz.piromania.reactiveengine.component.StackComponent;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.provider.Provider;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.Alignment;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import nz.piromania.reactiveengine.util.EdgeInsets;
import nz.piromania.ui.AppTextStyles;
import nz.piromania.ui.Screen;

/**
 * ComponentState for {@link WelcomeScreenComponent}.
 */
public class WelcomeScreenComponentState extends ComponentState<WelcomeScreenComponent> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final GameEnvironment gameEnvironment = Provider.of(GameEnvironment.class, context);
    final App app = Provider.of(App.class, context);

    return ArrayListBuilder.build(
        new StackComponent(
            new CentreComponent(
                new ListComponent(
                    Axis.VERTICAL,
                    new AlignmentComponent(
                        Alignment.CENTRE,
                        new PaddingComponent(
                            new EdgeInsets(20, 0),
                            new TextComponent(AppTextStyles.headingStyle, "Piromania")
                        )
                    ),
                    new AlignmentComponent(
                        Alignment.CENTRE,
                        new PaddingComponent(
                            new EdgeInsets(8, 0),
                            new ButtonComponent(
                                () -> {
                                  gameEnvironment.getScreenManager().show(
                                      Screen.PLAYER_DETAILS_1,
                                      null
                                  );
                                },
                                new TextComponent(TextStyle.plain, "New Adventure")
                            )
                        )
                    ),
                    new AlignmentComponent(
                        Alignment.CENTRE,
                        new PaddingComponent(
                            EdgeInsets.zero,
                            new ButtonComponent(
                                app::exit,
                                new TextSpan(TextStyle.plain, "Abandon Quest")
                            )
                        )
                    )

                )
            ),

            new ContainerComponent(
                null,
                Alignment.CENTRE,
                EdgeInsets.zero,
                Color.ORANGE,
                null
            )
        )
    );
  }
}
