package nz.piromania.util;

/**
 * An interface for a method that takes two parameters.
 */
@FunctionalInterface
public interface DualConsumer<A, B> {

  void accept(A a, B b);
}
