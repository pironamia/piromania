package nz.piromania.util;

import java.util.ArrayList;
import nz.piromania.reactiveengine.component.text.TextSpan;
import nz.piromania.reactiveengine.component.text.style.TextStyle;
import nz.piromania.reactiveengine.util.ArrayListBuilder;

/**
 * A utility class that takes a list of TextSpans, and wraps them to ensure that they stay within a
 * specified character limit per line, while retaining styles (even when a single TextSpan may need
 * to be wrapped across multiple lines).
 */
public class TextSpanWrapper {

  private boolean hasRun = false;

  private final ArrayList<TextSpan> children;
  private final int maxRowLength;

  private final String[] wrappedTextChars;
  private final ArrayList<ArrayList<TextSpan>> rows = new ArrayList<>();
  private ArrayList<TextSpan> currentRow = new ArrayList<>();

  private final StringBuilder currentTextBuilder = new StringBuilder();
  private TextStyle currentTextStyle = TextStyle.plain;

  /**
   * Constructs a new TextSpanWrapper.
   *
   * @param children     the TextSpans to wrap
   * @param maxRowLength the maximum number of characters to allow in a single line
   */
  public TextSpanWrapper(ArrayList<TextSpan> children, int maxRowLength) {
    this.children = children;
    this.maxRowLength = maxRowLength;

    final StringBuilder builder = new StringBuilder();
    for (final var child : children) {
      builder.append(child.getText());
    }

    wrappedTextChars = WordWrapUtility.wrap(
        builder.toString(),
        maxRowLength,
        "\n",
        true
    ).split("");
  }

  private int getNextSpanIndex(int currentIndex) {
    int nextIndex = currentIndex + 1;
    while (nextIndex < children.size() && children.get(nextIndex).getText().length() == 0) {
      nextIndex++;
    }
    return nextIndex;
  }

  private void flushCurrentSpan() {
    currentRow.add(new TextSpan(
        currentTextStyle.copyWith(null, null, null, null, null),
        currentTextBuilder.toString()
    ));

    currentTextBuilder.setLength(0);  // Clear the text buffer
  }

  /**
   * Executes the wrapping action on the <code>children</code>. Returning a list of lines of
   * TextSpans.
   */
  public ArrayList<ArrayList<TextSpan>> wrap() {
    if (hasRun) {
      throw new IllegalCallerException(
          "Cannot call wrap() more than once on the same TextSpanWrapper object.");
    }
    hasRun = true;

    if (children.size() == 0 || maxRowLength == 0) {
      return ArrayListBuilder.build();
    }

    int currentSpanIndex = getNextSpanIndex(-1);
    int charIndexInSpan = 0;
    int charIndexInText = 0;

    if (currentSpanIndex < children.size()) {
      currentTextStyle = children.get(currentSpanIndex).getStyle();
    }

    while (charIndexInText < wrappedTextChars.length && currentSpanIndex < children.size()) {
      final var span = children.get(currentSpanIndex);

      if (charIndexInSpan == span.getText().length()) {
        // We've run off the end of this text span.
        // Flush it and move to the next one.
        flushCurrentSpan();
        currentSpanIndex = getNextSpanIndex(currentSpanIndex);
        charIndexInSpan = 0;

        if (currentSpanIndex < children.size()) {
          currentTextStyle = children.get(currentSpanIndex).getStyle();
        }
      } else if (wrappedTextChars[charIndexInText].equals("\n")) {
        // Clear this line and start a new one.
        flushCurrentSpan();
        rows.add(currentRow);
        currentRow = new ArrayList<>();
        charIndexInText++;
      } else {
        // Base condition, nothing special to do so just consume the character
        currentTextBuilder.append(wrappedTextChars[charIndexInText]);
        charIndexInText++;
      }
    }

    if (currentTextBuilder.length() != 0) {
      flushCurrentSpan();
    }
    if (currentRow.size() != 0) {
      rows.add(currentRow);
    }

    return rows;
  }
}
