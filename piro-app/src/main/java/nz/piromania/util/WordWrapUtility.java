package nz.piromania.util;

import org.apache.commons.text.WordUtils;

/**
 * Extends the Apache word wrapping tools to add support for e.g. strings that already contain new
 * line characters.
 */
public class WordWrapUtility {

  /**
   * Wraps the given text so that it fits within the wrapLength. Respecting existing line breaks.
   */
  public static String wrap(String text, int wrapLength, String wrapString,
      boolean breakLongWords) {
    final String[] textParagraphs = text.split("\n");
    for (int i = 0; i < textParagraphs.length; i++) {
      textParagraphs[i] = WordUtils.wrap(
          textParagraphs[i],
          wrapLength,
          wrapString,
          breakLongWords
      );
    }

    return String.join(wrapString, textParagraphs);
  }
}
