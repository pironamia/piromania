# JSON

The JSON in these files need to follow a very specific format, in order for the JSON Manager to work.

Note: It is recommended for ANY JSON written, parse it through here first:

https://jsoneditoronline.org/beta/

## The JSON Format

The format itself is fairly understandable for all classes. In general, it will be an array of objects
which contain the data of the objects they're initializing.
That is,
```json
[
  {
    object1
  },
  {
    object2
  }
]
```
Note: empty lists are sometimes allowed.


All objects are guaranteed to have an ID tag, such as the following:
```JSON
id: #
```
The ID can be any number you would like. There may not be duplicate IDs however.

In terms of the data that the JSON should be loading, those are covered in JSON Class Specifics (See below).

WORD OF WARNING: All observable values (These should be denoted in this documentation) MUST be an object
with the value instead the object. For example:
```JSON
"hitpoint-max": {
  "val": 100
}
```
The val can be any JSON compatible value.

Here's an example using Currency (NOTE: Value must be in cents):
```JSON
currency: {
  "cents": 100
}
```
Note: If a tag is excluded, its value will default to null. This might cause some errors, so be wary.

## JSON General Example

```JSON
[
  {
    "id": 0,
    "name": "apple",
    "description": "an apple.",
    "image-path": null,
    "base-value": {
      "cents": 150
    },
    "type": "FOOD",
    "size": 1
  },
  {
    "id": 50,
    "name": "Supermassive cannons",
    "description": "Cannons guaranteed to pack a punch, these are three times the size of regular cannons and will decimate any pirates.\n§RAdds 200 £damage-potential£§Y, but removes 40 £cargo-capacity£ and makes you 20% slower.§!",
    "image-path": null,
    "base-value": {
      "cents": 25000
    },
    "type": "UPGRADE",
    "size": 60,
    "upgrade-types": [
      "WEAPONRY"
    ],
    "upgrade-stats": {
      "hitpoint-max": {
        "val": 0
      },
      "risk-multiplier": {
        "val": 0.5
      },
      "damage-potential": {
        "val": 200
      },
      "cargo-capacity": {
        "val": -40
      },
      "travel-multiplier": {
        "val": 0.8
      }
    },
    "additive-multipliers": true
  }
]
```

### JSON Item Specifics

The following data is required for an Item:
```JSON
id: integer,
name: "String",
description: "String",
image-path: "String",
"base-value": {
  "cents": integer
},
"type": ItemType,
"size": integer
```
#### Valid ItemTypes are one of the following:

```c
FOOD
DRINK
GEM
UPGRADE
```
If the Item is of type UPGRADE, then the following JSON should be added:
```JSON
"upgrade-types": [
  UPGRADETYPE,
  UPGRADETYPE,
  (for as many or as few upgrade types)
],
"upgrade-stats": {
  "hitpoint-max": {
    "val": integer
  },
  "risk-multiplier": {
    "val": float
  },
  "damage-potential": {
    "val": integer
  },
  "cargo-capacity": {
    "val": integer
  },
  "travel-multiplier": {
    "val": float
  }
},
"additive-multipliers": boolean
```
NOTE: If one of the stats values is 0, still make sure to include it or else the game will break.

#### On additives

Additive multipliers describes the multipliers of the stats. If they are additive, or `"additive-multipliers": true`, then 1 will be subtracted off of them, and then they will be added to existing stats should the be chosen to be added against something. For instance, if a ship had a 1 risk multiplier, and you select an additive multiplier with a risk multiplier of 1.2, then the new ship risk multiplier will be 1.2. If you add it again, then it will be 1.4.

If additive multipliers is false, or `"additive-multipliers": false`, then the value parsed will be multiplied straight to the existing value if stats are chosen to add together. For instance, if you had a ship with a risk multiplier of 1, and an item with a risk multiplier of 1.2, then the new risk multiplier would be 1.2. If you add it again however, then it would be 1.44.
 
#### Valid UpgradeTypes:

```c
HULL
STEERING
WEAPONRY
CARGO
SAIL
```
These are all the slots this particular UpgradeItem can be placed into on a ship.

### JSON Event Specifics

#### The file system

Events are split into 3 files currently: Bad events, neutral events, and good events. When the ship encounters a random event, it will determine what kind of event (good, neutral, bad), and call from a random event in the respective file.

When defining an event, this is the general format an event should follow:
```json
"id": integer,
"title": "String",
"description": "String",
"image": "String"
```
That's the defining features of an event. To add choices to the event, the following format should be added:
```json
"choices": [
  {
    Choice
  }
]
```
#### Defining a choice

Should be as following:
```json
"title": "String",
"tooltip": {
  "val": "String"
}
```
To add actions to a choice, the following format should be added:
```json
"actions": [
  {
    Action
  }
]
```
#### Defining an action

Should be as following:
```json
"action-type": "nz.piromania.gameenvironment.eventsandchoices.choiceactions.ACTION",
"DATA": {
  "ACTIONDATA": Value
}
```
Note: ACTION & ACTIONDATA should be changed according to whatever action you are implementing. For example, an action which damages the ship would be as such:
```json
"action-type": "nz.piromania.gameenvironment.eventsandchoices.choiceactions.ModifyShipAction",
"DATA": {
  "hitpointsToChange": -1
}
```
The value can be -1, as it will be changed later.

### JSON Personality specifics

Personalities are currently assigned to Shops, as a method of getting (unused) greeting and leaving dialogue, and generating prices. Syntax follows as:
```json
"id": integer,
"name": "String",
"greeting-dialogues": [
  "String",
  (For as many or as few Strings as you would like)
],
"leaving-dialogues": [
  "String",
  (For as many or as few Strings as you would like)
]
```
#### Then you can add optional values for:

The modifier to be applied to items that are of the right type
```json
"right-type-modifier": float
```
And the modifier to be applied to items that are of the right type when being sold
```json
"bartering-chance": float
```
#### Note: Higher values in general indicate more fluctuating prices.

#### Also note: If you do not add these, they will instead be randomly generated.

### JSON Stores specifics

The syntax of stores is as follows:
```json
"id": integer,
"name": "String",
"buying-type": ITEMTYPE,
"selling-type": ITEMTYPE
```
You may also tack a `Personality` and an `Inventory` on as well if you wish. If not, they will be randomly generated.

### JSON Ships specifics

Ship syntax is as follows:
```json
"id": integer,
"name": "String",
"description": "String",
"image": "String",
"crew-count": {
  "val": integer
},
"stats": {
  "hitpoint-max": {
    "val": integer
  },
  "risk-multiplier": {
    "val": float
  },
  "damage-potential": {
    "val": integer
  },
  "cargo-capacity": {
    "val": integer
  },
  "travel-multiplier": {
    "val": float
  }
},
"base-travel-speed": integer,
"upgrade-slots": {
  "val": {
    "HULL": integer,
    "STEERING": integer,
    "WEAPONRY": integer,
    "CARGO": integer,
    "SAIL": integer
  }
}
```
An inventory may also be tacked on.

For the upgrade slots, they consist of mapping UpgradeTypes with a value, representing how many slots the ship has for that certain UpgradeType.

### JSON StringStorage Specifics

StringStorages are anything that contains an array list of strings. This is used for Island Names and Island Descriptions currently.
They follow a very simple syntax:
```json
"strings": [
  "String",
  (For as many or as few Strings as you would like)
]
```
### JSON NameGeneratorStorage Specifics

This class is used to put a list of prefixes with a list of a list of postfixes together. Handy when the prefix is the same, but postfix is variable. Currently used for ShopNames.
They follow a similar syntax to StringStorages:
```json
"prefix": [
  "String",
  (For as many or as few Strings as you would like)
],
"postfixes": {
  [
    "String",
    (For as many or as few Strings as you would like)
  ],
  [
    "String",
    (For as many or as few Strings as you would like)
  ]
  (For as many postfix types you would like)
}
```