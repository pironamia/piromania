package nz.piromania;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class CurrencyTest {

  @Test
  public void initializationTests() {
    Currency fiftyCents = new Currency(50);
    Currency oneDollarInCents = new Currency(100);
    Currency oneDollar = new Currency(1,0);
    Currency aLot = new Currency(100000000);
    Currency aLotOfDollars = new Currency(100000000, 0);
    
    assertEquals(50, fiftyCents.getCents());
    assertEquals(100, oneDollarInCents.getCents());
    assertEquals(100, oneDollar.getCents());
    assertEquals(true, oneDollarInCents.equals(oneDollar));
    assertNotSame(oneDollarInCents, oneDollar);
    assertEquals(100000000, aLot.getCents());
    assertEquals(100000000*100, aLotOfDollars.getCents());
  }
  
  @Test
  public void currencyArithmeticTests() {
    Currency changingCurrency = new Currency(1,0);
    //100 cents + 100 cents == 200 cents
    changingCurrency = changingCurrency.add(new Currency(1,0));
    assertEquals(200, changingCurrency.getCents());
    //200 cents - 50 cents == 150 cents
    changingCurrency = changingCurrency.subtract(new Currency(50));
    assertEquals(150, changingCurrency.getCents());
    //150 cents * 2 cents == 300 cents
    changingCurrency = changingCurrency.multiply(new Currency(2));
    assertEquals(300, changingCurrency.getCents());
    //300 cents / 3 cents == 100 cents
    changingCurrency = changingCurrency.divide(new Currency(3));
    assertEquals(100, changingCurrency.getCents());
  }
  
  @Test
  public void errorHandlingTests() {
    try {
      Currency subtractionNegative = new Currency(1).subtract(new Currency(100));
      fail("Exception not thrown");
    } catch(IllegalArgumentException e) {}
    try {
      Currency negative = new Currency(-1);
      fail("Exception not thrown");
    } catch(IllegalArgumentException e) {}
    try {
      Currency negativeDollars = new Currency(-1, 0);
      fail("Exception not thrown");
    } catch(IllegalArgumentException e) {}
    try {
      Currency divideByZero = new Currency(100).divide(new Currency(0));
      fail("Exception not thrown");
    } catch(IllegalArgumentException e) {}
  }
}
