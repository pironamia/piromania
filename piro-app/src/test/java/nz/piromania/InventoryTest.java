package nz.piromania;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import nz.piromania.Currency;
import nz.piromania.Inventory;
import nz.piromania.Stats;
import nz.piromania.gameenvironment.items.Item;
import nz.piromania.gameenvironment.items.ItemType;
import nz.piromania.gameenvironment.items.UpgradeItem;
import nz.piromania.gameenvironment.items.UpgradeType;

class InventoryTest {

  @Test
  public void InventoryInitializationTests() {
    Inventory inv1 = new Inventory();
    Inventory inv2 = new Inventory(60);
    Item apple = new Item(0, "apple", "an apple", null, new Currency(20), ItemType.FOOD, 2, new Currency(30));
    Inventory inv3 = new Inventory(60, new ArrayList<Item>(Arrays.asList(apple)));
    ArrayList<Inventory> invs = new ArrayList<Inventory>(Arrays.asList(inv2, inv3));
    Inventory inv4 = new Inventory(new ArrayList<Item>(Arrays.asList(apple)));
  }
  
  @Test
  public void InventoryAddingTests() {
    //Initialize inventory
    Inventory inv1 = new Inventory(60);
    Item apple = new Item(0, "apple", "an apple", null, new Currency(20), ItemType.FOOD, 2, new Currency(30));
    //Initialize inventory with one apple
    Inventory inv2 = new Inventory(60, new ArrayList<Item>(Arrays.asList(apple)));
    //Fill both with apples
    while (inv1.addItem(apple)) {}
    assertEquals(new Currency(6,00).getCents(), inv1.getWorth().getCents());
    while (inv2.addItem(apple)) {}
    assertEquals(new Currency(6,20).getCents(), inv2.getWorth().getCents());
  }
  
  @Test
  public void InventorySortingTests() {
    Item apple = new Item(0, "apple", "an apple", null, new Currency(20), ItemType.FOOD, 2, new Currency(30));
    Item banana = new Item(1, "banana", "a banana", null, new Currency(30), ItemType.FOOD, 3, new Currency(50));
    Item water = new Item(2, "water", "just water", null, new Currency(25), ItemType.DRINK, 1, new Currency(30));
    UpgradeItem bananaSails = new UpgradeItem(3, "banana sails", "run of the mill sails clearly", null, 
        new Currency(20,00), ItemType.UPGRADE, 20, new Currency(25,00), new ArrayList<UpgradeType>(Arrays.asList(UpgradeType.SAIL)),
            new Stats(10, 1.2f, -10, 0, 1.2f), true);
    Inventory inv = new Inventory(60, new ArrayList<Item>(Arrays.asList(apple, banana, water, bananaSails)));
    assertEquals(new ArrayList<Item>(Arrays.asList(apple, banana, water, bananaSails)), inv.getItems());
    inv.sortItems("base-value", true);
    assertEquals(new ArrayList<Item>(Arrays.asList(bananaSails, banana, water, apple)), inv.getItems());
    inv.sortItems("size", true);
    assertEquals(new ArrayList<Item>(Arrays.asList(bananaSails, banana, apple, water)), inv.getItems());
    inv.sortItems("name");
    assertEquals(new ArrayList<Item>(Arrays.asList(apple, banana, bananaSails, water)), inv.getItems());
    inv.sortItems("type");
    assertEquals(new ArrayList<Item>(Arrays.asList(water, apple, banana, bananaSails)), inv.getItems());
    inv.sortItems("id", true);
    assertEquals(new ArrayList<Item>(Arrays.asList(bananaSails, water, banana, apple)), inv.getItems());
  }
  
  @Test
  public void InventoryCapacityTests() {
    Item apple = new Item(0, "apple", "an apple", null, new Currency(20), ItemType.FOOD, 2, new Currency(30));
    UpgradeItem bananaSails = new UpgradeItem(3, "banana sails", "run of the mill sails clearly", null, 
        new Currency(20,00), ItemType.UPGRADE, 20, new Currency(25,00), new ArrayList<UpgradeType>(Arrays.asList(UpgradeType.SAIL)),
            new Stats(10, 1.2f, -10, 0, 1.2f), true);
    Inventory inv = new Inventory(10);
    inv.addItem(bananaSails);
    assertTrue(inv.getItems().isEmpty());
    inv.changeCapacity(10);
    inv.addItem(bananaSails);
    assertEquals(0, inv.getCapacity());
    inv.changeCapacity(-10);
    assertEquals(0, inv.getCapacity());
  }
}
