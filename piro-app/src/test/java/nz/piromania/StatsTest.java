package nz.piromania;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import nz.piromania.Stats;

class StatsTest {

  private static Stats stats;
  
  @BeforeAll
  public static void beforeAll() {
    stats = new Stats(1,2,3,4,5);
  }
  
  @Test
  public void statsCheck() {
    assertEquals(1, stats.getMaxHitpoints());
    assertEquals(2, stats.getRiskMultiplier());
    assertEquals(3, stats.getDamagePotential());
    assertEquals(4, stats.getCargoCapacity());
    assertEquals(5, stats.getTravelMultiplier());
    stats.changeStats(new Stats(1,2,3,0,-5), true);
    assertEquals(2, stats.getMaxHitpoints());
    assertEquals(3, stats.getRiskMultiplier());
    assertEquals(6, stats.getDamagePotential());
    assertEquals(4, stats.getCargoCapacity());
    assertEquals(-1, stats.getTravelMultiplier());
    stats.changeStats(new Stats(1,2,3,0,-5), false);
    assertEquals(3, stats.getMaxHitpoints());
    assertEquals(6, stats.getRiskMultiplier());
    assertEquals(4, stats.getCargoCapacity());
    assertEquals(5, stats.getTravelMultiplier());
  }
  

}
