package nz.piromania;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import nz.piromania.gameenvironment.ships.Ship;

class TextFormatterTest {

  //TODO: Once Ship is implemented, use that to test too
  private final Ship ship = new Ship(0, "testship", "testdescription", null, 10, new Stats(100, 1.1f, 60, 80, 1.2f), 70, null);
  private final TextFormatter textFormatterAnsi = new TextFormatter(true, ship);
  private final TextFormatter textFormatterNotAnsi = new TextFormatter(false, ship);
  
  @Test
  public void testTextFormatter() {
    //Test color codes are working correctly with ANSI
    assertEquals("\u001B[30m", textFormatterAnsi.formatText("§_"));
    //Test color codes are working correctly with not ANSI
    assertEquals("", textFormatterNotAnsi.formatText("§_"));
    //Test $variables$ and $variables|\d*$
    String test1 = "$damage-potential$";
    assertEquals("60", textFormatterAnsi.formatText(test1));
    String test2 = "$damage-potential|2$";
    assertEquals("60.00", textFormatterAnsi.formatText(test2));
    String test3 = "$damage-potential$ text $damage-potential|3$";
    assertEquals("60 text 60.000", textFormatterAnsi.formatText(test3));
    String test4 = "$damage-potential|4$ text $damage-potential$";
    assertEquals("60.0000 text 60", textFormatterAnsi.formatText(test4));
    //Make sure it works with different variables
    String test5 = "$cargo-capacity|2$ + $damage-potential|8$";
    assertEquals("80.00 + 60.00000000", textFormatterAnsi.formatText(test5));
    //TODO: Replace once wage/damage costs are in.
    String test6 = "$wages|4$$damage-cost|2$$wages$";
    assertEquals("$50.0000$0.00$50", textFormatterAnsi.formatText(test6));
  }
}
