package nz.piromania.gameenvironment;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Map;
import org.junit.jupiter.api.Test;
import nz.piromania.Stats;
import nz.piromania.gameenvironment.items.UpgradeType;
import nz.piromania.gameenvironment.ships.Ship;

class GameEnvironmentTest {

  @Test
  void gameEnvironmentTest() {
    try {
      GameEnvironment gameEnvironment = new GameEnvironment("");
      gameEnvironment.startGame("TraderName", 50, 
        new Ship(0, "TestyMcTestface", null, null, 20, new Stats(100, 1, 250, 60, 1), 600, Map.of(
          UpgradeType.HULL, 2,
          UpgradeType.STEERING, 1,
          UpgradeType.WEAPONRY, 3,
          UpgradeType.CARGO, 1,
          UpgradeType.SAIL, 2
        ))
      );
      //Hop aboard a random route.
      //System.out.println(gameEnvironment.getCurrentRoutes().get(0).get(0));
      //System.out.println(gameEnvironment.takeRoute(gameEnvironment.getCurrentRoutes().get(0).get(0)));
    } catch (Exception e) { fail("Exception caught: " + e); }
  }

}
