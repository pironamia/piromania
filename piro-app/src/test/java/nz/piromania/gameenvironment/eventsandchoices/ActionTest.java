package nz.piromania.gameenvironment.eventsandchoices;

import org.junit.jupiter.api.Test;

class ActionTest {

  @Test
  void actionTests() {
    //Very simple test, makes sure an action can be created and run.
    TestingAction testingAction = new TestingAction();
    testingAction.runAction();
  }

}
