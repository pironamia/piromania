package nz.piromania.gameenvironment.eventsandchoices;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import nz.piromania.gameenvironment.eventsandchoices.Choice;

class ChoiceTest {

  @Test
  void choiceTests() {
    //Very simple test, assert that a choice can in fact be made correctly.
    Choice choice = new Choice("TestTitle", "TestTooltip", new ArrayList<>(Arrays.asList(new TestingAction())));
    assertEquals("TestTitle", choice.getTitle());
    choice.setTooltip("New Tooltip");
    assertEquals("New Tooltip", choice.getTooltip());
  }

}
