package nz.piromania.gameenvironment.eventsandchoices;

import static org.junit.jupiter.api.Assertions.*;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import nz.piromania.JsonManager;
import nz.piromania.gameenvironment.eventsandchoices.choiceactions.ActionSerializer;
import nz.piromania.gameenvironment.eventsandchoices.choiceactions.ChoiceAction;
import nz.piromania.gameenvironment.eventsandchoices.choiceactions.ModifyShipAction;

class EventManagerTest {

  @Test
  public void eventManagerGeneralTest() {
    try{
      JsonManager<Event> eventManager = JsonManager.createGeneric(Event.class);
      eventManager.generate("BadEvents.json");
      //Use to see event loaded. System.out.println(eventManager.getEvent(0));
      assertEquals("Unfortunate Weather", ((Event) eventManager.getData(2001)).getTitle());
    } catch (FileNotFoundException e) {fail("Exception thrown\n" + e);}
  }
  
  @Test
  public void eventManagerSerializingTest() {
    int val = 18;
    //Describes the event
    Event event = new Event(2001, "Unfortunate Weather", 
        "The seas surge and swell as a mighty storm brews. You're right in the middle of it, best to anticipate the worst...",
        "src/main/resources/events/StormEvent.jpg",
        new ArrayList<Choice>(Arrays.asList(
            new Choice("Brace!", "Lose " + val + " hitpoints.", new ArrayList<ChoiceAction>(Arrays.asList(
                new ModifyShipAction(val)
            )))
        ))
    );
    GsonBuilder builder = new GsonBuilder();
    builder.registerTypeAdapter(ChoiceAction.class, new ActionSerializer());
    Gson gson = builder.create();
    builder.excludeFieldsWithoutExposeAnnotation();
    try {
      JsonManager<Event> eventManager = JsonManager.createGeneric(Event.class);
      eventManager.generate("BadEvents.json");
      ((Event) eventManager.getData(2001)).getChoices().get(0).setTooltip("Lose " + val + " hitpoints.");
      ((Event) eventManager.getData(2001)).getChoices().get(0).getActions().get(0).setData("hitpointsToChange", 18, null);
      //Check toString() as otherwise it'd be comparing objects, which would not work.
      assertEquals(event.toString(), ((Event) eventManager.getData(2001)).toString());
    } catch (Exception e) {fail("Exception thrown\n" + e);}
  }
}