package nz.piromania.gameenvironment.eventsandchoices;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import nz.piromania.gameenvironment.eventsandchoices.Choice;
import nz.piromania.gameenvironment.eventsandchoices.Event;

class EventTest {

  @Test
  public void eventTests() {
    Choice choice = new Choice("TestTitle", "TestTooltip", new ArrayList<>(Arrays.asList(new TestingAction())));
    Event event = new Event(0, "TestTitle", "TestDescription", new ArrayList<>(Arrays.asList(choice)));
    Event eventWithImage = new Event(1, "TestTitle", "TestDescription", "TestImagePath", new ArrayList<>(Arrays.asList(choice)));
    assertEquals(event.getChoices().get(0), choice);
    assertEquals(event.getTitle(), eventWithImage.getTitle());
    assertEquals("TestImagePath", eventWithImage.getImagePath());
  }

}
