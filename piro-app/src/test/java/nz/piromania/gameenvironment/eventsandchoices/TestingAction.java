package nz.piromania.gameenvironment.eventsandchoices;

import nz.piromania.gameenvironment.eventsandchoices.choiceactions.ChoiceAction;

public class TestingAction<T> implements ChoiceAction {

  @Override
  public T runAction() {
    return null;
  }

  @Override
  public void setData(String variableToChange, Object dataToSet, Object dataTarget) {
    return;
  }

}
