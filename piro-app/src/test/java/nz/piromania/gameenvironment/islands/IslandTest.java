package nz.piromania.gameenvironment.islands;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import nz.piromania.JsonManager;
import nz.piromania.Stats;
import nz.piromania.StringStorage;
import nz.piromania.gameenvironment.islands.Island;
import nz.piromania.gameenvironment.ships.Ship;

class IslandTest {

  @Test
  public void IslandTests() {
    //Generate 5 islands 100 times. It should be essentially
    //impossible that the islands do not generate properly.
    for (int j = 0; j < 100; j++) {
      ArrayList<Island> islands = new ArrayList<Island>();
      for (int i = 0; i < 5; i++) {
        Island island = new Island(null, null, null, null, islands);
        islands.add(island);
      }
    }
    try {
      JsonManager<StringStorage> islandNameGenerator = JsonManager.createGeneric(StringStorage.class);
      islandNameGenerator.generate("IslandNames.json");
      JsonManager<StringStorage> islandDescGenerator = JsonManager.createGeneric(StringStorage.class);
      islandDescGenerator.generate("IslandDescriptions.json");
      for (int i=0;i<10;i++) {
        //System.out.println(new Island(islandNameGenerator.getData(0).generateRandomString(), islandDescGenerator.getData(0).generateRandomString(), null, null, i, i));
        assertNotNull(new Island(islandNameGenerator.getData(0).generateRandomString(), islandDescGenerator.getData(0).generateRandomString(), null, null, i, i).getName());
        assertNotNull(new Island(islandNameGenerator.getData(0).generateRandomString(), islandDescGenerator.getData(0).generateRandomString(), null, null, i, i).getDescription());
      }
    } catch (Exception e) { fail("Exception caught: " + e); }    
    //Try generate routes for some islands.
    Island island1 = new Island("Island1", "Island1Desc", null, null, new ArrayList<Island>());
    Island island2 = new Island("Island2", "Island2Desc", null, null, 0, 0);
    assertNotNull(
        island1.generateRoutes(island2, new Ship(0, null, null, null, 0, null, new Stats(0, 1, 0, 0, 1), 100, null), 1)
    );
    assertTrue(
        island2.generateRoutes(island1, new Ship(0, null, null, null, 0, null, new Stats(0, 1, 0, 0, 1), 100, null), 100).size() > 0
    );
    
  }
}
