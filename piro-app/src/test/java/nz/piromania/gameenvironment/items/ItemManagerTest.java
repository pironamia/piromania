package nz.piromania.gameenvironment.items;

import static org.junit.jupiter.api.Assertions.*;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import nz.piromania.Currency;
import nz.piromania.JsonManager;
import nz.piromania.gameenvironment.items.Item;
import nz.piromania.gameenvironment.items.ItemData;
import nz.piromania.gameenvironment.items.ItemType;
import nz.piromania.gameenvironment.items.UpgradeItem;
import nz.piromania.gameenvironment.items.UpgradeType;

class ItemManagerTest {

  @Test
  public void itemManagerTests() {
    try{
      JsonManager<ItemData> itemManager = JsonManager.createGeneric(ItemData.class);
      itemManager.generate("Items.json");
      //Uncomment to see item loaded from TestItems.json. System.out.println(itemManager.getItemData(0));
      //Check that the name of the item is indeed a banana, and as such it has loaded properly.
      assertEquals("banana", ((ItemData) itemManager.getData(0)).getItemName());
      //Now we've verified it's a banana, try instantiate it.
      Item banana = ((ItemData) itemManager.getData(0)).createItemInstance(new Currency(200));
      //Check ItemInstantiation works. Also see ItemTest.
      assertEquals(ItemType.FOOD, banana.getItemType());
      //Check UpgradeItems work too in the itemManager, as well as custom IDs.
      UpgradeItem superCannons = ((ItemData) itemManager.getData(50)).createUpgradeInstance();
      assertEquals(new ArrayList<UpgradeType>(Arrays.asList(UpgradeType.WEAPONRY)), superCannons.getUpgradeTypes());
      
      ItemData data = (ItemData) itemManager.getRandomData();
      assertNotNull(data);
    } catch (FileNotFoundException e) {fail("Exception thrown\n" + e);}
  }

}
