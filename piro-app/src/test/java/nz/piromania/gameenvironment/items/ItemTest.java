package nz.piromania.gameenvironment.items;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;
import nz.piromania.Currency;
import nz.piromania.gameenvironment.items.Item;
import nz.piromania.gameenvironment.items.ItemData;
import nz.piromania.gameenvironment.items.ItemType;

class ItemTest {
  private static final ItemData banana = new Item(1, "banana", "a banana", null, new Currency(2,0), ItemType.FOOD, 1);
  private static Item banana1, banana2;
  
  @BeforeAll
  public static void beforeAll() {
    Currency cost = new Currency(2,50);
    //Creates an instance of the banana purchased for nothing
    banana1 = banana.createItemInstance();
    //Creates an instance of the banana purchased for $2.50
    banana2 = banana.createItemInstance(cost);
  }
  
  @Test
  public void checkItemsInstantiated() {
    assertEquals("banana", banana1.getItemName());
    assertNotSame(banana1, banana2); 
  }
  
  @Test
  public void modifyItemsCheck() {
    banana1.setPurchasedValue(new Currency(2));
    assertEquals(2, banana1.getPurchasedValue().getCents());
    banana1.setSoldValue(new Currency(4));
    assertEquals(4, banana1.getSoldValue().getCents());
  }
  
  @Test
  public void checkInheritence() {
    assertTrue(banana1 instanceof ItemData);
    assertTrue(banana1 instanceof Item);
  }
  
  @Test
  public void checkItemDetails() {
    assertEquals(1, banana1.getId());
    assertEquals("banana", banana1.getItemName());
    assertEquals("a banana", banana1.getItemDescription());
    assertEquals(250, banana2.getPurchasedValue().getCents());
    assertEquals(ItemType.FOOD, banana1.getItemType());
  }
}
