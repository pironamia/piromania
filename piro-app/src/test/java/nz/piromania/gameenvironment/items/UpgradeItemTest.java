package nz.piromania.gameenvironment.items;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import nz.piromania.Currency;
import nz.piromania.Stats;
import nz.piromania.gameenvironment.items.ItemData;
import nz.piromania.gameenvironment.items.ItemType;
import nz.piromania.gameenvironment.items.UpgradeItem;
import nz.piromania.gameenvironment.items.UpgradeType;
import org.junit.jupiter.api.BeforeAll;
import java.util.ArrayList;
import java.util.Arrays;

class UpgradeItemTest {
  private static final Stats cannonUpgradeStats = new Stats(0, -0.2f, 50, -30, -0.1f);
  private static final ArrayList<UpgradeType> cannonUpgradeTypes = new ArrayList<>(Arrays.asList(UpgradeType.WEAPONRY));
  private static final ItemData cannon = new ItemData(1, "Cannon", "A cannon upgrade", null, new Currency(500,0), ItemType.UPGRADE, 100, cannonUpgradeTypes, cannonUpgradeStats, true);
  private static final Currency cannonPurchasedValue = new Currency(550,0);
  private static UpgradeItem cannon1;
  private static UpgradeItem cannon2;

  
  @BeforeAll
  public static void beforeAll() {
    cannonUpgradeTypes.add(UpgradeType.WEAPONRY);
    cannon1 = cannon.createUpgradeInstance();
    cannon2 = cannon.createUpgradeInstance(cannonPurchasedValue);
  }
  
  @Test
  void checkInstantiation() {
    UpgradeItem[] cannons = {cannon1, cannon2};
    
    for(UpgradeItem cannonUpgrade : cannons) {
      assertEquals(cannonUpgradeTypes, cannonUpgrade.getUpgradeTypes());
      assertEquals(cannonUpgradeStats, cannonUpgrade.getUpgradeStats());
    }
    assertEquals(cannonPurchasedValue, cannon2.getPurchasedValue());
    
    Stats newUpgradeStats = new Stats(0, -0.1f, 50, -30, -0.1f);
    cannon2.setUpgradeStats(newUpgradeStats);
    assertEquals(newUpgradeStats, cannon2.getUpgradeStats());
    
    cannon2.setSoldValue(new Currency(2));
    assertEquals(new Currency(2).getCents(), cannon2.getSoldValue().getCents());
    
    cannon2.changeUpgradeStats(newUpgradeStats, true);
    newUpgradeStats.changeStats(newUpgradeStats, true);
    assertEquals(newUpgradeStats.getRiskMultiplier(), cannon2.getUpgradeStats().getRiskMultiplier());
  }
}
