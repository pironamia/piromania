package nz.piromania.gameenvironment.routes;

import static org.junit.jupiter.api.Assertions.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import nz.piromania.JsonManager;
import nz.piromania.Stats;
import nz.piromania.StringStorage;
import nz.piromania.gameenvironment.islands.Island;
import nz.piromania.gameenvironment.routes.Route;
import nz.piromania.gameenvironment.ships.Ship;

class RouteTest {

  @Test
  public void RouteTests() {
    try {
      JsonManager<StringStorage> islandNameGenerator = JsonManager.createGeneric(StringStorage.class);
      islandNameGenerator.generate("IslandNames.json");
      JsonManager<StringStorage> islandDescGenerator = JsonManager.createGeneric(StringStorage.class);
      islandDescGenerator.generate("IslandDescriptions.json");
      //Standard ship
      Ship ship = new Ship(0, "TestyMcTestface", "Ol' reliable", null, 20, new Stats(100, 1, 250, 60, 1), 100, null);
      //Generate 2 random islands
      Island island1 = new Island(islandNameGenerator.getData(0).generateRandomString(), 
          islandDescGenerator.getData(0).generateRandomString(), null, null, new ArrayList<Island>());
      Island island2 = new Island(islandNameGenerator.getData(0).generateRandomString(), 
          islandDescGenerator.getData(0).generateRandomString(), null, null, new ArrayList<Island>(Arrays.asList(island1)));
      //Generate 2 islands 100m away from each other
      Island island3 = new Island(islandNameGenerator.getData(0).generateRandomString(), 
          islandDescGenerator.getData(0).generateRandomString(), null, null, 0, 0);
      Island island4 = new Island(islandNameGenerator.getData(0).generateRandomString(), 
          islandDescGenerator.getData(0).generateRandomString(), null, null, 0, 100);
      Route route1 = new Route(island1, island2, ship);
      Route route2 = new Route(island3, island4, ship);
      assertTrue(route1.calculateTravelTime() != 0);
      assertTrue(route2.calculateTravelTime() != 0);
      //10x faster, 50% more risk
      Ship fastShip = new Ship(1, "Fast TestyMcTestface", "Ol' reliable", null, 20, new Stats(100, 1.5f, 250, 60, 10f), 100, null);
      //4x stronger, 25% more risk, 25% slower
      Ship powerfulShip = new Ship(2, "Strong TestyMcTestface", "Ol' reliable", null, 20, new Stats(100, 1.25f, 1000, 60, 0.75f), 100, null);
      Route route3 = new Route(island1, island2, fastShip);
      Route route4 = new Route(island3, island4, fastShip);
      Route route5 = new Route(island1, island2, powerfulShip);
      Route route6 = new Route(island3, island4, powerfulShip);
      assertTrue(route3.calculateTravelTime() != 0);
      assertTrue(route4.calculateTravelTime() != 0);
      assertTrue(route5.calculateTravelTime() != 0);
      assertTrue(route6.calculateTravelTime() != 0);
    } catch (Exception e) { fail("Exception caught: " + e); }
  }
}
