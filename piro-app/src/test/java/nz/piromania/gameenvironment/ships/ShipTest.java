package nz.piromania.gameenvironment.ships;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import nz.piromania.Currency;
import nz.piromania.Inventory;
import nz.piromania.JsonManager;
import nz.piromania.Stats;
import nz.piromania.gameenvironment.items.ItemData;
import nz.piromania.gameenvironment.items.ItemType;
import nz.piromania.gameenvironment.items.UpgradeItem;
import nz.piromania.gameenvironment.items.UpgradeType;
import nz.piromania.gameenvironment.ships.Ship;
import nz.piromania.gameenvironment.ships.ShipVariables;

class ShipTest {

  @Test
  public void ShipTests() {
    Ship ship = new Ship(0, "TestyMcTestface", "description", null, 100, new Stats(100, 1.2f, 80, 60, 1.3f), 200, null);
    assertEquals(60, ship.getInventory().getCapacity());
    //Assert both methods of getting damage potential work
    assertEquals(80, (int) ship.get("damage-potential"));
    assertEquals(80, ship.getShipStats().getDamagePotential());
    
    Ship shipWithInventory = new Ship(1, "TestyMcTestface 2", "description", null, 100, new Inventory(60), new Stats(100, 1.2f, 80, 60, 1.3f), 200, null);
    assertEquals(60, ship.getInventory().getCapacity());
    //Try slightly modifying capacity
    assertTrue(shipWithInventory.changeStats(new Stats(0, 0, 0, 1, 0), true));
    //Check it worked
    assertEquals(61, shipWithInventory.getInventory().getCapacity());
    //Try reducing the inventory to below 0
    assertFalse(shipWithInventory.changeStats(new Stats(200, 0, 0, -70, 0), true));
    //Ensure capacity did not change
    assertEquals(61, shipWithInventory.getInventory().getCapacity());
    //Ensure stats weren't added
    assertFalse(shipWithInventory.getShipStats().getMaxHitpoints() == 300); 
  }
  
  @Test
  public void jsonAndUpgradeAndDowngradeTest () {
    try {
      JsonManager<Ship> shipManager = JsonManager.createGeneric(Ship.class);
      shipManager.generate("Ships.json");
      //Initializes all ships.
      for (int i = 0; i< shipManager.getDataSize(); i++) {
        shipManager.getData(i).initialize();
      }
      Ship jsonShip1 = shipManager.getData(0);
      Ship jsonShip2 = shipManager.getData(1);
      for (Ship jsonShip : Arrays.asList(jsonShip1, jsonShip2)) {
        for (ShipVariables variable : ShipVariables.values()) {
          //Asserts no values are null.
          assertNotNull(jsonShip.get(variable.getShipVariable().getVariableAlias()));
        }
        assertNotNull(jsonShip.getInventory());
        assertNotNull(jsonShip.getImagePath());
        assertNotNull(jsonShip.getUpgradeSlots());
      }
      
      //Finally, let's check the upgrading system.
      Stats cannonUpgradeStats = new Stats(0, -0.2f, 50, 0, -0.1f);
      ArrayList<UpgradeType> cannonUpgradeTypes = new ArrayList<>(Arrays.asList(UpgradeType.WEAPONRY));
      ItemData cannon = new ItemData(1, "Cannon", "A cannon upgrade", null, new Currency(500,0), ItemType.UPGRADE, 100, cannonUpgradeTypes, cannonUpgradeStats, true);
      Currency cannonPurchasedValue = new Currency(550,0);
      UpgradeItem cannon1 = cannon.createUpgradeInstance(cannonPurchasedValue);
      //jsonShip2 has 5 weaponry slots. Let's try add 5 cannons!
      try {
        jsonShip2.applyUpgrade(cannon1, UpgradeType.WEAPONRY);
        jsonShip2.applyUpgrade(cannon1, UpgradeType.WEAPONRY);
        jsonShip2.applyUpgrade(cannon1, UpgradeType.WEAPONRY);
        jsonShip2.applyUpgrade(cannon1, UpgradeType.WEAPONRY);
        jsonShip2.applyUpgrade(cannon1, UpgradeType.WEAPONRY);
      } catch (Exception e) { fail("Exception caught: " + e); }
      //Ensure allowing more fails
      try {
        jsonShip2.applyUpgrade(cannon1, UpgradeType.WEAPONRY);
        fail("Exception should have been thrown.");
      } catch (Exception e) {}
      //Make sure giving an invalid slot fails
      try {
        jsonShip2.applyUpgrade(cannon1, UpgradeType.SAIL);
        fail("Exception should have been thrown.");
      } catch (Exception e) {}
      //Great, let's make sure the amount of slots for WEAPONRY is 0.
      assertTrue(jsonShip2.getUpgradeSlots().get(UpgradeType.WEAPONRY) == 0);
      //Did 5 stats get added?
      assertTrue(jsonShip2.getShipStats().getDamagePotential() == 501);

      //Edge case time:
      Stats badThing = new Stats(-100, 0, 0, 0, 0);
      UpgradeItem badUpgradeThing = new UpgradeItem(0, null, null, null, null, ItemType.UPGRADE, 0, cannonUpgradeTypes, badThing, false);
      //Let's make some room for some new upgrades.
      jsonShip1.getUpgradeSlots().put(UpgradeType.WEAPONRY, 100);
      jsonShip2.getUpgradeSlots().put(UpgradeType.WEAPONRY, 100);
      //This would kill jsonShip1, so don't allow it.
      try {
        jsonShip1.applyUpgrade(badUpgradeThing, UpgradeType.WEAPONRY);
        fail("Exception should have been thrown.");
      } catch (Exception e) {}
      //For jsonShip2 however, it should set the current & max HP to be 1.
      jsonShip2.applyUpgrade(badUpgradeThing, UpgradeType.WEAPONRY);
      assertEquals(1, jsonShip2.getCurrentHitpoints());
      //Now check for capacity.
      Stats badThing2 = new Stats(0, 0, 0, -62, 0);
      UpgradeItem badUpgradeThing2 = new UpgradeItem(0, null, null, null, null, ItemType.UPGRADE, 0, cannonUpgradeTypes, badThing2, false);
      //Check adding the bad capacity works for ship 1, as it has 120 capacity by default.
      jsonShip1.applyUpgrade(badUpgradeThing2, UpgradeType.WEAPONRY);
      //Make sure it actually changes the capacity.
      assertEquals(58, jsonShip1.getInventory().getCapacity());
      //Ensure adding the bad capacity to ship 2 fails, as it only has 61 capacity.
      try {
        jsonShip2.applyUpgrade(badUpgradeThing2, UpgradeType.WEAPONRY);
        fail("Exception should have been thrown.");
      } catch (Exception e) {}
      
      //How about downgrading?
      //Removing this upgrade should be completely valid.
      jsonShip2.removeUpgrade(badUpgradeThing, UpgradeType.WEAPONRY);
      //Does it adjust the max hitpoints?
      assertEquals(101, jsonShip2.getShipStats().getMaxHitpoints());
      //Are current hitpoints still 1 as they should be?
      assertEquals(1, jsonShip2.getCurrentHitpoints());
      //Did it free a slot?
      assertEquals(100, jsonShip2.getUpgradeSlots().get(UpgradeType.WEAPONRY));
      //Edge case checking is used in both variations of code, so need to test for those
    } catch (Exception e) { fail("Exception caught: " + e); }
  }
}
