package nz.piromania.gameenvironment.stores;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import nz.piromania.JsonManager;
import nz.piromania.NameGeneratorStorage;

class NameGenerationTest {

  @Test
  void NameGenerationTests() {
    try {
      JsonManager<NameGeneratorStorage> nameGenerator = JsonManager.createGeneric(NameGeneratorStorage.class);
      nameGenerator.generate("StoreNames.json");
      NameGeneratorStorage randomNameGenerator = (NameGeneratorStorage) nameGenerator.getData(0);
      assertNotNull(randomNameGenerator.generateRandomName());
      assertNotNull(randomNameGenerator.generateRandomName(1));
      String randomName = randomNameGenerator.generateRandomName(0);
      assertTrue(randomName.contains("Foodstuffs") 
          || randomName.contains("Food")
          || randomName.contains("Bounty"));
    } catch (Exception e) { fail("Exception thrown: " + e); }
  }

}
