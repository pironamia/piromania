package nz.piromania.gameenvironment.stores;

import static org.junit.jupiter.api.Assertions.*;
import java.io.FileNotFoundException;
import org.junit.jupiter.api.Test;
import nz.piromania.JsonManager;
import nz.piromania.gameenvironment.stores.Personality;

/**
 * Note: This class by extension also tests
 * the PersonalitySystem class.
 */
class PersonalitySystemManagerTest {

  @Test
  public void PersonalitySystemManagerTest() {
    try{
      JsonManager<Personality> personalitySystemManager = JsonManager.createGeneric(Personality.class);
      personalitySystemManager.generate("Personalities.json");
      //Check that the first instance is a happy personality
      assertEquals("Happy", ((Personality) personalitySystemManager.getData(0)).getName());
      //Check rightTypeModifier & barteringModifier were set correctly
      assertEquals(0.25f, ((Personality) personalitySystemManager.getData(0)).getRightTypeModifier());
      assertEquals(0.1f, ((Personality) personalitySystemManager.getData(0)).getBarteringModifier());
      //Check it can randomly choose rightTypeModifier & barteringModifier
      assertNotNull(((Personality) personalitySystemManager.getData(3)).getRightTypeModifier());
      assertNotNull(((Personality) personalitySystemManager.getData(3)).getBarteringModifier());
      //Check it can load a random personality
      assertNotNull(((Personality) personalitySystemManager.getRandomData()));
      //Check getting a random string works from a personality
      assertNotNull(((Personality) personalitySystemManager.getRandomData()).getRandomDialogue("Greeting Dialogue"));
    } catch (FileNotFoundException e) {fail("Exception thrown\n" + e);}
  }
}
