package nz.piromania.gameenvironment.stores;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import nz.piromania.Currency;
import nz.piromania.Inventory;
import nz.piromania.JsonManager;
import nz.piromania.gameenvironment.items.Item;
import nz.piromania.gameenvironment.items.ItemData;
import nz.piromania.gameenvironment.items.ItemType;

class SoldLedgerTest {
  private static final ItemData banana = new Item(1, "banana", "a banana", null, new Currency(2,0), ItemType.FOOD, 1);
  
  @Test
  void soldLedgerTest() {
    JsonManager<Personality> personalitySystemManager = JsonManager.createGeneric(Personality.class);
    JsonManager<ItemData> im = JsonManager.createGeneric(ItemData.class);
    try {
      personalitySystemManager.generate("Personalities.json");
      im.generate("Items.json");
    } catch (Exception e) { fail("Exception caught: " + e); }
    Store store1 = new Store(0, "Store 1", personalitySystemManager, ItemType.FOOD, ItemType.DRINK);
    Store store2 = new Store(1, "Store 2", personalitySystemManager, ItemType.GEM, ItemType.UPGRADE);
    store1.randomizeInventory(im);
    store2.randomizeInventory(im);
    Map<Store, Map<Item, Currency>> soldLedger = new HashMap<Store, Map<Item, Currency>>(Map.of(
        store1, new HashMap<Item, Currency>(),
        store2, new HashMap<Item, Currency>()
    ));
    Item stolenBanana = SoldLedgerTest.banana.createItemInstance();
    store1.sellToStore(stolenBanana, new Inventory(new ArrayList<Item>(Arrays.asList(stolenBanana))), soldLedger);
    assertTrue(soldLedger.get(store1).get(stolenBanana).getCents() == 0);
    Item banana = SoldLedgerTest.banana.createItemInstance(new Currency(3,00));
    store2.sellToStore(banana, new Inventory(new ArrayList<Item>(Arrays.asList(banana))), soldLedger);
    assertTrue(soldLedger.get(store2).get(banana).getCents() == 300);
    store1.buyFromStore(stolenBanana, new Currency(500,0), soldLedger);
    assertNull(soldLedger.get(store1).get(stolenBanana));
    store2.buyFromStore(banana, new Currency(500,0), soldLedger);
    assertNull(soldLedger.get(store2).get(banana));
  }
}