package nz.piromania.gameenvironment.stores;

import static org.junit.jupiter.api.Assertions.*;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import nz.piromania.Currency;
import nz.piromania.Inventory;
import nz.piromania.JsonManager;
import nz.piromania.gameenvironment.items.Item;
import nz.piromania.gameenvironment.items.ItemData;
import nz.piromania.gameenvironment.items.ItemType;
import nz.piromania.gameenvironment.stores.Personality;
import nz.piromania.gameenvironment.stores.Store;

class StoreTest {
  public Store appleStore;
  public Store store1;
  public Store store2;
  public Store store3;
  public Store store4;
  public Store store5;
  public ArrayList<Store> stores;
  public Item apple = new Item(1, "apple", "an apple", null, new Currency(150), ItemType.FOOD, 2, new Currency(175));
  public JsonManager<Personality> personalities = null;
  
  @SuppressWarnings("unused")
  @BeforeEach
  public void initStores() {
    try {
       personalities = JsonManager.createGeneric(Personality.class);
       personalities.generate("Personalities.json");
       JsonManager<ItemData> im = JsonManager.createGeneric(ItemData.class);
       im.generate("Items.json");
    } catch (FileNotFoundException e) {fail("Exception thrown\n" + e);}
    if (personalities == null) {fail("Personalities not correctly assigned");}
    //Store with a random personality, wants to buy food and sell drinks
    store1 = new Store(0, null, personalities, ItemType.FOOD, ItemType.DRINK);
    //Store with a random personality, wants to buy drinks and sell food
    store2 = new Store(1, null, personalities, new Inventory(), ItemType.DRINK, ItemType.FOOD);
    Personality randomPersonality = personalities.getRandomData();
    //Store with a random personality, wants to buy gems and sell food
    store3 = new Store(2, null, randomPersonality, ItemType.GEM, ItemType.FOOD);
    //Happy personality, wants to buy AND sell food
    store4 = new Store(3, null, personalities.getData(0), ItemType.FOOD, ItemType.FOOD);
    //Greedy personality, wants to buy AND sell upgrades
    store5 = new Store(4, null, personalities.getData(1), ItemType.UPGRADE, ItemType.UPGRADE);
    stores = new ArrayList<Store>(Arrays.asList(store1, store2, store3, store4, store5));
    Inventory inv = new Inventory(new ArrayList<Item>(Arrays.asList(apple)));
    appleStore = new Store(5, null, personalities, inv, ItemType.FOOD, ItemType.FOOD);
  }
  
  @Test
  public void StoreGeneratingTest() {
    try {
      JsonManager<ItemData> im = JsonManager.createGeneric(ItemData.class);
      im.generate("Items.json");
      for (Store store : stores) {
        //The thing about randomness, is you have to test it lots!
        for (int i=0; i<500; i++) {
          store.randomizeInventory(im);
          //Ensure it generated
          assertTrue(store.getStoreInventory().getItems().size() > 0);
          //Make sure stores don't generate too many items.
          //TODO: Work out in balance patch.
          assertTrue(store.getStoreInventory().getItems().size() <= 100);
        }
        //Try generate store prices
        store.generateBuyingFromStorePrices(im);
        for (Item item : store.getBuyingInventory().getItems()) {
          //0 or less *shouldn't* be possible.
          assertTrue(item.getPurchasedValue().getCents() != 0);
        }
        //Try generate selling to store prices
        Item apple = new Item(0, "apple", "an apple", null, new Currency(20), ItemType.FOOD, 2, new Currency(30));
        Inventory inv = new Inventory(new ArrayList<Item>(Arrays.asList(apple)));
        store.generateSellingToStorePrices(im);
        //0 or less *shouldn't* be possible.
        assertTrue(store.getSellingInventory().getItems().get(0).getPurchasedValue().getCents() != 0);
      }
    } catch(Exception e) { fail("Exception thrown: " + e);}
  }
  
  @Test
  public void storeModifyingTest() {
    Map<Store, Map<Item, Currency>> soldLedger = new HashMap<Store, 
        Map<Item, Currency>>(
            Map.of(appleStore, new HashMap<Item, Currency>()));
    //Make sure you can't purchase from a store when you have invalid funds
    JsonManager<ItemData> im = JsonManager.createGeneric(ItemData.class);
    try {
      im.generate("Items.json");
      appleStore.buyFromStore(apple, new Currency(0), soldLedger);
      fail("Exception should have been thrown");
    } catch (Exception e) {}
    //Make sure you can purchase from a store when you have valid funds
    //Also checks to ensure the item was NOT removed during previous check
    appleStore.generateBuyingFromStorePrices(im);
    appleStore.generateSellingToStorePrices(im);
    assertTrue(appleStore.buyFromStore(apple, new Currency(50000,0), soldLedger) == apple);
    //Check it was actually removed
    assertTrue(appleStore.getStoreInventory().getItems().size() == 0);
    //Restock the apple store
    appleStore.getStoreInventory().addItem(apple);
    Inventory playerInventory = new Inventory(new ArrayList<>(Arrays.asList(apple))); 
    appleStore.sellToStore(apple, playerInventory, soldLedger);
    //Check it adds to the player's ledger
    assertTrue(soldLedger.get(appleStore).values().size() == 1);
    //Check it adds to the store's inventory
    assertTrue(appleStore.getStoreInventory().getItems().size() == 2);
    playerInventory.addItem(appleStore.buyFromStore(apple, new Currency(50000,0), soldLedger));
    //Check it removes it from the ledger if you rebuy the same item
    assertTrue(soldLedger.get(appleStore).values().size() == 0);
    for (Item item : playerInventory.getItems()) {
      assertEquals(0, item.getSoldValue().getCents());
    }
  }
  
  @Test
  public void jsonStoreTest() {
    try {
      JsonManager<Store> storeManager = JsonManager.createGeneric(Store.class);
      storeManager.generate("Stores.json");
      for (int i = 0; i < storeManager.getDataSize(); i++) {
        storeManager.getData(i).initialize(personalities);
      }
      Store jsonStore1 = storeManager.getData(0);
      Store jsonStore2 = storeManager.getData(1);
    } catch (Exception e) { fail("Exception caught: " + e); }
  }
}
