package nz.piromania.reactiveengine;

import static org.junit.jupiter.api.Assertions.*;

import nz.piromania.reactiveengine.exception.KeyNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class TestClass {
  public String testString;
  public int testNumber;
}

public class BuildContextTest {
  BuildContext context;

  @BeforeEach
  public void beforeEach() {
    this.context = new BuildContext();
  }

  @Nested
  public class ProvideAndLookup {
    @Test
    public void fetchesSavedValue() throws KeyNotFoundException {
      context.provideInheritedData("test value");

      assertDoesNotThrow(() -> {
        context.getInheritedDataOfExactType(String.class);
      });
      assertEquals("test value", context.getInheritedDataOfExactType(String.class));
    }

    @Test
    public void throwsWhenValueDoesntExist() {
      context.provideInheritedData("test value");

      assertThrows(KeyNotFoundException.class, () -> context.getInheritedDataOfExactType(Double.class));
    }

    @Test
    public void fetchesWithMultipleValues() throws KeyNotFoundException {
      context.provideInheritedData("test value");
      context.provideInheritedData(0xff);

      assertDoesNotThrow(() -> {
        context.getInheritedDataOfExactType(String.class);
      });
      assertEquals("test value", context.getInheritedDataOfExactType(String.class));

      assertDoesNotThrow(() -> {
        context.getInheritedDataOfExactType(Integer.class);
      });
      assertEquals(0xff, context.getInheritedDataOfExactType(Integer.class));
    }

    @Test
    public void fetchesWithComplexValues() throws KeyNotFoundException {
      context.provideInheritedData(new TestClass());

      assertDoesNotThrow(() -> {
        context.getInheritedDataOfExactType(TestClass.class);
      });

      final TestClass value = context.getInheritedDataOfExactType(TestClass.class);
      assertEquals(0, value.testNumber);
      assertNull(value.testString);

      value.testNumber = -1;
      value.testString = "abc";

      final TestClass valueAgain = context.getInheritedDataOfExactType(TestClass.class);
      assertEquals(-1, valueAgain.testNumber);
      assertEquals("abc", valueAgain.testString);
    }
  }

  @Nested
  public class Copy {
    @Test
    public void lookup() throws KeyNotFoundException {
      context.provideInheritedData("test value");
      context.provideInheritedData(0xff);

      final BuildContext contextCopy = context.copy();

      assertDoesNotThrow(() -> {
        contextCopy.getInheritedDataOfExactType(String.class);
      });
      assertEquals("test value", contextCopy.getInheritedDataOfExactType(String.class));

      assertDoesNotThrow(() -> {
        contextCopy.getInheritedDataOfExactType(Integer.class);
      });
      assertEquals(0xff, contextCopy.getInheritedDataOfExactType(Integer.class));

      assertThrows(KeyNotFoundException.class, () -> contextCopy.getInheritedDataOfExactType(Double.class));
    }

    @Test
    public void oneWayInheritance() throws KeyNotFoundException {
      context.provideInheritedData("test value");

      final BuildContext contextCopy = context.copy();

      contextCopy.provideInheritedData(0xff);

      assertDoesNotThrow(() -> {
        contextCopy.getInheritedDataOfExactType(String.class);
      });
      assertEquals("test value", contextCopy.getInheritedDataOfExactType(String.class));

      assertDoesNotThrow(() -> {
        contextCopy.getInheritedDataOfExactType(Integer.class);
      });
      assertEquals(0xff, contextCopy.getInheritedDataOfExactType(Integer.class));

      assertThrows(KeyNotFoundException.class, () -> contextCopy.getInheritedDataOfExactType(Double.class));

      // Check that the modifications to contextCopy don't affect the original context.
      assertThrows(KeyNotFoundException.class, () -> context.getInheritedDataOfExactType(Integer.class));
    }
  }
}
