package nz.piromania.reactiveengine;

import nz.piromania.reactiveengine.key.ValueKey;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ComponentStateTest {

    @Nested
    @DisplayName("shouldMountWithComponent")
    class ShouldMountWithComponent {
        @Test
        public void testFirstMount() {
            final ComponentState<TestComponentWithNoChildren> state = new TestComponentWithNoChildrenState();
            assertTrue(state.shouldMountWithComponent(new TestComponentWithNoChildren()));
            assertTrue(state.shouldMountWithComponent(new TestComponentWithNoChildren(new ValueKey("abc123"))));
        }

        @Test
        public void testWithoutKey() {
            final ComponentState<TestComponentWithNoChildren> state = new TestComponentWithNoChildrenState();
            state.setComponent(new TestComponentWithNoChildren());

            assertTrue(state.shouldMountWithComponent(new TestComponentWithNoChildren()));
            assertFalse(state.shouldMountWithComponent(new TestComponentWithNoChildren(new ValueKey("abc123"))));
        }

        @Test
        public void testWithKey() {
            final ComponentState<TestComponentWithNoChildren> state = new TestComponentWithNoChildrenState();
            state.setComponent(new TestComponentWithNoChildren(new ValueKey("abc123")));

            assertFalse(state.shouldMountWithComponent(new TestComponentWithNoChildren()));
            assertFalse(state.shouldMountWithComponent(new TestComponentWithNoChildren(new ValueKey("other value"))));
            assertTrue(state.shouldMountWithComponent(new TestComponentWithNoChildren(new ValueKey("abc123"))));
        }

        @Test
        public void testWithIncompatibleTypes() {
            final ComponentState<TestComponentWithNoChildren> state1 = new TestComponentWithNoChildrenState();
            state1.setComponent(new TestComponentWithNoChildren());

            assertFalse(state1.shouldMountWithComponent(new TestComponentWithNChildren(10)));

            // Still don't match them, even if they have the same key.
            final ComponentState<TestComponentWithNoChildren> state2 = new TestComponentWithNoChildrenState();
            state2.setComponent(new TestComponentWithNoChildren(new ValueKey("abc123")));

            assertFalse(state2.shouldMountWithComponent(new TestComponentWithNChildren(new ValueKey("abc123"), 10)));
        }
    }

    @Test
    public void testSetComponent() {
        final ComponentState<TestComponentWithNoChildren> stateMock = Mockito.mock(ComponentState.class, Mockito.CALLS_REAL_METHODS);

        final TestComponentWithNoChildren initialComponent = new TestComponentWithNoChildren();
        final TestComponentWithNoChildren secondComponent = new TestComponentWithNoChildren();

        stateMock.setComponent(initialComponent);
        verify(stateMock, VerificationModeFactory.noInteractions()).didChangeDependencies(initialComponent);

        stateMock.setComponent(secondComponent);
        verify(stateMock).didChangeDependencies(initialComponent);
        verify(stateMock, VerificationModeFactory.times(0)).didChangeDependencies(secondComponent);
    }

    @Test
    public void testSetState() {
        final ComponentState<TestComponentWithNoChildren> state = new TestComponentWithNoChildrenState();

        assertTrue(state.getIsDirty());  // State is intially dirty (since it hasn't been built yet)
        state.clearDirt();
        assertFalse(state.getIsDirty());


        AtomicInteger callCount = new AtomicInteger();
        state.setState(() -> {
            callCount.set(callCount.get() + 1);
        });

        assertEquals(1, callCount.get());
        assertTrue(state.getIsDirty());
    }
}
