package nz.piromania.reactiveengine;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import nz.piromania.reactiveengine.component.KeyboardDetector;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.component.text.TextComponent;
import nz.piromania.reactiveengine.layout.Axis;
import nz.piromania.reactiveengine.util.MultipleChildrenComponentState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class EngineTest {
    private Engine engine;

    @BeforeEach
    public void beforeEach() {
        this.engine = new Engine();
    }

    @Test
    public void testInitialise() {
        final Component comp = new TestComponentWithNoChildren();

        this.engine.initialise(comp);

        // Investigate resultant state.
        assertEquals(0, engine.getRootStateNode().getChildren().size());
        assertEquals(0, engine.getRootRenderNode().getChildren().size());
        assertEquals(comp, engine.getRootRenderNode().getComponent());
        assertTrue(engine.getRootStateNode().getComponentState() instanceof TestComponentWithNoChildrenState);
    }

    @Test
    public void testInitialiseWithChildren() {
        final Component comp = new TestComponentWithNChildren(10);

        this.engine.initialise(comp);

        // Investigate resultant state.
        assertEquals(10, engine.getRootStateNode().getChildren().size());
        assertEquals(10, engine.getRootRenderNode().getChildren().size());
        assertEquals(comp, engine.getRootRenderNode().getComponent());
        assertTrue(engine.getRootStateNode().getComponentState() instanceof TestComponentWithNChildrenState);

        for (int i=0; i < engine.getRootRenderNode().getChildren().size(); i++) {
            final RenderNode node = engine.getRootRenderNode().getChildren().get(i);

            assertEquals(0, node.getChildren().size());
            assertTrue(node.getComponent() instanceof TestComponentWithNoChildren);
            assertEquals(i, ((TestComponentWithNoChildren) node.getComponent()).data);
        }
    }

    @Test
    public void testTickWithUpdates() {
        final Component comp = new TestComponentWithNChildren(5, TestKeyboardDetectingComponent::new);

        this.engine.setRenderConsumer((RenderNode tree) -> {
            // Takes the tree and simulates key presses on the KeyboardDetectors.
            Integer i = 0;
            for (RenderNode childNode : tree.getChildren()) {
                // Iterating over the TestKeyboardDetectingComponent
                assertEquals(1, childNode.getChildren().size());

                final RenderNode keyboardDetectorNode = childNode.getChildren().get(0);
                assertTrue(keyboardDetectorNode.getComponent() instanceof KeyboardDetector);
                final KeyboardDetector detector = (KeyboardDetector) keyboardDetectorNode.getComponent();
                detector.getOnKeyboardCallback().accept(i.toString());

                i++;
            }
        });
        this.engine.initialise(comp);


        this.engine.setRenderConsumer((RenderNode tree) -> {
            // Check that the leaf nodes have the expected string values.
            Integer i = 0;
            for (RenderNode childNode : tree.getChildren()) {
                // Iterating over the TestKeyboardDetectingComponent
                assertEquals(1, childNode.getChildren().size());

                final RenderNode keyboardDetectorNode = childNode.getChildren().get(0);
                assertEquals(1, keyboardDetectorNode.getChildren().size());

                final RenderNode leafNode = keyboardDetectorNode.getChildren().get(0);
                assertTrue(leafNode.getComponent() instanceof TestComponentWithNoChildren);
                assertEquals(i.toString(), ((TestComponentWithNoChildren) leafNode.getComponent()).data);

                i++;
            }
        });
        final boolean didUpdate = this.engine.tick();
        assertTrue(didUpdate);
    }

    @Test
    public void testTickWithNoUpdates() {
        final Component comp = new TestComponentWithNChildren(5, TestKeyboardDetectingComponent::new);

        AtomicInteger renderEmitCount = new AtomicInteger(0);

        this.engine.setRenderConsumer((RenderNode tree) -> {
            renderEmitCount.set(renderEmitCount.get() + 1);
        });
        this.engine.initialise(comp);

        this.engine.tick();
        this.engine.tick();

        assertEquals(1, renderEmitCount.get());
    }

    @Test
    public void testTickWithTreeExpandUpdates() {
        final Component comp = new TestKeyboardDetectingListChangingComponent((key) -> 8);

        this.engine.setRenderConsumer((RenderNode rootNode) -> {
            // Simulate some key presses on the KeyboardDetector.
            assertEquals(1, rootNode.getChildren().size());
            final RenderNode keyboardDetectorNode = rootNode.getChildren().get(0);
            assertTrue(keyboardDetectorNode.getComponent() instanceof KeyboardDetector);
            final KeyboardDetector keyboardDetector = (KeyboardDetector) keyboardDetectorNode.getComponent();

            // Trigger 3 key presses (each should correspond to one new node being added to the list).
            keyboardDetector.getOnKeyboardCallback().accept("a");
            keyboardDetector.getOnKeyboardCallback().accept("a");
            keyboardDetector.getOnKeyboardCallback().accept("a");
        });
        this.engine.initialise(comp);

        this.engine.setRenderConsumer((RenderNode rootNode) -> {
            assertEquals(1, rootNode.getChildren().size());
            final RenderNode keyboardDetectorRenderNode = rootNode.getChildren().get(0);
            assertEquals(1, keyboardDetectorRenderNode.getChildren().size());
            final RenderNode listRenderNode = keyboardDetectorRenderNode.getChildren().get(0);
            assertEquals(8, listRenderNode.getChildren().size());

            // Also drill down into the state nodes.
            // The TestComponentWithNoChildren states should have kept the original value (5) for the first 5 components
            // whereas the last 3 should have stored the new value. All Components themselves should only have the new value.
            assertEquals(1, this.engine.getRootStateNode().getChildren().size());  // TestKeyboardDetectingIncreasingListComponent
            assertEquals(1, ((StateNode) this.engine.getRootStateNode().getChildren().get(0)).getChildren().size());  // KeyboardDetector
            assertEquals(8, ((StateNode) ((StateNode) this.engine.getRootStateNode().getChildren().get(0)).getChildren().get(0)).getChildren().size());  // ListComponent
            final StateNode listComponentState = ((StateNode) ((StateNode) this.engine
                .getRootStateNode().getChildren().get(0)).getChildren().get(0));

            for (int i=0; i < listRenderNode.getChildren().size(); i++) {
                // The first 5 children should still hold the original value, whereas the new children should hold the
                // new one.

                final RenderNode childRenderNode = listRenderNode.getChildren().get(i);
                final StateNode childStateNode = (StateNode) listComponentState.getChildren().get(i);

                assertTrue(childRenderNode.getComponent() instanceof TestComponentWithNoChildren);

                assertTrue(childStateNode.getComponentState() instanceof TestComponentWithNoChildrenState);
                final TestComponentWithNoChildrenState childState = (TestComponentWithNoChildrenState) childStateNode.getComponentState();

                if (i < 5) {
                    assertEquals(childState.initialData, 5);
                }
                else {
                    assertEquals(childState.initialData, 8);
                }
                assertEquals(8, ((TestComponentWithNoChildren) childRenderNode.getComponent()).data);
            }
        });
        this.engine.tick();
    }

    /**
     * Test that when the tree shrinks the following occurs:
     * 1. That components that are still present are still correctly matched with their respective states.
     * 2. That states that are no longer used are deleted and are not 'resurrected' with a later render with more children.
     */
    @Test
    public void testTickWithTreeShrinkUpdates() {
        final Component comp = new TestKeyboardDetectingListChangingComponent((key) -> (key.equals("phase 1")) ? 1 : 10);

        this.engine.setRenderConsumer((RenderNode rootNode) -> {
            // Simulate some key presses on the KeyboardDetector.
            assertEquals(1, rootNode.getChildren().size());
            final RenderNode keyboardDetectorNode = rootNode.getChildren().get(0);
            assertTrue(keyboardDetectorNode.getComponent() instanceof KeyboardDetector);
            final KeyboardDetector keyboardDetector = (KeyboardDetector) keyboardDetectorNode.getComponent();

            // 'key' = "phase 1" signals to the component to shrink size to 1.
            keyboardDetector.getOnKeyboardCallback().accept("phase 1");
        });
        this.engine.initialise(comp);

        this.engine.setRenderConsumer((RenderNode rootNode) -> {
            assertEquals(1, rootNode.getChildren().size());
            final RenderNode keyboardDetectorRenderNode = rootNode.getChildren().get(0);
            assertEquals(1, keyboardDetectorRenderNode.getChildren().size());
            final RenderNode listRenderNode = keyboardDetectorRenderNode.getChildren().get(0);
            assertEquals(1, listRenderNode.getChildren().size());

            // Also drill down into the state nodes.
            // The TestComponentWithNoChildren state should have kept the original value (5).
            // There should be no other extant ComponentStates.
            assertEquals(1, this.engine.getRootStateNode().getChildren().size());  // TestKeyboardDetectingIncreasingListComponent
            assertEquals(1, ((StateNode) this.engine.getRootStateNode().getChildren().get(0)).getChildren().size());  // KeyboardDetector
            assertEquals(1, ((StateNode) ((StateNode) this.engine.getRootStateNode().getChildren().get(0)).getChildren().get(0)).getChildren().size());  // ListComponent
            final StateNode listComponentState = ((StateNode) ((StateNode) this.engine
                .getRootStateNode().getChildren().get(0)).getChildren().get(0));

            final RenderNode childRenderNode = listRenderNode.getChildren().get(0);
            final StateNode childStateNode = (StateNode) listComponentState.getChildren().get(0);

            assertTrue(childRenderNode.getComponent() instanceof TestComponentWithNoChildren);
            assertTrue(childStateNode.getComponentState() instanceof TestComponentWithNoChildrenState);
            final TestComponentWithNoChildrenState childState = (TestComponentWithNoChildrenState) childStateNode.getComponentState();

            assertEquals(5, childState.initialData);
            assertEquals(1, ((TestComponentWithNoChildren) childRenderNode.getComponent()).data);

            // Expand the size again.
            assertTrue(keyboardDetectorRenderNode.getComponent() instanceof KeyboardDetector);
            final KeyboardDetector keyboardDetector = (KeyboardDetector) keyboardDetectorRenderNode.getComponent();
            keyboardDetector.getOnKeyboardCallback().accept("phase 2");  // Expand the component tree to 10.
        });
        this.engine.tick();

        this.engine.setRenderConsumer((RenderNode rootNode) -> {
            assertEquals(1, rootNode.getChildren().size());
            final RenderNode keyboardDetectorRenderNode = rootNode.getChildren().get(0);
            assertEquals(1, keyboardDetectorRenderNode.getChildren().size());
            final RenderNode listRenderNode = keyboardDetectorRenderNode.getChildren().get(0);
            assertEquals(10, listRenderNode.getChildren().size());

            // Also drill down into the state nodes.
            // The TestComponentWithNoChildren states should have kept the original value (5) for the first component
            // whereas the last 9 should have stored the new value. All Components themselves should only have the new value.
            assertEquals(1, this.engine.getRootStateNode().getChildren().size());  // TestKeyboardDetectingIncreasingListComponent
            assertEquals(1, ((StateNode) this.engine.getRootStateNode().getChildren().get(0)).getChildren().size());  // KeyboardDetector
            assertEquals(10, ((StateNode) ((StateNode) this.engine.getRootStateNode().getChildren().get(0)).getChildren().get(0)).getChildren().size());  // ListComponent
            final StateNode listComponentState = ((StateNode) ((StateNode) this.engine
                .getRootStateNode().getChildren().get(0)).getChildren().get(0));

            for (int i=0; i < listRenderNode.getChildren().size(); i++) {
                final RenderNode childRenderNode = listRenderNode.getChildren().get(i);
                final StateNode childStateNode = (StateNode) listComponentState.getChildren().get(i);

                assertTrue(childRenderNode.getComponent() instanceof TestComponentWithNoChildren);
                assertTrue(childStateNode.getComponentState() instanceof TestComponentWithNoChildrenState);
                final TestComponentWithNoChildrenState childState = (TestComponentWithNoChildrenState) childStateNode.getComponentState();

                if (i == 0) {
                    assertEquals(5, childState.initialData);
                }
                else {
                    assertEquals(10, childState.initialData);
                }
                assertEquals(10, ((TestComponentWithNoChildren) childRenderNode.getComponent()).data);
            }
        });
        this.engine.tick();
    }

    /**
     * Tests that the engine prevents cached ComponentStates from matching with type-incompatible types.
     * As a side-effect, tests that cached ComponentStates never migrate up the state tree.
     */
    @Test
    public void testTickWithDifferentChildTypes() {
        final Component comp = new TestComponentWithMultipleTypesOfChildren((key) -> {
            if (key % 2 == 0) {
                final ArrayList<Component> children = new ArrayList<>();

                for (int i=0; i < 1000; i++) {
                    children.add(new TestComponentWithNoChildren(key));
                }

                return new ListComponent(
                    Axis.HORIZONTAL,
                    children
                );
            }
            else {
                return new TestComponentWithNoChildren(key);
            }
        });

        this.engine.setRenderConsumer((RenderNode rootNode) -> {
            assertEquals(1, rootNode.getChildren().size());
            final RenderNode keyboardDetectorRenderNode = rootNode.getChildren().get(0);
            assertTrue(keyboardDetectorRenderNode.getComponent() instanceof KeyboardDetector);
            ((KeyboardDetector) keyboardDetectorRenderNode.getComponent()).getOnKeyboardCallback().accept("");

            assertEquals(1, keyboardDetectorRenderNode.getChildren().size());
            final RenderNode listRenderNode = keyboardDetectorRenderNode.getChildren().get(0);
            assertTrue(listRenderNode.getComponent() instanceof ListComponent);

            assertEquals(1000, listRenderNode.getChildren().size());
            assertTrue(listRenderNode.getChildren().get(0).getComponent() instanceof TestComponentWithNoChildren);
        });
        this.engine.initialise(comp);

        this.engine.setRenderConsumer((RenderNode rootNode) -> {
            assertEquals(1, rootNode.getChildren().size());
            final RenderNode keyboardDetectorRenderNode = rootNode.getChildren().get(0);
            assertTrue(keyboardDetectorRenderNode.getComponent() instanceof KeyboardDetector);
            ((KeyboardDetector) keyboardDetectorRenderNode.getComponent()).getOnKeyboardCallback().accept("");

            assertEquals(1, keyboardDetectorRenderNode.getChildren().size());
            final RenderNode leafRenderNode = keyboardDetectorRenderNode.getChildren().get(0);
            assertTrue(leafRenderNode.getComponent() instanceof TestComponentWithNoChildren);

            assertEquals(0, leafRenderNode.getChildren().size());

            // Check that state is new.
            final StateNode leafStateNode = (StateNode) ((StateNode) this.engine.getRootStateNode().getChildren().get(0)).getChildren().get(0);
            assertTrue(leafStateNode.getComponentState() instanceof TestComponentWithNoChildrenState);
            assertEquals(1, ((TestComponentWithNoChildrenState) leafStateNode.getComponentState()).initialData);
        });
        this.engine.tick();

        this.engine.setRenderConsumer((RenderNode rootNode) -> {
            assertEquals(1, rootNode.getChildren().size());
            final RenderNode keyboardDetectorRenderNode = rootNode.getChildren().get(0);
            assertTrue(keyboardDetectorRenderNode.getComponent() instanceof KeyboardDetector);
            ((KeyboardDetector) keyboardDetectorRenderNode.getComponent()).getOnKeyboardCallback().accept("");

            assertEquals(1, keyboardDetectorRenderNode.getChildren().size());
            final RenderNode listRenderNode = keyboardDetectorRenderNode.getChildren().get(0);
            assertTrue(listRenderNode.getComponent() instanceof ListComponent);

            assertEquals(1000, listRenderNode.getChildren().size());
            assertTrue(listRenderNode.getChildren().get(0).getComponent() instanceof TestComponentWithNoChildren);

            // Check that child states are new.
            final StateNode listStateNode = (StateNode) ((StateNode) this.engine.getRootStateNode().getChildren().get(0)).getChildren().get(0);
            assertTrue(listStateNode.getComponentState() instanceof MultipleChildrenComponentState);

            for (Object childStateNodeMaybe : listStateNode.getChildren()) {
                final StateNode childStateNode = (StateNode) childStateNodeMaybe;
                assertTrue(childStateNode.getComponentState() instanceof TestComponentWithNoChildrenState);
                assertEquals(2, ((TestComponentWithNoChildrenState) childStateNode.getComponentState()).initialData);
            }
        });
        this.engine.tick();
    }

    @Test
    public void testTickBeforeInitialisation() {
        assertThrows(IllegalStateException.class, this.engine::tick);
    }

    @Nested
    public class BuildContextTest {
        String getProvidedDataFromTextComponent(RenderNode rootNode) {
            assertEquals(1, rootNode.getChildren().size());
            final RenderNode keyboardDetectorNode = rootNode.getChildren().get(0);
            assertEquals(1, keyboardDetectorNode.getChildren().size());
            final RenderNode providerNode = keyboardDetectorNode.getChildren().get(0);
            assertEquals(1, providerNode.getChildren().size());
            final RenderNode dataNode = providerNode.getChildren().get(0);
            assertEquals(1, dataNode.getChildren().size());
            final RenderNode textNode = dataNode.getChildren().get(0);
            assertTrue(textNode.getComponent() instanceof TextComponent);
            final TextComponent textComponent = (TextComponent) textNode.getComponent();

            return textComponent.getText().get(0).getText();
        }

        @Test
        public void dataIsProvided() {
            final Component comp = new TestComponentThatProvidesData("default data", new TestComponentWithProvidedData());

            engine.setRenderConsumer((RenderNode rootNode) -> {
                assertEquals("default data", getProvidedDataFromTextComponent(rootNode));
            });
            engine.initialise(comp);
        }

        @Test
        public void dataChangePropagates() {
            final Component comp = new TestComponentThatProvidesData("default data", new TestComponentWithProvidedData());

            engine.setRenderConsumer((RenderNode rootNode) -> {
                // Simulate some key presses on the KeyboardDetector.
                assertEquals(1, rootNode.getChildren().size());
                final RenderNode keyboardDetectorNode = rootNode.getChildren().get(0);
                assertTrue(keyboardDetectorNode.getComponent() instanceof KeyboardDetector);
                final KeyboardDetector keyboardDetector = (KeyboardDetector) keyboardDetectorNode.getComponent();

                assertEquals("default data", getProvidedDataFromTextComponent(rootNode));

                // 'key' = "phase 1" signals to the component to shrink size to 1.
                keyboardDetector.getOnKeyboardCallback().accept("phase 1");
            });
            engine.initialise(comp);

            engine.setRenderConsumer((RenderNode rootNode) -> {
                assertEquals("phase 1", getProvidedDataFromTextComponent(rootNode));
            });
            engine.tick();
        }
    }
}