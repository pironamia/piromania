package nz.piromania.reactiveengine;

import java.util.ArrayList;
import java.util.List;
import nz.piromania.reactiveengine.component.KeyboardDetector;
import nz.piromania.reactiveengine.component.provider.Provider;

public class TestComponentThatProvidesData extends Component {
  final Component child;
  final String defaultData;

  TestComponentThatProvidesData(String defaultData, Component child) {
    this.defaultData = defaultData;
    this.child = child;
  }

  @Override
  protected TestComponentThatProvidesDataState createState() {
    return new TestComponentThatProvidesDataState();
  }
}

class TestComponentThatProvidesDataState extends ComponentState<TestComponentThatProvidesData> {
  String lastPressedKey;

  void setLastPressedKey(String key) {
    setState(() -> {
      this.lastPressedKey = key;
    });
  }

  @Override
  public ArrayList<Component> build(BuildContext context) {
    return new ArrayList<>(List.of(
        new KeyboardDetector(
            this::setLastPressedKey,
            new Provider(
                (lastPressedKey != null) ? lastPressedKey : getComponent().defaultData,
                getComponent().child
            )
        )
    ));
  }
}