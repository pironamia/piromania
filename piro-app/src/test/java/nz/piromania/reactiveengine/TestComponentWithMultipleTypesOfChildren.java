package nz.piromania.reactiveengine;

import nz.piromania.reactiveengine.component.KeyboardDetector;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class TestComponentWithMultipleTypesOfChildren extends Component {
    final Function<Integer, Component> builder;

    TestComponentWithMultipleTypesOfChildren(Function<Integer, Component> builder) {
        this.builder = builder;
    }

    @Override
    protected TestComponentWithMultipleTypesOfChildrenState createState() {
        return new TestComponentWithMultipleTypesOfChildrenState();
    }
}

class TestComponentWithMultipleTypesOfChildrenState extends ComponentState<TestComponentWithMultipleTypesOfChildren> {
    int currentValue = 0;

    @Override
    public ArrayList<Component> build(BuildContext context) {
        return new ArrayList<>(List.of(
                new KeyboardDetector(
                        (key) -> setState(() -> currentValue++),
                        getComponent().builder.apply(currentValue)
                )
        ));
    }
}