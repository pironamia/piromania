package nz.piromania.reactiveengine;

import java.util.ArrayList;
import java.util.function.Function;

public class TestComponentWithNChildren extends Component {
    final int childCount;
    final Function<Integer, Component> childBuilder;
    final ComponentKey key;

    @Override
    public ComponentKey getKey() {
        return key;
    }

    TestComponentWithNChildren(int childCount) {
        this.childCount = childCount;
        this.childBuilder = TestComponentWithNoChildren::new;
        this.key = null;
    }

    TestComponentWithNChildren(int childCount, Function<Integer, Component> builder) {
        this.childCount = childCount;
        this.childBuilder = builder;
        this.key = null;
    }

    TestComponentWithNChildren(ComponentKey key, int childCount) {
        this.key = key;
        this.childCount = childCount;
        this.childBuilder = TestComponentWithNoChildren::new;
    }

    TestComponentWithNChildren(ComponentKey key, int childCount, Function<Integer, Component> builder) {
        this.childCount = childCount;
        this.childBuilder = builder;
        this.key = key;
    }

    @Override
    protected TestComponentWithNChildrenState createState() {
        return new TestComponentWithNChildrenState();
    }
}

class TestComponentWithNChildrenState extends ComponentState<TestComponentWithNChildren> {
    @Override
    public ArrayList<Component> build(BuildContext context) {
        final ArrayList<Component> res = new ArrayList<>();
        for (int i=0; i < getComponent().childCount; i++) {
            res.add(getComponent().childBuilder.apply(i));
        }
        return res;
    }
}