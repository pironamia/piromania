package nz.piromania.reactiveengine;

import java.util.ArrayList;

public class TestComponentWithNoChildren extends Component {
    final Object data;
    private final ComponentKey key;

    @Override
    public ComponentKey getKey() {
        return key;
    }

    TestComponentWithNoChildren(Object data) {
        this.data = data;
        this.key = null;
    }

    TestComponentWithNoChildren() {
        this.data = null;
        this.key = null;
    }

    TestComponentWithNoChildren(ComponentKey key) {
        this.key = key;
        this.data = null;
    }

    TestComponentWithNoChildren(ComponentKey key, Object data) {
        this.key = key;
        this.data = data;
    }

    @Override
    protected TestComponentWithNoChildrenState createState() {
        return new TestComponentWithNoChildrenState();
    }
}


class TestComponentWithNoChildrenState extends ComponentState<TestComponentWithNoChildren> {
    Object initialData;

    @Override
    public void initState() {
        super.initState();

        this.initialData = getComponent().data;
    }

    @Override
    public ArrayList<Component> build(BuildContext context) {
        return new ArrayList<>();
    }
}