package nz.piromania.reactiveengine;

    import java.util.ArrayList;
    import java.util.List;
    import nz.piromania.reactiveengine.component.text.TextComponent;
    import nz.piromania.reactiveengine.component.provider.Provider;

public class TestComponentWithProvidedData extends Component {

  @Override
  protected TestComponentWithProvidedDataState createState() {
    return new TestComponentWithProvidedDataState();
  }
}

class TestComponentWithProvidedDataState extends ComponentState<TestComponentWithProvidedData> {

  @Override
  public ArrayList<Component> build(BuildContext context) {
    final String data = Provider.of(String.class, context);

    return new ArrayList<>(List.of(
        new TextComponent(data)
    ));
  }
}