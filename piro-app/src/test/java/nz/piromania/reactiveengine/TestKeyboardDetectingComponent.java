package nz.piromania.reactiveengine;

import nz.piromania.reactiveengine.component.KeyboardDetector;

import java.util.ArrayList;
import java.util.List;

public class TestKeyboardDetectingComponent extends Component {
    final Object data;

    TestKeyboardDetectingComponent(Object data) {
        this.data = data;
    }

    TestKeyboardDetectingComponent() {
        this.data = null;
    }

    @Override
    protected TestKeyboardDetectingComponentState createState() {
        return new TestKeyboardDetectingComponentState();
    }
}

class TestKeyboardDetectingComponentState extends ComponentState<TestKeyboardDetectingComponent> {
    String lastPressedKey = null;

    void setLastPressedKey(String key) {
        setState(() -> {
            //System.out.printf("Setting lastPressedKey to: %s%n", key);
            this.lastPressedKey = key;
        });
    }

    @Override
    public ArrayList<Component> build(BuildContext context) {
        //System.out.printf("Building with lastPressedKey: %s%n", lastPressedKey);

        return new ArrayList<>(List.of(
                new KeyboardDetector(
                        this::setLastPressedKey,
                        new TestComponentWithNoChildren(lastPressedKey)
                )
        ));
    }
}