package nz.piromania.reactiveengine;

import nz.piromania.reactiveengine.component.KeyboardDetector;
import nz.piromania.reactiveengine.component.list.ListComponent;
import nz.piromania.reactiveengine.layout.Axis;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * A component that has a list of children which changed size on keyboard events (as specified by <code>onGetNewListSize</code>).
 * Used to test that state matching still occurs for the children that previously existed.
 */
public class TestKeyboardDetectingListChangingComponent extends Component {
    final Function<String, Integer> onGetNewListSize;

    TestKeyboardDetectingListChangingComponent(Function<String, Integer> onGetNewListSize) {
        this.onGetNewListSize = onGetNewListSize;
    }

    @Override
    protected TestKeyboardDetectingIncreasingListComponentState createState() {
        return new TestKeyboardDetectingIncreasingListComponentState();
    }
}

class TestKeyboardDetectingIncreasingListComponentState extends ComponentState<TestKeyboardDetectingListChangingComponent> {
    final int originalListSize = 5;
    int listSize = originalListSize;

    @Override
    public ArrayList<Component> build(BuildContext context) {
        final ArrayList<Component> res = new ArrayList<>();

        for (int i=0; i < listSize; i++) {
            res.add(new TestComponentWithNoChildren(listSize));
        }

        return new ArrayList<>(List.of(new KeyboardDetector(
                (String s) -> setState(() -> this.listSize = getComponent().onGetNewListSize.apply(s)),
                new ListComponent(
                        Axis.VERTICAL,
                        res
                )
        )));
    }
}
