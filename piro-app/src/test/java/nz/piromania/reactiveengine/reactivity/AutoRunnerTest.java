package nz.piromania.reactiveengine.reactivity;

import static org.junit.jupiter.api.Assertions.*;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Supplier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AutoRunnerTest {
  private TransactionTracker transactionTracker;
  private ObserverTracker observerTracker;

  private Observable<String> observable;
  private Observable<String> observable2;

  @BeforeEach
  public void beforeEach() {
    this.transactionTracker = new TransactionTracker();
    this.observerTracker = new ObserverTracker();
    this.observable = new Observable<>("o1", observerTracker, transactionTracker);
    this.observable2 = new Observable<>("o2", observerTracker, transactionTracker);
  }

  @Test
  public void testInitialise() {
    AtomicBoolean onUpdateHasRun = new AtomicBoolean(false);

    final Supplier<String> mainFunction = () -> {
      return this.observable.get() + this.observable2.get();
    };

    final Consumer<String> onUpdate = (String val) -> {
      onUpdateHasRun.set(true);
      assertEquals("o1o2", val);
    };

    final AutoRunner<String> runner = new AutoRunner<>(observerTracker, mainFunction, onUpdate);
    assertFalse(onUpdateHasRun.get());

    runner.initialise();
    assertTrue(onUpdateHasRun.get());
  }

  @Test
  public void testUpdateNotification() {
    AtomicInteger updateId = new AtomicInteger(0);

    final Supplier<String> mainFunction = () -> {
      return this.observable.get() + this.observable2.get();
    };

    final Consumer<String> onUpdate = (String val) -> {
      final int currentUpdateId = updateId.getAndIncrement();

      switch (currentUpdateId) {
        case 0 -> assertEquals("o1o2", val);
        case 1 -> assertEquals("o1newValue", val);
        case 2 -> assertEquals("newValue1newValue", val);  // NB: If update deduplication is not working, this will be called twice!
        case 3 -> assertEquals("v1v2", val);
        default -> fail("onUpdate called with unknown currentUpdateId");
      }
    };

    final AutoRunner<String> runner = new AutoRunner<>(observerTracker, mainFunction, onUpdate);
    runner.initialise();
    assertEquals(1, updateId.get());

    Action.run(() -> {
      this.observable2.set("newValue");
    }, transactionTracker, observerTracker);
    assertEquals(2, updateId.get());

    Action.run(() -> {
      this.observable.set("newValue1");
    }, transactionTracker, observerTracker);
    assertEquals(3, updateId.get());

    Action.run(() -> {
      this.observable.set("v1");
      this.observable2.set("v2");
    }, transactionTracker, observerTracker);
    assertEquals(4, updateId.get());

    runner.dispose();
  }
}
