package nz.piromania.reactiveengine.reactivity;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class ObservableTest {
  private TransactionTracker transactionTracker;
  private ObserverTracker observerTracker;
  private Observable<String> observable;

  @BeforeEach
  public void beforeEach() {
    this.transactionTracker = new TransactionTracker();
    this.observerTracker = new ObserverTracker();
    this.observable = new Observable<>("abc123", observerTracker, transactionTracker);
  }

  @Nested
  class TestGet {
    @Test
    public void testWithNoContext() {
      assertDoesNotThrow(observable::get);
      assertEquals("abc123", observable.get());
    }

    @Test
    public void testWithContext() {
      observerTracker.pushObservationContext();
      assertEquals("abc123", observable.get());
      observerTracker.popObservationContext(() -> {}).dispose();
    }
  }

  @Nested
  class TestSet {
    @Test
    public void setWithNoContext() {
      assertThrows(UnsupportedOperationException.class, () -> observable.set("banana"));
    }

    @Test
    public void setWithContext() {
      transactionTracker.beginTransaction();
      assertDoesNotThrow(() -> observable.set("banana"));
      transactionTracker.commitTransaction();

      assertEquals("banana", observable.get());
    }

    @Test
    public void setToNull() {
      transactionTracker.beginTransaction();
      assertDoesNotThrow(() -> observable.set(null));
      transactionTracker.commitTransaction();

      assertNull(observable.get());
    }

    @Test
    public void setFromNull() {
      final Observable<String> nullObservable = new Observable<String>(null, observerTracker, transactionTracker);

      transactionTracker.beginTransaction();
      assertDoesNotThrow(() -> nullObservable.set("Test"));
      transactionTracker.commitTransaction();

      assertEquals("Test", nullObservable.get());
    }
  }
}
