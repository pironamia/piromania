package nz.piromania.reactiveengine.reactivity.collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

import nz.piromania.reactiveengine.reactivity.Action;
import nz.piromania.reactiveengine.reactivity.AutoRunner;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ObservableArrayListTest {

  private ObservableArrayList<String> list;

  @BeforeEach
  public void beforeEach() {
    list = new ObservableArrayList<>();
  }

  @Test
  public void testSizeUpdate() {
    final var ref = new Object() {
      int expectedMapSize = 0;
      int runCount = 0;
    };

    final var runner = new AutoRunner<Integer>(() -> {
      return list.size();
    }, (Integer length) -> {
      assertEquals(ref.expectedMapSize, length);
      ref.runCount++;
    });
    runner.initialise();

    ref.expectedMapSize = 1;
    Action.run(() -> {
      list.add("a");
    });
    assertEquals(2, ref.runCount);

    ref.expectedMapSize = 3;
    Action.run(() -> {
      list.addAll(ArrayListBuilder.build("a", "b"));
    });
    assertEquals(3, ref.runCount);

    ref.expectedMapSize = 2;
    Action.run(() -> {
      list.remove("b");
    });
    assertEquals(4, ref.runCount);

    ref.expectedMapSize = 0;
    Action.run(() -> {
      list.clear();
    });
    assertEquals(5, ref.runCount);

    runner.dispose();
  }

  @Test
  public void testElementUpdate() {
    Action.run(() -> {
      list.add("b");
      list.add(0, "a");
    });

    final var ref = new Object() {
      String expectedValue = "a";
      int runCount = 0;
    };

    final var runner = new AutoRunner<>(() -> {
      return list.get(0);
    }, (val) -> {
      assertEquals(ref.expectedValue, val);
      ref.runCount++;
    });
    runner.initialise();

    ref.expectedValue = "c";
    Action.run(() -> {
      list.set(0, "c");
    });
    assertEquals(2, ref.runCount);

    ref.expectedValue = "Z";
    Action.run(() -> {
      list.add(0, "Z");
    });
    assertEquals(3, ref.runCount);

    runner.dispose();
  }

  @Test
  public void testIndexOf() {
    Action.run(() -> {
      list.add("b");
      list.add(0, "a");
    });

    final var ref = new Object() {
      int expectedValue = 0;
      int runCount = 0;
    };

    final var runner = new AutoRunner<>(() -> {
      return list.indexOf("a");
    }, (val) -> {
      assertEquals(ref.expectedValue, val);
      ref.runCount++;
    });
    runner.initialise();

    ref.expectedValue = 1;
    Action.run(() -> {
      list.add(0, "c");
    });
    assertEquals(2, ref.runCount);

    ref.expectedValue = 2;
    Action.run(() -> {
      list.add(0, "Z");
    });
    assertEquals(3, ref.runCount);

    ref.expectedValue = 0;
    Action.run(() -> {
      list.add(0, "a");
    });
    assertEquals(4, ref.runCount);

    ref.expectedValue = 9000;
    Action.run(() -> {
      list.remove(3);
    });
    assertEquals(4, ref.runCount);  // Value doesn't change so no update triggered.

    ref.expectedValue = -1;
    Action.run(() -> {
      list.set(0, "y");
    });
    assertEquals(5, ref.runCount);

    runner.dispose();
  }

  @Test
  public void testLastIndexOf() {
    Action.run(() -> {
      list.add("b");
      list.add(0, "a");
    });

    final var ref = new Object() {
      int expectedValue = 0;
      int runCount = 0;
    };

    final var runner = new AutoRunner<>(() -> {
      return list.lastIndexOf("a");
    }, (val) -> {
      assertEquals(ref.expectedValue, val);
      ref.runCount++;
    });
    runner.initialise();

    ref.expectedValue = 1;
    Action.run(() -> {
      list.add(0, "c");
    });
    assertEquals(2, ref.runCount);

    ref.expectedValue = 2;
    Action.run(() -> {
      list.add(0, "Z");
    });
    assertEquals(3, ref.runCount);

    ref.expectedValue = 3;
    Action.run(() -> {
      list.add(3, "a");
    });
    assertEquals(4, ref.runCount);

    ref.expectedValue = 2;
    Action.run(() -> {
      list.remove(3);
    });
    assertEquals(5, ref.runCount);

    ref.expectedValue = -1;
    Action.run(() -> {
      list.set(2, "y");
    });
    assertEquals(6, ref.runCount);

    runner.dispose();
  }

  @Test
  public void testContains() {
    Action.run(() -> {
      list.add("b");
      list.add(0, "a");
    });

    final var ref = new Object() {
      boolean expectedValue = true;
      int runCount = 0;
    };

    final var runner = new AutoRunner<>(() -> {
      return list.contains("a");
    }, (val) -> {
      assertEquals(ref.expectedValue, val);
      ref.runCount++;
    });
    runner.initialise();

    ref.expectedValue = true;
    Action.run(() -> {
      list.add(0, "c");
    });
    assertEquals(1, ref.runCount);

    ref.expectedValue = false;
    Action.run(() -> {
      list.set(1, "Z");
    });
    assertEquals(2, ref.runCount);

    runner.dispose();
  }

  @Test
  public void testContainsAll() {
    Action.run(() -> {
      list.add("b");
      list.add(0, "a");
    });

    final var ref = new Object() {
      boolean expectedValue = true;
      int runCount = 0;
    };

    final var runner = new AutoRunner<>(() -> {
      return list.containsAll(ArrayListBuilder.build("a", "b"));
    }, (val) -> {
      assertEquals(ref.expectedValue, val);
      ref.runCount++;
    });
    runner.initialise();

    ref.expectedValue = true;
    Action.run(() -> {
      list.add(0, "c");
    });
    assertEquals(1, ref.runCount);

    ref.expectedValue = false;
    Action.run(() -> {
      list.set(1, "Z");
    });
    assertEquals(2, ref.runCount);

    runner.dispose();
  }

  @Test
  public void testRetainAll() {
    Action.run(() -> {
      list.add("b");
      list.add(0, "a");
    });

    final var ref = new Object() {
      String expectedValue = "a,b";
      int runCount = 0;
    };

    final var runner = new AutoRunner<>(() -> {
      final StringBuilder builder = new StringBuilder();

      for (final var el : list) {
        builder.append(el);
        builder.append(",");
      }
      if (builder.length() != 0) {
        builder.deleteCharAt(builder.length() - 1);
      }

      return builder.toString();
    }, (val) -> {
      assertEquals(ref.expectedValue, val);
      ref.runCount++;
    });
    runner.initialise();

    ref.expectedValue = "a";
    Action.run(() -> {
      list.retainAll(ArrayListBuilder.build("a"));
    });
    assertEquals(2, ref.runCount);

    ref.expectedValue = "";
    Action.run(() -> {
      list.retainAll(ArrayListBuilder.build("z", "x"));
    });
    assertEquals(3, ref.runCount);

    runner.dispose();
  }

  @Test
  public void testAddAll() {
    final var ref = new Object() {
      String expectedValue = "";
      int runCount = 0;
    };

    final var runner = new AutoRunner<>(() -> {
      final StringBuilder builder = new StringBuilder();

      for (final var el : list) {
        builder.append(el);
        builder.append(",");
      }
      if (builder.length() != 0) {
        builder.deleteCharAt(builder.length() - 1);
      }

      return builder.toString();
    }, (val) -> {
      assertEquals(ref.expectedValue, val);
      ref.runCount++;
    });
    runner.initialise();

    ref.expectedValue = "a,b";
    Action.run(() -> {
      list.addAll(ArrayListBuilder.build("a", "b"));
    });
    assertEquals(2, ref.runCount);

    ref.expectedValue = "a,z,x,b";
    Action.run(() -> {
      list.addAll(1, ArrayListBuilder.build("z", "x"));
    });
    assertEquals(3, ref.runCount);

    runner.dispose();
  }

  @Test
  public void testRemoveAll() {
    Action.run(() -> {
      list.addAll(ArrayListBuilder.build("a", "b", "c", "d"));
    });

    final var ref = new Object() {
      String expectedValue = "a,b,c,d";
      int runCount = 0;
    };

    final var runner = new AutoRunner<>(() -> {
      final StringBuilder builder = new StringBuilder();

      for (final var el : list) {
        builder.append(el);
        builder.append(",");
      }
      if (builder.length() != 0) {
        builder.deleteCharAt(builder.length() - 1);
      }

      return builder.toString();
    }, (val) -> {
      assertEquals(ref.expectedValue, val);
      ref.runCount++;
    });
    runner.initialise();

    ref.expectedValue = "a,c";
    Action.run(() -> {
      list.removeAll(ArrayListBuilder.build("d", "b"));
    });
    assertEquals(2, ref.runCount);

    ref.expectedValue = "";
    Action.run(() -> {
      list.removeAll(ArrayListBuilder.build("a", "b", "c", "d"));
    });
    assertEquals(3, ref.runCount);

    runner.dispose();
  }
}
