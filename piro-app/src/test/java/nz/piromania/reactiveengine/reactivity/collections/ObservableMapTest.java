package nz.piromania.reactiveengine.reactivity.collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Map;
import nz.piromania.reactiveengine.reactivity.Action;
import nz.piromania.reactiveengine.reactivity.AutoRunner;
import nz.piromania.reactiveengine.util.ArrayListBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ObservableMapTest {

  private ObservableMap<String, String> map;

  @BeforeEach
  public void beforeEach() {
    map = new ObservableMap<>();
  }

  @Test
  public void testSizeUpdate() {
    final var ref = new Object() {
      int expectedMapSize = 0;
    };

    final var runner = new AutoRunner<Integer>(() -> {
      return map.size();
    }, (Integer length) -> {
      assertEquals(ref.expectedMapSize, length);
    });
    runner.initialise();

    ref.expectedMapSize = 1;
    Action.run(() -> {
      map.put("a", "A");
    });

    ref.expectedMapSize = 3;
    Action.run(() -> {
      map.putAll(Map.of(
          "b", "B",
          "c", "C"
      ));
    });

    ref.expectedMapSize = 2;
    Action.run(() -> {
      map.remove("b");
    });

    ref.expectedMapSize = 0;
    Action.run(() -> {
      map.clear();
    });

    runner.dispose();
  }

  @Test
  public void testElementUpdate() {
    Action.run(() -> {
      map.put("a", "A");
      map.put("b", "B");
    });

    final var ref = new Object() {
      String expectedAValue = "A";
      int runCount = 0;
    };

    final var runner = new AutoRunner<String>(() -> {
      return map.get("a");
    }, (val) -> {
      ref.runCount++;
      assertEquals(ref.expectedAValue, val);
    });
    runner.initialise();

    ref.expectedAValue = "C";
    Action.run(() -> {
      map.put("a", "C");
    });
    assertEquals(2, ref.runCount);
    runner.dispose();
  }

  @Test
  public void testEntrySetIterationUpdate() {
    Action.run(() -> {
      map.put("a", "A");
      map.put("b", "B");
    });

    final var ref = new Object() {
      ArrayList<String> expectedValue = ArrayListBuilder.build("a: A", "b: B");
      int runCount = 0;
    };

    final var runner = new AutoRunner<>(() -> {
      final var res = new ArrayList<String>();
      for (var el : map.entrySet()) {
        res.add(String.format("%s: %s", el.getKey(), el.getValue()));
      }
      res.sort(String::compareTo);
      return res;
    }, (val) -> {
      assertEquals(ref.expectedValue, val);
      ref.runCount++;
    });
    runner.initialise();

    ref.expectedValue = ArrayListBuilder.build("a: C", "b: B");
    Action.run(() -> {
      map.put("a", "C");
    });
    assertEquals(2, ref.runCount);

    runner.dispose();
  }

  @Test
  public void testKeySetIterationUpdate() {
    Action.run(() -> {
      map.put("a", "A");
      map.put("b", "B");
    });

    final var ref = new Object() {
      ArrayList<String> expectedValue = ArrayListBuilder.build("a", "b");
      int runCount = 0;
    };

    final var runner = new AutoRunner<>(() -> {
      final var res = new ArrayList<String>();
      for (var key : map.keySet()) {
        res.add(String.format("%s", key));
      }
      res.sort(String::compareTo);
      return res;
    }, (val) -> {
      assertEquals(ref.expectedValue, val);
      ref.runCount++;
    });
    runner.initialise();

    ref.expectedValue = ArrayListBuilder.build();
    Action.run(() -> {
      map.put("a", "C");
    });
    assertEquals(1, ref.runCount);  // Shouldn't actually run since the entrySet is identical

    ref.expectedValue = ArrayListBuilder.build("a");
    Action.run(() -> {
      map.remove("b");
    });
    assertEquals(2, ref.runCount);

    runner.dispose();
  }

  @Test
  public void testValuesIterationUpdate() {
    Action.run(() -> {
      map.put("a", "A");
      map.put("b", "B");
    });

    final var ref = new Object() {
      ArrayList<String> expectedValue = ArrayListBuilder.build("A", "B");
      int runCount = 0;
    };

    final var runner = new AutoRunner<>(() -> {
      final var res = new ArrayList<String>();
      for (var val : map.values()) {
        res.add(String.format("%s", val));
      }
      res.sort(String::compareTo);
      return res;
    }, (val) -> {
      assertEquals(ref.expectedValue, val);
      ref.runCount++;
    });
    runner.initialise();

    ref.expectedValue = ArrayListBuilder.build("A", "A", "B");
    Action.run(() -> {
      map.put("d", "A");
    });
    assertEquals(2, ref.runCount);

    Action.run(() -> {
      map.remove("xyz");
    });
    assertEquals(2, ref.runCount);  // Shouldn't actually run since the key doesn't exist

    ref.expectedValue = ArrayListBuilder.build("A", "A");
    Action.run(() -> {
      map.remove("b");
    });
    assertEquals(3, ref.runCount);

    runner.dispose();
  }
}
