package nz.piromania.ui.screen;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class PlayerDetailEntryScreenStoreTest {
  private PlayerDetailEntryScreenStore store;

  @BeforeEach
  public void beforeEach() {
    store = new PlayerDetailEntryScreenStore();
  }

  @Nested
  public class PlayerName {
    @Test
    public void noErrorBeforeUserInput() {
      assertEquals("", store.getPlayerNameError());
    }

    @Test
    public void lengthErrors() {
      // Cannot be shorter than 3
      store.setPlayerName("");
      assertTrue(store.getPlayerNameError().length() != 0);
      store.setPlayerName("ab");
      assertTrue(store.getPlayerNameError().length() != 0);


      store.setPlayerName("abc");
      assertEquals(0, store.getPlayerNameError().length());
      store.setPlayerName("Timothy");
      assertEquals(0, store.getPlayerNameError().length());
      store.setPlayerName("abcdefghijklmno");
      assertEquals(0, store.getPlayerNameError().length());

      // Cannot be longer than 15
      store.setPlayerName("abcdefghijklmnop");
      assertTrue(store.getPlayerNameError().length() != 0);
    }

    @Test
    public void specialCharacterErrors() {
      // Can only contain alphabet and spaces
      store.setPlayerName("abc defg");
      assertEquals(0, store.getPlayerNameError().length());

      store.setPlayerName("abc_defg'");
      assertTrue(store.getPlayerNameError().length() != 0);
    }

    @Test
    public void spaceRuleErrors() {
      // Cannot start with a space
      store.setPlayerName(" abc");
      assertTrue(store.getPlayerNameError().length() != 0);

      // Cannot have multiple spaces in a row
      store.setPlayerName("abc  d");
      assertTrue(store.getPlayerNameError().length() != 0);

      // Cannot end with a space
      store.setPlayerName("abcd ");
      assertTrue(store.getPlayerNameError().length() != 0);
    }
  }

  @Nested
  public class GameDuration {
    @Test
    public void noErrorBeforeUserInput() {
      assertEquals("", store.getGameDurationError());
    }

    @Test
    public void numberErrors() {
      // Must be an integer
      store.setGameDuration("25");
      assertEquals(0, store.getGameDurationError().length());

      store.setGameDuration(" 25");
      assertTrue(store.getGameDurationError().length() != 0);
      store.setGameDuration("thirty");
      assertTrue(store.getGameDurationError().length() != 0);
      store.setGameDuration("");
      assertTrue(store.getGameDurationError().length() != 0);

    }

    @Test
    public void boundaryErrors() {
      // Cannot be less than 20
      store.setGameDuration("18");
      assertTrue(store.getGameDurationError().length() != 0);
      store.setGameDuration("-30");
      assertTrue(store.getGameDurationError().length() != 0);

      store.setGameDuration("20");
      assertEquals(0, store.getGameDurationError().length());
      store.setGameDuration("32");
      assertEquals(0, store.getGameDurationError().length());
      store.setGameDuration("50");
      assertEquals(0, store.getGameDurationError().length());

      // Cannot be more than 50
      store.setGameDuration("51");
      assertTrue(store.getGameDurationError().length() != 0);
    }
  }
}
